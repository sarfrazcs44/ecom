-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 02, 2020 at 08:57 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecomscho_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `location` varchar(500) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `building_no` varchar(255) NOT NULL,
  `floor_no` varchar(255) NOT NULL,
  `apartment_no` varchar(255) NOT NULL,
  `is_default` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `user_id`, `location`, `city_id`, `district_id`, `country`, `lat`, `lng`, `building_no`, `floor_no`, `apartment_no`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 5, 'Saudi Arabia', 1, 1, 'Pakistan ', '0.0', '0.0', '', '', '', 0, '0000-00-00 00:00:00', '2020-04-21 08:38:20'),
(2, 5, 'Saudi Arabia', 1, 1, 'Saudi Arabia', '0.0', '0.0', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 28, '201–499 Launiu St, Waikiki, Honolulu, 96815, HI, United States', 1, 1, '', '21.2827778', '-157.8294444', '245', 'first ', '9', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 0, '23815, أبحر الشمالية, جدّة‎, 23815, منطقة مكة, المملكة العربية السعودية', 1, 1, '', '21.758470523787818', '39.10576345337705', '64;', '578', '558', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 0, '23818, الفردوس, جدّة‎, 23818, منطقة مكة, المملكة العربية السعودية', 1, 1, '', '21.767430026056246', '39.11436115427632', 'hvvc', '4', '45', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 28, 'North Atlantic Ocean', 1, 1, '', '0.0', '0.0', 'gggggggggggnnnnnnnnkkkkkkkyyy', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 0, '2444 شارع طه خصيفان, الشاطى, جدّة‎, 23511, منطقة مكة, المملكة العربية السعودية', 1, 1, '', '21.60227200098163', '39.10832233255207', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 0, '2444 شارع طه خصيفان, الشاطى, جدّة‎, 23511, منطقة مكة, المملكة العربية السعودية', 1, 1, '', '21.602270991534894', '39.10832910244606', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 0, '2444 شارع طه خصيفان, الشاطى, جدّة‎, 23511, منطقة مكة, المملكة العربية السعودية', 1, 1, '', '21.602270566887295', '39.10832908197642', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 28, 'Geary & Powell, Union Square, San Francisco, 94102, CA, United States', 1, 1, '', '37.7873589', '-122.408227', 'he’d', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 0, '2444 شارع طه خصيفان, الشاطى, جدّة‎, 23511, منطقة مكة, المملكة العربية السعودية', 1, 1, '', '21.602265020798313', '39.10832033823111', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 81, '香港, 中環, هونغ كونغ إقليم إدارة خاصة، الصين', 1, 1, '', '22.284681', '114.158177', 'ansckjansc', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 31, '2192 Al Udaba, Al Aziziyah, Jeddah, 23342, Makkah, Saudi Arabia', 1, 1, '', '21.558329441953433', '39.203798115929345', '1', '', '', 1, '0000-00-00 00:00:00', '2020-08-04 10:10:36'),
(18, 10, 'Kacha Shahi Road, Punjab, Pakistan', 1, 1, '', '28.311742705386965', '70.13053821888487', 'jsjs', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 89, '35 Bridge Street, Sydney CBD, Sydney, 2000, NSW, Australia', 1, 1, '', '-33.8634', '151.211', 'x aa', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `ad_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `ad_title_en` varchar(255) NOT NULL,
  `ad_title_ar` varchar(255) NOT NULL,
  `ad_description_en` varchar(2000) NOT NULL,
  `ad_description_ar` varchar(2000) NOT NULL,
  `image` varchar(2000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`ad_id`, `product_id`, `ad_title_en`, `ad_title_ar`, `ad_description_en`, `ad_description_ar`, `image`, `created_at`, `updated_at`, `is_active`) VALUES
(11, 151, 'Lounge Q', 'لونج كيو', 'Elegant, ultra-fast wireless charging stand. Now available for ordering.', 'حامل شحن لاسلكي أنيق فائق السرعة. متاح الآن للطلب. حامل شحن لاسلكي أنيق فائق السرعة. متاح الآن للطلب.', 'uploads/slider_images/24574015287202008050445340_0002_Layer_1.jpg', '2018-07-12 17:05:04', '2020-08-05 16:35:52', 1),
(12, 152, 'Create your own bag', 'اصنع حقيبتك الخاصة', 'The Treya Collection is a range of minimalist yet ultra-functional bags and add-on accessories. Mix-and-match your bag, clutch, and strap to create your own unique look.', 'مجموعة Treya هي مجموعة من الحقائب البسيطة والفائقة الوظائف والإكسسوارات الإضافية. امزج وطابق حقيبتك وقابضك وحزامك لخلق مظهر فريد خاص بك.', 'uploads/slider_images/71199592055202008050445370_0001_Layer_2.jpg', '2020-08-05 16:37:45', '2020-08-05 16:37:45', 1),
(13, 151, 'Connect', 'السلك', 'Everything you need to stay connected. Shop our premium range of cables, adapters, hubs and portable batteries.', 'كل ما تحتاجه للبقاء على اتصال. تسوق في مجموعتنا الممتازة من الكابلات والمحولات والمحاور والبطاريات المحمولة.', 'uploads/slider_images/38290373585202008050415390_0000_Layer_3.jpg', '2020-08-05 16:39:15', '2020-08-05 16:39:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `assigned_orders_for_delivery`
--

CREATE TABLE `assigned_orders_for_delivery` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `assigned_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assigned_orders_for_delivery`
--

INSERT INTO `assigned_orders_for_delivery` (`id`, `order_id`, `user_id`, `assigned_at`) VALUES
(3, 2, 10, '2020-06-15 10:37:44'),
(4, 1, 10, '2020-06-15 10:37:44'),
(5, 5, 10, '2020-06-15 10:37:44'),
(6, 14, 10, '2020-07-14 06:53:12'),
(7, 15, 14, '2020-07-14 11:58:19'),
(8, 10, 10, '2020-07-30 11:53:05'),
(9, 12, 10, '2020-07-30 12:08:38'),
(10, 17, 10, '2020-07-30 12:41:53'),
(16, 18, 13, '2020-07-30 13:21:39'),
(12, 19, 14, '2020-07-30 13:08:37'),
(13, 16, 10, '2020-07-30 13:10:44'),
(17, 13, 10, '2020-08-09 14:53:59'),
(18, 20, 10, '2020-08-09 16:52:20');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `description_en` varchar(2000) NOT NULL,
  `description_ar` varchar(2000) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `title_en`, `title_ar`, `description_en`, `description_ar`, `image`, `created_at`, `updated_at`, `is_active`) VALUES
(10, 'Moshi', 'موشي', '', '', 'uploads/brand_images/8502033609720200805094548logo.jpg', '2018-02-28 11:22:43', '2020-08-05 21:48:45', 1),
(24, 'Targus', 'تارجس', '', '', 'uploads/brand_images/3732285818920200805093151targus_logo.jpg', '2020-08-05 21:51:31', '2020-08-05 21:51:31', 1),
(25, 'Belkin', 'بلكن', '', '', 'uploads/brand_images/2343422665220200805095651belkin_logo.jpg', '2020-08-05 21:51:56', '2020-08-05 21:51:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(11) NOT NULL,
  `full_name` varchar(60) NOT NULL,
  `company_name` varchar(255) NOT NULL COMMENT 'for become a seller request',
  `director_name` varchar(255) NOT NULL COMMENT 'for become a seller request',
  `email` varchar(120) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `request_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 for career 1 for become a seller',
  `nationality` varchar(120) NOT NULL,
  `file` varchar(255) NOT NULL,
  `to_be_notified` enum('yes','no') NOT NULL DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `description_en` varchar(2000) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `description_ar` varchar(2000) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_mbl` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hide` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `parent_id`, `title_en`, `description_en`, `title_ar`, `description_ar`, `image`, `image_mbl`, `created_at`, `updated_at`, `hide`, `is_active`) VALUES
(4, 2, 'Child Category', '', 'Child Category Arb', '', 'uploads/category_images/88091285925201802230533561516149531946.JPEG.jpg', '', '2018-02-23 09:19:53', '2018-02-24 20:18:04', 0, 1),
(5, 2, 'Child Category 2 ', '', 'Child Category 2 arb', '', 'uploads/category_images/6015417576201802230926200fb16035-95a7-42aa-8f8f-41a1fb136162.jpg', '', '2018-02-23 09:20:26', '2018-02-23 09:20:26', 0, 1),
(8, 7, 'Sub tITL ENGLISH', '', 'Sub Title Arabic', '', 'uploads/category_images/37819141825201802240127531516149532113.JPEG.jpg', '', '2018-02-24 13:53:27', '2018-02-24 13:53:27', 0, 1),
(9, 2, ' basittaha', '', 'hihihihihi', '', 'uploads/category_images/986383905132018022403330103Success.jpg', '', '2018-02-24 15:01:33', '2018-02-24 15:01:33', 0, 1),
(12, 11, 'jjj', '', 'kk\\', '', 'uploads/category_images/7759672003320180224082446cctv_2.png', '', '2018-02-24 20:46:24', '2018-02-24 20:46:24', 0, 1),
(14, 13, 'Basit', '', 'nfv', '', 'uploads/category_images/5560315197020180225013851logoHIKVision.png', '', '2018-02-25 13:51:38', '2018-02-25 13:51:38', 0, 1),
(16, 15, 'abc', '', 'abc', '', 'uploads/category_images/8678642485120180227093238City.jpg', '', '2018-02-27 21:38:32', '2018-02-27 21:38:32', 0, 1),
(17, 0, 'Protection', '', 'حماية', '', 'uploads/category_images/46564319484202008050953302_dsktp.jpg', 'uploads/category_images/41537992541202008050953302_mbl.jpg', '2018-02-28 07:00:20', '2020-08-05 21:30:53', 0, 1),
(31, 30, 'Coming soon', '', 'Coming soon', '', 'uploads/category_images/8282157512720180712050425Product-Image-Coming-Soon.png', 'uploads/category_images/395066944120180712050425Product-Image-Coming-Soon.png', '2018-03-01 07:29:50', '2018-07-12 17:25:04', 0, 1),
(40, 18, 'Network Video Recorder ', '', 'Network Video Recorder ', '', 'uploads/category_images/8572475144820180812014403xrn-1610_f.png', 'uploads/category_images/2825553817920180812014403xrn-1610_f.png', '2018-08-12 13:03:44', '2018-08-12 13:03:44', 0, 1),
(41, 18, 'Wisenet X Series', '', 'Wisenet X Series', '', 'uploads/category_images/7534191999320180813074232x_ser.png', 'uploads/category_images/1585319548320180813074232x_ser.png', '2018-08-13 07:31:18', '2018-08-13 07:32:42', 0, 1),
(42, 18, 'Wisenet P Series', '', 'Wisenet P Series', '', 'uploads/category_images/6611042654920180813073839p_ser.png', 'uploads/category_images/6935601568720180813073839p_ser.png', '2018-08-13 07:39:38', '2018-08-13 07:39:50', 0, 1),
(43, 18, 'Wisenet Q Series', '', 'Wisenet Q Series', '', 'uploads/category_images/5079337110020180813071746q_ser.png', 'uploads/category_images/5889719069220180813071746q_ser.png', '2018-08-13 07:46:17', '2018-08-13 07:46:17', 0, 1),
(44, 18, 'Wisenet T Series', '', 'Wisenet T Series', '', 'uploads/category_images/9667542973220180813105004t_ser.png', 'uploads/category_images/4921271018920180813105004t_ser.png', '2018-08-13 10:04:50', '2018-08-13 10:04:50', 0, 1),
(45, 18, 'Wisenet L Series', '', 'Wisenet L Series', '', 'uploads/category_images/136270522120180813105633l_ser.png', 'uploads/category_images/9875634404220180813105633l_ser.png', '2018-08-13 10:33:56', '2018-08-13 10:33:56', 0, 1),
(46, 19, 'Network Integrated Controller', '', 'Network Integrated Controller', '', 'uploads/category_images/39685018617201808131128031331.png', 'uploads/category_images/11843648752201808131128031331.png', '2018-08-13 11:03:28', '2018-08-13 11:03:28', 0, 1),
(47, 36, 'Zigbee ', '', 'Zigbee ', '', 'uploads/category_images/6813528744620180813024703or.png', 'uploads/category_images/9572606515920180813024703or.png', '2018-08-13 14:03:47', '2018-08-13 14:03:47', 0, 1),
(54, 19, 'HID Readers', '', 'HID Readers', '', 'uploads/category_images/5097191102220180813043754readers.png', 'uploads/category_images/8134541400020180813043754readers.png', '2018-08-13 16:54:37', '2018-08-13 16:54:37', 0, 1),
(57, 19, 'Suprema Biostar', '', 'Suprema Biostar', '', 'uploads/category_images/4206045742220180813054316bio.png', 'uploads/category_images/1440402599020180813054316bio.png', '2018-08-13 17:16:43', '2018-08-13 17:16:43', 0, 1),
(58, 19, 'HID Cards', '', 'HID Cards', '', 'uploads/category_images/2236061122720180813051517card.png', 'uploads/category_images/6458416809520180813051517card.png', '2018-08-13 17:17:15', '2018-08-13 17:17:15', 0, 1),
(59, 19, 'Hidden Gates', '', 'Hidden Gates', '', 'uploads/category_images/7126322410520180813054317sgate.png', 'uploads/category_images/7534173768720180813054317sgate.png', '2018-08-13 17:17:43', '2018-08-14 12:25:45', 0, 1),
(60, 49, 'Firebrand Fire Panels', '', 'Firebrand Addressable Fire Panel', '', 'uploads/category_images/4423326587920180814071102f1.png', 'uploads/category_images/1915135353820180814071102f1.png', '2018-08-14 07:02:11', '2018-08-14 07:35:22', 0, 1),
(61, 48, 'UTC Intrusion Alarm System', '', 'UTC Intrusion Alarm System', '', 'uploads/category_images/3980418472820180814070747sfas.png', 'uploads/category_images/2508503300420180814070747sfas.png', '2018-08-14 07:47:07', '2018-08-14 07:47:07', 0, 1),
(62, 51, 'Metal Detection Technology', '', 'Metal Detection Technology', '', 'uploads/category_images/8775950406120180814122047hf.png', 'uploads/category_images/7215543723620180814122047hf.png', '2018-08-14 12:47:20', '2018-08-14 12:47:20', 0, 1),
(63, 53, 'Pedestal EAS Systems - Ad Guard  ', '', 'Pedestal EAS Systems - Ad Guard  ', '', 'uploads/category_images/798086962201811050830111-160415154Z9457.png', 'uploads/category_images/48465416238201811050830111-160415154Z9457.png', '2018-11-05 08:11:30', '2018-11-05 08:37:24', 0, 1),
(64, 0, 'Connect', '', 'ربط', '', 'uploads/category_images/86814658777202008050916331_dsktp.jpg', 'uploads/category_images/29920889086202008050916331_mbl.jpg', '2020-04-18 19:25:33', '2020-08-05 21:33:16', 0, 1),
(65, 0, 'Carry', '', 'احمل', '', 'uploads/category_images/98691950729202008050902363.jpg', 'uploads/category_images/34317701451202008050902363.jpg', '2020-04-18 19:29:34', '2020-08-05 21:36:02', 0, 1),
(66, 0, 'Cords', '', 'حبال', '', 'uploads/category_images/20199105485202008050938364.jpg', 'uploads/category_images/11814363503202008050938364.jpg', '2020-04-18 19:34:57', '2020-08-05 21:36:38', 0, 1),
(69, 17, 'Fruits', '', 'الفاكهة', '', 'uploads/category_images/7278188008020200418112609fruits.jpg', 'uploads/category_images/7605922531320200418112609fruits_mobile.jpg', '2020-04-18 23:09:26', '2020-04-18 23:09:26', 0, 1),
(70, 17, 'Vegetables', '', 'خضروات', '', 'uploads/category_images/8618783697720200418113611veges_pc.jpg', 'uploads/category_images/2198032804420200418113611veges.jpg', '2020-04-18 23:11:36', '2020-04-18 23:11:36', 0, 1),
(71, 0, 'Bags', '', 'أكياس', '', 'uploads/category_images/1837425668202008050904425.jpg', 'uploads/category_images/83316304107202008050904425.jpg', '2020-08-05 21:42:04', '2020-08-05 21:42:04', 0, 1),
(72, 0, 'Camera', '', 'كاميرا', '', 'uploads/category_images/84604335392202008050901446.jpg', 'uploads/category_images/2257928479202008050901446.jpg', '2020-08-05 21:44:01', '2020-08-05 21:44:01', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat_request`
--

CREATE TABLE `chat_request` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `is_admin` int(11) NOT NULL,
  `is_closed` int(11) NOT NULL DEFAULT '0',
  `is_in_progress` int(11) NOT NULL,
  `to_be_notified` enum('yes','no') NOT NULL DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_request`
--

INSERT INTO `chat_request` (`id`, `username`, `email`, `subject`, `message`, `is_admin`, `is_closed`, `is_in_progress`, `to_be_notified`, `created_at`) VALUES
(1, 'Sarfraz Riaz', 'sarfraz.cs10@gmail.com', 'dfdsafdas', '', 0, 0, 1, 'no', '2020-07-30 11:25:02'),
(2, 'cxvczx', 'sarfraz.cs10@gmail.com', 'zcvzxv', '', 0, 0, 1, 'no', '2020-07-30 11:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `eng_name` char(35) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `countrycode` char(3) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `arb_name` varchar(500) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `eng_name`, `countrycode`, `arb_name`) VALUES
(1, 'Jeddah', '', 'جدة');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_types`
--

CREATE TABLE `complaint_types` (
  `complaint_type_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `complaint_title_en` varchar(255) NOT NULL,
  `complaint_title_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `placeholder_en` varchar(255) NOT NULL,
  `placeholder_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complaint_types`
--

INSERT INTO `complaint_types` (`complaint_type_id`, `parent_id`, `complaint_title_en`, `complaint_title_ar`, `placeholder_en`, `placeholder_ar`, `is_active`) VALUES
(1, 0, 'Maintenance Request', 'طلب صيانة', 'Service Type', 'نوع الخدمة', 1),
(2, 0, 'Sale and Marketing Request', 'طلبات المبيعات والتسويق', 'Request Type', 'نوع الطلب', 1),
(3, 0, 'Technical Support', 'الدعم الفني', 'Request Type', 'نوع الطلب', 1),
(4, 0, 'Complaints', 'الشكاوى', 'Select Department', 'الرجاء إختيار الإدارة', 1),
(5, 1, 'CCTV Systems', 'كاميرات المراقبة', '', '', 1),
(6, 1, 'Access Control Systems', 'نظام التحكم بالدخول والخروج', '', '', 1),
(7, 1, 'Smart Home Systems', 'أنظمة البيت الذكي', '', '', 1),
(8, 1, 'Intrusion Alarm systems', 'نظام الإنذار ضد السرقة', '', '', 1),
(9, 1, 'Fire Alarm Systems', 'نظام الإنذار ضد الحريق', '', '', 1),
(10, 1, 'Time Attendance systems', 'أنظمة الحضور والإنصراف', '', '', 1),
(11, 1, 'Metal and Contraband Detector', 'أنظمة فحص وتفتيش الحقائب والطرود وكشف المعادن', '', '', 1),
(12, 1, 'Paging and Music Systems', 'نظام صوتي للنداء والوسائط', '', '', 1),
(13, 1, 'Anti-Shoplifting Systems', 'أنظمة حماية المعروضات من السرقة', '', '', 1),
(14, 1, 'Other Systems', 'أنظمة أخرى', '', '', 1),
(15, 2, 'Quotation Request', 'طلب عرض سعر', '', '', 1),
(16, 2, 'Site survey Request', 'طلب مسح موقع', '', '', 1),
(17, 2, 'Become a re-seller', 'طلب توزيع لمنتجات الشركة', '', '', 1),
(18, 2, 'Others', 'أخرى', '', '', 1),
(19, 3, 'Remote Support', 'دعم عن بعد', '', '', 1),
(20, 3, 'Technical Enquiries', 'إستفسارات فنيةّ', '', '', 1),
(21, 3, 'Others', 'أخرى', '', '', 1),
(22, 4, 'Sales and marketing Team', 'طاقم المبيعات والتسويق', '', '', 1),
(23, 4, 'Maintenance Team', 'طاقم الصيانة', '', '', 1),
(24, 4, 'Project Team', 'طاقم المشاريع', '', '', 1),
(25, 4, 'Support Team', 'طاقم الدعم', '', '', 1),
(26, 4, 'IT Team', 'الشبكات وتكنولوجيا المعلومات', '', '', 1),
(27, 4, 'Admin Team', 'طاقم  الإدارة', '', '', 1),
(28, 4, 'Others', 'أخرى', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `complaint_types_new`
--

CREATE TABLE `complaint_types_new` (
  `complaint_type_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `complaint_title_en` varchar(255) NOT NULL,
  `complaint_title_ar` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complaint_types_new`
--

INSERT INTO `complaint_types_new` (`complaint_type_id`, `parent_id`, `complaint_title_en`, `complaint_title_ar`) VALUES
(1, 0, 'Maintenance Request', 'طلب صيانة'),
(2, 0, 'Sale and Marketing Request', 'طلبات المبيعات والتسويق'),
(3, 0, 'Technical Support', 'الدعم الفني'),
(4, 0, 'Complaints', 'الشكاوى'),
(5, 1, 'CCTV Systems', 'كاميرات المراقبة'),
(6, 1, 'Access Control Systems', 'نظام التحكم بالدخول والخروج'),
(7, 1, 'Smart Home Systems', 'أنظمة البيت الذكي'),
(8, 1, 'Intrusion Alarm systems', 'نظام الإنذار ضد السرقة'),
(9, 1, 'Fire Alarm Systems', 'نظام الإنذار ضد الحريق'),
(10, 1, 'Time Attendance systems', 'أنظمة الحضور والإنصراف'),
(11, 1, 'Metal and Contraband Detector', 'أنظمة فحص وتفتيش الحقائب والطرود وكشف المعادن'),
(12, 1, 'Paging and Music Systems', 'نظام صوتي للنداء والوسائط'),
(13, 1, 'Anti-Shoplifting Systems', 'أنظمة حماية المعروضات من السرقة'),
(14, 1, 'Other Systems', 'أنظمة أخرى'),
(15, 2, 'Quotation Request', 'طلب عرض سعر'),
(16, 2, 'Site survey Request', 'طلب مسح موقع'),
(17, 2, 'Become a re-seller', 'طلب توزيع لمنتجات الشركة'),
(18, 2, 'Others', 'أخرى'),
(19, 3, 'Remote Support', 'دعم عن بعد'),
(20, 3, 'Technical Enquiries', 'إستفسارات فنيةّ'),
(21, 3, 'Others', 'أخرى'),
(22, 4, 'Sales and marketing Team', 'طاقم المبيعات والتسويق'),
(23, 4, 'Maintenance Team', 'طاقم الصيانة'),
(24, 4, 'Project Team', 'طاقم المشاريع'),
(25, 4, 'Support Team', 'طاقم الدعم'),
(26, 4, 'IT Team', 'الشبكات وتكنولوجيا المعلومات'),
(27, 4, 'Admin Team', 'طاقم  الإدارة'),
(28, 4, 'Others', 'أخرى');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `eng_country_name` varchar(100) NOT NULL DEFAULT '',
  `arb_country_name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `phone_code` varchar(500) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `eng_country_name`, `arb_country_name`, `code`, `phone_code`, `latitude`, `longitude`) VALUES
(1, 'United States', 'الولايات المتحدة', 'USA', '+1', '37.09024	', '-95.712891	'),
(2, 'Canada', 'كندا', 'CAN', '+1', '56.130366	', '-106.346771	'),
(3, 'Afghanistan', 'أفغانستان', 'AFG', '+93', '33.93911', '67.709953	'),
(4, 'Albania', 'ألبانيا', 'ALB', '+355', '41.153332', '20.168331	'),
(5, 'Algeria', 'الجزائر', 'DZA', '+213', '28.033886', '1.659626'),
(6, 'American Samoa', 'ساموا-الأمريكي', 'ASM', '', '', ''),
(7, 'Andorra', 'أندورا', 'AND', '+376', '42.546245', '1.601554'),
(8, 'Angola', 'أنغولا', 'AGO', '+244', '-11.202692	', '17.873887	'),
(9, 'Anguilla', 'أنغيلا', 'AIA', '+1', '18.220554', '-63.068615	'),
(10, 'Antarctica', 'أنتاركتيكا', 'ATA', '', '-75.250973	', '-0.071389	'),
(11, 'Antigua and/Barbuda', 'أنتيغوا وبربودا', 'ATG', '+1', '17.060816', '-61.796428	'),
(12, 'Argentina', 'الأرجنتين', 'ARG', '+54', '-38.416097	', '-63.616672	'),
(13, 'Armenia', 'أرمينيا', 'ARM', '+374', '40.069099', '45.038189'),
(14, 'Aruba', 'أروبا', 'ABW', '+297', '12.52111	', '-69.968338	'),
(15, 'Australia', 'أستراليا', 'AUS', '+61', '-25.274398	', '133.775136	'),
(16, 'Austria', 'النمسا', 'AUT', '+43', '47.516231', '14.550072'),
(17, 'Azerbaijan', 'أذربيجان', 'AZE', '+994', '40.143105	', '47.576927	'),
(18, 'Bahamas', 'الباهاماس', 'BHS', '+1', '25.03428	', '-77.39628	'),
(19, 'Bahrain', 'البحرين', 'BHR', '+973', '25.930414', '50.637772	'),
(20, 'Bangladesh', 'بنغلاديش', 'BGD', '+880', '23.684994', '90.356331'),
(21, 'Barbados', 'بربادوس', 'BRB', '+1', '13.193887', '-59.543198	'),
(22, 'Belarus', 'روسيا البيضاء', 'BLR', '+375', '53.709807', '27.953389	'),
(23, 'Belgium', 'بلجيكا', 'BEL', '+32', '50.503887', '4.469936'),
(24, 'Belize', 'بيليز', 'BLZ', '+501', '17.189877	', '-88.49765	'),
(25, 'Benin', 'بنين', 'BEN', '+229', '9.30769	', '2.315834'),
(26, 'Bermuda', 'جزر برمود', 'BMU', '+1', '32.321384	', '-64.75737	'),
(27, 'Bhutan', 'بوتان', 'BTN', '+975', '27.514162', '90.433601	'),
(28, 'Bolivia', 'بوليفيا', 'BOL', '+591', '-16.290154	', '-63.588653	'),
(29, 'Bosnia and Herzegovina', 'البوسنة و الهرسك', 'BIH', '+387', '43.915886	', '17.679076'),
(30, 'Botswana', 'بوتسوانا', 'BWA', '+267', '-22.328474	', '24.684866	'),
(31, 'Bouvet Island', 'جزيرة بوفيت', 'BVT', '', '-54.423199	', '3.413194	'),
(32, 'Brazil', 'البرازيل', 'BRA', '+55', '-14.235004	', '-51.92528	'),
(33, 'British Indian Ocean Territory', 'إقليم المحيط الهندي البريطاني', 'IOT', '+246', '-6.343194	', '71.876519'),
(34, 'Brunei Darussalam', 'بروناي دار السلام', 'BRN', '+673', '4.535277', '114.727669'),
(35, 'Bulgaria', 'بلغاريا', 'BGR', '+359', '', ''),
(36, 'Burkina Faso', 'بوركينا فاسو', 'BFA', '+226', '12.238333	', '-1.561593	'),
(37, 'Burundi', 'بوروندي', 'BDI', '+257', '-3.373056	', '29.918886'),
(38, 'Cambodia', 'كمبوديا', 'KHM', '+855', '12.565679	', '104.990963'),
(39, 'Cameroon', 'كاميرون', 'CMR', '+237', '7.369722', '12.354722	'),
(40, 'Cape Verde', 'الرأس الأخضر', 'CPV', '+238', '16.002082	', '-24.013197	'),
(41, 'Cayman Islands', 'جزر كايمان', 'CYM', '+1', '19.513469', '-80.566956	'),
(42, 'Central African Republic', 'جمهورية افريقيا الوسطى', 'CAF', '+236', '6.611111', '20.939444'),
(43, 'Chad', 'تشاد', 'TCD', '+235', '15.454166	', '18.732207'),
(44, 'Chile', 'تشيلي', 'CHL', '+56', '-35.675147	', '-71.542969	'),
(45, 'China', 'الصين', 'CHN', '+86', '35.86166	', '104.195397	'),
(46, 'Christmas Island', 'جزيرة الكريسماس', 'CXR', '', '-10.447525	', '105.690449	'),
(47, 'Cocos (Keeling) Islands', 'جزر كوكوس ( كيلينغ)', 'CCK', '', '-12.164165	', '96.870956	'),
(48, 'Colombia', 'كولومبيا', 'COL', '+57', '4.570868	', '-74.297333	'),
(49, 'Comoros', 'جزر القمر', 'COM', '+269', '-11.875001	', '43.872219	'),
(50, 'Congo', 'الكونغو', 'COG', '+242', '-0.228021	', '15.827659	'),
(51, 'Cook Islands', 'جزر كوك', 'COK', '+682', '-21.236736	', '-159.777671	'),
(52, 'Costa Rica', 'كوستا ريك', 'CRI', '+506', '9.748917	', '-83.753428'),
(53, 'Croatia (Hrvatska)', 'كرواتيا ( هرافاتسكا )', 'HRV', '+385', '45.1', '15.2	'),
(54, 'Cuba', 'كوبا', 'CUB', '+53', '21.521757', '-77.781167	'),
(55, 'Cyprus', 'قبرص', 'CYP', '+357', '35.126413', '33.429859	'),
(56, 'Czech Republic', 'جمهورية التشيك', 'CZE', '+420', '49.817492', '15.472962	'),
(57, 'Denmark', 'الدنمارك', 'DNK', '+45', '56.26392	', '9.501785	'),
(58, 'Djibouti', 'جيبوتي', 'DJI', '+253', '11.825138	', '42.590275'),
(59, 'Dominica', 'دومينيكا', 'DMA', '+1', '15.414999	', '-61.370976	'),
(60, 'Dominican Republic', 'جمهورية الدومنيكان', 'DOM', '+1', '18.735693', '-70.162651	'),
(61, 'East Timor', 'تيمور الشرقية', 'TMP', '', '', ''),
(62, 'Ecuador', 'الإكوادور', 'ECU', '+593', '-1.831239	', '-78.183406	'),
(63, 'Egypt', 'مصر', 'EGY', '+20', '26.820553', '30.802498'),
(64, 'El Salvador', 'السلفادور', 'SLV', '+503', '13.794185', '-88.89653	'),
(65, 'Equatorial Guinea', 'غينيا الإستوائية', 'GNQ', '+240', '1.650801', '10.267895'),
(66, 'Eritrea', 'إريتريا', 'ERI', '+291', '15.179384', '39.782334'),
(67, 'Estonia', 'استونيا', 'EST', '+372', '58.595272', '25.013607'),
(68, 'Ethiopia', 'أثيوبيا', 'ETH', '+251', '9.145', '40.489673	'),
(69, 'Falkland Islands (Malvinas)', 'جزر فوكلاند ( مالفيناس )', 'FLK', '+500', '-51.796253	', '-59.523613	'),
(70, 'Faroe Islands', 'جزر فارو', 'FRO', '+298', '61.892635	', '-6.911806	'),
(71, 'Fiji', 'فيجي', 'FJI', '+679', '-16.578193	', '179.414413	'),
(72, 'Finland', 'فنلندا', 'FIN', '+358', '61.92411', '25.748151'),
(73, 'France', 'فرنسا', 'FRA', '+33', '46.227638	', '2.213749	'),
(74, 'France, Metropolitan', 'فرنسا ، متروبوليتان', '', '', '', ''),
(75, 'French Guiana', 'جيانا الفرنسية', 'GUF', '+594', '3.933889', '-53.125782	'),
(76, 'French Polynesia', 'بولينيزيا الفرنسية', 'PYF', '+689', '-17.679742	', '-149.406843	'),
(77, 'French Southern Territories', 'الاقاليم الجنوبية الفرنسية', 'ATF', '', '-49.280366	', '69.348557'),
(78, 'Gabon', 'الغابون', 'GAB', '+241', '-0.803689	', '11.609444	'),
(79, 'Gambia', 'غامبيا', 'GMB', '+220', '13.443182', '-15.310139	'),
(80, 'Georgia', 'جورجيا', 'GEO', '+995', '42.315407', '43.356892'),
(81, 'Germany', 'ألمانيا', 'DEU', '+49', '51.165691	', '10.451526'),
(82, 'Ghana', 'غانا', 'GHA', '+233', '7.946527	', '-1.023194	'),
(83, 'Gibraltar', 'جبل طارق', 'GIB', '+350', '36.137741', '-5.345374	'),
(246, 'Guernsey', 'غيرنسي', '', '', '', ''),
(84, 'Greece', 'اليونان', 'GRC', '+30', '39.074208', '21.824312'),
(85, 'Greenland', 'ارض خضراء', 'GRL', '+299', '71.706936	', '-42.604303	'),
(86, 'Grenada', 'غرينادا', 'GRD', '+1', '12.262776', '-61.604171	'),
(87, 'Guadeloupe', 'جوادلوب', 'GLP', '+590', '16.995971	', '-62.067641	'),
(88, 'Guam', 'غوام', 'GUM', '+1', '13.444304', '144.793731	'),
(89, 'Guatemala', 'غواتيمالا', 'GTM', '+502', '15.783471', '-90.230759	'),
(90, 'Guinea', 'غينيا', 'GIN', '+224', '9.945587', '-9.696645	'),
(91, 'Guinea-Bissau', 'غينيا بيساو', 'GNB', '+245', '11.803749', '-15.180413	'),
(92, 'Guyana', 'غيانا', 'GUY', '+592', '4.860416	', '-58.93018	'),
(93, 'Haiti', 'هايتي', 'HTI', '+509', '18.971187	', '-72.285215	'),
(94, 'Heard and Mc Donald Islands', 'سمع و ماك دونالد جزر', 'HMD', '', '-53.08181	', '73.504158'),
(95, 'Honduras', 'هندوراس', 'HND', '+504', '15.199999', '-86.241905	'),
(96, 'Hong Kong', 'هونج كونج', 'HKG', '+852', '22.396428	', '114.109497'),
(97, 'Hungary', 'هنغاريا', 'HUN', '+36', '47.162494', '19.503304'),
(98, 'Iceland', 'أيسلندا', 'ISL', '+354', '64.963051', '-19.020835	'),
(99, 'India', 'الهند', 'IND', '+91', '20.593684	', '78.96288'),
(100, 'Indonesia', 'أندونيسيا', 'IDN', '+62', '-0.789275	', '113.921327	'),
(101, 'Iran (Islamic Republic of)', 'إيران ( الجمهورية الإسلامية)', 'IRN', '+98', '32.427908	', '53.688046'),
(102, 'Iraq', 'العراق', 'IRQ', '+964', '33.223191', '43.679291'),
(103, 'Ireland', 'أيرلندا', 'IRL', '+353', '53.41291', '-8.24389	'),
(104, 'Israel', 'إسرائيل', 'ISR', '+972', '31.046051	', '34.851612'),
(105, 'Italy', 'إيطاليا', 'ITA', '+39', '41.87194', '12.56738'),
(106, 'Ivory Coast', 'ساحل العاج', 'CIV', '+225', '7.539989	', '-5.54708	'),
(245, 'Jersey', 'جيرسي', '', '', '49.214439', '-2.13125	'),
(107, 'Jamaica', 'جامايكا', 'JAM', '+1', '18.109581', '-77.297508	'),
(108, 'Japan', 'اليابان', 'JPN', '+81', '36.204824', '138.252924'),
(109, 'Jordan', ' الأردن', 'JOR', '+962', '30.585164', '36.238414	'),
(110, 'Kazakhstan', 'كازاخستان', 'KAZ', '+7', '48.019573', '66.923684'),
(111, 'Kenya', 'كينيا', 'KEN', '+254', '-0.023559	', '37.906193'),
(112, 'Kiribati', 'كيريباتي', 'KIR', '+686', '-3.370417	', '-168.734039	'),
(114, 'Korea', 'جمهورية كوريا', 'KOR', '+82', '35.907757', '127.766922	'),
(115, 'Kosovo', 'كوسوفو', '', '+381', '42.602636	', '20.902977'),
(116, 'Kuwait', 'الكويت', 'KWT', '+965', '29.31166	', '47.481766'),
(117, 'Kyrgyzstan', 'قرغيزستان', 'KGZ', '+996', '41.20438', '74.766098	'),
(118, 'Lao People\'s Democratic Republic', 'جمهورية لاو الديمقراطية الشعبية', 'LAO', '+856', '19.85627', '102.495496'),
(119, 'Latvia', 'لاتفيا', 'LVA', '+371', '56.879635', '24.603189'),
(120, 'Lebanon', 'لبنان', 'LBN', '+961', '33.854721', '35.862285'),
(121, 'Lesotho', 'ليسوتو', 'LSO', '+266', '-29.609988	', '28.233608'),
(122, 'Liberia', 'ليبيريا', 'LBR', '+231', '6.428055', '-9.429499	'),
(123, 'Libyan Arab Jamahiriya', 'الجماهيرية العربية الليبية', 'LBY', '+218', '26.3351', '17.228331'),
(124, 'Liechtenstein', 'ليختنشتاين', 'LIE', '+423', '47.166', '9.555373'),
(125, 'Lithuania', 'ليتوانيا', 'LTU', '+370', '55.169438', '23.881275'),
(126, 'Luxembourg', 'لوكسمبورغ', 'LUX', '+352', '49.815273', '6.129583'),
(127, 'Macau', 'ماكاو', 'MAC', '+853', '22.198745', '113.543873'),
(128, 'Macedonia', 'مقدونيا', 'MKD', '+389', '41.608635', '21.745275	'),
(129, 'Madagascar', 'مدغشقر', 'MDG', '+261', '-18.766947	', '46.869107	'),
(130, 'Malawi', 'ملاوي', 'MWI', '+265', '-13.254308	', '34.301525'),
(131, 'Malaysia', 'ماليزيا', 'MYS', '+60', '4.210484', '101.975766'),
(132, 'Maldives', 'جزر المالديف', 'MDV', '+960', '3.202778', '73.22068'),
(133, 'Mali', 'مالي', 'MLI', '+223', '17.570692', '-3.996166	'),
(134, 'Malta', 'مالطا', 'MLT', '+356', '35.937496', '14.375416'),
(135, 'Marshall Islands', 'جزر مارشال', 'MHL', '+692', '7.131474', '171.184478'),
(136, 'Martinique', 'مارتينيك', 'MTQ', '+596', '14.641528	', '-61.024174	'),
(137, 'Mauritania', 'موريتانيا', 'MRT', '+222', '21.00789', '-10.940835	'),
(138, 'Mauritius', 'موريشيوس', 'MUS', '+230', '-20.348404	', '57.552152'),
(139, 'Mayotte', 'مايوت', '', '', '', ''),
(140, 'Mexico', 'المكسيك', 'MEX', '+52', '23.634501	', '-102.552784	'),
(141, 'Micronesia, Federated States of', 'ولايات ميكرونيزيا الموحدة من', 'FSM', '+691', '7.425554	', '150.550812'),
(142, 'Moldova, Republic of', 'جمهورية مولدوفا', 'MDA', '+373', '47.411631', '28.369885'),
(143, 'Monaco', 'موناكو', 'MCO', '+377', '43.750298	', '7.412841'),
(144, 'Mongolia', 'منغوليا', 'MNG', '+976', '46.862496', '103.846656'),
(145, 'Montenegro', 'الجبل الأسود', '', '+382', '42.708678	', '19.37439'),
(146, 'Montserrat', 'مونتسيرات', 'MSR', '+1', '16.742498', '-62.187366	'),
(147, 'Morocco', 'المغرب', 'MAR', '+212', '31.791702', '-7.09262	'),
(148, 'Mozambique', 'موزمبيق', 'MOZ', '+258', '-18.665695	', '35.529562'),
(149, 'Myanmar', 'ميانمار', 'MMR', '+95', '21.913965', '95.956223'),
(150, 'Namibia', 'ناميبيا', 'NAM', '+264', '-22.95764	', '18.49041'),
(151, 'Nauru', 'ناورو', 'NRU', '+674', '-0.522778	', '166.931503	'),
(152, 'Nepal', 'نيبال', 'NPL', '+977', '28.394857', '84.124008'),
(153, 'Netherlands', 'هولندا', 'NLD', '+31', '52.132633', '5.291266'),
(154, 'Netherlands Antilles', 'جزر الأنتيل الهولندية', 'ANT', '+599', '12.226079', '-69.060087	'),
(155, 'New Caledonia', 'كاليدونيا الجديدة', 'NCL', '+687', '-20.904305	', '165.618042	'),
(156, 'New Zealand', 'نيوزيلندا', 'NZL', '+64', '-40.900557	', '174.885971	'),
(157, 'Nicaragua', 'نيكاراغوا', 'NIC', '+505', '12.865416', '-85.207229	'),
(158, 'Niger', 'النيجر', 'NER', '+227', '17.607789', '8.081666	'),
(159, 'Nigeria', 'نيجيريا', 'NGA', '+234', '9.081999', '8.675277	'),
(160, 'Niue', 'نيوي', 'NIU', '+683', '-19.054445	', '-169.867233	'),
(161, 'Norfolk Island', 'جزيرة نورفولك', 'NFK', '+672', '-29.040835	', '167.954712'),
(162, 'Northern Mariana Islands', 'جزر مريانا الشمالية', 'MNP', '+1', '17.33083', '145.38469'),
(163, 'Norway', 'النرويج', 'NOR', '+47', '60.472024	', '8.468946'),
(164, 'Oman', 'سلطنة عمان', 'OMN', '+968', '21.512583', '55.923255	'),
(165, 'Pakistan', 'باكستان', 'PAK', '+92', '30.375321', '69.345116'),
(166, 'Palau', 'بالاو', 'PLW', '+680', '7.51498', '134.58252'),
(243, 'Palestine', 'فلسطين', 'PSE', '+970', '31.952162', '35.233154'),
(167, 'Panama', 'بناما', 'PAN', '+507', '8.537981', '-80.782127	'),
(168, 'Papua New Guinea', 'بابوا غينيا الجديدة', 'PNG', '+675', '-6.314993	', '143.95555'),
(169, 'Paraguay', 'باراغواي', 'PRY', '+595', '-23.442503	', '-58.443832	'),
(170, 'Peru', 'بيرو', 'PER', '+51', '-9.189967	', '-75.015152	'),
(171, 'Philippines', 'الفلبين', 'PHL', '+63', '12.879721	', '121.774017	'),
(172, 'Pitcairn', 'بيتكيرن', 'PCN', '', '-24.703615	', '-127.439308	'),
(173, 'Poland', 'بولندا', 'POL', '+48', '51.919438	', '19.145136	'),
(174, 'Portugal', 'البرتغال', 'PRT', '+351', '39.399872	', '-8.224454	'),
(175, 'Puerto Rico', 'بورتوريكو', 'PRI', '+1', '18.220833	', '-66.590149	'),
(176, 'Qatar', 'دولة قطر', 'QAT', '+974', '25.354826', '51.183884	'),
(177, 'Reunion', 'جمع شمل', 'REU', '+262', '-21.115141	', '55.536384'),
(178, 'Romania', 'رومانيا', 'ROM', '+40', '45.943161	', '24.96676'),
(179, 'Russian Federation', 'الفيدرالية الروسية', 'RUS', '+7', '61.52401	', '105.318756	'),
(180, 'Rwanda', 'رواندا', 'RWA', '+250', '-1.940278	', '29.873888'),
(181, 'Saint Kitts and Nevis', 'سانت كيتس ونيفيس', 'KNA', '+1', '17.357822	', '-62.782998	'),
(182, 'Saint Lucia', 'سانت لوسيا', 'LCA', '+1', '13.909444	', '-60.978893	'),
(183, 'Saint Vincent and the Grenadines', 'سانت فنسنت وجزر غرينادين', 'VCT', '+1', '12.984305', '-61.287228	'),
(184, 'Samoa', 'ساموا', 'WSM', '+685', '-13.759029	', '-172.104629	'),
(185, 'San Marino', 'سان مارينو', 'SMR', '+378', '43.94236', '12.457777'),
(186, 'Sao Tome and Principe', 'ساو تومي و برينسيبي', 'STP', '+239', '0.18636', '6.613081	'),
(187, 'Saudi Arabia', 'المملكة العربية السعودية', 'SAU', '+966', '23.885942', '45.079162'),
(188, 'Senegal', 'السنغال', 'SEN', '+221', '14.497401', '-14.452362	'),
(189, 'Serbia', 'صربيا', '', '+381', '44.016521	', '21.005859'),
(190, 'Seychelles', 'سيشيل', 'SYC', '+248', '-4.679574	', '55.491977'),
(191, 'Sierra Leone', 'سيرا ليون', 'SLE', '+232', '8.460555', '-11.779889	'),
(192, 'Singapore', 'سنغافورة', 'SGP', '+65', '1.352083', '103.819836	'),
(193, 'Slovakia', 'سلوفاكيا', 'SVK', '+421', '48.669026	', '19.699024	'),
(194, 'Slovenia', 'سلوفينيا', 'SVN', '+386', '46.151241	', '14.995463'),
(195, 'Solomon Islands', 'جزر سليمان', 'SLB', '+677', '-9.64571	', '160.156194'),
(196, 'Somalia', 'الصومال', 'SOM', '+252', '5.152149', '46.199616'),
(197, 'South Africa', 'جنوب أفريقيا', 'ZAF', '+27', '-30.559482	', '22.937506	'),
(198, 'South Georgia South Sandwich Islands', 'جزر ساندويتش الجنوبية جورجيا الجنوبية', 'SGS', '', '-54.429579	', '-36.587909	'),
(199, 'Spain', 'إسبانيا', 'ESP', '+34', '40.463667', '-3.74922	'),
(200, 'Sri Lanka', 'سيريلانكا', 'LKA', '+94', '7.873054', '80.771797	'),
(201, 'St. Helena', 'سانت هيلانة', 'SHN', '+290', '-24.143474	', '-10.030696	'),
(202, 'St. Pierre and Miquelon', 'سانت بيير و ميكلون', 'SPM', '+508', '46.941936', '-56.27111	'),
(203, 'Sudan', 'سودان', 'SDN', '+249', '12.862807', '30.217636'),
(204, 'Suriname', 'سورينام', 'SUR', '+597', '3.919305', '-56.027783	'),
(205, 'Svalbard and Jan Mayen Islands', 'جزر سفالبارد وجان ماين', 'SJM', '', '77.553604	', '23.670272'),
(206, 'Swaziland', 'سوازيلاند', 'SWZ', '+268', '-26.522503	', '31.465866	'),
(207, 'Sweden', 'السويد', 'SWE', '+46', '60.128161', '18.643501'),
(208, 'Switzerland', 'سويسرا', 'CHE', '+41', '46.818188	', '8.227512'),
(209, 'Syrian Arab Republic', 'الجمهورية العربية السورية', 'SYR', '+963', '34.802075', '38.996815'),
(210, 'Taiwan', 'تايوان', 'TWN', '+886', '23.69781	', '120.960515'),
(211, 'Tajikistan', 'طاجيكستان', 'TJK', '+992', '38.861034', '71.276093'),
(212, 'Tanzania, United Republic of', 'جمهورية تنزانيا المتحدة', 'TZA', '+255', '-6.369028	', '34.888822	'),
(213, 'Thailand', 'تايلاند', 'THA', '+66', '15.870032', '100.992541'),
(214, 'Togo', 'توغو', 'TGO', '+228', '8.619543	', '0.824782'),
(215, 'Tokelau', 'توكيلاو', 'TKL', '+690', '-8.967363	', '-171.855881	'),
(216, 'Tonga', 'تونغا', 'TON', '+676', '-21.178986	', '-175.198242	'),
(217, 'Trinidad and Tobago', 'ترينداد وتوباغو', 'TTO', '+1', '10.691803	', '-61.222503	'),
(218, 'Tunisia', 'تونس', 'TUN', '+216', '33.886917	', '9.537499	'),
(219, 'Turkey', 'تركيا', 'TUR', '+90', '38.963745', '35.243322'),
(220, 'Turkmenistan', 'تركمانستان', 'TKM', '+993', '38.969719', '59.556278	'),
(221, 'Turks and Caicos Islands', 'جزر تركس وكايكوس', 'TCA', '+1', '21.694025', '-71.797928	'),
(222, 'Tuvalu', 'توفالو', 'TUV', '+688', '-7.109535	', '177.64933'),
(223, 'Uganda', 'أوغندا', 'UGA', '+256', '1.373333', '32.290275'),
(224, 'Ukraine', 'أوكرانيا', 'UKR', '+380', '48.379433	', '31.16558'),
(225, 'United Arab Emirates', 'الإمارات العربية المتحدة', 'ARE', '+971', '23.424076', '	53.847818'),
(226, 'United Kingdom', 'المملكة المتحدة', 'GBR', '+44', '55.378051	', '-3.435973	'),
(227, 'United States minor outlying islands', 'الولايات المتحدة الجزر الصغيرة النائية', 'UMI', '', '', ''),
(228, 'Uruguay', 'أوروغواي', 'URY', '+598', '-32.522779	', '-55.765835	'),
(229, 'Uzbekistan', 'أوزبكستان', 'UZB', '+998', '41.377491', '64.585262'),
(230, 'Vanuatu', 'فانواتو', 'VUT', '+678', '-15.376706	', '166.959158'),
(231, 'Vatican City State', 'دولة مدينة الفاتيكان', 'VAT', '+39', '41.902916	', '12.453389'),
(232, 'Venezuela', 'فنزويلا', 'VEN', '+58', '6.42375', '-66.58973	'),
(233, 'Vietnam', 'فيتنام', 'VNM', '+84', '14.058324', '108.277199	'),
(234, 'Virgin Islands (British)', 'جزر العذراء (البريطانية )', 'VGB', '+1', '18.420695', '-64.639968	'),
(235, 'Virgin Islands (U.S.)', 'جزر العذراء ( الولايات المتحدة )', 'VIR', '+1', '18.335765', '-64.896335	'),
(236, 'Wallis and Futuna Islands', 'واليس و فوتونا', 'WLF', '+681', '-13.768752	', '-177.156097	'),
(237, 'Western Sahara', 'الصحراء الغربية', 'ESH', '', '24.215527	', '-12.885834	'),
(238, 'Yemen', 'اليمن', 'YEM', '+967', '15.552727	', '48.516388	'),
(239, 'Yugoslavia', 'يوغوسلافيا', 'YUG', '', '', ''),
(240, 'Zaire', 'زائير', '', '', '', ''),
(241, 'Zambia', 'زامبيا', 'ZMB', '+260', '-13.133897	', '27.849332'),
(242, 'Zimbabwe', 'زيمبابوي', 'ZWE', '+263', '-19.015438	', '29.154857');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `district_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `eng_name` varchar(255) NOT NULL,
  `arb_name` varchar(255) NOT NULL,
  `delivery_charges` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`district_id`, `city_id`, `eng_name`, `arb_name`, `delivery_charges`) VALUES
(1, 1, 'Jeddah District 1', 'Jeddah District 1', '15'),
(2, 1, 'Jeddah District 2', 'Jeddah District 2', '15'),
(3, 2, 'Madina District 1', 'Madina District 1', '15'),
(4, 2, 'Madina District 2', 'Madina District 2', '15');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `faq_id` int(11) NOT NULL,
  `question_en` varchar(5000) NOT NULL,
  `question_ar` varchar(5000) NOT NULL,
  `answer_en` varchar(5000) NOT NULL,
  `answer_ar` varchar(5000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `home_images`
--

CREATE TABLE `home_images` (
  `homeimage_id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_ar` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` varchar(500) NOT NULL,
  `is_new_tab` tinyint(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_images`
--

INSERT INTO `home_images` (`homeimage_id`, `title_en`, `title_ar`, `image`, `url`, `is_new_tab`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Awards Ceremony ', 'حفلات تكريم الشركات ', 'uploads/home_images/49952099447202008050415211.jpg', 'https://www.msj.com.sa/page/awards', 0, '2018-03-30 09:49:03', '2020-08-05 16:21:15', 1),
(3, 'Become a Reseller', 'كن موزعاً لمنتجاتنا', 'uploads/home_images/26910704464202008050429172.jpg', 'https://www.msj.com.sa/page/become_a_seller', 0, '2018-03-30 09:17:11', '2020-08-05 16:17:29', 1),
(4, 'Special  Events', 'فعاليات خاصة', 'uploads/home_images/83624693898202008050442173.jpg', ' ', 0, '2018-03-30 09:17:46', '2020-08-05 16:17:42', 1),
(5, 'Modern Technology', 'تقنيات حديثة', 'uploads/home_images/46301663494202008050454174.jpg', '', 0, '2018-03-30 09:18:26', '2020-08-05 16:17:54', 1),
(6, 'Outstanding Projects', 'المشاريع الحاليه', 'uploads/home_images/54908245328202008050412186.jpg', ' ', 0, '2018-03-30 09:18:50', '2020-08-05 16:18:12', 1),
(7, 'Download Center', 'لتحميل المواصفات والنشرات', 'uploads/home_images/62479563223202008050424185.jpg', ' https://www.msj.com.sa/page/download_center', 0, '2018-03-30 09:45:42', '2020-08-05 16:18:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_video`
--

CREATE TABLE `home_video` (
  `id` int(11) NOT NULL,
  `video_url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_video`
--

INSERT INTO `home_video` (`id`, `video_url`) VALUES
(1, 'https://www.youtube.com/watch?v=Y-lBvI6u_hw');

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `newsletter_id` int(11) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `subscribed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`newsletter_id`, `email`, `subscribed_at`) VALUES
(3, 'yaser@msj.com.sa', '2018-07-30 09:13:33'),
(8, 'Sk110956@gmail.com', '2018-08-14 08:02:52'),
(10, 'umar@5tech.com.sa', '2018-10-10 07:22:18'),
(12, 'SHIVAANSHENTP@GMAIL.COM', '2018-10-13 05:07:28'),
(15, 'arifkhanbd25524@gmail.com', '2018-11-03 17:28:29'),
(16, 'hatimradwan33@gmail.com', '2018-11-25 12:11:59'),
(17, 'tal_balawy@hotmail.com', '2018-11-29 18:55:59'),
(18, 'bandaralsaud76@gmail.com', '2018-12-06 06:54:22'),
(19, 'ahsadek0@gmail.com', '2018-12-08 06:58:46'),
(20, 'hadjimed2804@gmail.com', '2018-12-13 11:36:45'),
(21, 'kmmbaawad@uroptimum.com', '2018-12-14 15:29:16'),
(22, 'noviri5@yahoo.it', '2018-12-15 17:59:14'),
(23, 'foo-bar@example.com', '2018-12-17 13:08:02'),
(25, 'foo-bar@example.com\'', '2018-12-17 13:33:38'),
(27, 'foo-bar@example.com\"', '2018-12-17 13:33:39'),
(29, 'foo-bar@example.com;', '2018-12-17 13:33:39'),
(31, 'foo-bar@example.com)', '2018-12-17 13:33:40'),
(32, 'foo-bar@example.com AND 1=1 -- ', '2018-12-17 13:33:41'),
(33, 'foo-bar@example.com\' AND \'1\'=\'1\' -- ', '2018-12-17 13:33:41'),
(34, 'foo-bar@example.com\" AND \"1\"=\"1\" -- ', '2018-12-17 13:33:41'),
(35, 'foo-bar@example.com AND 1=1', '2018-12-17 13:33:42'),
(36, 'foo-bar@example.com\' AND \'1\'=\'1', '2018-12-17 13:33:42'),
(37, 'foo-bar@example.com\" AND \"1\"=\"1', '2018-12-17 13:33:42'),
(38, 'foo-bar@example.com UNION ALL select NULL -- ', '2018-12-17 13:33:42'),
(39, 'foo-bar@example.com\' UNION ALL select NULL -- ', '2018-12-17 13:33:43'),
(40, 'foo-bar@example.com\" UNION ALL select NULL -- ', '2018-12-17 13:33:43'),
(41, 'foo-bar@example.com) UNION ALL select NULL -- ', '2018-12-17 13:33:43'),
(42, 'foo-bar@example.com\') UNION ALL select NULL -- ', '2018-12-17 13:33:44'),
(43, 'rohail@elegantservices.com', '2018-12-18 12:07:38'),
(44, 'afnazbv@gmail.com', '2019-01-10 07:02:20'),
(45, 'Faris.Alasmari93@gmail.com', '2019-01-19 03:06:23'),
(46, 'Kpaslam50@gmail.com', '2019-01-19 14:15:00'),
(47, 'azeemakhtar82@gmail.com', '2019-01-20 09:42:54'),
(48, 'mohammedazmath99@gmail.com', '2019-01-24 09:08:13'),
(49, 'margarida.costa@actualsales.com', '2019-01-24 11:58:53'),
(50, 'account3@msj.com.sa', '2019-01-30 08:02:50'),
(51, 'mbilalafzal08@gmail.com', '2019-01-31 10:39:08'),
(52, 'shahul.najmudeen@alkhozama.com', '2019-02-03 08:05:55'),
(54, 'baha.t.ksa@gmail.com', '2019-02-04 06:29:11'),
(55, 'karimshawky@topscreens.com.sa', '2019-02-18 04:24:45'),
(56, 'm-naseer@comcrafts.net', '2019-02-27 10:37:01'),
(57, 'mzakeerali@gmail.com', '2019-03-03 10:47:19'),
(58, 'yaramoon.cctv@gmail.com', '2019-03-07 09:52:35'),
(59, 'mahmoudelkomy2050@gmail.com', '2019-03-16 15:36:18'),
(60, 'persedia2021@gmail.com', '2019-03-24 04:53:49'),
(62, 'a.zaki@expohorizon.com', '2019-04-03 08:50:10'),
(63, 'nisar.ahmad@dhurom.com', '2019-04-04 11:33:30'),
(64, 'mustaqueemansari34@gmail.com', '2019-04-05 10:53:12'),
(65, 'persedia2024@gmail.com', '2019-04-06 16:56:30'),
(66, 'Immie7@gmail.com', '2019-04-11 14:51:58'),
(67, 'abt@abtss.com.sa', '2019-04-16 09:08:03'),
(68, 'eng_ahmed_kamal15@yahoo.com', '2019-04-16 09:29:57'),
(69, 'alaa8472@hotmail.com', '2019-04-23 20:48:28'),
(70, 'aleid.abdulaziz@yahoo.con', '2019-05-10 01:58:53'),
(71, 'aleid.abdulaziz@yahoo.com', '2019-05-10 01:59:16'),
(72, 'coreipro76@gmail.com', '2019-05-19 08:13:29'),
(73, 'samiranm@vfsglobal.com', '2019-06-13 09:33:37'),
(74, 'gisler@armin.com.tr', '2019-06-16 10:06:25'),
(75, 'mn172100@yahoo.com', '2019-06-19 14:45:02'),
(76, 'humayun3871@yahoo.com', '2019-06-19 20:46:11'),
(77, 'joseglenlapastora@gmail.com', '2019-06-20 11:52:30'),
(78, 'ashrafaradwan@nasco.com.sa', '2019-06-24 07:10:04'),
(79, 'service@avpro.ae', '2019-06-25 11:14:16'),
(80, 'cordellhamlin06@gmail.com', '2019-06-26 11:17:34'),
(81, 'rahulrnair414@yahoo.com', '2019-06-29 06:18:14'),
(82, 'c.yilmaz@na-de.com.tr', '2019-07-08 15:12:32'),
(83, 'hzakir435@gmail.com', '2019-07-20 10:19:32'),
(84, 'r.khalil@arabcomputers.com.sa', '2019-07-30 07:56:02'),
(85, 'adeelarshad@fmco.sa', '2019-07-31 11:31:00'),
(86, 'timmcclintic@gmail.com', '2019-08-09 20:40:18'),
(87, 'dashen1@hotmail.com', '2019-08-09 23:36:04'),
(88, 'krawling@harding.edu', '2019-08-09 23:39:05'),
(89, 'saeed9bkh@gmail.com', '2019-08-12 15:12:11'),
(90, 'mekarkrishna@yahoo.com', '2019-08-13 21:57:22'),
(91, 'uknowamo@yahoo.com', '2019-08-14 05:22:22'),
(92, 'steven125@comcast.net', '2019-08-14 14:08:29'),
(93, 'nicole.grimlie@hotmail.com', '2019-08-14 14:55:25'),
(94, 's_zentara@yahoo.com', '2019-08-15 17:38:43'),
(95, 'nanofrano55@gmail.com', '2019-08-16 16:31:48'),
(96, 'jkbest@eastlink.ca', '2019-08-16 23:50:37'),
(97, 'ramon.quedit@gmail.com', '2019-08-19 11:33:28'),
(98, 'amyhbrown@gmail.com', '2019-08-19 21:01:08'),
(99, 'network@tamkeenhr.com', '2019-08-20 08:10:59'),
(100, 'SMATHIS@PUMPCONCRETE.COM', '2019-08-20 09:50:32'),
(101, 'pandabamb00@yahoo.com', '2019-08-20 22:41:36'),
(102, 'xnoelle25@gmail.com', '2019-08-21 08:30:12'),
(103, 'lisa.gainder@att.net', '2019-08-21 20:08:19'),
(104, 'debrajones12@comcast.net', '2019-08-22 15:10:47'),
(105, 'katherinepanke@gmail.com', '2019-08-22 16:25:17'),
(106, 'wraith755@gmail.com', '2019-08-22 16:45:25'),
(107, 'AHILT1@YAHOO.COM', '2019-08-23 02:30:01'),
(108, 'ralphcapotorto@yahoo.com', '2019-08-23 15:49:35'),
(109, 'AHILT@YAHOO.COM', '2019-08-23 16:24:27'),
(110, 'HUNTER@THETERRACECLUB.COM', '2019-08-23 21:32:18'),
(111, 'draymond3@sbcglobal.net', '2019-08-24 04:12:21'),
(112, 'garlaf@outlook.com', '2019-08-24 04:20:43'),
(113, 'leokarinas@yahoo.com', '2019-08-24 09:46:11'),
(114, 'dodson_annette@hotmail.com', '2019-08-24 17:35:15'),
(115, 'pbrantin@sfu.ca', '2019-08-24 20:32:00'),
(116, 'shaundenton@sbcglobal.net', '2019-08-25 03:37:51'),
(117, 'm.taha@sraco.com.sa', '2019-08-25 06:26:52'),
(118, 'mcclendonbickley60+506656@gmail.com', '2019-08-25 19:40:53'),
(119, 'bikerdick@comcast.net', '2019-08-26 08:40:43'),
(120, 'boydshirley28@yahoo.com', '2019-08-26 16:11:33'),
(121, 'judygjohnson@sbcglobal.net', '2019-08-26 16:12:15'),
(122, 'altifan07@yahoo.com', '2019-08-27 00:01:22'),
(123, 'shannontfwang@gmail.com', '2019-08-27 00:28:40'),
(124, 'FRYAN@BUTLERPAPPAS.COM', '2019-08-27 00:55:31'),
(125, 'justens21@gmail.com', '2019-08-27 03:47:41'),
(126, 'danwhitham@hotmail.com', '2019-08-27 05:48:28'),
(127, 'kmccusker2@comcast.net', '2019-08-27 08:46:12'),
(128, 'cdwyer@hyflodraulic.ca', '2019-08-27 17:57:44'),
(130, 'annemariesalerno@rogers.com', '2019-08-28 07:42:21'),
(131, 'AARZATA@BZESPORTS.ORG', '2019-08-28 09:18:42'),
(132, 'agul.lotta@gmail.com', '2019-08-28 17:35:17'),
(133, 'antionettescbell@hotmail.com', '2019-08-29 17:26:03'),
(134, 'tonykostel2856+506656@gmail.com', '2019-09-05 04:28:25'),
(135, 'mriyas@icad.com', '2019-09-05 14:53:12'),
(136, 'Amir@tdzgs.com', '2019-09-12 01:56:25'),
(137, 'khaledasal442442@gmail.com', '2019-09-25 10:22:16'),
(138, 'faisalfarhan9885@gmal.com', '2019-10-06 13:01:38'),
(139, 'ahmed.atta@profisonal.com', '2019-10-06 18:02:18'),
(140, 'm.aboshleh@gmail.com', '2019-10-07 00:42:44'),
(141, 'RhlA', '2019-10-09 22:55:39'),
(142, 'RhlA)\'),)),))\"', '2019-10-09 22:55:39'),
(143, 'RhlA\'CXYkzk<\'\">QPCpQZ', '2019-10-09 22:55:40'),
(144, 'RhlA/**/ORDER/**/BY/**/1--/**/jMHs', '2019-10-09 22:55:42'),
(145, 'RhlA/**/ORDER/**/BY/**/5528--/**/aTpf', '2019-10-09 22:55:42'),
(146, 'RhlA/**/ORDER/**/BY/**/1--/**/NMyh', '2019-10-09 22:55:42'),
(147, 'RhlA/**/ORDER/**/BY/**/9300--/**/fSeV', '2019-10-09 22:55:43'),
(148, 'kashifmalhi16@gmail.com', '2019-10-12 17:14:45'),
(149, 'danishahmad7d@gmail.com', '2019-10-22 06:00:38'),
(150, 'sobichan30@hotmail.com', '2019-10-29 07:27:09'),
(151, 'sc.rastanura@highseas.com.sa', '2019-12-15 13:30:27'),
(152, 'jaspermcpherson.ms.154864@supersendme.org', '2019-12-22 09:57:39'),
(153, 'adilsign000@gmail.com', '2019-12-29 08:09:50'),
(154, 'freedom@host-system.ru', '2020-01-02 19:32:54'),
(155, 'yfshalabi@saudi-rsc.com', '2020-01-15 07:28:14'),
(156, 'venkatesh96600@gmail.com', '2020-01-20 09:40:34'),
(157, 'faisal05016@gmail.com', '2020-01-22 08:13:15'),
(158, 'syedanwar08@gmail.com', '2020-01-26 07:25:43'),
(159, 'rafieaa@hotmail.com', '2020-01-27 13:56:10'),
(160, 'laureljackson.sc.151139632@supersendme.org', '2020-01-29 07:42:15'),
(161, 'laureljackson.sc.151139633@supersendme.org', '2020-01-29 07:42:15'),
(162, 'g.sharawi@gypsco.com.sa', '2020-01-30 08:06:28'),
(163, 'mariecaldwell.sc.59265301@digitalfall.ga', '2020-01-31 19:08:44'),
(164, 'mariecaldwell.sc.59265300@digitalfall.ga', '2020-01-31 19:08:44'),
(165, 'jaydalucero.sc.647086983@supersendme.org', '2020-02-15 01:28:19'),
(166, 'marywagner.sc.647086984@supersendme.org', '2020-02-15 01:28:19'),
(167, 'noreply1@z9x.space', '2020-02-16 04:29:39'),
(168, 'noreply2@z9x.space', '2020-02-16 04:29:40'),
(169, 'noreply3@z9x.space', '2020-02-16 04:29:41'),
(170, 'egsystem1990@gmail.com', '2020-02-17 09:17:36'),
(171, 'noreply3@i4b.space', '2020-02-18 12:38:20'),
(172, 'a.gomez@almadani-est.com', '2020-02-19 10:40:43'),
(173, 'raelynnbruce.gm.432714094@gcheck.xyz', '2020-02-21 03:26:12'),
(174, 'raelynnbruce.gm.432714093@gcheck.xyz', '2020-02-21 03:26:13'),
(175, 'protectiontime2030@gmail.com', '2020-02-23 12:30:35'),
(176, 'averihumphrey.sc.1142662362@supersendme.org', '2020-03-03 00:21:31'),
(177, 'averihumphrey.sc.1142662361@supersendme.org', '2020-03-03 00:21:31'),
(178, 'zeeshanakram70@gmail.com', '2020-03-08 09:54:16'),
(179, 'dkataria1@kent.co.in', '2020-03-13 10:45:37'),
(180, 'aliciadixon.gm.928289473@gcheck.xyz', '2020-03-14 09:50:52'),
(181, 'syed.kabeer87@gmail.com', '2020-03-15 06:54:38'),
(182, 'cheyannekey.sc.31809563@supersendme.org', '2020-03-30 02:57:36'),
(183, 'jaylenepennington.sc.31809562@supersendme.org', '2020-03-30 02:57:37'),
(184, 'eringreen.gm.44134899@gcheck.xyz', '2020-04-04 01:54:37'),
(185, 'jaliyahharris.gm.44134900@gcheck.xyz', '2020-04-04 01:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `order_track_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `status` enum('Received','Dispatched','Delivered','Cancelled By Customer','Cancelled By Admin') NOT NULL DEFAULT 'Received',
  `transaction_id` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `delivery_charges` varchar(255) DEFAULT NULL,
  `vat_total` varchar(255) NOT NULL,
  `total_items` int(11) NOT NULL,
  `otp` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_track_id`, `user_id`, `address_id`, `status`, `transaction_id`, `total_amount`, `delivery_charges`, `vat_total`, `total_items`, `otp`, `created_at`, `updated_at`) VALUES
(1, '30351', 15, 1, 'Dispatched', '', '', NULL, '', 0, '1234', '2020-04-21 08:44:42', '2020-06-09 08:32:40'),
(2, '98232', 15, 1, 'Delivered', '', '', NULL, '', 0, '', '2020-04-23 12:45:34', '2020-06-16 10:39:20'),
(3, '76563', 28, 3, 'Received', '', '', NULL, '', 0, '', '2020-06-17 11:13:22', '2020-06-17 11:13:22'),
(4, '04114', 28, 3, 'Received', '', '', NULL, '', 0, '', '2020-06-17 12:44:33', '2020-06-17 12:44:33'),
(5, '60045', 28, 3, 'Received', '491082', '3.96', NULL, '', 1, '', '2020-06-18 07:24:42', '2020-06-18 07:24:42'),
(6, '35956', 28, 4, 'Received', '491359', '229.68001', NULL, '', 7, '', '2020-06-18 12:02:19', '2020-06-18 12:02:19'),
(7, '15697', 28, 3, 'Received', '491458', '7.92', NULL, '', 3, '', '2020-06-18 13:52:25', '2020-06-18 13:52:25'),
(8, '72678', 28, 3, 'Received', '', '9.0', NULL, '', 4, '', '2020-06-19 06:51:35', '2020-06-19 06:51:35'),
(9, '33559', 31, 5, 'Received', '491772', '4.23', NULL, '', 2, '', '2020-06-19 07:38:12', '2020-06-19 07:38:12'),
(10, '141410', 28, 6, 'Delivered', '491949', '5.94', NULL, '', 3, '', '2020-06-19 12:15:07', '2020-07-30 12:08:09'),
(11, '036611', 31, 0, 'Received', '493380', '1.98', NULL, '', 1, '', '2020-06-23 06:09:08', '2020-06-23 06:09:08'),
(12, '803112', 31, 17, 'Delivered', '600667', '21.15', NULL, '', 10, '7334', '2020-07-13 12:24:24', '2020-07-13 12:46:00'),
(13, '568213', 31, 17, 'Dispatched', '600912', '2.25', NULL, '', 1, '8871', '2020-07-13 12:54:09', '2020-07-13 12:56:42'),
(14, '494514', 10, 17, 'Delivered', '601317', '7.92', NULL, '', 4, '', '2020-07-13 13:41:25', '2020-07-30 12:43:11'),
(15, '677215', 10, 18, 'Received', '608356', '1.98', NULL, '', 1, '', '2020-07-14 11:57:58', '2020-07-14 11:57:58'),
(16, '477616', 10, 18, 'Received', '608415', '1.98', NULL, '', 14, '', '2020-07-14 12:41:09', '2020-07-14 12:41:09'),
(17, '865517', 10, 18, 'Delivered', '609024', '13.78', '10.00', '1.80', 1, '', '2020-07-15 11:29:25', '2020-07-30 12:42:24'),
(18, '100918', 89, 19, 'Received', '617817', '111.46', '89.00', '14.54', 4, '3888', '2020-07-30 13:01:45', '2020-07-30 13:06:53'),
(19, '163319', 89, 19, 'Dispatched', '617819', '104.63', '89.00', '13.65', 1, '', '2020-07-30 13:08:23', '2020-07-30 13:11:54'),
(20, '786520', 31, 17, 'Received', '', '12.15', NULL, '0.1215', 2, '8016', '2020-08-04 10:10:40', '2020-08-04 10:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `order_item_it` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`order_item_it`, `order_id`, `product_id`, `quantity`, `price`) VALUES
(1, 1, 151, 4, 1.98),
(2, 2, 151, 6, 1.98),
(3, 3, 151, 1, 1.98),
(4, 4, 151, 4, 1.98),
(5, 4, 152, 11, 2.25),
(6, 5, 151, 1, 1.98),
(7, 6, 151, 7, 1.98),
(8, 7, 151, 3, 1.98),
(9, 8, 152, 4, 2.25),
(10, 9, 151, 1, 1.98),
(11, 9, 152, 1, 2.25),
(12, 10, 151, 3, 1.98),
(13, 11, 151, 1, 1.98),
(14, 12, 151, 5, 1.98),
(15, 12, 152, 5, 2.25),
(16, 13, 152, 1, 2.25),
(17, 14, 151, 4, 1.98),
(18, 15, 151, 1, 1.98),
(19, 16, 151, 1, 1.98),
(20, 17, 151, 1, 1.98),
(21, 18, 151, 4, 1.98),
(22, 19, 151, 1, 1.98),
(23, 20, 151, 5, 1.98),
(24, 20, 152, 1, 2.25);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL COMMENT 'for now it is only for download center page',
  `page_title_en` varchar(255) NOT NULL,
  `page_title_ar` varchar(255) NOT NULL,
  `page_description_en` varchar(5000) NOT NULL,
  `page_description_ar` varchar(5000) NOT NULL,
  `location` varchar(255) NOT NULL,
  `address1_en` varchar(1000) NOT NULL,
  `address2_en` varchar(1000) NOT NULL,
  `address3_en` varchar(1000) NOT NULL,
  `address1_ar` varchar(1000) NOT NULL,
  `address2_ar` varchar(1000) NOT NULL,
  `address3_ar` varchar(1000) NOT NULL,
  `address4_en` text NOT NULL,
  `address4_ar` text NOT NULL,
  `address5_en` text NOT NULL,
  `address5_ar` text NOT NULL,
  `address6_en` text NOT NULL,
  `address6_ar` text NOT NULL,
  `lat` varchar(225) NOT NULL,
  `lng` varchar(225) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `parent_id`, `page_title_en`, `page_title_ar`, `page_description_en`, `page_description_ar`, `location`, `address1_en`, `address2_en`, `address3_en`, `address1_ar`, `address2_ar`, `address3_ar`, `address4_en`, `address4_ar`, `address5_en`, `address5_ar`, `address6_en`, `address6_ar`, `lat`, `lng`, `image`, `created_at`, `updated_at`) VALUES
(1, 0, 'Company Profile', 'مقدمة', '<p><font face=\"Helvetica Neue\"><b style=\"\">ShopBox</b> is a demo solution created by Schöpfen, to demonstrate ecommerce structure.&nbsp;<span style=\"font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" text-align:=\"\" justify;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi diam nulla, porttitor et commodo sed, gravida sit amet enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed neque commodo, lobortis mi eu, pharetra lorem. Phasellus ut arcu lobortis magna tempus commodo. Cras at dui nisi. Mauris blandit quam metus, nec efficitur libero ultrices vel. Proin et lacus semper, dapibus sapien elementum, sagittis ex. Nulla a orci quis magna pretium commodo et sed leo. Sed ligula enim, venenatis non facilisis eu, molestie id lorem. Aenean vitae justo egestas augue egestas volutpat eu sit amet quam. Donec id ullamcorper est.</span></font></p>', ' تأسست شركة مناخ شبه الجزيرة للأنظمة الأمنية عام 1991م، والتي تمتلكها شركة العجلان القابضة. حيث توفّر الحلول المتكاملة للأنظمة الأمنية في جميع مدن المملكة العربية السعودية ودول مجلس التعاون الخليجي ، لخدمة جميع القطاعات الحكومية والتجارية. لدينا  سلسلة من الفروع والمكاتب في أغلب مدن المملكة الرئيسية، تعمل باستمرار على بناء قاعدة من الثقة والمصداقية لتحقيق المزيد من النجاح والتقدم.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/908304020820180227110707profile.png', '2018-02-28 00:00:00', '2020-04-18 23:58:31'),
(2, 0, 'Commitment', 'إلتزاماتنا', '<p>We strongly believe in providing our clients with innovative, outstanding quality products that are competitively priced and always available to the local security market. One of our most important aims is to do whatever it takes to satisfy every client by providing fast responds on pricing inquires and other technical requests about the related supports and services. We will continue introducing the most recent up- to-date technology and integration services that are always available to fulfil our client’s requirements.</p>', '<p>نلتزم لعملائنا بتقديم أحدث المنتجات ذات الجودة العالية والاسعارالتنافسية في سوق منتجات الأنظمة الأمنية. حيث أن من اهم أهدافنا هو إرضاء عملائنا من خلال تقديم الخدمات السريعة في الاستعلام عن المنتجات&nbsp;والأسعار و الاستفسارات الفنية مع أقصى درجات الدعم والخدمة،  وتوفير احدث  التقنيات المتاحة لتلبية جميع متطلبات عملائنا.</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/1335252165120180228082611shutterstock_728200222@1x.jpg', '2018-02-28 00:00:00', '2018-02-28 20:48:25'),
(3, 0, 'Mission', 'مهمة', '<p>Our mission is to continue as the leader in rapidly advanced security industry by providing most reliable products and services (quality products, strong management, and great services with an extensive historical experience in the business throughout the past years). It is our mission also to introduce a professional and long-term business relationship with the clients and to continue growing and having the capability of adapting to the changing environment with providing new solutions and concepts.</p>', '<p>أن نكون الرواد في توفير خدمات وحلول الأنظمة الأمنية ، وذلك بتقديم منتجات ذات جودة عالية،و خدمات متميزة تتحقق بفعل خبرتنا الواسعة  في هذا المجال لسنوات عديدة . و إنشاء علاقة مهنية بعيدة المدى مع عملائنا. والاستمرار في النمو وتحقيق القدرة على التكيف مع الظروف المتغيرة بتقديم مفاهيم وحلول جديدة.</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/2130435174220180228080938mission.jpg', '2018-02-28 00:00:00', '2018-03-14 09:10:46'),
(4, 0, 'Partners', 'شركاء', 'The Brands that we are representing&nbsp;', '<p>العلامات التجارية التي نمثلها&nbsp;</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/908304020820180227110707profile.png', '2018-02-28 00:00:00', '2018-10-11 06:21:07'),
(5, 0, 'Awards & Recognition', 'الجوائز والتقدير', '<p>MSJ P<span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\">resents</span><span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\"> </span><span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\">partner excellence </span><span style=\"font-weight: bold; color: rgb(106, 106, 106); font-family: arial, sans-serif; font-size: small;\">awards</span><span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\"> 2017</span></p>', '<p>نحن نسعى جاهدين لتوفير أفضل نوعية للعمل. نحن نعمة من الجوائز التالية.</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/2593469047120180227102102328552180124035935back-to-school.jpg', '2018-02-28 00:00:00', '2018-07-12 22:26:23'),
(6, 0, 'Our Team', 'فريقنا', '<p>Through empowerment of the employees, we are able to build a durable base of talented individuals forming the foundation of our future. Their determination and dedication continues to contribute to the overall harmony of the MSJ family. We are glad and proud of speaking highly about our team. The team is highly educated and well trained due to the historical experience of our team with the capability to manage and handle all kinds of projects.</p>', '<p>إننا نشعر بالسعادة والفخر ونحن نمتدح فريقنا، إذ يمتاز هذا الفريق بدرجات علمية عالية، كما أنه مدرب تدريبا جيداً من قبل شركائنا المنتجين<font face=\"Tahoma\">، وكذلك بفعل التجربة الزمنية بقدرته على التعامل مع جميع المشار</font>يع، كما لدينا المعرفة والكفاءة التقنية لحماية جميع ممتلكاتكم.والخبرةللقيام بترجمة ناجحة للمفاهيم الأمنية.</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/908304020820180227110707profile.png', '2018-02-28 00:00:00', '2018-08-07 12:50:37'),
(7, 0, 'Careers', 'وظائف', 'You can be one of our successful members by applying today, you will need just to send your CV, our HR will be more than happy to contact you if your qualification is matching our needs.', 'بإمكانك أن تكون أحد أعضاء فريق النجاح معنا كل ما عليك هو إرسال سيرتك الذاتية وسيكون فريق الموارد البشرية سعيد بالتواصل معك إذا كانت مؤهلاتك تتوافق مع متطلبات العمل معنا.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/908304020820180227110707profile.png', '2018-02-28 00:00:00', '2018-10-11 06:30:05'),
(8, 0, 'Become A Reseller', 'كن موزع', '<h4>Reseller Program:</h4>\r\n            <p>Qualified system integrators and reseller can apply for our dedicated partner\'s program membership with following details:</p>\r\n\r\n            <h5>Key Benefits:</h5>\r\n            <ul>\r\n            <li>Support in System design & identifying proper solutions</li>\r\n            <li>Exclusive pricing support for large scale projects</li>\r\n            <li>Dedicated technical support & online remote assistance</li>\r\n            <li>Free periodical training programs  on latest products</li>\r\n            <li>On-demand training program for project based solutions</li>\r\n            <li>Large stock for immediate delivery on order</li>\r\n            <li>Spare parts stock for quick after sales support</li>\r\n            </ul>\r\n\r\n            <h5>Who can apply?</h5>\r\n            <ul>\r\n            <li>Security system integrators & resellers</li>\r\n            <li>Low current systems</li>\r\n            <li>Computer and network firms</li>\r\n            <li>Electro mechanical contractors & Main contractors in the projects</li>\r\n\r\n            </ul>\r\n\r\n\r\n            <h5>Requirements for registration:</h5>\r\n\r\n            <ul>\r\n            <li>Duly updated company profile</li>\r\n            <li>Full contact information with postal address</li>\r\n            <li>A copy of valid Commercial registration </li>\r\n            </ul>', '<h4>كن موزع:</h4><p>بإمكان جميع الشركات والمؤسسات التي تعمل في مجال التقنية التقدم بطلب للحصول على عضوية برنامج الشريك المتفاني لدينا مع للتمتع بالمميزات التالية:</p><h5><b>الفوائد الرئيسية:</b></h5><ul><li>الدعم في تصميم الأنظمة الأمنية وتحديد الحلول المناسبة.</li><li>الحصول على أسعار حصرية للمشاريع.</li><li>دعم فني خاص والمساعدة عن بعد.</li><li>برامج تدريب دورية مجانية على أحدث المنتجات.</li><li>برنامج تدريبي حسب الطلب للحلول القائمة على المشاريع.</li><li>مخزون كبير من المواد للتسليم الفوري.</li><li>مخزون قطع الغيار للدعم السريع لخدمات الصيانة وبعد البيع.</li></ul><h5><b>من يمكنه التقدم بطلب التوزيع؟</b></h5><ul><li>شركات ومؤسسات الأنظمة المتكاملة.</li><li>شركات ومؤسسات حلول التيار الخفيف.</li><li>شركات ومؤسسات تقنية المعلومات.</li><li>مقاولين الكهروميكانيك.</li><li>المقاولين الرئيسيين في المشاريع.</li></ul><h5><b>متطلبات التسجيل:</b></h5><ul><li>خطاب طلب التعامل.</li><li>معلومات الاتصال الكاملة مع العنوان البريدي.</li><li>نسخة من السجل التجاري ساري المفعول.</li></ul>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/4568795490120180228125513dummy.pdf', '2018-02-28 00:00:00', '2018-10-11 05:52:38'),
(9, 0, 'Download Center', 'مركز التحميل', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/Profile.pdf', '2018-03-06 00:00:00', '2018-03-15 07:24:05'),
(10, 0, 'Our Suppliers', 'موردينا', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-06 00:00:00', '2020-04-18 21:42:13'),
(11, 0, 'Contact Us', 'Contact Us', '<p>MSJ is covering most of the major cities in Saudi Arabia by a well-organized branch with full facilities with qualified Sales and Technical teams.</p>', '<p>تغطي شركة MSJ جميع المدن الرئيسية في المملكة العربية السعودية ومملكة البحرين بسلسلة من الفروع المجهزة بجميع المرافق وفريق مؤهل ومدرب لتقديم الدعم الفني والتسويق.</p>', '6749 Khair Addin Az Zarkali, Al Ghadir, Riyadh 13311 3029, Saudi Arabia', '<p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\"><b>Head Office - Riyadh</b></p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Rabie District - Rabie Plaza</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">First Floor Office 1 & 2</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">P.O. Box 53492, Riyadh 11583</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Email: info@msj.com.sa</p>', '<p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\"><b>Riyadh Branch Office </b></p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Makkah Road - Olaya</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Ph.: 9200 00 675</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Fax: 011 462 3359</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Email: info@msj.com.sa</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\"><br></p>', '<p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\"><span style=\"font-weight: 600; font-family: \" hind=\"\" madurai\",=\"\" sans-serif;\"=\"\">Jeddah Branch Office </span></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">MadinaRoad,</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Ph.: 012 664 1799<br></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Fax: 011 462 3359</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Email: info@msj.com.sa</p>', '<p class=\"MsoNormal\" align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;\r\nmargin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;\r\nline-height:normal;mso-outline-level:5\"><br></p>', '<h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\"><br></h5><h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\">\r\n\r\n</h5>', '<h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\"><br></h5><h5 style=\"text-align: right; \">\r\n\r\n<p align=\"right\" style=\"margin: 0in 0in 0.0001pt;\"><font face=\"Arial\"></font></p></h5>', '<p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\"><span style=\"font-weight: 600; font-family: \" hind=\"\" madurai\",=\"\" sans-serif;\"=\"\">Dammam Branch Office </span></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">King Fahad Road,</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Ph.: 013 867 8085<br></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Fax: 011 462 3359</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Email: info@msj.com.sa</p>', '<h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\"><br></h5>', '<p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\"><span hind=\"\" madurai\",=\"\" sans-serif;\"=\"\" style=\"font-weight: 600;\">Abha Branch Office </span></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">King Fahad Road,</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Ph.: 9200 00 675</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Fax: 011 462 3359</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Email: info@msj.com.sa</p>', '<h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\"><br></h5>', '<p class=\"MsoNormal\"><font face=\"Hind Madurai, sans-serif\"><span style=\"font-size: 16px;\"><b> </b></span></font></p>', '<h5> </h5><p> </p>', '24.78962213606099', '46.648573858169584', '', '0000-00-00 00:00:00', '2019-09-04 12:19:58'),
(12, 9, 'Catelog', 'Catelog', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/catelog.pdf', '0000-00-00 00:00:00', '2018-03-15 07:25:00'),
(13, 9, 'Catelog', 'Catelog', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/catelog.pdf', '2018-03-15 00:00:00', '2018-03-15 07:25:00'),
(14, 0, 'Outstanding Projects ', 'Outstanding Projects ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-06 00:00:00', '2018-05-06 12:45:31'),
(15, 0, 'Events', 'Events', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-06 00:00:00', '2018-05-06 12:45:31'),
(16, 14, 'Speed gates & Turnstile gates', 'Speed gates & Turnstile gates', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-09 20:27:08'),
(17, 14, 'IP CCTV Systems ', 'IP CCTV Systems ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-09 20:29:29'),
(18, 15, 'Event 1', 'Event 1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-09 20:25:24'),
(19, 15, 'Event 2', 'Event 2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-05-27 03:19:49'),
(20, 14, 'Access Control Systems', 'Access Control Systems', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-10 08:56:42'),
(21, 0, 'Products', 'منتجات', 'Products', 'Products', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2020-04-20 07:06:15');

-- --------------------------------------------------------

--
-- Table structure for table `page_images`
--

CREATE TABLE `page_images` (
  `page_image_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_images`
--

INSERT INTO `page_images` (`page_image_id`, `page_id`, `image`) VALUES
(53, 18, 'uploads/page_images/6730090529720180527031219download.png'),
(54, 18, 'uploads/page_images/8286718198920180527031219image.png'),
(55, 19, 'uploads/page_images/8079790129420180527034919Pizza_01.png'),
(56, 19, 'uploads/page_images/6214825097820180527034919Screenshot_1.png'),
(59, 16, 'uploads/page_images/8891921899220180609080827IMG_20170807_103020.jpg'),
(60, 17, 'uploads/page_images/77199759844201806090829297.jpg'),
(61, 20, 'uploads/page_images/88363901385201806100842562018-06-05_(1).png'),
(62, 20, 'uploads/page_images/10798761413201806100842562018-06-05_(2).png'),
(63, 20, 'uploads/page_images/91647386920201806100842562018-06-05_(3).png'),
(64, 5, 'uploads/page_images/95588201958201807120649571.jpg'),
(65, 5, 'uploads/page_images/70399147670201807120744132.jpg'),
(66, 5, 'uploads/page_images/75464876260201807120753163.jpg'),
(67, 5, 'uploads/page_images/42831793602201807120752244.jpg'),
(68, 5, 'uploads/page_images/21142714159201807120739255.jpg'),
(69, 5, 'uploads/page_images/47957980062201807120739266.jpg'),
(70, 5, 'uploads/page_images/81788542592201807120745297.jpg'),
(71, 5, 'uploads/page_images/33005961955201807120706347.jpg'),
(72, 5, 'uploads/page_images/18059888759201807120703378.jpg'),
(73, 5, 'uploads/page_images/611903711372018071210232610.jpg'),
(74, 5, 'uploads/page_images/153227556412018071210232611.jpg'),
(75, 5, 'uploads/page_images/56300985452018071210232612.jpg'),
(76, 5, 'uploads/page_images/808667895882018071210232613.jpg'),
(77, 5, 'uploads/page_images/686300565652018071210232614.jpg'),
(78, 5, 'uploads/page_images/746746403802018071210232615.jpg'),
(79, 5, 'uploads/page_images/457853085082018071210232616.jpg'),
(80, 5, 'uploads/page_images/301605399212018071210232617.jpg'),
(81, 5, 'uploads/page_images/815022403702018071210232619.jpg'),
(82, 5, 'uploads/page_images/2754037457420180712102326122.jpg'),
(153, 4, 'uploads/page_images/41409135833201807300835151.jpg'),
(154, 4, 'uploads/page_images/49552874189201807300835152.jpg'),
(155, 4, 'uploads/page_images/27189230795201807300835153.jpg'),
(156, 4, 'uploads/page_images/61338403682201807300835154.jpg'),
(157, 4, 'uploads/page_images/61298952070201807300818165.jpg'),
(158, 4, 'uploads/page_images/60147428259201807300818166.jpg'),
(159, 4, 'uploads/page_images/53039309002201807300818167.jpg'),
(160, 4, 'uploads/page_images/58452315824201807300818168.jpg'),
(161, 4, 'uploads/page_images/518716426722018073008141700.jpg'),
(162, 4, 'uploads/page_images/54428881213201807300814179.jpg'),
(163, 4, 'uploads/page_images/202148430192018073008141710.jpg'),
(164, 4, 'uploads/page_images/641237033542018073008141711.jpg'),
(165, 4, 'uploads/page_images/895673215572018073008481712.jpg'),
(166, 4, 'uploads/page_images/705432484042018073008481713.jpg'),
(167, 4, 'uploads/page_images/905518663242018073008481714.jpg'),
(168, 4, 'uploads/page_images/990104720982018073008481715.jpg'),
(169, 4, 'uploads/page_images/562589879112018073008121816.jpg'),
(170, 4, 'uploads/page_images/673332707092018073008121817.jpg'),
(171, 4, 'uploads/page_images/983007348442018073008121818.jpg'),
(172, 4, 'uploads/page_images/161522980792018073008121819.jpg'),
(173, 10, 'uploads/page_images/4453173913520190130075407sdd.png'),
(174, 10, 'uploads/page_images/4446440899020200418091342Untitled-1_0000_download-1.jpg'),
(175, 10, 'uploads/page_images/6275549945320200418091342Untitled-1_0001_untitled-1_182.jpg'),
(176, 10, 'uploads/page_images/2806930236720200418091342Untitled-1_0002_Nesto-Hypermarket-Dubai.jpg'),
(177, 10, 'uploads/page_images/5979081538220200418091342Untitled-1_0003_images.jpg'),
(178, 10, 'uploads/page_images/2597091179120200418091342Untitled-1_0004_HyperPanda-logo-1150x1150.jpg'),
(179, 10, 'uploads/page_images/2147615736420200418091342Untitled-1_0005_download.jpg'),
(180, 10, 'uploads/page_images/8737318120420200418091342Untitled-1_0006_download-3.jpg'),
(181, 10, 'uploads/page_images/7673360025320200418091342Untitled-1_0007_download-2.jpg'),
(182, 10, 'uploads/page_images/2653861519620200418091342Untitled-1_0008_Background.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `point_of_sales`
--

CREATE TABLE `point_of_sales` (
  `point_of_sale_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `transaction_user` varchar(255) NOT NULL,
  `type` enum('Debit','Credit') NOT NULL,
  `description` text NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `invoice` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `point_of_sales`
--

INSERT INTO `point_of_sales` (`point_of_sale_id`, `order_id`, `transaction_user`, `type`, `description`, `total_amount`, `invoice`, `created_at`) VALUES
(1, 0, 'Sarfraz', 'Credit', 'Point of Sale', '2000', 'uploads/pos/4205544435020200804084125logo.png', '2020-08-04 00:00:00'),
(3, 0, 'Sarfraz 2', 'Debit', 'Debit Entry', '1200', 'uploads/pos/4895317776320200804082737logo.png', '2020-08-04 08:37:27'),
(4, 0, 'Sarfraz', 'Credit', 'sdfas', '2000', 'uploads/pos/3430222780720200804090910logo.png', '2020-08-04 09:10:09'),
(5, 20, 'Basit Chughtai', 'Credit', 'Online Purchase', '12.15', '', '2020-08-04 10:10:00');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_title_en` varchar(255) NOT NULL,
  `product_title_ar` varchar(255) NOT NULL,
  `product_description_en` varchar(2000) NOT NULL,
  `product_description_ar` varchar(2000) NOT NULL,
  `product_colors` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `model_en` varchar(255) NOT NULL,
  `model_ar` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_featured` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_title_en`, `product_title_ar`, `product_description_en`, `product_description_ar`, `product_colors`, `brand_id`, `category_id`, `sub_category_id`, `model_en`, `model_ar`, `price`, `is_active`, `is_featured`, `created_at`, `updated_at`) VALUES
(151, 'Pluma Laptop Sleeve', 'فيمتو شراب 250 مل', 'Protect your 16\" MacBook Pro with USB-C port with this custom fit laptop sleeve. Made from Neoprene and quad-stitched for durability. ', 'مشروب فيمتو بنكهة الفاكهة', '', 10, 17, 69, 'MacBook Pro 16 - Herringbone Gray', '250 مل', '1.98', 1, 1, '2020-08-05 22:00:53', '2020-08-05 22:00:53'),
(152, 'Protective Carrying Case with Viscotex Memory Foam', 'حقيبة حمل واقية مع رغوة الذاكرة فيسكوتكس', 'Codex provides stylish, wraparound protection for your MacBook Pro and cradles it with Moshi\'s proprietary Viscotex™ memory foam. Compatible with both MacBook Pro 16 and MacBook Pro 15 models.', 'يوفر Codex حماية ملفوفة وأنيقة لجهاز MacBook Pro الخاص بك ويضعه مع رغوة الذاكرة Viscotex ™ الخاصة بشركة Moshi. متوافق مع طرازي MacBook Pro 16 و MacBook Pro 15.', '', 24, 17, 70, 'MacBook Pro 15/16-inch - Onyx Black', 'MacBook Pro 15/16-inch - Onyx Black', '2.25', 1, 1, '2020-08-05 22:08:01', '2020-08-05 22:08:01');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_image_id`, `product_id`, `product_image`) VALUES
(189, 151, 'uploads/product_images/1098197703920200805092757sleeve_0002_Layer_1.jpg'),
(190, 151, 'uploads/product_images/3327080380520200805092258sleeve_0000_Layer_3.jpg'),
(191, 151, 'uploads/product_images/8626265304620200805094458sleeve_0001_Layer_2.jpg'),
(193, 152, 'uploads/product_images/286468046620200805104107targus_0002_Layer_4.jpg'),
(194, 152, 'uploads/product_images/8105154366620200805100108targus_0000_Layer_6.jpg'),
(195, 152, 'uploads/product_images/663036403620200805100108targus_0001_Layer_5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_specifications`
--

CREATE TABLE `product_specifications` (
  `product_specification_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `specification_title_en` varchar(255) NOT NULL,
  `specification_title_ar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_specifications`
--

INSERT INTO `product_specifications` (`product_specification_id`, `product_id`, `specification_title_en`, `specification_title_ar`) VALUES
(9, 0, 'Specification 2 eng', 'Specification 2 eng'),
(10, 0, 'specification 1 eng', 'Specification 1 eng again'),
(11, 0, 'Specification 2 eng', 'Specification 2 eng'),
(12, 0, 'specification 1 eng', 'Specification 1 eng again');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_title`) VALUES
(1, 'Admin'),
(2, 'Support'),
(3, 'Client'),
(4, 'Warehouse'),
(5, 'Delivery'),
(6, 'Career Support');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `ios_app_version` decimal(10,1) NOT NULL,
  `ios_team_app_version` decimal(10,1) NOT NULL,
  `android_app_version` int(11) NOT NULL,
  `android_team_app_version` int(11) NOT NULL,
  `vat` int(11) DEFAULT NULL,
  `login_screen_logo` varchar(255) NOT NULL,
  `splash_screen_logo` varchar(255) NOT NULL,
  `header_logo` varchar(255) NOT NULL,
  `header_gradian_color` varchar(255) NOT NULL,
  `footer_gradian_color` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `ios_app_version`, `ios_team_app_version`, `android_app_version`, `android_team_app_version`, `vat`, `login_screen_logo`, `splash_screen_logo`, `header_logo`, `header_gradian_color`, `footer_gradian_color`) VALUES
(1, 1.0, 1.0, 1, 1, 15, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `support_tickets`
--

CREATE TABLE `support_tickets` (
  `id` int(11) NOT NULL,
  `ticket_id` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('open','closed') NOT NULL DEFAULT 'open',
  `ticket_type` varchar(120) NOT NULL,
  `full_name` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `city` varchar(120) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `type_id` varchar(120) NOT NULL COMMENT 'complaint table primary key',
  `product` varchar(500) NOT NULL,
  `apartment_no` varchar(120) NOT NULL,
  `visit_time` varchar(120) NOT NULL,
  `location` varchar(400) NOT NULL,
  `message` text NOT NULL,
  `lat` varchar(120) NOT NULL,
  `lng` varchar(120) NOT NULL,
  `timestamp` varchar(120) NOT NULL COMMENT 'created at timestamp',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closed_request` int(11) NOT NULL COMMENT '0 for nothing 1 for admin want to close 2 close 3 user request to re open',
  `ticket_taken_by` int(11) NOT NULL COMMENT 'This is the user id of the support user who has taken the ticket and has made 1st reply. now only this user will be able to see this ticket and admin',
  `last_closed_by` int(11) NOT NULL COMMENT 'This is the user ID who has closed this ticket lastly'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `support_tickets`
--

INSERT INTO `support_tickets` (`id`, `ticket_id`, `user_id`, `status`, `ticket_type`, `full_name`, `email`, `city`, `phone`, `type_id`, `product`, `apartment_no`, `visit_time`, `location`, `message`, `lat`, `lng`, `timestamp`, `created_at`, `closed_request`, `ticket_taken_by`, `last_closed_by`) VALUES
(1, '1596107433', 10, 'open', '1', 'Rizwan', 'r@r.com', 'Jeddah', '+96611111111', '5', '', '', '', 'House nhbash', 'high', '', '', '1596107433', '2020-07-30 08:10:33', 0, 0, 0),
(2, '1596108975', 89, 'open', '1', 'Muhammad Salman', 'm.salman@zynq.net', 'Jeddah', '966512312312', '5', '', '', '', 'Allende, Centro-Área 1, Mexico City, 06010, CDMX, Mexico', 'Jnjknjknjkn', '19.4354778', '-99.1364789', '1596108975', '2020-07-30 08:36:15', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `support_users`
--

CREATE TABLE `support_users` (
  `id` int(11) NOT NULL,
  `complaint_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_orders`
--

CREATE TABLE `temp_orders` (
  `temp_order_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL COMMENT 'this is actually aa cookie value not user id logic change',
  `product_id` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_orders`
--

INSERT INTO `temp_orders` (`temp_order_id`, `user_id`, `product_id`, `product_quantity`) VALUES
(1, '1586240820', 14, 3),
(2, '1586348587', 14, 2),
(3, '1586862714', 145, 112),
(4, '1586949431', 42, 22),
(5, '609011807', 151, 6),
(6, '1587382835', 151, 5),
(7, '202', 151, 1),
(8, '609066756', 151, 1),
(9, '609085272', 151, 2),
(10, '1587466323', 151, 3),
(11, '1587476161', 151, 2),
(13, '7', 151, 1),
(14, '613562075', 151, 2),
(16, '613576519', 151, 1),
(17, '613655572', 152, 1),
(18, '613982112', 151, 4),
(23, '613574760', 151, 109),
(29, '614181078', 151, 1),
(32, '28', 151, 2),
(33, '1592825258', 151, 1),
(34, '1592825269', 151, 4),
(38, '614630338', 151, 3),
(39, '', 151, 2),
(40, '615116022', 151, 3),
(41, '615202595', 151, 4),
(42, '', 152, 1),
(43, '615208417', 151, 1),
(44, '81', 152, 1),
(45, '615208719', 152, 1),
(46, '615209287', 151, 1),
(47, '615209408', 151, 1),
(48, '615291647', 151, 1),
(55, '10', 151, 2),
(56, '617026399', 151, 11),
(59, '89', 151, 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `position_en` varchar(255) NOT NULL,
  `position_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) NOT NULL,
  `description_en` varchar(255) NOT NULL,
  `description_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_comments`
--

CREATE TABLE `ticket_comments` (
  `comment_id` int(11) NOT NULL,
  `ticket_id` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` varchar(120) NOT NULL COMMENT 'created at timestamp',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ticket_comments`
--

INSERT INTO `ticket_comments` (`comment_id`, `ticket_id`, `user_id`, `message`, `is_read`, `timestamp`, `created_at`) VALUES
(1, '1596107433', 10, 'high', 0, '1596107433', '2020-07-30 08:10:33'),
(2, '1596107433', 10, 'He’d', 0, '1596107469', '2020-07-30 08:11:09'),
(3, '1596107433', 10, 'Nddddddd', 0, '1596108200', '2020-07-30 08:23:20'),
(4, '1596108975', 89, 'Jnjknjknjkn', 0, '1596108975', '2020-07-30 08:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL COMMENT 'it is warehouse user id for delivery users',
  `full_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '3',
  `phone` varchar(255) NOT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `city_id` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `is_verified` tinyint(4) NOT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '1',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `address` varchar(500) DEFAULT NULL,
  `latitude` varchar(25) DEFAULT NULL,
  `longitude` varchar(25) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `iqama_number` varchar(255) DEFAULT NULL,
  `iqama_expiry` varchar(255) DEFAULT NULL,
  `driving_license_no` varchar(255) DEFAULT NULL,
  `driving_license_expiry` varchar(255) DEFAULT NULL,
  `vehicle_make` varchar(255) DEFAULT NULL,
  `vehicle_model` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `emergency_contact` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `driving_license_img` varchar(255) NOT NULL,
  `registration_license_img` varchar(255) NOT NULL,
  `iban` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `chat_request_id` int(11) NOT NULL,
  `receive_career_email_at` varchar(255) NOT NULL DEFAULT 'NULL' COMMENT 'This field is being used only for career support user type with role id 6 to specify on which email address to send career request emails'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `branch_id`, `full_name`, `username`, `email`, `role_id`, `phone`, `gender`, `city_id`, `device_type`, `device_token`, `country`, `password`, `verification_code`, `is_verified`, `is_approved`, `status`, `address`, `latitude`, `longitude`, `dob`, `iqama_number`, `iqama_expiry`, `driving_license_no`, `driving_license_expiry`, `vehicle_make`, `vehicle_model`, `registration_no`, `emergency_contact`, `profile_pic`, `driving_license_img`, `registration_license_img`, `iban`, `bank_name`, `created_at`, `updated_at`, `chat_request_id`, `receive_career_email_at`) VALUES
(1, NULL, 'Admin', 'Admin', 'backend@schopfen.com', 1, '', 'male', '', 'ios', '', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 0, 'active', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2018-02-22 00:00:00', '2020-04-14 05:15:59', 0, ''),
(9, NULL, 'Sarfraz Riaz', 'driver', 'sarfraz@driver_user.com', 5, '+923008094941', 'male', 'al-Bah̨ah', '', '', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 0, 'active', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-09 08:34:12', '2020-04-14 05:15:59', 0, 'NULL'),
(10, 12, 'Rizwan', '', 'r@r.com', 5, '+96611111111', 'male', '1', 'ios', '', '', 'ce6442a5cce4fdf5186fab469a2eb042', '', 1, 1, 'active', 'House# 231-k', NULL, NULL, '820350000', '1111', '1623351600', '1111', '1623351600', 'Suzuki', '1111', '1111', '+96622222221', 'uploads/images/48317920200706075432profile_pic.jpg', 'uploads/images/4960220200620062630IMG_20200620_112933.jpg', 'uploads/images/29047920200620062630IMG_20200620_113061.jpg', '11111111111111', 'HBL', '2020-06-10 09:53:11', '2020-06-10 09:53:11', -1, 'NULL'),
(11, NULL, 'Rizwan A', '', 'ri@ri.com', 5, '+9661111111', 'male', 'Lahore', 'Android', '', '', 'ce6442a5cce4fdf5186fab469a2eb042', '7213', 0, 0, 'active', 'House# 231 K', NULL, NULL, '0000-00-00', '121212', '1686423600', '213123', '1592420400', 'Suzuki', '2019', '1231313', '+9661111111', '1', '1', '1', '22222222222222', 'HBL', '2020-06-10 12:42:42', '2020-06-10 09:53:11', -1, 'NULL'),
(12, NULL, 'Store', 'warehouse', 'warehouse@gmail.com', 4, '+923008094941', 'male', '1', '', '', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 1, 'active', 'Lahore, Pakistan', '31.52036960000001', '74.35874729999999', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-11 06:55:35', '2020-06-10 09:53:11', 0, 'NULL'),
(13, NULL, 'Delivery User', 'delivery', 'delivery@gmail.com', 5, '+923008094941', 'male', '1', '', '', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 2, 'active', 'Lahore', NULL, NULL, '2020-06-11', '2312321321312', '2020-07-03', '21321312', '2020-06-18', 'Suzuki', '2013', 'BAS 396', '923001234567', 'uploads/images/32125219225202006110609579887800953920200424102229user_image.jpg', 'uploads/images/16448085122202006110609572020-03-09.jpg', 'uploads/images/44726816876202006110609572020-03-09.jpg', '324323214124', 'Meezan', '2020-06-11 06:57:09', '2020-06-10 09:53:11', 0, 'NULL'),
(14, NULL, 'Sarfraz', 'test', 'sarfraz@deliveryuser.com', 5, '', NULL, '1', '', '', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 0, 'active', 'test', NULL, NULL, '332432143214', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-11 07:21:47', '0000-00-00 00:00:00', -1, 'NULL'),
(16, NULL, 'fdsafd', 'erwerde', 'sarfraz@userd2.com', 5, '', NULL, '1', '', '', '', '25d55ad283aa400af464c76d713c07ad', '2415', 0, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'uploads/images/9527201331920200611075529Screenshot_2020-05-04_at_11.50.17_PM.png', '', '', NULL, NULL, '2020-06-11 07:29:55', '0000-00-00 00:00:00', -1, 'NULL'),
(17, NULL, 'fdsafd', 'erwesrde', 'sarfraz@suserd2.com', 5, '', NULL, '1', '', '', '', '25d55ad283aa400af464c76d713c07ad', '6644', 0, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'uploads/images/8430631573720200611071731Screenshot_2020-05-04_at_11.50.17_PM.png', '', '', NULL, NULL, '2020-06-11 07:31:17', '0000-00-00 00:00:00', -1, 'NULL'),
(18, NULL, 'fdsafd', 'erwesrdes', 'sarfraz@susserd2.com', 5, '', NULL, '1', '', '', '', '25d55ad283aa400af464c76d713c07ad', '0802', 0, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '', '', NULL, NULL, '2020-06-11 07:31:32', '0000-00-00 00:00:00', -1, 'NULL'),
(19, NULL, 'fdsafd', 'erwesrsdes', 'sarfraz@sussserd2.com', 5, '', NULL, '1', '', '', '', '25d55ad283aa400af464c76d713c07ad', '5668', 0, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '69524120200611074034Screenshot 2020-05-04 at 11.50.17 PM.png', '', '', NULL, NULL, '2020-06-11 07:34:40', '0000-00-00 00:00:00', -1, 'NULL'),
(20, NULL, 'fdsafd', 'erwesrsdesd', 'sarfraz@sussserdd2.com', 5, '', NULL, '1', '', '', '', '25d55ad283aa400af464c76d713c07ad', '8619', 0, 2, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'uploads/images/57850620200611073935Screenshot 2020-05-04 at 11.50.17 PM.png', '', '', NULL, NULL, '2020-06-11 07:35:39', '0000-00-00 00:00:00', -1, 'NULL'),
(21, NULL, 'Rizwan', '', 'a@a.com', 5, '+96621212121', 'male', '1', 'Android', '', '', 'ce6442a5cce4fdf5186fab469a2eb042', '', 1, 2, 'active', 'House# 231K', NULL, NULL, '788814000', '222222', '1654974000', '222222', '1686510000', 'Suzuki', '2020', '333333', '+9662121123', 'uploads/images/39233820200611081129IMG_20200610_145299.jpg', 'uploads/images/69042520200611081129IMG_20200610_143034.jpg', 'uploads/images/89189820200611081129IMG_20200610_142726.jpg', '11111111111111', 'HBL', '2020-06-11 08:29:11', '0000-00-00 00:00:00', -1, 'NULL'),
(22, NULL, 'Rizwan', '', 'aa@a.com', 5, '+96612121', 'male', '1', 'Android', '', '', 'ce6442a5cce4fdf5186fab469a2eb042', '7806', 0, 0, 'active', 'House# 231k', NULL, NULL, '1041274800', '2123123', '1686510000', '2313', '1686510000', 'Suzuki', '2020', '23123123', '+9662312313', 'uploads/images/84783220200611092253IMG_20200610_145299.jpg', 'uploads/images/43630720200611092253IMG_20200610_143034.jpg', 'uploads/images/77614920200611092253IMG_20200610_142726.jpg', '22222222222222', 'HBL', '2020-06-11 09:53:22', '0000-00-00 00:00:00', -1, 'NULL'),
(23, NULL, 'Rizwan', '', 'aa@aa.com', 5, '+9662313123', 'male', '1', 'Android', '', '', 'ce6442a5cce4fdf5186fab469a2eb042', '0535', 0, 0, 'active', 'House#231k', NULL, NULL, '851972400', '21312313123', '1686510000', '4234234234', '1718132400', 'Suzuki', '2020', '2313123123', '+966234234234', 'uploads/images/31374020200611093658IMG_20200611_145547.jpg', 'uploads/images/32639520200611093658IMG_20200610_144371.jpg', 'uploads/images/30344620200611093658IMG_20200610_144371.jpg', '556565656565', 'HBL', '2020-06-11 09:58:36', '0000-00-00 00:00:00', -1, 'NULL'),
(24, NULL, 'Rizwan', '', 'a@aa.com', 5, '+9662112312', 'male', '1', 'Android', '', '', 'ce6442a5cce4fdf5186fab469a2eb042', '0601', 0, 0, 'active', 'House#231k', NULL, NULL, '851972400', '12312313123', '1749668400', '23123123123', '1686510000', 'Toyota', '2020', '34324234', '+96623123123', 'uploads/images/6508420200611100304IMG_20200611_145547.jpg', 'uploads/images/41184220200611100304IMG_20200610_144371.jpg', 'uploads/images/73484020200611100304IMG_20200610_144371.jpg', '33333333333333', 'HBL', '2020-06-11 10:04:03', '0000-00-00 00:00:00', -1, 'NULL'),
(25, NULL, 'Rizwan', '', 'a@b.com', 5, '+966342342424', 'male', '1', 'Android', '', '', 'ce6442a5cce4fdf5186fab469a2eb042', '', 1, 0, 'active', 'House#231K', NULL, NULL, '851972400', '2342342', '1686510000', '323423424', '1654974000', 'Toyota', '2019', '2312334', '+9662323424', 'uploads/images/68044820200611111632IMG_20200611_145547.jpg', 'uploads/images/16830620200611111632IMG_20200610_144371.jpg', 'uploads/images/18926320200611111632IMG_20200610_144371.jpg', '14234234232355245', 'HBL', '2020-06-11 11:32:16', '0000-00-00 00:00:00', -1, 'NULL'),
(28, NULL, 'salman a wast', 'salman1', 'salman@salman.com', 3, '966512345678', 'male', '2', 'ios', 'fFGlG7z20YY:APA91bF0NV8UEr04VuncsV0ecSNHroPxTfriN9iUdBRcOPjj002ETRlE_0i9RDDcU146kWBfvGWPL_aVRmE-aOd38FEtAY9hVNSitnDZbqYlv9xNQiRHYGC2JYhSDgG8trUhKN-fvFje', '', 'e10adc3949ba59abbe56e057f20f883e', '', 1, 1, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-17 07:40:19', '0000-00-00 00:00:00', -1, 'NULL'),
(31, NULL, 'Basit Chughtai', 'basitc', 'me@zynq.net', 3, '966581057444', 'male', '1', 'android', 'd-Do3osDLrU:APA91bFam3elM1OL5QRF0d4WDEB8QzB_79zbxhhCP7EhaMKXv40ixyE9KK4aXM4PhjWuqQ2rvCVJYoUdpxMXKaiwevkW5xRDX3-2ZxInQbCnp9QtjwfdhUMZeOohk_dS_1r4KgceVyAX', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 1, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-18 12:08:16', '0000-00-00 00:00:00', -1, 'NULL'),
(32, NULL, 'Rizwan', '', 'rizwan@rizwaan.com', 5, '+9662555447', 'male', '1', 'Android', '', '', 'ce6442a5cce4fdf5186fab469a2eb042', '', 1, 0, 'active', 'House#231 K', NULL, NULL, '1041274800', '1234', '1719082800', '1234', '1719082800', 'Honda', '2009', '23456', '+9664575427', NULL, 'uploads/images/62924020200622085620IMG_20200620_112933.jpg', 'uploads/images/58079620200622085620IMG_20200620_112933.jpg', '2737374747474', 'HBL', '2020-06-22 08:20:56', '0000-00-00 00:00:00', -1, 'NULL'),
(33, NULL, '+966551238888766', 'salman232', 'dummy@gmail.com', 5, '923323911622', 'male', '1', 'IOS', 'JNJKANCJKANJC', '', '25d55ad283aa400af464c76d713c07ad', '9132', 0, 0, 'active', 'test', NULL, NULL, 'JNJKANCJKANJC', 'test', 'JNJKANCJKANJC', 'JNJKANCJKANJC', 'JNJKANCJKANJC', 'GLI Corolla', '201822', 'JNJKANCJKANJC', 'JNJKANCJKANJC', NULL, '', '', 'JNJKANCJKANJC', 'Habib Bank', '2020-06-22 08:21:09', '0000-00-00 00:00:00', -1, 'NULL'),
(34, NULL, 'Sikandar', 'sikandar123', 's@gmail.com', 5, '+96786768876', 'male', '1', 'iOS', '', '', '22f77f7cc412197eafbe2e281d7b32f3', '8999', 0, 0, 'active', 'Sadiqabad', NULL, NULL, '1024423200', '123', '1655891064', '6t6', '1655891053', 'huh', 'gh657', 'bnhg65', 'jnhbgvf', 'uploads/images/61063920200622091245profile_pic.jpg', 'uploads/images/95438720200622091245driving_license_img.jpg', 'uploads/images/45043920200622091245registration_license_img.jpg', '78', 'HBL', '2020-06-22 09:45:12', '0000-00-00 00:00:00', -1, 'NULL'),
(35, NULL, 's1', 'nun', 'dkk@mmm.com', 5, '+96786876786', 'male', '1', 'iOS', '', '', 'a1614dbb30fbe88da2a2e0739fe63c5e', '9471', 0, 0, 'active', 'seed', NULL, NULL, '961614000', 'seed', '1655897188', 'xde3', '1592997997', 'did', 'd', 'd', '+968767867', 'uploads/images/45330120200622114427profile_pic.jpg', 'uploads/images/79187420200622114427driving_license_img.jpg', 'uploads/images/9065820200622114427registration_license_img.jpg', 'd', 'dose', '2020-06-22 11:27:44', '0000-00-00 00:00:00', -1, 'NULL'),
(36, NULL, 'did', 'hi hug', 's@jjj.com', 5, '+968768768768', 'male', '1', 'iOS', '', '', 'ea59b4a55f39a54ef45d4c2a5aa1d69f', '9830', 0, 0, 'active', 'ddd', NULL, NULL, '961614000', 'ddd', '1624365752', 'djhkgjkhgkj342', '1655901758', 'jhghjg7687', 'Hutus', 'hg767', '+9687687868', 'uploads/images/94165520200622122843profile_pic.jpg', 'uploads/images/90307920200622122843driving_license_img.jpg', 'uploads/images/44356420200622122843registration_license_img.jpg', 'ugh', 'HBL', '2020-06-22 12:43:28', '0000-00-00 00:00:00', -1, 'NULL'),
(37, NULL, 'jjkhkj', 'ddd', 'hhh@hh.com', 5, '+968676868', 'male', '1', 'iOS', '', '', '81e803082ec41a823629a848b986d566', '3743', 0, 0, 'active', 'jhghjgg', NULL, NULL, '929991600', 'hghjg3244', '1655903753', 'zzzs', '1687439756', 'cd', 'c', 'did', '+9689686868', 'uploads/images/67039220200622014316profile_pic.jpg', 'uploads/images/28332420200622014316driving_license_img.jpg', 'uploads/images/59151220200622014316registration_license_img.jpg', 'ss33', 'HBL', '2020-06-22 13:16:43', '0000-00-00 00:00:00', -1, 'NULL'),
(38, NULL, 'HH', 'hghg@hgjh.com', 's@gg.com', 5, '+968767868687', 'male', '1', 'iOS', '', '', '773795665acb7d22e0f173fb4b635acf', '0829', 0, 0, 'active', 'SadiqAbad', NULL, NULL, '993150000', 'dde33', '1624368495', 'dde33', '1655904501', 'see', '3d3', 'de3', '+96898697897', 'uploads/images/28341520200622010429profile_pic.jpg', 'uploads/images/81840420200622010429driving_license_img.jpg', 'uploads/images/60008720200622010429registration_license_img.jpg', 'sss', 'saws', '2020-06-22 13:29:04', '0000-00-00 00:00:00', -1, 'NULL'),
(39, NULL, 'ABC', 'bh88', 'gg@kk.com', 5, '+96876876785', 'male', '1', 'iOS', '', '', 'd4af37ef4df4254a357f8ddbc1d3aa2c', '4783', 0, 0, 'active', 'Lahore', NULL, NULL, '961700400', '878', '1529744915', '897', '1561280921', 'nhik87', 'nh77', 'nh87', '+969868969', 'uploads/images/58580520200623094709profile_pic.jpg', 'uploads/images/80083520200623094709driving_license_img.jpg', 'uploads/images/46921920200623094709registration_license_img.jpg', 'kj', 'HBL', '2020-06-23 09:09:47', '0000-00-00 00:00:00', -1, 'NULL'),
(40, NULL, 'Sikandar', '', 'sikg@gg.com', 5, '+9668568588', 'male', '1', 'Android', '', '', '1de5c43a900abeaad3f1a20978b3339d', '', 1, 0, 'active', 'lahore', NULL, NULL, '1039633200', 'gh56', '1592938800', 'chcf', '1592938800', 'ghc', 'ghc', 'cvg6', '+9666858888', 'uploads/images/83699020200623113534Screenshot_2020-06-23-15-33-59-80_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/87375920200623113534Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/68956020200623113534Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'ghg', 'hbl', '2020-06-23 11:34:35', '0000-00-00 00:00:00', -1, 'NULL'),
(41, NULL, 'Sikandar', '', 'sikhu@gg.com', 5, '+9668568588', 'male', '1', 'Android', '', '', '1de5c43a900abeaad3f1a20978b3339d', '5096', 0, 0, 'active', 'lahore', NULL, NULL, '1039633200', 'gh56', '1592938800', 'chcf', '1592938800', 'ghc', 'ghc', 'cvg6', '+9666858888', 'uploads/images/74179120200623110956Screenshot_2020-06-23-15-33-59-80_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/92086220200623110956Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/20142120200623110956Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'ghg', 'hbl', '2020-06-23 11:56:09', '0000-00-00 00:00:00', -1, 'NULL'),
(42, NULL, 'Sikandar', '', 'sikhtgu@gg.com', 5, '+9668568588', 'male', '1', 'Android', '', '', '1de5c43a900abeaad3f1a20978b3339d', '9388', 0, 0, 'active', 'lahore', NULL, NULL, '1039633200', 'gh56', '1592938800', 'chcf', '1592938800', 'ghc', 'ghc', 'cvg6', '+9666858888', 'uploads/images/40263820200623110559Screenshot_2020-06-23-15-33-59-80_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/87135620200623110559Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/40201520200623110559Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'ghg', 'hbl', '2020-06-23 11:59:05', '0000-00-00 00:00:00', -1, 'NULL'),
(43, NULL, 'Sikandar', '', 'sikhy@gg.com', 5, '+9668568588', 'male', '1', 'Android', '', '', '1de5c43a900abeaad3f1a20978b3339d', '7340', 0, 0, 'active', 'lahore', NULL, NULL, '1039633200', 'gh56', '1592938800', 'chcf', '1592938800', 'ghc', 'ghc', 'cvg6', '+9666858888', 'uploads/images/48313520200623120300Screenshot_2020-06-23-15-33-59-80_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/80476620200623120300Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/46759920200623120300Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'ghg', 'hbl', '2020-06-23 12:00:03', '0000-00-00 00:00:00', -1, 'NULL'),
(44, NULL, 'Sikandar', '', 'siy@gg.com', 5, '+9668568588', 'male', '1', 'Android', '', '', '1de5c43a900abeaad3f1a20978b3339d', '', 1, 0, 'active', 'lahore', NULL, NULL, '1039633200', 'gh56', '1592938800', 'chcf', '1592938800', 'ghc', 'ghc', 'cvg6', '+9666858888', 'uploads/images/5048220200623122801Screenshot_2020-06-23-15-33-59-80_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/82275920200623122801Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'uploads/images/78401020200623122801Screenshot_2020-06-23-15-25-01-49_2c39cdb6d097798d9c20199747cf458e.png', 'ghg', 'hbl', '2020-06-23 12:01:28', '0000-00-00 00:00:00', -1, 'NULL'),
(45, NULL, 'Ali', 'dcvf43', 'jjj@j.com', 5, '+96982367999', 'male', '1', 'iOS', '', '', '35b2723c70dc3a1d71c5616f6841dc80', '2908', 0, 0, 'active', 'Lahore', NULL, NULL, '961786800', '123', '1656049523', 'mjnhbg', '1656049533', 'Suzuki', '123', 'qw12', '+968778978', 'uploads/images/17549320200624054046profile_pic.jpg', 'uploads/images/5448520200624054046driving_license_img.jpg', 'uploads/images/32703320200624054046registration_license_img.jpg', 'azsx', 'HBL', '2020-06-24 05:46:40', '0000-00-00 00:00:00', -1, 'NULL'),
(46, NULL, 'Sikandar', 'sik123', 'sik.s@gmail.com', 5, '+9678678687', 'male', '1', 'iOS', '', '', '22f77f7cc412197eafbe2e281d7b32f3', '4010', 0, 0, 'active', 'Lahore', NULL, NULL, '961786800', 'asd12', '1687585974', '12', '1656049981', 'Carols', 'xs12', 'as34', '+96786768777', 'uploads/images/47571120200624054853profile_pic.jpg', 'uploads/images/58105620200624054853driving_license_img.jpg', 'uploads/images/61019320200624054853registration_license_img.jpg', 'as12', 'UBL', '2020-06-24 05:53:48', '0000-00-00 00:00:00', -1, 'NULL'),
(47, NULL, 'Allama', 'as', 'salmanmjnh@mk.com', 5, '+966876876', 'male', '1', 'iOS', '', '', '73f09e8979698c5e70e4de84f2003eeb', '8115', 0, 0, 'active', 'Islamabad', NULL, NULL, '961873200', 'sss', '1750853992', 'ss2', '1656159599', 'Suzuki', 'ss22', 'xxx22', '+968767888', 'uploads/images/1422420200625123420profile_pic.jpg', '', '', 'mm88', 'UBL', '2020-06-25 12:20:34', '0000-00-00 00:00:00', -1, 'NULL'),
(48, NULL, 'nah', 'Makkah', 'jj@jj.com', 5, '+96868768767', 'male', '1', 'iOS', '', '', '8389ce324c552289763c42ea07c7403c', '7334', 0, 0, 'active', 'SadiqAbad', NULL, NULL, '930337200', '3e', '1624689776', 'de', '1624689782', 'Suzuki', '2012', '87uy', '+968767868', 'uploads/images/95571020200626060344profile_pic.jpg', 'uploads/images/81150820200626060344driving_license_img.jpg', 'uploads/images/62024420200626060344registration_license_img.jpg', 'did', 'HBL', '2020-06-26 06:44:03', '0000-00-00 00:00:00', -1, 'NULL'),
(49, NULL, 'hjhk', 'jjlkl', 'deedjj@jj.com', 5, '+96876786876', 'male', '1', 'iOS', '', '', '7a750c676f5d3732501039fab83ed47e', '1716', 0, 0, 'active', 'Sadiqabad', NULL, NULL, '993754800', 'uu33', '1719641366', '44r', '1624946975', 's', 'f', 'Fred', '+96876876876', 'uploads/images/11737520200629065711profile_pic.jpg', 'uploads/images/84052820200629065711driving_license_img.jpg', 'uploads/images/10452220200629065711registration_license_img.jpg', 'frfr4', 'Alfalah', '2020-06-29 06:11:57', '0000-00-00 00:00:00', -1, 'NULL'),
(50, NULL, 'Sikandar', 'I’ll', 'Lam@gmail.com', 5, '+9687687688', 'male', '1', 'iOS', '', '', 'bd0a7c3ee63f66b763f5e29fe0fac85f', '4742', 0, 0, 'active', 'Lahore', NULL, NULL, '962218800', 'mk99', '1656484545', 'll00', '1624948553', 'Suzuki', 'pp0', 'jjj', '+9687687678', 'uploads/images/35931120200629064036profile_pic.jpg', 'uploads/images/38921720200629064036driving_license_img.jpg', 'uploads/images/62231020200629064036registration_license_img.jpg', 'ddd', 'abc', '2020-06-29 06:36:40', '0000-00-00 00:00:00', -1, 'NULL'),
(51, NULL, 'did', 'defrgtb', 'r555s@gmail.com', 5, '+968768768', 'male', '1', 'iOS', '', '', 'e2f52991ad8839b6ccfa80fa5d2e4b46', '6091', 0, 0, 'active', 'Punjab', NULL, NULL, '962218800', 'jj88', '1656571301', 'dd33', '1656484911', 'Suzuki', 'ssd44', 'dd44', '+96876876786', 'uploads/images/32069020200629065142profile_pic.jpg', 'uploads/images/57325820200629065142driving_license_img.jpg', 'uploads/images/4606620200629065142registration_license_img.jpg', 'fff', 'ABC', '2020-06-29 06:42:51', '0000-00-00 00:00:00', -1, 'NULL'),
(52, NULL, 'UMAIR', 'kk', 'mm@nn.com', 5, '+96986989887', 'male', '1', 'iOS', '', '', '8011a0501520a8063f9f8f54a4a995e2', '4084', 0, 0, 'active', 'Lily', NULL, NULL, '930596400', 'll9', '1656485354', 'kk9', '1656485413', 'kk', 'hhh', 'kk', '+967886867', 'uploads/images/83151720200629060051profile_pic.jpg', 'uploads/images/47671920200629060051driving_license_img.jpg', 'uploads/images/15537620200629060051registration_license_img.jpg', 'jj', 'la', '2020-06-29 06:51:00', '0000-00-00 00:00:00', -1, 'NULL'),
(53, NULL, 'I', 'lll', 'jj@jjnnnn.com', 5, '+968768768', 'male', '1', 'iOS', '', '', '17044cbe130ac771793e355877191b0d', '0610', 0, 0, 'active', 'Ayazpur', NULL, NULL, '962218800', 'rr44', '1656486065', 'll44', '1656486072', 'sukkah', 'ee', 'e', 'ee', 'uploads/images/64684420200629074202profile_pic.jpg', 'uploads/images/82806220200629074202driving_license_img.jpg', 'uploads/images/80636020200629074202registration_license_img.jpg', '00', 'ee', '2020-06-29 07:02:42', '0000-00-00 00:00:00', -1, 'NULL'),
(54, NULL, 'Sikandar Ali', 'gg', 'kkmm@nn.com', 5, '+97876757656', 'male', '1', 'iOS', '', '', '03a0ec94985b10d330eafa6cef136885', '8128', 0, 0, 'active', 'MM', NULL, NULL, '962218800', 'dd4', '1656574032', 'k', '1688023642', 'lol', 'hh7', '99', '+968768678', 'uploads/images/5960920200629072628profile_pic.jpg', 'uploads/images/42356420200629072628driving_license_img.jpg', 'uploads/images/81169420200629072628registration_license_img.jpg', 'mm', 'Kiki', '2020-06-29 07:28:26', '0000-00-00 00:00:00', -1, 'NULL'),
(55, NULL, 'Sikandar', 'mmnh44', 'mm@nhgv.com', 5, '+96788768', 'male', '1', 'iOS', '', '', 'eb6f9c25749ff8c45535a5185557613b', '9526', 0, 0, 'active', 'Milton', NULL, NULL, '962218800', 'mm98', '1656487891', 'ss22', '1688023905', 'Suzuki', 'kkk', 'hhh8', 'jjj', 'uploads/images/14282820200629072832profile_pic.jpg', 'uploads/images/31401620200629072832driving_license_img.jpg', 'uploads/images/71130020200629072832registration_license_img.jpg', 'bb', 'ss', '2020-06-29 07:32:28', '0000-00-00 00:00:00', -1, 'NULL'),
(56, NULL, 'Umair Ali Randhawa', 'meh', 'nn@dddhh.com', 5, '+96687687', 'male', '1', 'iOS', '', '', '17e89b5387ac14ea5b0dacc934f4f4df', '9815', 0, 0, 'active', 'Ahmedpur', NULL, NULL, '930596400', 'm', '1624952947', 'ju', '1624952983', 'dd', 'dd', 'dd', 'd', 'uploads/images/7651120200629070950profile_pic.jpg', 'uploads/images/79316920200629070950driving_license_img.jpg', 'uploads/images/31979020200629070950registration_license_img.jpg', 'd', 'dd', '2020-06-29 07:50:09', '0000-00-00 00:00:00', -1, 'NULL'),
(57, NULL, 'Sikandar', 'mmj@hh', 'gg@gm.com', 5, '+9689787897', 'male', '1', 'iOS', '', '', '6118a69807d2a8996a8f964d05a9a3ae', '0773', 0, 0, 'active', 'my', NULL, NULL, '993754800', 'my', '1656490037', 'Kiki', '1624954042', 'jj', 'bb', 'bbb', 'bbb', 'uploads/images/20687320200629085407profile_pic.jpg', 'uploads/images/40772020200629085407driving_license_img.jpg', 'uploads/images/12994620200629085407registration_license_img.jpg', 'bb', 'hh', '2020-06-29 08:07:54', '0000-00-00 00:00:00', -1, 'NULL'),
(58, NULL, 'LLLL', 'JJJ', 'mjnbbcj2KKJ@HH.COM', 5, '+9677991210', 'male', '1', 'iOS', '', '', '53e91b67dc2c8ff2bb9477663541c628', '', 1, 0, 'active', 'JJJ', NULL, NULL, '962218800', 'KK', '1656490261', 'HHH', '1656490266', 'JJJBB', 'BB', 'BB', 'HJJ', 'uploads/images/12992520200629083112profile_pic.jpg', 'uploads/images/4130020200629083112driving_license_img.jpg', 'uploads/images/58427420200629083112registration_license_img.jpg', 'NN', 'NNN', '2020-06-29 08:12:31', '0000-00-00 00:00:00', -1, 'NULL'),
(59, NULL, 'Sikandar', 'dd', 'kk@jjswe.com', 5, '+9686876786', 'male', '1', 'iOS', '', '', '5583edf1baf6ef82446e83fd6b5d4342', '', 1, 0, 'active', 'GG', NULL, NULL, '993754800', '9oi8', '1656490962', 'hhju7', '1656490968', 'jj7hgg', 'gggy77', 'hhy7', 'ddfr33', 'uploads/images/20752920200629083023profile_pic.jpg', 'uploads/images/56570520200629083023driving_license_img.jpg', 'uploads/images/61938320200629083023registration_license_img.jpg', 'ddf33', 'dde33', '2020-06-29 08:23:30', '0000-00-00 00:00:00', -1, 'NULL'),
(60, NULL, 'hello', 'hh3hHhggug', 's@dddhh.com', 5, '+968767686', 'male', '1', 'iOS', '', '', '492f80ca071fb9bdedcf279518ed1850', '', 1, 0, 'active', 'dd', NULL, NULL, '1025287200', 'ddd', '1624955216', 'ede3', '1656491221', '3dede', 'see', 'de', 'de', 'uploads/images/73300120200629082627profile_pic.jpg', 'uploads/images/97686720200629082627driving_license_img.jpg', 'uploads/images/89984720200629082627registration_license_img.jpg', 'ed', 'ed', '2020-06-29 08:27:26', '0000-00-00 00:00:00', -1, 'NULL'),
(61, NULL, 'ss', 'no', 'kknbg@g.com', 5, '+9678678678', 'male', '1', 'iOS', '', '', 'eb12b2ab63540949cbf1d8ceb9ce37e4', '', 1, 0, 'active', 'sss', NULL, NULL, '1025287200', 'ss', '1688027415', 'sss', '1688027419', 'sees', 'ss', 'ss', 'Dax’s', 'uploads/images/18202420200629084430profile_pic.jpg', 'uploads/images/41820520200629084430driving_license_img.jpg', 'uploads/images/69866720200629084430registration_license_img.jpg', 'dads', 'DSW’s', '2020-06-29 08:30:44', '0000-00-00 00:00:00', -1, 'NULL'),
(62, NULL, 'hh', 'Si', 'jjmnshjchjd@hh.com', 5, '+9678687687', 'male', '1', 'iOS', '', '', '3f25cedb0826c0a3b0fca34bf4f2b6d4', '', 1, 0, 'active', 'Sadiqabad', NULL, NULL, '1025287200', 'ddf4', '1656491642', 'ss', '1656491646', 'wade', 'sss', 'xxx', 'zxzzx', 'uploads/images/35117320200629085734profile_pic.jpg', 'uploads/images/79719120200629085734driving_license_img.jpg', 'uploads/images/57362820200629085734registration_license_img.jpg', 'zzz', 'ss', '2020-06-29 08:34:57', '0000-00-00 00:00:00', -1, 'NULL'),
(63, NULL, 'high', 'hhh@hh', 'vv@vjhj.com', 5, '+96987878786', 'male', '1', 'iOS', '', '', 'fd538e206fe6b6fe3b0c7785e837236c', '', 1, 0, 'active', 'nnb@h', NULL, NULL, '962218800', 'h', '1624955846', 'ss', '1656491850', 'ss', 'ss', 'ss', 'ss', 'uploads/images/55988520200629085937profile_pic.jpg', 'uploads/images/26167120200629085937driving_license_img.jpg', 'uploads/images/24767420200629085937registration_license_img.jpg', 'ss', 'ss', '2020-06-29 08:37:59', '0000-00-00 00:00:00', -1, 'NULL'),
(64, NULL, 'HHgg', 'nhbgvvv', 'JJggg@vv.com', 5, '+968978798', 'male', '1', 'iOS', '', '', 'b85fab8fb4b6993c6e5b08f4af893953', '', 1, 0, 'active', 'Lahore', NULL, NULL, '1025287200', 'ddc44c', '1656492654', 'hhhu', '1656492659', 'hh88', 'hh88', 'bob hh', 'hhhb', 'uploads/images/19581120200629084951profile_pic.jpg', 'uploads/images/46542920200629084951driving_license_img.jpg', 'uploads/images/77053320200629084951registration_license_img.jpg', 'hhh', 'bb', '2020-06-29 08:51:49', '0000-00-00 00:00:00', -1, 'NULL'),
(65, NULL, 'Sikandar', 'hub', 'jkmnjkd@mm.com', 5, '+967867687', 'male', '1', 'iOS', '', '', '86cae0805941e37e786dac03b4010c5d', '', 1, 0, 'active', 'ss', NULL, NULL, '1025287200', 'cdc', '1656495066', 'cd3', '1688031071', 'dice', 'dude', 'dude', 'dude', 'uploads/images/43520520200629094833profile_pic.jpg', 'uploads/images/51506920200629094833driving_license_img.jpg', 'uploads/images/5625920200629094833registration_license_img.jpg', 'code', 'code', '2020-06-29 09:33:48', '0000-00-00 00:00:00', -1, 'NULL'),
(66, NULL, 'Majid', '', 'm.alrumaim@gmail.com', 3, '966503920211', NULL, '1', 'ios', 'eJcYXu7xs0w:APA91bHdoEHjZx7woCr8myfzEvEXtYRigOiC7qdBOF51FuwIJPukj4hMYDgnxLp5QJLykmNP5qcFtAya_6hHJWuLPPgY_PfLFO7sYd3CnJJMPc3yInMReQjne_JvJby_q_lC1FgRWgaK', '', '0d7363894acdee742caf7fe4e97c4d49', '3929', 0, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-29 09:40:13', '0000-00-00 00:00:00', -1, 'NULL'),
(67, NULL, 'jjjh', 'bubble', 'hhb@bb.com', 5, '+967869879', 'male', '1', 'iOS', '', '', 'c852457017dcff2d8b9f541147d83c66', '', 1, 0, 'active', 'John', NULL, NULL, '1025287200', 'ss', '1688031867', 'Shaw', '1656495872', 'ss', 'ss', 'ss', 'ss', 'uploads/images/28209820200629092645profile_pic.jpg', 'uploads/images/83637220200629092645driving_license_img.jpg', 'uploads/images/87475820200629092645registration_license_img.jpg', 'ss', 'ss', '2020-06-29 09:45:26', '0000-00-00 00:00:00', -1, 'NULL'),
(68, NULL, 'dcd2', 'cd', 'mmHgggv@dd.com', 5, '+9687674522', 'male', '1', 'iOS', '', '', '231ba2d0548884aab6c673d87ad262f2', '', 1, 0, 'active', 'sss', NULL, NULL, '962218800', 'ss', '1656496137', 'sxww', '1624960144', 'sxsw', 'ax', 's', 'w', 'uploads/images/52500820200629095450profile_pic.jpg', 'uploads/images/36662720200629095450driving_license_img.jpg', 'uploads/images/40075220200629095450registration_license_img.jpg', 'we', 'we', '2020-06-29 09:50:54', '0000-00-00 00:00:00', -1, 'NULL'),
(69, NULL, 'de', 'ss', 'ss@nnjnjnj.com', 5, '+989879879', 'male', '1', 'iOS', '', '', 'b66c354af8bf8539f83f86fcf472f759', '', 1, 0, 'active', 'ss', NULL, NULL, '962218800', 'xx', '1656496370', 'x', '1656496373', 's', 'six', 's', 'x', 'uploads/images/29681620200629090954profile_pic.jpg', 'uploads/images/34479920200629090954driving_license_img.jpg', 'uploads/images/13440920200629090954registration_license_img.jpg', 'ss', 's', '2020-06-29 09:54:09', '0000-00-00 00:00:00', -1, 'NULL'),
(70, NULL, 'ss', 'sss', 'jjjnbg@mm.com', 5, '+96876767665', 'male', '1', 'iOS', '', '', 'b0655d1ef54e26f10fa20ed8f796fe54', '', 1, 0, 'active', 'ss', NULL, NULL, '930596400', 'Xsan', '1656497575', 'ss', '1656497579', 'xox', 'was', 'ax', 'alas', 'uploads/images/87888820200629105913profile_pic.jpg', 'uploads/images/8045320200629105913driving_license_img.jpg', 'uploads/images/51817920200629105913registration_license_img.jpg', 'sxww', 'ax', '2020-06-29 10:13:59', '0000-00-00 00:00:00', -1, 'NULL'),
(71, NULL, 'ddd', 'doc', 'dcd@lll.com', 5, '+968768687', 'male', '1', 'iOS', '', '', '4b5fe16d33750b93581ab98175030593', '', 1, 0, 'active', 'sss', NULL, NULL, '962218800', 'ax', '1656497981', 'sss', '1656497988', 'sx', 'we', 'de', 'de', 'uploads/images/43765120200629100322profile_pic.jpg', 'uploads/images/13604320200629100322driving_license_img.jpg', 'uploads/images/20493520200629100322registration_license_img.jpg', 'de', 'ed', '2020-06-29 10:22:03', '0000-00-00 00:00:00', -1, 'NULL'),
(72, NULL, 'ss', 'nnjhjhjh', 'hhjhsg@hj.com', 5, '+9689731622', 'male', '1', 'iOS', '', '', '2588c7a345045a900d2c68637ba324a2', '', 1, 0, 'active', 'sss', NULL, NULL, '962218800', 'ss', '1656499792', 's', '1656499796', 'sxsx', 'ax', 's', 'ax', 'uploads/images/4258720200629103351profile_pic.jpg', 'uploads/images/50985120200629103351driving_license_img.jpg', 'uploads/images/47929120200629103351registration_license_img.jpg', 'sxww', 'sx', '2020-06-29 10:51:33', '0000-00-00 00:00:00', -1, 'NULL'),
(73, NULL, 'ddd', 'mnjhbgvfcrt', 'ddccmjj@m.com', 5, '+9687687675', 'male', '1', 'iOS', '', '', '4ae7746c9e52dcd20203dcfc428cd390', '', 1, 0, 'active', 'de', NULL, NULL, '1025287200', 'sx', '1593428001', 'xsx', '1624964006', 'sxww', 'sxsx', 'sx', 'z22', 'uploads/images/33686520200629103055profile_pic.jpg', 'uploads/images/64412720200629103055driving_license_img.jpg', 'uploads/images/91353920200629103055registration_license_img.jpg', 'sxs', 'sxsx', '2020-06-29 10:55:30', '0000-00-00 00:00:00', -1, 'NULL'),
(74, NULL, 'sikandar', 'hghjgsuyb', 'jhjhjgb@mmm.com', 5, '+96876786876', 'male', '1', 'iOS', '', '', '36d236075036de9a63f04e8782cd85a4', '', 1, 0, 'active', 'hhNBN', NULL, NULL, '930682800', 'jhhhj', '1688113023', 'jghj99', '1688113029', 'Suzuki', 'hhh', 'hhg98', '980983', 'uploads/images/93415220200630084417profile_pic.jpg', 'uploads/images/33424920200630084417driving_license_img.jpg', 'uploads/images/56850020200630084417registration_license_img.jpg', 'as', 'did', '2020-06-30 08:17:44', '0000-00-00 00:00:00', -1, 'NULL'),
(75, NULL, 'Jim', 'hnbjur', 'mjirf@lo.com', 5, '+9678904567', 'male', '1', 'iOS', '', '', '910e2a1c48b2c0e7465de998cc1f6ecc', '', 1, 0, 'active', 'h', NULL, NULL, '962305200', 'j', '1625041278', 'j', '1625041283', 'just', 'fff', 'ddd', '8i8', 'uploads/images/8310420200630085821profile_pic.jpg', 'uploads/images/49698020200630085821driving_license_img.jpg', 'uploads/images/47065320200630085821registration_license_img.jpg', 'veg', 'u87', '2020-06-30 08:21:58', '0000-00-00 00:00:00', -1, 'NULL'),
(76, NULL, 'no', 'Hyatt', 'mm0788@mm.com', 5, '+968786530', 'male', '1', 'iOS', '', '', '6483eb9b04d192c94220bf179188ba67', '', 1, 0, 'active', 'nnn', NULL, NULL, '930682800', 'u88', '1656577852', 'hhh88', '1688113859', 'juju', '0p0', 'lop0', '88ii', 'uploads/images/37148820200630085031profile_pic.jpg', 'uploads/images/76071520200630085031driving_license_img.jpg', 'uploads/images/36403020200630085031registration_license_img.jpg', 'ccd43', 'xzassaq12', '2020-06-30 08:31:50', '0000-00-00 00:00:00', -1, 'NULL'),
(77, NULL, 'Bills', 'Bills', 'bilal@gmail.com', 5, '+968768768768', 'male', '1', 'iOS', '', '', 'b9f82755f8a27e4b37f14da422487234', '', 1, 0, 'active', 'Lahore', NULL, NULL, '993841200', 'ninja', '1656580320', 'ee44', '1656580326', 'Honda', 'man', 'ok', '+9688778979', 'uploads/images/58274420200630094912profile_pic.jpg', 'uploads/images/50451620200630094912driving_license_img.jpg', 'uploads/images/55495820200630094912registration_license_img.jpg', 'did', 'did', '2020-06-30 09:12:49', '0000-00-00 00:00:00', -1, 'NULL'),
(78, NULL, 'Usama', 'usama123', 'usama@gmail.com', 5, '+96877874444', 'male', '1', 'iOS', '', '', '6492d38d732122c58b44e3fdc3e9e9f3', '', 1, 1, 'active', 'Lahore', NULL, NULL, '930682800', '34', '1656581748', 'de', '1656581753', 'for', 'got', 'from', 'de', 'uploads/images/57837720200630093137profile_pic.jpg', 'uploads/images/30369820200630093137driving_license_img.jpg', 'uploads/images/34557220200630093137registration_license_img.jpg', 'lop', 'monk', '2020-06-30 09:37:31', '0000-00-00 00:00:00', -1, 'NULL'),
(79, NULL, 'Norman', 'noman123', 'noman@gmail.com', 5, '+9676786438', 'male', '1', 'iOS', '', '', 'be8f5ded0b358a365247f22fa23abcba', '', 1, 1, 'active', 'Hedrabad', NULL, NULL, '962305200', '909', '1654078394', '09io', '1656584000', 'lko09', 'nh55', 'de45', '45', 'uploads/images/75919220200630103014profile_pic.jpg', 'uploads/images/43906020200630103014driving_license_img.jpg', 'uploads/images/20671920200630103014registration_license_img.jpg', 'de456', 'UBL', '2020-06-30 10:14:30', '0000-00-00 00:00:00', -1, 'NULL'),
(80, NULL, 'Majid', '', 'Majid.a@schopfen.com', 3, '96650392021١', 'male', '1', 'ios', 'refreshedToken', '', '25f9e794323b453885f5181f1b624d0b', '6998', 0, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-30 10:18:35', '0000-00-00 00:00:00', -1, 'NULL'),
(81, NULL, 'Muhammad Salman', '', 'salman@gmail.com', 3, '966513241212', 'male', '1', 'ios', '', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-30 11:10:02', '0000-00-00 00:00:00', -1, 'NULL'),
(82, NULL, 'Muhammad Salman', '', 'Salman@Salman.salman', 3, '966512341324', 'male', '1', 'ios', '', '', '25d55ad283aa400af464c76d713c07ad', '9419', 0, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-06-30 11:38:57', '0000-00-00 00:00:00', -1, 'NULL'),
(83, 12, 'Arbaz A', 'arbaz123', 'arbaz@gmail.com', 5, '+9687687600', 'male', '1', 'iOS', '', '', '1ba3409f7ebfe79eb20f8e1b79bd6099', '', 1, 1, 'active', 'Sadiqabad', NULL, NULL, '930682800', '123', '1688967082', '34ee', '1783661573', 'dcvf', 'as', '12ws', '+9687687600', 'uploads/images/68093820200630124108profile_pic.jpg', 'uploads/images/81005720200710053433driving_license_img.jpg', 'uploads/images/36736220200710053433registration_license_img.jpg', '099887766', 'UBL', '2020-06-30 12:08:41', '0000-00-00 00:00:00', -1, 'NULL'),
(84, NULL, 'Ali', 'Ali12345', 'ali@gmail.com', 5, '+9612098754', 'male', '1', 'iOS', '', '', '8fec6f91137c5a8fad682d6fa8e71e33', '2219', 0, 0, 'active', 'Sadiqabad', NULL, NULL, '899233200', '123', '1688188570', '09', '1688188576', '123', 'de4', 'CBC', '+968767664', 'uploads/images/97351820200701050217profile_pic.jpg', 'uploads/images/58529320200701050217driving_license_img.jpg', 'uploads/images/92896520200701050217registration_license_img.jpg', 'xsza12', 'UBL', '2020-07-01 05:17:02', '0000-00-00 00:00:00', -1, 'NULL'),
(85, NULL, 'he', '', 'kih@Hugh', 3, '966503920211', 'male', '1', 'ios', 'refreshedToken', '', '25f9e794323b453885f5181f1b624d0b', '1700', 0, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-07-01 10:23:24', '0000-00-00 00:00:00', -1, 'NULL'),
(86, NULL, 'Sikandar', 'sikandar0099', 'sikandarwaraich1@gmail.com', 5, '+96109847665', 'male', '1', 'iOS', '', '', 'd1c373ab1570cfb9a7dbb53c186b37a2', '', 1, 0, 'active', 'Minh', NULL, NULL, '962564400', '898', '1625292276', '887', '1656828283', 'Suzuki', 'my', 'bb88', '+9668687687', 'uploads/images/51955220200703064106profile_pic.jpg', 'uploads/images/12276220200703064106driving_license_img.jpg', 'uploads/images/72221020200703064106registration_license_img.jpg', 'saw', 'we', '2020-07-03 06:06:41', '0000-00-00 00:00:00', -1, 'NULL'),
(87, NULL, 'xox', 'curio', 'jmjnhyui@gmail.com', 5, '+9678676899', 'male', '1', 'iOS', '', '', 'd4040d45d33138104b830b7e226a70a2', '', 1, 0, 'active', 'NH', NULL, NULL, '994100400', '33', '1656850805', '33', '1656850809', 'we', 'e3', 'de3', '333', 'uploads/images/91286420200703120921profile_pic.jpg', 'uploads/images/45958420200703120921driving_license_img.jpg', 'uploads/images/97424120200703120921registration_license_img.jpg', 'seed', 'dddd', '2020-07-03 12:21:09', '0000-00-00 00:00:00', -1, 'NULL'),
(88, NULL, 'Wasim', '', 'wasim@gmail.com', 5, '+96696385274', 'male', '1', 'Android', '', '', '8971cbd8c1187c76a13a40d1f6800bcb', '', 1, 0, 'active', 'Sadiqabaf', NULL, NULL, '1039460400', '5743', '1596049200', '5754', '1596135600', 'Honda', 'hhg44', '575', '+9668521478', 'uploads/images/49709320200713113851IMG_20200712_232102.jpg', 'uploads/images/86848120200713113851IMG_20200713_165047.jpg', 'uploads/images/37003420200713113851IMG_20200713_165165.jpg', '3532', 'hbl', '2020-07-13 11:51:38', '0000-00-00 00:00:00', -1, 'NULL'),
(89, NULL, 'Muhammad Salman', '', 'm.salman@zynq.net', 3, '966512312312', 'male', '1', 'ios', 'f08FbOIhp0Udm-bRHjhSD-:APA91bHk6uvr0shGrteU0GYiIyM5wUthUxub4q0JHgHPgj7200CmEhaS6lXB1tIF11ULi1yRCZiOeDXn280USG47yDZQTsAuXgneGnqefBwteik-uDr1tZhY6LCw9PmoYHbCTenDchIJ', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 1, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-07-30 11:27:22', '0000-00-00 00:00:00', -1, 'NULL'),
(90, NULL, 'Ali', 'hi', 'ali123@gmail.com', 5, '+9686876868', 'female', '1', 'iOS', '', '', '8fec6f91137c5a8fad682d6fa8e71e33', '', 1, 0, 'active', 'as', NULL, NULL, '964897200', '334', '1659181694', 'ww2', '1627645699', 'ref', 'ddd', '09', '+96876876876', 'uploads/images/22135620200730113551profile_pic.jpg', 'uploads/images/69878320200730113551driving_license_img.jpg', 'uploads/images/74413020200730113551registration_license_img.jpg', '23ee', 'Hal', '2020-07-30 11:51:35', '0000-00-00 00:00:00', -1, 'NULL');

-- --------------------------------------------------------

--
-- Table structure for table `user_districts`
--

CREATE TABLE `user_districts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `user_type` enum('warehouse','delivery') NOT NULL DEFAULT 'warehouse'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_districts`
--

INSERT INTO `user_districts` (`id`, `user_id`, `district_id`, `user_type`) VALUES
(1, 10, 1, 'delivery'),
(2, 12, 1, 'warehouse'),
(3, 12, 2, 'warehouse'),
(4, 13, 1, 'delivery'),
(5, 14, 1, 'delivery'),
(6, 14, 2, 'delivery'),
(7, 0, 1, 'delivery'),
(8, 0, 2, 'delivery'),
(9, 15, 1, 'delivery'),
(10, 15, 2, 'delivery'),
(11, 16, 1, 'delivery'),
(12, 16, 2, 'delivery'),
(13, 17, 1, 'delivery'),
(14, 17, 2, 'delivery'),
(15, 18, 1, 'delivery'),
(16, 18, 2, 'delivery'),
(17, 19, 1, 'delivery'),
(18, 19, 2, 'delivery'),
(19, 20, 1, 'delivery'),
(20, 20, 2, 'delivery');

-- --------------------------------------------------------

--
-- Table structure for table `visit_complaint_types`
--

CREATE TABLE `visit_complaint_types` (
  `visit_complaint_type_id` int(11) NOT NULL,
  `visit_complaint_title_en` varchar(255) NOT NULL,
  `visit_complaint_title_ar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visit_complaint_types`
--

INSERT INTO `visit_complaint_types` (`visit_complaint_type_id`, `visit_complaint_title_en`, `visit_complaint_title_ar`, `created_at`, `updated_at`, `is_active`) VALUES
(7, 'Maintenance ', 'Maintenance ', '2018-11-01 09:46:12', '2018-11-01 09:46:12', 1),
(8, 'Technical', 'Technical', '2018-11-01 09:46:33', '2018-11-01 09:47:59', 1),
(9, 'Sales and Marketing', 'Sales and Marketing', '2018-11-01 09:47:21', '2018-11-01 09:47:21', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `assigned_orders_for_delivery`
--
ALTER TABLE `assigned_orders_for_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `chat_request`
--
ALTER TABLE `chat_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaint_types`
--
ALTER TABLE `complaint_types`
  ADD PRIMARY KEY (`complaint_type_id`);

--
-- Indexes for table `complaint_types_new`
--
ALTER TABLE `complaint_types_new`
  ADD PRIMARY KEY (`complaint_type_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`district_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `home_images`
--
ALTER TABLE `home_images`
  ADD PRIMARY KEY (`homeimage_id`);

--
-- Indexes for table `home_video`
--
ALTER TABLE `home_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`newsletter_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`order_item_it`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `page_images`
--
ALTER TABLE `page_images`
  ADD PRIMARY KEY (`page_image_id`);

--
-- Indexes for table `point_of_sales`
--
ALTER TABLE `point_of_sales`
  ADD PRIMARY KEY (`point_of_sale_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `product_specifications`
--
ALTER TABLE `product_specifications`
  ADD PRIMARY KEY (`product_specification_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `support_tickets`
--
ALTER TABLE `support_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_users`
--
ALTER TABLE `support_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_orders`
--
ALTER TABLE `temp_orders`
  ADD PRIMARY KEY (`temp_order_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_districts`
--
ALTER TABLE `user_districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_complaint_types`
--
ALTER TABLE `visit_complaint_types`
  ADD PRIMARY KEY (`visit_complaint_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `assigned_orders_for_delivery`
--
ALTER TABLE `assigned_orders_for_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `chat_request`
--
ALTER TABLE `chat_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `complaint_types`
--
ALTER TABLE `complaint_types`
  MODIFY `complaint_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `complaint_types_new`
--
ALTER TABLE `complaint_types_new`
  MODIFY `complaint_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `district_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_images`
--
ALTER TABLE `home_images`
  MODIFY `homeimage_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `home_video`
--
ALTER TABLE `home_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `newsletter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `order_item_it` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `page_images`
--
ALTER TABLE `page_images`
  MODIFY `page_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT for table `point_of_sales`
--
ALTER TABLE `point_of_sales`
  MODIFY `point_of_sale_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT for table `product_specifications`
--
ALTER TABLE `product_specifications`
  MODIFY `product_specification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `support_tickets`
--
ALTER TABLE `support_tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `support_users`
--
ALTER TABLE `support_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_orders`
--
ALTER TABLE `temp_orders`
  MODIFY `temp_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `user_districts`
--
ALTER TABLE `user_districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `visit_complaint_types`
--
ALTER TABLE `visit_complaint_types`
  MODIFY `visit_complaint_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

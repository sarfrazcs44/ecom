<?php
if (isset($_GET['checkout']))
{
    $q = '?checkout';
}else{
    $q = '';
}
?>
<div class=" pt90 pb60">
    <div class="container-fluid">
        <div class="col-lg-12 pt30 mb30">
            <h3 class="text-red"><?php echo lang('address'); ?></h3>
        </div>
        <div class="row col-lg-12 msg alert" style="display:none;">
            <div class="col-lg-12 msg_text">

            </div>
        </div>
        <?php if ($this->session->flashdata('message')) { ?>
            <div class="row col-lg-12 msg alert">
                <div class="col-lg-12 msg_text">
                    <?php echo $this->session->flashdata('message'); ?>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <?php $this->load->view('front/layouts/usertabs'); ?>
        </div>

        <div class="row pt20">

            <div class="col-lg-12 pb20" align="right">

                <span class="input-group-btn" style="float:right;">
                    <a href="<?php echo base_url('address/add').$q; ?>"><button
                                class="btn btn-md btn-primary">+ <?php echo lang('add_address'); ?></button></a>
                </span>

            </div>

        </div>

        <?php
        $has_address = false;
        if ($address) {
            $has_address = true;
            foreach ($address as $value) {
                ?>
                <div class="col-lg-4 mb30 wow fadeInUp" data-wow-delay=".2s" id="<?php echo $value->address_id; ?>">
                    <div class="entry-card">
                        <div align="right" class="verified">
                            <a href="javascript:void(0);" class="simple-hover">

                                <span <?php echo($value->is_default == 0 ? 'onclick="deleteRecord(' . $value->address_id . ',\'address/delete\',\'\');"' : ''); ?>
                                        class='product-label bg-<?php echo($value->is_default == 0 ? 'danger' : 'success'); ?>'><i
                                            class="fa fa-<?php echo($value->is_default == 0 ? 'trash' : 'check'); ?>"
                                            aria-hidden="true"></i></span>

                            </a>
                        </div>

                        <div class="entry-content">
                            <div class="entry-meta mb5">
                                    
                            </div>
                            <h4 class="entry-title text-capitalize">
                                <a href="<?php echo base_url('address/edit/' . $value->address_id).$q; ?>">
                                    <?php echo $value->location; ?>
                                </a>
                            </h4>

                            <div class="text-right">
                                <p align="center">
                                    <?php if ($value->is_default == 1) { ?>
                                        <a href="#"
                                           class="btn font-button btn-md btn-rounded btn-outline-secondary disabled"><span><?php echo lang('use_this_address'); ?></span></a>
                                    <?php } else { ?>
                                        <a href="javascript:void();"
                                           class="btn font-button btn-md btn-rounded btn-primary use_this_address"
                                           id="<?php echo $value->address_id; ?>"><span><?php echo lang('use_this_address'); ?></span></a>
                                    <?php } ?>

                                </p>

                            </div>
                        </div><!--/entry content-->
                    </div>
                </div><!--/.col-->
            <?php }
        } ?>


    </div>

    <?php if (isset($_GET['checkout'])) {
        if ($has_address) { ?>
            <div class="col-lg-12 text-field-column" align="center">
                <a href="<?php echo base_url('checkout/place_order'); ?>">
                    <button class="btn btn-filter btn-primary"><?php echo lang('proceed'); ?></button>
                </a>
            </div>
        <?php } else { ?>
            <div class="col-lg-12 text-field-column" align="center">
                <span>Please add atleast one address first....</span>
            </div>
        <?php }
    } ?>
</div>
</div>
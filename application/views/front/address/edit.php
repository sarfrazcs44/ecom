<?php
if (isset($_GET['checkout']))
{
    $from_checkout = 1;
}else{
    $from_checkout = 0;
}
?>
<div class="pt90 pb90">
    <div class="container-fluid">
     <div class="row pt60">
    <?php $this->load->view('front/layouts/usertabs');?>
    </div>
        <div class="row pt60">
            <div class="col-lg-12" align="right">

                <h6 class="text-blue">
                    <?php echo lang('welcome');?>,<br /><?php echo $this->session->userdata['user']['full_name'];?>

                </h6>
              
                
            </div>
            <form action="<?php echo base_url();?>address/action" method="post" class="GeoDetails form-horizontal form_data" autocomplete="off" enctype="multipart/form-data" onsubmit="return false;">
                <input type="hidden" name="form_type" value="update">
                <input type="hidden" name="address_id" value="<?php echo $address_id;?>">
                <input type="hidden" name="from_checkout" value="<?php echo $from_checkout; ?>">
                <div class="alert  validatio-msg" style="display: none;"></div>           
                    <div class="col-lg-12">

                        <div class="mb40 pt10" align="center">

                            <h4 class="text-blue"><?php echo lang('address');?></h4>



                        </div><!--/col-->

                        </div>
                     
                
                     <div class="col-lg-6 text-field-column">

                            <div class="mb10 mt10">


                                <div class="input-group">
                                    
                                    <input type="text" name="location" class="form-control field-color required text-field-border" data-geo="formatted_address" placeholder="<?php echo lang('pin_your_location');?>" data-toggle="modal" data-target="#locationModal" value="<?php echo $result->location;?>">
                                </div>

                            </div><!--/col-->

                        </div>
					 

                        <div class="col-lg-6 text-field-column">
                            <div class="mb10 mt10">

                                <div class="input-group">
                                    <input type="text" name="apartment_no" class="form-control required text-field-border" placeholder="<?php echo lang('apartment_no');?>" value="<?php echo $result->apartment_no;?>">
                                </div>

                            </div><!--/col-->
                        </div>

                    <div class="col-lg-6 text-field-column">
                        <div class="mb10 mt10">

                            <div class="input-group">
                                <input type="text" name="building_no" class="form-control required text-field-border" placeholder="<?php echo lang('block');?>" value="<?php echo $result->building_no;?>">
                            </div>

                        </div><!--/col-->
                    </div>

                    <div class="col-lg-6 text-field-column">
                        <div class="mb10 mt10">

                            <div class="input-group">
                                <input type="text" name="floor_no" class="form-control required text-field-border" placeholder="<?php echo lang('flat');?>" value="<?php echo $result->floor_no;?>">
                            </div>

                        </div><!--/col-->
                    </div>
                    <div class="col-lg-6 text-field-column">
                            <div class="mb10 mt10">

                                <div class="input-group">
                                    <input type="text" name="Country" class="form-control required text-field-border" placeholder="<?php echo lang('country');?>" value="Saudi Arabia" readonly="">
                                </div>

                            </div><!--/col-->
                        </div>

                    <div class="col-lg-6 text-field-column">
                        <div class="mb10 mt10">

                            <div class="input-group">
                                <select name="city_id" id="city_id" class="form-control required text-field-border">
                                    <option value="">Please select your nearest city... *</option>
                                    <?php
                                    $ksa_cities = getAllKsaCities();
                                    foreach ($ksa_cities as $ksa_city)
                                    { ?>
                                        <option value="<?php echo $ksa_city->id; ?>" <?php echo ($result->city_id == $ksa_city->id ? 'selected' : ''); ?>><?php echo $ksa_city->eng_name; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>

                        </div><!--/col-->
                    </div>

                    <div class="col-lg-6 text-field-column">
                        <div class="mb10 mt10">

                            <div class="input-group">
                                <select class="form-control required text-field-border" id="districts" name="district_id" required>
                                        <option value="">Please select your nearest district... *</option>
                                        <?php foreach ($districts as $district)
                                    { ?>
                                        <option value="<?php echo $district->district_id; ?>" <?php echo ($result->district_id == $district->district_id ? 'selected' : ''); ?>><?php echo $district->eng_name; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>

                        </div><!--/col-->
                    </div>




                        

                    <input name="lat" type="hidden" data-geo="lat" value="<?php echo $result->lat;?>">
					<input name="lng" type="hidden" data-geo="lng" value="<?php echo $result->lng;?>">
                        <div class="col-lg-12 text-field-column" align="center">

                            <button class="btn btn-filter btn-primary" type="submit"><?php echo lang('submit');?></button>

                        </div>
                    
            </form>
                        
                    </div>
               
            </div>
        </div>
<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<input type="text" class="form-control geocomplete_map_draggable">
				<a class="btn confirm-loc-btn btn-success" data-dismiss="modal"><?php echo lang('confirm');?></a>
			</div>
			<div class="modal-body">
				<div class="address_map"></div>
			</div>
		</div>
	</div>
</div><!--/modal-->
<script>
    
$(document).ready(function () {

    $('#city_id').on('change',function(){

        var city_id = $(this).val();
        

        

        $.ajax({
            type: "POST",
            url: base_url + 'address/getDistricts',
            data: {'city_id': city_id},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
                $('#districts').html(result.html);
            }, complete: function () {
                //$.unblockUI();
            }
        });
        return true;

     });
     });
</script>

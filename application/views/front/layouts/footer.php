<div class="col-lg-12 sticky-footer">
<footer class="footer footer-standard pt50 pb20">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 mb40">
                <h3><?php echo lang('home_newsletter')?></h3>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 padding-right-zero" style="padding-left:0px !important;">
                            <form id="widget-subscribe-form" action="<?php echo base_url();?>page/newsletter"  role="form" method="post" class="mb0 form-horizontal form_data">
                            
                            <div class="alert  validatio-msg" style="display: none;"></div>
                            
                            
                                <div class="input-group input-group-lg">

                                    <input  type="email" name="email" class="form-control " required placeholder="<?php echo lang('home_newsletterplaceholder');?>">
                                    <span class="input-group-btn">
                                                    <button class="btn btn-primary" type="submit"><?php echo lang('home_newsletterbutton');?></button>
                                                </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-6 col-md-6 mb40">

                <ul class="list-unstyled latest-news ul-home-bottom">
                    <li class="media">
                        <a href="#"> <img class="d-flex ml-3 img-fluid" width="64" src="<?php echo base_url('assets/front')?>/images/logo_1.png" alt="Generic placeholder image"></a>
                        <a href="#"> <img class="d-flex ml-3 img-fluid" width="64" src="<?php echo base_url('assets/front')?>/images/logo_2.png" alt="Generic placeholder image"></a>
                        <a href="#"> <img class="d-flex ml-3 img-fluid" width="64" src="<?php echo base_url('assets/front')?>/images/logo_3.png" alt="Generic placeholder image"></a>
                        <a href="#"> <img class="d-flex ml-3 img-fluid" width="64" src="<?php echo base_url('assets/front')?>/images/logo_4.png" alt="Generic placeholder image"></a>

                    </li>


                </ul>
            </div>

        </div>
    </div>
    <div class="show_friends" style="display: none;"></div>
</footer><!--/footer-->
<div class="footer-bottomAlt">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-xs-4" style="width:50% !important;">
                <span style="display:inline;color:white;">&copy; <?php echo lang('home_footercopyright');?>, <?php echo date('Y'); ?></span>
            </div>
            <div class="col-lg-8 col-xs-8" style="width:50% !important;">
                <div class="clearfix float-right" style="display:inline;">
                   <!-- <a href="#" class="social-icon-sm si-dark  ">
                        <span><?php //echo lang('home_faq');?></span>
                    </a>-->
                    <a href="<?php echo base_url('page/become_a_seller');?>" class="social-icon-sm si-dark ">
                        <span><?php echo lang('home_becomeareseller');?></span>
                    </a>
                    
                    <a href="<?php echo base_url('page/download_center');?>" class="social-icon-sm si-dark ">
                        <span><?php echo lang('home_downloadcenter');?></span>
                    </a>
                   
                    <a href="<?php echo base_url('page/contact_us');?>" class="social-icon-sm si-dark ">
                        <span><?php echo lang('home_contactus');?></span>
                    </a>

                    <a href="<?php echo base_url('page/terms_and_conditions');?>" class="social-icon-sm si-dark ">
                        <span><?php echo lang('terms_and_conditions');?></span>
                    </a>


                    <a href="https://www.facebook.com/MSJ.SEC/" target="_blank" class="social-icon-sm si-dark " style="font-size:16px !important;">
                        <i class="fa fa-facebook"></i>
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="https://twitter.com/msjsecurity" target="_blank" class="social-icon-sm si-dark " style="font-size:16px !important;">
                        <i class="fa fa-twitter"></i>
                        <i class="fa fa-twitter"></i>
                    </a>

                </div>
            </div>

        </div>
    </div>
</div><!--/footer bottom-->

</div>


<img class="d-flex ml-3 img-fluid image-chat" style="bottom:10px;right:10px;position:fixed;z-index:100;cursor:pointer;" width="64" src="<?php echo base_url('assets/front')?>/images/Messages-icon.png" alt="Generic placeholder image">




<div class="main-section chat_window" style="display: none;">
	<div class="row border-chat">
		<div class="col-md-12 col-sm-12 col-xs-12 first-section">
			<div class="row">
				<div class="col-md-8 col-sm-6 col-xs-6 left-first-section">
					<p class="text-align-right"><?php echo lang('ticketing_chat');?></p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6 right-first-section">
					<a href="#"><i class="fa fa-close chatbox" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row border-chat">
		<div class="col-md-12 col-sm-12 col-xs-12 second-section">
			<div class="chat-section">
				<ul class="show_messages conversation-list slimscroll-alt">
                    <li>
                        <div style="text-align: center;">
                            <span class="queued-chat-count"></span>
                            <br><br>
                            <div id="countdown"></div>
                        </div>
                    </li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row border-chat chatBoxMessageField">
		<div class="col-md-12 col-sm-12 col-xs-12 third-section">
			<div class="text-bar">

				<input type="text" class="message" placeholder="Write messege"><a href="#" class="SendMessage"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>

    
            </div>
		</div>
	</div>
</div>


<div class="main-section-complaint start_chat_window">
	<div class="row border-chat">
		<div class="col-md-12 col-sm-12 col-xs-12 first-section-complaint">
			<div class="row">
				<div class="col-md-8 col-sm-6 col-xs-6 left-first-section-complaint">
					<p class="text-align-right"><?php echo lang('chat_livechat');?></p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6 right-first-section">
					<a href="#" title="End Chat"><i class="fa fa-close complaintbox" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row border-chat">
        <form onsubmit="return false;" id="chatForm" >
    		<div class="col-md-12 col-sm-12 col-xs-12 second-section-complaint" >
    			<div class="chat-section-complaint">
        			<div class="col-lg-12" >
                        <div class="pt20" >
                            <div class="input-group" style="margin-top:10%;">
                                <input type="text" name="name" class="form-control required text-field-border customer_fullname" placeholder="<?php echo lang('customer_fullname');?>" required>
                            </div>
                        </div><!--/col-->
                    </div>
                    <div class="col-lg-12">
                        <div class="pt20">
                            <div class="input-group">
                                <input type="email" name="email" class="form-control required text-field-border customer_email" placeholder="<?php echo lang('customer_email');?>" required>
                            </div>
                        </div><!--/col-->
                    </div>
                    <div class="col-lg-12">
                        <div class="pt20">
                            <div class="input-group">
                                <input type="text" name="subject" class="form-control required text-field-border chat_subject" placeholder="<?php echo lang('chat_subject');?>" required>
                            </div>
                        </div><!--/col-->
                    </div>
                                        
                    <!--<div class="col-lg-12">
                        <div class="pt20">
                            <div class="input-group">
                                <textarea name="message" class="form-control field-color" rows="3" cols="50" placeholder="<?php //echo lang('customer_message');?>"></textarea>
                            </div>
                        </div>
                    </div>-->
                    <div class="col-lg-12 text-field-column" align="center" style="margin-top:10%;">            
                        <div class="pt20">
                            <div class="input-group">
                                <button class="btn btn-filter btn-success" type="submit"><?php echo lang('customer_submitbutton');?></button>
                            </div>    
                        </div>
                    </div>                                                  
    			</div>
    		</div>
        </form>
	</div>

</div>





    
<!--back to top-->
<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/js/plugins/plugins.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/js/assan.custom.js"></script>
<!-- load cubeportfolio -->
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/jquery.themepunch.revolution.min.js"></script>


<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/tobia/Pause/master/jquery.pause.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.marquee/1.3.1/jquery.marquee.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/js/jquery.countdown360.js"></script>

<script>
    function start_my_counter() {
        var my_countdown_timer =  $("#countdown").countdown360({
            radius      : 60.5,
            seconds     : 120,
            strokeWidth : 15,
            fillStyle   : '#F3F3F3',
            strokeStyle : '#D3D3D3',
            fontSize    : 50,
            fontColor   : '#D3D3D3',
            autostart: false,
            label: false,
            clockwise: true,
            onComplete  : function () {
                $('#countdown').hide();
                location.reload();
            }
        });
        //console.log('countdown360 ',my_countdown_timer);
        my_countdown_timer.start();
    }


    $(document).ready(function(){
        // start_my_counter();

    });

    function sendNotificationEmail(email_address) {
        //alert(email_address);
        // send ajax call here to send email
    }

</script>

<script type="text/javascript">
	$(document).ready(function(){
    	$(".left-first-section").click(function(){
            $('.main-section').toggleClass("open-more");
        });
    });
    $(document).ready(function(){
    	$(".chatbox").click(function(){
            $('.main-section').toggleClass("open-more");
        });
    });
	$(document).ready(function(){
    	$(".left-first-section-complaint").click(function(){
            $('.main-section-complaint').toggleClass("open-more-complaint");
        });
    });
    $(document).ready(function(){
    	$(".complaintbox").click(function(){
            $('.main-section-complaint').toggleClass("open-more-complaint");
        });
    });
	
	
	$(document).ready(function(){
    	$(".image-chat").click(function(){
            $('.main-section-complaint').toggleClass("open-more-complaint");
        });
    });
	
</script>

<script>
    /**Hero  script**/
    var tpj = jQuery;

    var revapi1078;
    tpj(document).ready(function () {
        if (tpj("#rev_slider_1078_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1078_1");
        } else {
            revapi1078 = tpj("#rev_slider_1078_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "revolution/js/",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 8000,
                navigation: {
                    arrows: {
                        enable: true,
                        style: 'uranus',
                        tmp: '',
                        rtl: false,
                        hide_onleave: false,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_over: 9999,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        left: {
                            container: 'slider',
                            h_align: 'left',
                            v_align: 'center',
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            container: 'slider',
                            h_align: 'right',
                            v_align: 'center',
                            h_offset: 0,
                            v_offset: 0
                        }
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "80%",
                    presize: false
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1140, 992, 700, 465],
                gridheight: [600, 600, 500, 480],
                lazyType: "none",
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 46, 47, 48, 49, 50, 55]
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false
                }
            });
        }
    });	/*ready*/
    //cube portfolio init
    (function ($, window, document, undefined) {
        'use strict';

        // init cubeportfolio
        $('#js-grid-mosaic-flat').cubeportfolio({
            filters: '#js-filters-mosaic-flat',
            layoutMode: 'mosaic',
            sortToPreventGaps: true,
            mediaQueries: [{
                width: 1500,
                cols: 6
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2,
                options: {
                    caption: '',
                    gapHorizontal: 15,
                    gapVertical: 15
                }
            }],
            defaultFilter: '*',
            animationType: 'fadeOutTop',
            gapHorizontal: 0,
            gapVertical: 0,
            gridAdjustment: 'responsive',
            caption: 'fadeIn',
            displayType: 'fadeIn',
            displayTypeSpeed: 100,
            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',
            plugins: {
                loadMore: {
                    selector: '#js-loadMore-mosaic-flat',
                    action: 'click',
                    loadItems: 3
                }
            }
        });
    })


    (jQuery, window, document);
</script>


<script>

    //thumbnail slider
    (function ($, window, document, undefined) {
        'use strict';

        // init cubeportfolio
        $('#js-grid-slider-thumbnail').cubeportfolio({
            layoutMode: 'slider',
            drag: true,
            auto: false,
            autoTimeout: 5000,
            autoPauseOnHover: true,
            showNavigation: false,
            showPagination: false,
            rewindNav: true,
            scrollByPage: true,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 0,
                cols: 1
            }],
            gapHorizontal: 0,
            gapVertical: 0,
            caption: '',
            displayType: 'fadeIn',
            displayTypeSpeed: 400,
            plugins: {
                slider: {
                    pagination: '#js-pagination-slider',
                    paginationClass: 'cbp-pagination-active'
                }
            }
        });
    })(jQuery, window, document);
</script>


<script>


    //$(document).ready(function () {


//$('.owl-prev').empty();
        //$('.owl-next').empty();

        //var prev = $('.owl-prev');
        //var next = $('.owl-next');

        //prev.empty();
        //next.empty();

        //var newprev = '<img src="<?php // echo base_url('assets/front')?>/images/prev.png" alt="" class="move-testimonial circle inline-display">';
       // prev.append(newprev);

        //var newnext = '<img src="<?php // echo base_url('assets/front')?>/images/next.png" alt="" class="move-testimonial next circle inline-display">';
       // next.append(newnext);
    //setInterval(function(){
            //$(".owl-next").click();
        //},5000);


    //});




</script>


<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>flatpickr(".flatpicker_time", {minDate: "today", enableTime: true});</script>


<script type="text/javascript">jssor_1_slider_init();</script>
<!--smart-form script-->
<script src="<?php echo base_url('assets/front')?>/smart-form/contact/js/jquery.form.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/front')?>/smart-form/contact/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url('assets/front')?>/smart-form/contact/js/additional-methods.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('assets/front')?>/smart-form/contact/js/smart-form.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.8.1/parsley.min.js" async defer></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>

<script src="<?php echo base_url()?>assets/js/jquery.min.map" async defer></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWJjL2YrT5FefvnQ71XtYtt7E73cwcFXE&libraries=places&callback=initMap&region=sa" async defer></script>
<script src="<?php echo base_url()?>assets/js/jquery.geocomplete.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>

<script>
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initMap() {
        // var map = new google.maps.Map(document.getElementById('map'), {
        //     center: {lat: 50.064192, lng: -130.605469},
        //     zoom: 3
        // });
        //var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        //var countries = document.getElementById('country-selector');

        // map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input, { types: ['(cities)'], language: ['(ar)']});

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions({'country': ['sa']});

    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
</script>




<script src="<?php echo base_url('assets/front')?>/js/script.js?v=<?php echo rand(); ?>" type="text/javascript"></script>



</body>
</html>

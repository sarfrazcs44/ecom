<!DOCTYPE html>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ShopBox</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets')?>/images/favicon.ico">
    <!-- Plugins CSS -->
    <?php if($this->session->lang == 'en'){?>
    <link href="<?php echo base_url('assets/front')?>/css/plugins/plugins.css" rel="stylesheet">
    <?php } ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134768657-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134768657-2');
</script>


    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front')?>/revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front')?>/revolution/css/layers.css">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front')?>/revolution/css/navigation.css">
    <!-- load css for cubeportfolio -->
    <?php if($this->session->lang == 'en'){?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/front')?>/cubeportfolio/css/cubeportfolio.min.css">
    <?php } ?>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/front')?>/smart-form/contact/css/smart-forms.css">
    <link href="<?php echo base_url('assets/front')?>/css/font-awesome.css" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />

    <?php if($this->session->lang == 'en'){?>
    <link href="<?php echo base_url('assets/front')?>/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/front')?>/css/whoweare.css" rel="stylesheet">
    <?php } ?>
    <link href="<?php echo base_url('assets/front')?>/css/custom_styles.css" rel="stylesheet">

    <!-- Arabic Files  Start-->
    <?php if($this->session->lang == 'ar'){?>
    <link href="<?php echo base_url('assets/front')?>/css/arabic/plugins_ar.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/front')?>/css/arabic/cubeportfolio_ar.min.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/front')?>/css/arabic/style_ar.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/front')?>/css/arabic/whoweare_ar.css" rel="stylesheet">
    <?php } ?>

    <!-- Arabic Files  End-->


    <style>
        .btn-blue-outline {
            background-color: #2c263c !important;
            color: #ffffff !important;
        }
		.dropbtn {
    color: #9d9d9d;
    /* padding: 16px; */
    font-size: 10px;
    border: none;
    background: none;
    display: inline;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 80px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: #676767;
    padding: 9px 12px;
    text-decoration: none;
    display: block;
	font-size:10px;
}



.dropdown:hover .dropdown-content {
    display: block;
}


    </style>


    <script src="<?php echo base_url('assets/front')?>/js/jquery.min.js"></script>


    <script>

    </script>

    <script> var base_url = "<?php echo base_url() ?>";
             var delete_message = "<?php echo lang('delete_message');?>";
             var system_language = '<?php echo $this->session->lang; ?>';
    </script>
    <script>
        var CTRL        = "",
        MTHD        = "Welcome",
        USER_ID     = '0',
        RECEIVER    = '0',
        USER_EMAIL  = "",
        USER_NAME  = "";

    </script>
<!-- Chat Include -->
        <script src="https://www.gstatic.com/firebasejs/4.0.0/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.0.0/firebase-messaging.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.0.0/firebase-database.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.10.0/firebase.js"></script>
        <script src="<?php echo base_url();?>assets/js/front/firebase_custom.js"></script>

    <script src="<?php echo base_url('assets/front')?>/js/jssor.slider-27.0.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
                $AutoPlay: 0,
                $Idle: 2000,
                $SlideEasing: $Jease$.$InOutSine,
                $DragOrientation: 3,
                $Cols: 1,
                $Align: 0,
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            //make sure to clear margin of the slider container element
            jssor_1_slider.$Elmt.style.margin = "";

            /*#region responsive code begin*/

            /*
                parameters to scale jssor slider to fill parent container

                MAX_WIDTH
                    prevent slider from scaling too wide
                MAX_HEIGHT
                    prevent slider from scaling too high, default value is original height
                MAX_BLEEDING
                    prevent slider from bleeding outside too much, default value is 1
                    0: contain mode, allow up to 0% to bleed outside, the slider will be all inside parent container
                    1: cover mode, allow up to 100% to bleed outside, the slider will cover full area of parent container
                    0.1: flex mode, allow up to 10% to bleed outside, this is better way to make full window slider, especially for mobile devices
            */

            var MAX_WIDTH = 3000;
            var MAX_HEIGHT = 3000;
            var MAX_BLEEDING = 1;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {
                    var originalWidth = jssor_1_slider.$OriginalWidth();
                    var originalHeight = jssor_1_slider.$OriginalHeight();

                    var containerHeight = containerElement.clientHeight || originalHeight;

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);
                    var expectedHeight = Math.min(MAX_HEIGHT || containerHeight, containerHeight);

                    //scale the slider to expected size
                    jssor_1_slider.$ScaleSize(expectedWidth, expectedHeight, MAX_BLEEDING);

                    //position slider at center in vertical orientation
                    jssor_1_slider.$Elmt.style.top = ((containerHeight - expectedHeight) / 2) + "px";

                    //position slider at center in horizontal orientation
                    jssor_1_slider.$Elmt.style.left = ((containerWidth - expectedWidth) / 2) + "px";
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>


    <script src='https://www.google.com/recaptcha/api.js'></script>

    <style>
        .marquee {
            width: 100%;
            overflow: hidden;
        }
        .marquee img {
            background: white;
            margin: auto;
            margin-left: 25px;
            padding: 10px;
        }
    </style>
</head>

<body class="content-<?php echo $this->session->lang; ?>">


<!-- Common Layout Start -->

<div id="preloader">
    <div id="preloader-inner"></div>
</div><!--/preloader-->


<!-- Pushy Menu -->



<aside class="pushy pushy-right">
    <div class="cart-content">
        <div class="clearfix">
            <a href="javascript:void(0)" class="pushy-link text-white-gray">Close</a>
        </div>
        <ul class="list-unstyled">
            <li class="clearfix">
                <a href="#" class="float-left">
                    <img src="<?php echo base_url('assets/front')?>/images/shop/p1.jpg" class="img-fluid" alt="" width="60">
                </a>
                <div class="oHidden">
                    <span class="close"><i class="ti-close"></i></span>
                    <h4><a href="#">Men's Special Watch</a></h4>
                    <p class="text-white-gray"><strong>$299.00</strong> x 1</p>
                </div>
            </li><!--/cart item-->
            <li class="clearfix">
                <a href="#" class="float-left">
                    <img src="<?php echo base_url('assets/front')?>/images/shop/p2.jpg" class="img-fluid" alt="" width="60">
                </a>
                <div class="oHidden">
                    <span class="close"><i class="ti-close"></i></span>
                    <h4><a href="#">Men's tour beg</a></h4>
                    <p class="text-white-gray"><strong>$99.00</strong> x 1</p>
                </div>
            </li><!--/cart item-->
            <li class="clearfix">
                <a href="#" class="float-left">
                    <img src="<?php echo base_url('assets/front')?>/images/shop/p3.jpg" class="img-fluid" alt="" width="60">
                </a>
                <div class="oHidden">
                    <span class="close"><i class="ti-close"></i></span>
                    <h4><a href="#">Women's T-shirts</a></h4>
                    <p class="text-white-gray"><strong>$199.00</strong> x 1</p>
                </div>
            </li><!--/cart item-->
            <li class="clearfix">

                <div class="float-right">
                    <a href="#" class="btn btn-primary">Checkout</a>
                </div>
                <h4 class="text-white">
                    <small>Subtotal - </small> $49.99
                </h4>
            </li><!--/cart item-->
        </ul>
    </div>
</aside>

<!-- Pushy Menu End -->



<!-- Site Overlay -->


<div class="site-overlay"></div>


<!-- nav start -->


<nav class="navbar navbar-expand-lg navbar-light navbar-transparent bg-faded nav-sticky">
    <div class="search-inline">
        <form>
            <input type="text" class="form-control" placeholder="Type and hit enter...">
            <button type="submit"><i class="ti-search"></i></button>
            <a href="javascript:void(0)" class="search-close"><i class="ti-close"></i></a>
        </form>
    </div><!--/search form-->
    <div class="container-fluid">

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url()?>page/">
            <img class='logo logo-dark' src="<?php echo base_url('assets/front')?>/images/logo.png" width="150" height="30" alt="">
            <img class='logo logo-light hidden-md-down' src="<?php echo base_url('assets/front')?>/images/logo.png" width="200" height="30" alt="">
        </a>
        <div id="navbarNavDropdown" class="navbar-collapse collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown active">
                    <a class="nav-link" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url().'page/who_we_are' ?>"><?php echo lang('nav_whoweare');?></a>

                </li>
                <li class="nav-item dropdown dropdown-full-width">
                    <a class="nav-link" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url().'page/categories' ?>"><?php echo lang('nav_ourbrands');?></a>

                </li>
                <li class="nav-item dropdown dropdown-full-width">
                    <a class="nav-link" aria-haspopup="true" aria-expanded="false" href="<?php echo base_url().'support/' ?>"><?php echo lang('nav_forcustomers');?></a>

                </li>
            </ul>
            <div class=" navbar-right-elements">
                <ul class="list-inline">
                    <?php if($this->session->userdata('user')){ ?>
                    <li class="list-inline-item">
                    <span class="dropdown">
  					<button class="dropbtn"><?php echo lang('my_account');?></button>
  					<span class="dropdown-content">
    				<a href="<?php echo base_url() ?>orders"><?php echo lang('account');?></a>
                    <a href="<?php echo base_url() ?>account/logout" ><?php echo lang('logout');?></a>
    				
    				
  					</span>
					</span>
                    </li>
                    <?php } else { ?>
                        <li class="list-inline-item">

                        <a href="<?php echo base_url() ?>page/login" class="">
                            <?php echo ($this->session->userdata('lang') == 'en' ? 'Login' : 'تسجيل الدخول'); ?>
                        </a>

                        </li>
                    <?php }?>
                    <li class="list-inline-item"><a href="#" class="">|</a></li>
                    <li class="list-inline-item"><a href="javascript:void(0);" onclick="changeLanguage('<?php echo ($this->session->userdata('lang') == 'en' ? 'ar' : 'en');?>');" class=""><?php echo ($this->session->userdata('lang') == 'en' ? 'العربية' : 'Eng'); ?></a></li>
                   <?php if(get_cookie('temp_order_key') || $this->session->userdata('user')){
                         if($this->session->userdata('user')){
                             $user_id = $this->session->userdata['user']['user_id'];
                         }else{
                             $user_id = get_cookie('temp_order_key');
                         }
                        $user_total_product = getTotalProduct($user_id);
                         // cart_full.png
                    ?>
                    <li class="list-inline-item" ><a href="<?php echo($user_total_product > 0 ? base_url('checkout') : 'javascript:void(0);');?>" >
                            <img src="<?php echo base_url()?>assets/front/images/cart_empty.png"> <span class="badge badge-default" style="font-weight: bold;"><?php echo ($user_total_product > 0 ? $user_total_product : ''); ?></span></a></li>
                    <?php }else{ ?>
                    <li class="list-inline-item" ><a href="javascript:void(0);" ><img src="<?php echo base_url()?>assets/front/images/cart_empty.png"> <span class="badge badge-default"></span></a></li>
                    <?php } ?>
                </ul>
            </div><!--right nav icons-->
        </div>

    </div>
</nav>


<!-- nav end -->



<!--Common Layout End -->

<script src="<?php echo base_url('assets/front')?>/js/jquery-3.3.1.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>




<style>

    .owl-prev{
        background : url(<?php echo base_url('assets/front')?>/images/prev.png);
    }

    .owl-next{
        background : url(<?php echo base_url('assets/front')?>/images/prev.png);
        background : url(<?php echo base_url('assets/front')?>/images/prev.png);
    }


</style>
<script>
function changeLanguage(lang){
    window.location = "<?php echo base_url();?>page/changeLanguage/"+lang;

}
</script>      
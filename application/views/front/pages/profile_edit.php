<link href="<?php echo base_url('assets/front/');?>css/whoweare.css" rel="stylesheet" />

<script></script>


<div class="pt90 pb90">
	<div class="container">
		<div class="row pt60">
        
        <?php $this->load->view('front/layouts/usertabs');?>
        
			<div class="col-lg-12 pt20">

                <h6 class="text-blue">
                    <?php echo($this->session->lang == 'en' ? 'Welcome' : 'أهلا بك'); ?>
                    <br />
                    <?php echo $user['full_name'] ?>

                </h6>
			</div>

			<div class="col-lg-12 pt20 pb30">
				<div class="row">



					<div class="w3-sidebar w3-bar-block w3-light-grey w3-card col-lg-3 col-sm-12 col-xs-12">
						<button class="w3-bar-item w3-button tablink text-blue" onclick="openWhoweare(event, 'item1')"><b><?php echo($this->session->lang == 'en' ? 'Change Password' : 'غير كلمة السر'); ?></b></button>
						<!--<button class="w3-bar-item w3-button tablink text-light-gray" onclick="openWhoweare(event, 'item2')"><b>Email Update</b></button>
						-->
						<a href="<?php echo base_url() ?>account/logout"><button  class="w3-bar-item w3-button tablink text-light-gray" onclick="openWhoweare(event, 'item3')"><b><?php echo($this->session->lang == 'en' ? 'Logout' : 'الخروج'); ?></b></button></a>
						
					</div>

					<div class="col-lg-9 col-sm-12 col-xs-12 tabs-content">

						<div id="item1" class="w3-container content" >

							<form method="post" action="<?php echo base_url();?>/account/forgotPassword" class="form_data" id="forgotPasswordForm" onsubmit="return false;">
                                <div class="alert  validatio-msg" style="display: none;">
                                </div>
                             <div class="col-lg-12">

                                    <div class="col-lg-12">
                                    <h4 class="text-blue">
                                        <?php echo($this->session->lang == 'en' ? 'Change Password' : 'غير كلمة السر'); ?>
                                    </h4>
									</div>
                                    
                                    <div class="col-lg-6">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="password" name="old_password" class="form-control required text-field-border" placeholder="<?php echo($this->session->lang == 'en' ? 'Old Password' : 'كلمة المرور القديمة'); ?>">
                                        </div>

                                    </div><!--/col-->
                                    </div>
                                    
                                    <div class="col-lg-6">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="password" name="new_password" class="form-control required text-field-border" placeholder="<?php echo($this->session->lang == 'en' ? 'New Password' : 'كلمة السر الجديدة'); ?>">
                                        </div>

                                    </div>
                                    </div>
                                    
                                    <div class="col-lg-6">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="password" name="confirm_password" class="form-control required text-field-border" placeholder="<?php echo($this->session->lang == 'en' ? 'Confirm Password' : 'تأكيد كلمة المرور'); ?>">
                                        </div>

                                    </div>
                                    </div>
                                    
                                    <div class="col-lg-12">

                                    <button class="btn btn-filter btn-primary" type="submit"><?php echo($this->session->lang == 'en' ? 'Submit' : 'تقديم'); ?></button>

                                </div>

                                </div>

                                </form>
                                
						</div>

						<div id="item2" class="w3-container content" style="display:none">
							
                            <div>
                            
                             <div class="col-lg-12">

                                    <div class="col-lg-6">
                                    <h4 class="text-blue">
                                        <?php echo($this->session->lang == 'en' ? 'Update Email' : 'تحديث البريد الإلكتروني'); ?>
                                    </h4>
                                    </div>

                                    
                                    <div class="col-lg-6">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="email" name="email" class="form-control required text-field-border" placeholder="<?php echo($this->session->lang == 'en' ? 'New Email' : 'بريد إلكتروني جديد'); ?>">
                                        </div>

                                    </div><!--/col-->
                                    </div>

                                    
                                    <div class="col-lg-12">

                                    <button class="btn btn-filter btn-primary" type="submit"><?php echo($this->session->lang == 'en' ? 'Submit' : 'تقديم'); ?></button>

                                </div>

                                </div>

                                </div>
                            
                            
						</div>

						<div id="item3" class="w3-container content" style="display:none">

							

						</div>


						

					</div>



				</div>

			</div>

		</div>


	</div>
</div>

<script>
    function openWhoweare(evt, Name) {
        var i, x, tablinks;
        x = document.getElementsByClassName("content");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" text-blue", " text-light-gray");
        }


        document.getElementById(Name).style.display = "block";

        // $(".tab").addClass("active"); // instead of this do the below
        evt.currentTarget.className += " text-blue";
    }
</script>


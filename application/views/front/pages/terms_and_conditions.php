<style>
    h5, p{
        width: 100%;
    }
</style>
<div class="pt90 pb90">
    <div class="container">
        <div class="col-lg-12 pb20"><h3> MSJ Privacy Policy </h3></div>
        <p class="col-lg-12">
        <p> This privacy policy has been compiled to better serve those who are concerned with how their 'Personally
            Identifiable Information' (PII) is being used online. PII, as described in US privacy law and
            information security, is information that can be used on its own or with other information to identify,
            contact, or locate a single person, or to identify an individual in context. Please read our privacy
            policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your
            Personally Identifiable Information in accordance with our website. </p>
        <h5>What personal information do we collect from the people that visit our blog, website or app?</h5>
        <p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email
            address, mailing address, phone number or other details to help you with your experience.</p>
        <h5>When do we collect information?</h5>
        <p>We collect information from you when you register on our site, place an order, subscribe to a newsletter,
            fill out a form, Use Live Chat, Open a Support Ticket or enter information on our site. Provide us with
            feedback on our products or services.</p>
        <h5>How do we use your information?</h5>
        <p>We may use the information we collect from you when you register, make a purchase, sign up for our
            newsletter, respond to a survey or marketing communication, surf the website, or use certain other site
            features in the following ways:</p>
        <ul style="width:100%;">
            <li> To personalize your experience and to allow us to deliver the type of content and product offerings
                in which you are most interested.
            </li>
            <li>To improve our website in order to better serve you.</li>
            <li>To allow us to better service you in responding to your customer service requests.</li>
            <li>To administer a contest, promotion, survey or other site feature.</li>
            <li>To quickly process your transactions.</li>
            <li>To ask for ratings and reviews of services or products</li>
            <li>To follow up with them after correspondence (live chat, email or phone inquiries)</li>
        </ul>
        <h5>How do we protect your information?</h5>
        <p>We do not use vulnerability scanning and/or scanning to PCI standards. An external PCI compliant payment
            gateway handles all CC transactions. We use regular Malware Scanning. Your personal information is
            contained behind secured networks and is only accessible by a limited number of persons who have special
            access rights to such systems, and are required to keep the information confidential. In addition, all
            sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.  We
            implement a variety of security measures when a user places an order to maintain the safety of your
            personal information.  All transactions are processed through a gateway provider and are not stored or
            processed on our servers.</p>

        <h5>Do we use 'cookies'?</h5>
        <p>We do not use cookies for tracking purposes You can choose to have your computer warn you each time a
            cookie is being sent, or you can choose to turn off all cookies. You do this through your browser
            settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way
            to modify your cookies.  If you turn cookies off .</p>
        <h5>Third-party disclosure</h5>
        <p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable
            Information.</p>
        <h5>Third-party links</h5>
        <p>Occasionally, at our discretion, we may include or offer third-party products or services on our website.
            These third-party sites have separate and independent privacy policies. We therefore have no
            responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek
            to protect the integrity of our site and welcome any feedback about these sites.</p>
        <h5>California Online Privacy Protection Act</h5>
        <p>CalOPPA is the first state law in the nation to require commercial websites and online services to post a
            privacy policy. The law's reach stretches well beyond California to require any person or company in the
            United States (and conceivably the world) that operates websites collecting Personally Identifiable
            Information from California consumers to post a conspicuous privacy policy on its website stating
            exactly the information being collected and those individuals or companies with whom it is being shared.
            See more at:
            <a href="http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf" target="_blank">
                http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf
            </a>
        </p>
        <h5>According to CalOPPA, we agree to the following:</h5>
        <p>Users can visit our site anonymously. Once this privacy policy is created, we will add a link to it on
            our home page or as a minimum, on the first significant page after entering our website. Our Privacy
            Policy link includes the word 'Privacy' and can easily be found on the page specified above. You will be
            notified of any Privacy Policy changes:</p>
        <ul style="width:100%;">
            <li>On our Privacy Policy Page</li>
        </ul>
        <p>Can change your personal information:</p>
        <ul style="width:100%;">
            <li>By emailing us</li>
            <li>By chatting with us or by sending us a support ticket</li>
        </ul>
        <h5>How does our site handle Do Not Track signals?</h5>
        <p>We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track
            (DNT) browser mechanism is in place.</p>
        <h5>Does our site allow third-party behavioral tracking?</h5>
        <p>It's also important to note that we do not allow third-party behavioral tracking</p>
        <h5>COPPA (Children Online Privacy Protection Act)</h5>
        <p>When it comes to the collection of personal
            information from children under the age of 13 years old, the Children's Online Privacy Protection Act
            (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection
            agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do
            to protect children's privacy and safety online. We do not specifically market to children under the age
            of 13 years old.</p>
        <h5>Fair Information Practices</h5>
        <p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have
            played a significant role in the development of data protection laws around the globe. Understanding the
            Fair Information Practice Principles and how they should be implemented is critical to comply with the
            various privacy laws that protect personal information.</p>
        <h5>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</h5>
        <p>We will notify you via email</p>
        <ul style="width:100%;">
            <li>Within 7 business days</li>
        </ul>
        <p>We also agree to the Individual Redress Principle which requires that individuals have the right to
            legally pursue enforceable rights against data collectors and processors who fail to adhere to the law.
            This principle requires not only that individuals have enforceable rights against data users, but also
            that individuals have recourse to courts or government agencies to investigate and/or prosecute
            non-compliance by data processors.</p>
    </div>
</div>
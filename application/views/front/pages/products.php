<div class=" pt90 pb90 grey">
	<div class="container">
        
		<div class="row pt90">
            <?php if($bread_crumb) { ?>
            <div class="col-lg-12">
				
				<h3 class="text-blue">
					<a href="<?php echo base_url()?>page/products"><img src="<?php echo base_url('assets/front')?>/images/back.png" /></a>
					<?php if(!empty($bread_crumb['cat'])) { ?>
						<?php echo ($this->session->lang == 'en') ? $bread_crumb['cat']['title_en'] : $bread_crumb['cat']['title_ar'] ?>
                    <?php } ?>
					<?php if(!empty($bread_crumb['sub_cat'])) { ?>
						/ <?php echo ($this->session->lang == 'en') ? $bread_crumb['sub_cat']['title_en'] : $bread_crumb['sub_cat']['title_ar'] ?>
					<?php } ?>
				</h3>
				
			</div>
            <?php }else{ ?>
            <div class="col-lg-12">

				<h3 class="text-blue"><?php echo lang('home_filter_heading');?></h3>
			</div>
            <?php } ?>
            
			<div class="col-lg-3">
				<div class="mb30 mt30">
            <form method="get" action="<?php echo base_url() ?>page/products">
					<div class="input-group">
						<select name="category" class="form-control main_categories" data-lang="<?php echo ($this->session->lang) ?>">
                            <option value="-1"><?php echo lang('home_filter_category');?>...</option>
							<?php if($categories){  foreach ($categories as $category) { ?>
                                <option value="<?php echo $category['category_id'] ?>">
	                                <?php echo ($this->session->lang == 'en') ? $category['title_en'] : $category['title_ar'] ?>
                                </option>
                            <?php } } ?>
						</select>
					</div>

				</div><!--/col-->
			</div>

			<div class="col-lg-3">
				<div class="mb30 mt30">
					<div class="input-group">
                        <select name="sub_category" class="form-control sub_categories">
                            <option value="-1"><?php echo lang('home_filter_subvategory');?>...</option>
<!--							--><?php //if($categories){  foreach ($categories as $category) { ?>
<!--                                <option value="--><?php //echo $category['category_id'] ?><!--">-->
<!--									--><?php //echo ($this->session->lang == 'en') ? $category['title_en'] : $category['title_ar'] ?>
<!--                                </option>-->
<!--							--><?php //} } ?>
                        </select>
					</div>

				</div><!--/col-->
			</div>

			<div class="col-lg-3">
				<div class="mb30 mt30">

                    <div class="input-group">
                        <select name="brand_id" class="form-control">
                            <option value="-1"><?php echo lang('home_filter_brand');?>...</option>
							<?php if($brands){  foreach ($brands as $brand) { ?>
                                <option value="<?php echo $brand['brand_id'] ?>">
									<?php echo ($this->session->lang == 'en') ? $brand['title_en'] : $brand['title_ar'] ?>
                                </option>
							<?php } } ?>

                        </select>
                    </div>

				</div><!--/col-->
			</div>

			<div class="col-lg-3">
				<div class="mb30 mt30">
                        <span class="input-group-btn" style="float:right;">
                            <button class="btn btn-filter btn-primary" type="submit"><?php echo lang('home_filter_button');?></button>
                        </span>
				</div><!--/col-->
			</div>
            </form>
			<?php if($products) { ?>
            <?php foreach($products as $product) { ?>
			<div class="col-lg-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s">
				<div class="entry-card">
					<a href="<?php echo base_url()?>page/product_details?id=<?php echo $product['product_id']?>" class="entry-thumb product-thumb">
                        <?php if(isset($product['product_images'][0]['product_image'])){
                                    $image = base_url() . $product['product_images'][0]['product_image'];
                        }else{
                                $image = '';
} ?>
						<img src="<?php echo $image; ?>" alt="" class="img-fluid">
						<span class="thumb-hover ti-back-right"></span>
					</a><!--/entry thumb-->
					<div class="entry-content">
						<div class="entry-meta mb5" align="center">
                            <span><?php echo ($this->session->lang == 'en') ? $product['category_title_en'] : $product['category_title_en'] ?></span>
						</div>
						<h4 class="entry-title text-capitalize" align="center">
							<a href="#">
                                <?php echo ($this->session->lang == 'en') ? $product['model_en'] : $product['model_ar'] ?>
							</a>
						</h4>
						<p align="center">
							<a href="javascript:void(0);" data-pro-id = "<?php echo $product['product_id'];?>" class="btn font-button btn-md btn-rounded btn-success add_to_cart_on_products_page"><span><?php echo (stristr($product['price'], '.') == false) ? $product['price'].'.00' : $product['price'] ?> SAR</span><span style="margin-left:10px !important;"></span></a>
						</p>
                        <div class="row msg alert" style="display:none;">
                            <div class="col-lg-12 msg_text" style="text-align: center;">
                                added to cart
                            </div>
                        </div>

					</div><!--/entry content-->
				</div>
			</div><!--/.col-->
            <?php } ?>
            <?php }else { ?>
                <div class="col-lg-6 col-lg-offset-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s" style="text-align: center; margin: 0 auto"><div class="alert alert-info">No products found!</div></div>
            <?php } ?>
		</div>
	</div>
</div>
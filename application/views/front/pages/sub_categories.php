<?php
$product = getPageData(21);
?>
<div class=" pt90 pb90 ">
	<div class="container">
		<div class="row pt90 pb90">
			<div class="col-lg-12">

				<h3 class="text-red">
                    <?php echo $this->session->lang == 'en' ? $product->page_title_en : $product->page_title_ar; ?>
				</h3>
                <?php echo $this->session->lang == 'en' ? $product->page_description_en : $product->page_description_ar; ?>
			</div>

		</div>

		<div class="row">
			<div class="col-lg-10">

                <h3 class="text-blue">
                    <a href="<?php echo base_url()?>page/categories"><img src="<?php echo base_url('assets/front')?>/images//back.png" /></a>
					<?php if($category) { ?>
						<?php echo ($this->session->lang == 'en') ? $category['title_en'] : $category['title_ar'] ?>
					<?php } ?>
                </h3>
			</div>

			<div class="col-lg-2">

				<h3 class="text-blue">
                        <span class="input-group-btn" style="float:right;">
                            <a href="<?php echo base_url()?>page/products"><button class="btn btn-product btn-primary" type="button"><?php echo ($this->session->lang == 'en') ? 'Find a Product' : 'ابحث عن منتج'; ?></button></a>
                        </span>

				</h3>
			</div>




			<?php if($sub_cats) {  ?>
			<?php $i = 0; foreach ($sub_cats as $category) { ?>
                <div class="col-lg-3 mb30 mt30 wow fadeInUp custom_css_change" data-wow-delay=".2s">
                    <div class="item">
                        <a href="<?php echo base_url()?>page/products?sub_category=<?php echo $category['category_id']; ?>" class="project-overlay">
                            <img alt="image" class="img-fluid border-round" src="<?php echo base_url() . $category['image']; ?>">
                            <div class="project-overlay-caption">
                                <h4><?php echo ($this->session->lang == 'en') ? $category['title_en'] : $category['title_ar']; ?></h4>
                            </div>
                        </a><!--project overlay-->
                    </div>

                </div><!--/.col-->
                <?php $i++;  } ?>
            <?php } ?>
		</div>
	</div>
</div>
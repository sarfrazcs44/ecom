<style>

@media (min-height:992px)  and (max-height: 1199px) {
	
	.min-height{
	
	min-height:66%;
		
		}


}


@media (min-height:1200px)  and (max-height: 1600px) {
	
	.min-height{
	
	min-height:69.5%;
		
		}


}

</style>


<div class="bg-parallax parallax-overlay accounts-page min-height" data-jarallax='{"speed": 0.2}' style='background-image: url("images/bg6.jpg")'>
	<div class="container">
		<div class="row pb30">
			<div class="col-lg-6 col-md-6 mr-auto ml-auto col-sm-8">
				<h4 ><span class="text-blue"><?php echo lang('Login_heading');?></span><br /><span style="font-size:12px;color:#a8a8a8;"><?php echo lang('Login_subheading');?></span></h4>

				<br />
				<form action="<?php echo base_url();?>/account/checkLogin" method="post" class="form-horizontal form_data" autocomplete="off" data-parsley-validate>
                    <div class="alert  validatio-msg" style="display: none;">
                    </div>
                    <div class="form-group">
						<input type="email" name="email" class="form-control text-field-border" placeholder="<?php echo lang('register_email');?>" autocomplete="off">
					</div>
					<div class="form-group">
						<input type="password" class="form-control text-field-border" name="password" placeholder="<?php echo lang('Login_password');?>">
					</div>
					<div class="form-group" align="center">
						<button type="submit" class="btn btn-success btn-md"><?php echo lang('Login_button');?></button>
					</div>
					<div class="text-center text-blue"><a href="#" data-toggle="modal" data-target="#myModalForgotPassword" class="btn text-blue"><?php echo lang('Login_forgot');?></a></div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="container pt20 pb20">
	<div class="row">
		<div class="col-lg-12">

			<p align="center" class="text-align-center">
				<span class="text-blue"><?php echo lang('Login_noaccount');?>? </span><span><a href="#"  data-toggle="modal" data-target="#myModal" class="text-success"><?php echo lang('Login_register');?></a></span>

			</p>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-body">


					<div class="col-lg-12">

						<div class="mb40 pt10" align="center">

							<h4 class="text-blue"><?php echo lang('register_heading');?></h4>



						</div><!--/col-->

					</div>

                    <form action="<?php echo base_url();?>account/signup" method="post" onsubmit="return false;"  class="form-horizontal form_data" autocomplete="off" data-parsley-validate>
                        <div class="alert validatio-msg" style="display: none;"></div>
					<div class="col-lg-12 text-field-column">

						<div class="mb10 mt10">


							<div class="input-group">
								<input type="text" name="full_name" class="form-control required text-field-border" placeholder="<?php echo lang('register_fullname');?>" data-parsley-required="true">
							</div>

						</div><!--/col-->

					</div>

					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
								<input type="text" name="username" class="form-control required text-field-border username check_unique" placeholder="<?php echo lang('register_username');?>">
							</div>

						</div><!--/col-->



					</div>



					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
								<input type="email" name="email" class="form-control required text-field-border email check_unique" placeholder="<?php echo lang('register_email');?>">
							</div>

						</div><!--/col-->
					</div>

					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
                                 
                                                               
								<input type="text" name="phone" class="form-control required text-field-border" data-mask="0999999999"  placeholder="<?php echo lang('register_phone');?>">
                                 <span class="font-13 text-muted" style="margin:10px;">(05XXXXXXXX)</span>
							</div>

						</div><!--/col-->
					</div>

					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
								<input type="text" name="city" id="pac-input" class="form-control required text-field-border" placeholder="<?php echo lang('register_city');?>">
							</div>

						</div><!--/col-->
					</div>

					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">


							<div class="input-group">
								<input type="text" name="country" class="form-control required text-field-border" value="Saudi Arabia" placeholder="<?php echo lang('register_country');?>" readonly>
							</div>

						</div><!--/col-->
					</div>

					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
								<input type="password" name="password" class="form-control required text-field-border" placeholder="<?php echo lang('register_password');?>">
							</div>

						</div><!--/col-->
					</div>



					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
								<input type="password" name="confirm_password" class="form-control required text-field-border" placeholder="<?php echo lang('register_confirmpassword');?>">
							</div>

						</div><!--/col-->
					</div>


					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
								<input type="radio" name="gender" value="male" style="margin-left:23px !important" checked> <?php echo lang('register_male');?><br>
								<input type="radio" name="gender" value="female"> <?php echo lang('register_female');?><br>
							</div>

						</div><!--/col-->
					</div>


					<div class="col-lg-12 text-field-column" align="center">
                        <div class="g-recaptcha" data-sitekey="6LcR41UUAAAAABO2-XMM_V4gPdzreT3BbZbozYNk"></div><br>
						<button class="btn btn-filter btn-primary submit_reg_form_btn" type="submit"><?php echo lang('register_submit');?></button>

					</div>
                    </form>
				</div>

			</div>
		</div>
	</div><!--/modal-->
    
    
    	<!-- Modal -->
	<div class="modal fade" id="myModalForgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalForgotPassword" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-body">


					<div class="col-lg-12">

						<div class="mb40 pt10" align="center">

							<h4 class="text-blue">Forgot Password</h4>



						</div><!--/col-->

					</div>

                    <form action="<?php echo base_url();?>account/sendForgotPasswordEmail" method="post" onsubmit="return false;"  class="form-horizontal sendForgotPasswordEmail" autocomplete="off">
                        <div class="alert validatio-msg" style="display: none;"></div>
					
                    <div class="col-lg-12 text-field-column">
					<div class="mb10 mt10">
							<div class="input-group">
								<input type="email" name="email" class="form-control text-field-border" placeholder="Email Address" data-parsley-required="true">
							</div>

						</div><!--/col-->

					</div>

					

					<div class="col-lg-12 text-field-column" align="center">

						<button class="btn btn-filter btn-primary forgot_password_btn" type="submit"><?php echo lang('register_submit');?></button>

					</div>
                    </form>
				</div>

			</div>
		</div>
	</div><!--/modal-->

</div>


<!--<div class="pac-card" id="pac-card">-->
<!--    <div>-->
<!--        <div id="title">-->
<!--            Countries-->
<!--        </div>-->
<!--        <div id="country-selector" class="pac-controls">-->
<!--            <input type="radio" name="type" id="changecountry-usa">-->
<!--            <label for="changecountry-usa">USA</label>-->
<!---->
<!--            <input type="radio" name="type" id="changecountry-usa-and-uot" checked="checked">-->
<!--            <label for="changecountry-usa-and-uot">USA and unincorporated organized territories</label>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div id="pac-container">-->
<!--        <input id="pac-input" type="text" placeholder="Enter a location">-->
<!--    </div>-->
<!--</div>-->
<!--<div id="map"></div>-->
<!--<div id="infowindow-content">-->
<!--    <img src="" width="16" height="16" id="place-icon">-->
<!--    <span id="place-name"  class="title"></span><br>-->
<!--    <span id="place-address"></span>-->
<!--</div>-->
<link href="<?php echo base_url('assets/front/');?>css/whoweare.css" rel="stylesheet" />
<style>

@media (min-height:992px)  and (max-height: 1199px) {
	
	.min-height{
	
	min-height:73.5%;
		
		}


}



@media (min-height:1200px)  and (max-height: 1600px) {
	
	.min-height{
	
	min-height:76%;
		
		}


}


</style>



<script></script>


<div class="pt90 pb90 min-height">
	<div class="container-fluid">
		<div class="row pt60">
			<div class="col-lg-12">

				<h3 class="text-red">
					<?php echo lang('whoweare_heading');?>

				</h3>
			</div>

			<div class="col-lg-12 pt30 pb30">
				<div class="row">



					<div class="w3-sidebar w3-bar-block w3-light-grey w3-card col-lg-3 col-sm-12 col-xs-12">
						<button class="w3-bar-item w3-button tablink text-light-gray" onclick="redirect('page/who_we_are');"><b><?php $profile = getPageData(1); echo ($this->session->lang == 'en' ? $profile->page_title_en : $profile->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink text-light-gray" onclick="redirect('page/commitment');"><b><?php $commitment = getPageData(2); echo ($this->session->lang == 'en' ? $commitment->page_title_en : $commitment->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink  text-light-gray" onclick="redirect('page/mission');"><b><?php $mission = getPageData(3); echo ($this->session->lang == 'en' ? $mission->page_title_en : $mission->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink text-light-gray" onclick="redirect('page/partner');"><b><?php $partner = getPageData(4);echo ($this->session->lang == 'en' ? $mission->page_title_en : $mission->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink text-light-gray" onclick="redirect('page/awards');"><b><?php $awards = getPageData(5); echo ($this->session->lang == 'en' ? $awards->page_title_en : $awards->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink  text-light-gray" onclick="redirect('page/team');"><b><?php $teams = getPageData(6); echo ($this->session->lang == 'en' ? $teams->page_title_en : $teams->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink text-blue text-light-gray" onclick="redirect('page/career');"><b><?php $careers = getPageData(7); echo ($this->session->lang == 'en' ? $careers->page_title_en : $careers->page_title_ar ); ?></b></button>
					</div>

					<div class="col-lg-9 col-sm-12 col-xs-12 tabs-content">

				        <div id="item7" class="w3-container content">


							<div>
								<div class="container-fluid">

									<div class="col-lg-12 col-sm-12 col-xs-12 pb10">
                                        <?php 
                                    $careers = getPageData(7);
                                    
                                    
                                    ?>
										<h3 class="text-blue">
											<?php echo ($this->session->lang == 'en' ? $careers->page_title_en : $careers->page_title_ar ); ?>

										</h3>

										<?php echo ($this->session->lang == 'en' ? $careers->page_description_en : $careers->page_description_ar ); ?>

									</div>
									<form action="<?php echo base_url();?>page/career_apply" method="post" class="form-horizontal form_data" autocomplete="off" enctype="multipart/form-data">

										<div class="alert  validatio-msg" style="display: none;"></div>

										<div class="col-lg-6">

										<div class="mb20 mt20">

											<div class="input-group">
												<input type="text" name="full_name" class="form-control required text-field-border"
                                                       placeholder="<?php echo ($this->session->lang == 'en' ? 'Full Name' : 'الاسم الكامل'); ?>" required>
											</div>

										</div><!--/col-->

									</div>


									<div class="col-lg-6">
										<div class="mb20 mt20">

											<div class="input-group">
												<input type="email" name="email" class="form-control required text-field-border" placeholder="<?php echo ($this->session->lang == 'en' ? 'Email' : 'البريد الإلكتروني'); ?>" required>
											</div>

										</div><!--/col-->



									</div>



									<div class="col-lg-6">
										<div class="mb20 mt20">

											<div class="input-group">
												<input type="text" name="phone" class="form-control required text-field-border" placeholder="<?php echo ($this->session->lang == 'en' ? 'Mobile' : 'التليفون المحمول'); ?>" required>
											</div>

										</div><!--/col-->
									</div>

									<div class="col-lg-6">
										<div class="mb20 mt20">

											<div class="input-group">
												<input type="text" name="nationality" class="form-control required text-field-border" placeholder="<?php echo ($this->session->lang == 'en' ? 'Nationality' : 'جنسية'); ?>" required>
											</div>

										</div><!--/col-->
									</div>


									<div class="col-lg-6">
										<div class="mb20 mt20">

											<div class="input-group">
												<input type="file" name="file[]" class="form-control required text-field-border" accept=".pdf,.doc,.docx" required>
											</div>

										</div><!--/col-->
									</div>

									<div class="col-lg-12">
                                        <div class="g-recaptcha" data-sitekey="6LcR41UUAAAAABO2-XMM_V4gPdzreT3BbZbozYNk"></div><br>
										<button class="btn btn-filter btn-primary" type="submit"><?php echo ($this->session->lang == 'en' ? 'Submit' : 'خضع'); ?></button>

									</div>
									</form>
								</div>
							</div>

						</div>

					</div>



				</div>

			</div>

		</div>


	</div>
</div>

<script>
    function openWhoweare(evt, Name) {
        var i, x, tablinks;
        x = document.getElementsByClassName("content");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" text-blue", " text-light-gray");
        }


        document.getElementById(Name).style.display = "block";

        // $(".tab").addClass("active"); // instead of this do the below
        evt.currentTarget.className += " text-blue";
    }
</script>
<div class="pt90 pb90">
    <div class="container-fluid">
        <div class="row pt60">
        
        
<!-- Heading -->    
            <div class="col-lg-12">
<?php 
                                    $become_a_seller = getPageData(8);
                                    
                                    
                                    ?>
                <h3 class="text-red">
                   <?php echo ($this->session->lang == 'en' ? $become_a_seller->page_title_en : $become_a_seller->page_title_ar ); ?>

                </h3>
                
                
              </div>  
                


<!-- Left Colum Form -->    
              
               <div class="col-lg-6 pt30"> 
                 <form action="<?php echo base_url();?>page/become_a_seller_apply" method="post" class="form-horizontal form_data" autocomplete="off" enctype="multipart/form-data">   
                <div class="alert  validatio-msg" style="display: none;"></div>
                 <div class="col-lg-12">


                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="text" name="company_name" class="form-control required text-field-border" placeholder="<?php echo lang('company_name');?>" required>
                                        </div>

                                    </div><!--/col-->

                                </div>


                                <div class="col-lg-12">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="text" name="director_name" class="form-control required text-field-border" placeholder="<?php echo lang('directory_name');?>" required>
                                        </div>

                                    </div><!--/col-->



                                </div>



                                <div class="col-lg-12">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="text" name="mobile" class="form-control required text-field-border" placeholder="<?php echo lang('mobile');?>" >
                                        </div>

                                    </div><!--/col-->
                                </div>
                                
                                
                                 <div class="col-lg-12">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="email" name="email" class="form-control required text-field-border" placeholder="<?php echo lang('customer_email');?>" required>
                                        </div>

                                    </div><!--/col-->
                                </div>

                                <div class="col-lg-12">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="text" name="phone" class="form-control required text-field-border" placeholder="<?php echo lang('tel');?>">
                                        </div>

                                    </div><!--/col-->
                                </div>
                                
                                 <div class="col-lg-12">
                                    <div class="mb20 mt20">

                                        <div class="input-group">
                                            <input type="text" name="fax" class="form-control required text-field-border" placeholder="<?php echo lang('fax');?>">
                                        </div>

                                    </div><!--/col-->
                                </div>


                                <div class="col-lg-12">
                                    <div class="mb20 mt20">
 <p><?php echo lang('pdf_upload_bocome'); ?>

 </p>
                                        <div class="input-group">
                                       
                                            <input type="file" name="file[]" class="form-control required text-field-border" placeholder="Upload PDF">
                                        </div>

                                    </div><!--/col-->
                                </div>



            
              <div class="col-lg-12">
                  <div class="g-recaptcha" data-sitekey="6LcR41UUAAAAABO2-XMM_V4gPdzreT3BbZbozYNk"></div><br>
                    <button class="btn btn-filter btn-success" type="submit"><?php echo lang('send');?></button>   
                       

            </div>
                   </form>
            
            </div>
            
            
            <div class="col-lg-6 pt30">

			<div class="col-lg-12" id="reseller-right">
            <?php echo ($this->session->lang == 'en' ? $become_a_seller->page_description_en : $become_a_seller->page_description_ar );?>
			</div>
            
            
           


               
            </div>

          

        </div>

        
    </div>
</div>

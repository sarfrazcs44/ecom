
<div class=" pt90 pb20 grey">
	<div class="container">
		<div class="row pt90">
			<div class="col-lg-10">
                <?php if($product) {?>
				<h3 class="text-blue">
					<a href="<?php echo base_url() ?>page/products"><img src="<?php echo base_url('assets/front') ?>/images/back.png" /></a>
					<?php if($product['category_title_en']) { ?>
						<?php echo ($this->session->lang == 'en') ? $product['category_title_en'] : $product['category_title_ar'] ?>
                    <?php } ?>
					<?php if($product['sub_category_title_en']) { ?>
                     /
                    <b><?php echo ($this->session->lang == 'en') ? $product['sub_category_title_en'] : $product['sub_category_title_ar'] ?></b>
					<?php } ?>
				</h3>
                <?php } ?>
			</div>

			<div class="col-lg-2">

				<h3 class="text-blue">
                    <span class="input-group-btn" style="float:right;">
                        <a href="<?php echo base_url()?>page/products"><button class="btn btn-product btn-primary" type="button"><?php echo lang('productdetail_button');?></button></a>
                    </span>

				</h3>
			</div>


		</div>
	</div>

</div>

<div class=" pt50 pb50">

	<div class="container">
        <div class="row msg alert" style="display:none;">
            <div class="col-lg-12 msg_text">
                
            </div>
        </div>    
		<div class="row">
           
			<div class="col-lg-2">

                <?php if($product['product_images']) { ?>
				<div id="js-pagination-slider">
                    <?php foreach($product['product_images'] as $image){   ?>
					<div class="cbp-pagination-item cbp-pagination-active">
						<img src="<?php echo base_url() ?><?php echo $image['product_image'] ?>" alt="">
					</div>
                    <?php } ?>
				</div>
                <?php } ?>

			</div>


			<div class="col-lg-6">
				<?php if($product['product_images']) { ?>
				<div id="js-grid-slider-thumbnail" class="cbp">
					<?php foreach($product['product_images'] as $image){   ?>
					<div class="cbp-item">
						<div class="cbp-caption">
							<div class="cbp-caption-defaultWrap">
								<img src="<?php echo base_url() ?><?php echo $image['product_image'] ?>" alt="">
							</div>
						</div>
					</div>
                    <?php } ?>
				</div>
                <?php } ?>
				<!--thumbnails-->

			</div>



			<div class="col-lg-4">
				<h3 class="text-blue">
					<?php echo ($this->session->lang == 'en') ? $product['brand_title_en'] : $product['brand_title_ar'] ?>
				</h3>
				<p><b><?php echo ($this->session->lang == 'en') ? $product['model_en'] : $product['model_ar'] ?></b></p>
				<p><?php echo ($this->session->lang == 'en') ? $product['product_title_en'] : $product['product_title_ar'] ?></p>

				<h4 class="text-blue">
					<?php echo ($this->session->lang == 'en' ? 'Description' : 'وصف'); ?>
				</h4>
				<p><?php echo ($this->session->lang == 'en') ? $product['product_description_en'] : $product['product_description_ar'] ?></p>

				<h4 class="text-blue">
                    <?php echo ($this->session->lang == 'en' ? 'Specifications' : 'مواصفات'); ?>
				</h4>
				<p>
                    <?php if($product['product_specifications']) {?>
                    <ul>
                        <?php foreach ($product['product_specifications'] as $item) { ?>
                                <li><?php echo ($this->session->lang == 'en') ? $item['specification_title_en'] : $item['specification_title_ar'] ?></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </p>

				<p style="display: inline-flex;">
					<b class="text-red"><?php echo (stristr($product['price'], '.') == false) ? $product['price'].'.00' : $product['price'] ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'ريال سعودي'); ?></b>
                    <span style="color: black;margin-<?php echo ($this->session->lang == 'en' ? 'left' : 'right'); ?>: 30px;"><strong><?php echo ($this->session->lang == 'en' ? 'Quantity' : 'كمية'); ?>:</strong> <input type="number" style="width: 50px;text-align: center;" value="1" id="quantity_field" min="1"></span>
					<span style="float: <?php echo ($this->session->lang == 'en' ? 'right' : 'left'); ?> !important;margin-<?php echo ($this->session->lang == 'en' ? 'left' : 'right'); ?>: 20px;">
                       <a href="javascript:void(0)" data-pro-id = "<?php echo $product['product_id'];?>" class="btn font-button btn-sm btn-rounded btn-success add_to_cart"><?php echo lang('add_to_cart');?></a>
                  </span>
				</p>


			</div>
		</div>
	</div>

</div>

<div class="text-center text-blue">
    <a href="#" data-toggle="modal" data-target="#addToCartConfirmationModal" class="btn text-blue" id="openAddToCartConfirmationModal"></a>
</div>

<div class="modal fade" id="addToCartConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="addToCartConfirmationModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">


                <div class="col-lg-12">

                    <div class="mb40 pt10" align="center">

                        <h4 class="text-blue"><?php echo ($this->session->lang == 'en' ? 'Added to cart successfully' : 'تمت الإضافة إلى السلة بنجاح'); ?></h4>

                    </div><!--/col-->

                </div>

                <div class="col-lg-12" style="text-align: center;">
                    <a href="javascript:void(0);" id="close_modal"><button class="btn btn-success" type="button"><?php echo ($this->session->lang == 'en' ? 'Continue Shopping' : 'مواصلة التسوق'); ?></button></a>
                    <a href="<?php echo base_url('checkout'); ?>"><button class="btn btn-primary" type="button"><?php echo ($this->session->lang == 'en' ? 'Proceed To Checkout' : 'باشرالخروج من الفندق'); ?></button></a>
                </div>




            </div>

        </div>
    </div>
</div>









<?php if($related) { ?>
<div class=" pt50 pb50 grey">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h4 class="text-blue">
					<?php echo lang('productdetail_related');?>
				</h4>
			</div>

			<?php foreach($related as $product) { ?>
                <div class="col-lg-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s">
                    <div class="entry-card">
                        <a href="<?php echo base_url()?>page/product_details?id=<?php echo $product['product_id']?>" class="entry-thumb product-thumb">
                            <?php if(isset($product['product_images'][0]['product_image'])){
                                    $image = base_url() . $product['product_images'][0]['product_image'];
                         }else{
                                $image = '';
} ?>
                            <img src="<?php echo $image; ?>" alt="" class="img-fluid img-full">
                            <span class="thumb-hover ti-back-right"></span>
                        </a><!--/entry thumb-->
                        <div class="entry-content">
                            <div class="entry-meta mb5" align="center">
                                <span><?php echo ($this->session->lang == 'en') ? $product['category_title_en'] : $product['category_title_ar'] ?></span>
                            </div>
                            <h4 class="entry-title text-capitalize" align="center">
                                <a href="<?php echo base_url()?>page/product_details?id=<?php echo $product['product_id']?>">
									<?php echo ($this->session->lang == 'en') ? $product['model_en'] : $product['model_ar'] ?>
                                </a>
                            </h4>
                            <p align="center">
                                <a href="<?php echo base_url()?>page/product_details?id=<?php echo $product['product_id']?>" class="btn font-button btn-md btn-rounded btn-success"><span><?php echo (stristr($product['price'], '.') == false) ? $product['price'].'.00' : $product['price'] ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'ريال سعودي'); ?></span><span style="margin-left:10px !important;"></span></a><br>
                            </p>

                        </div><!--/entry content-->
                    </div>
                </div><!--/.col-->
			<?php } ?>

		</div>
	</div>
</div>
<?php } ?>
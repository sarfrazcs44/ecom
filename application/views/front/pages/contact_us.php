<style>
    .address-div h1,
    .address-div h2,
    .address-div h3,
    .address-div h4,
    .address-div h5,
    .address-div h6,
    .address-div strong {
        color: #1c4191 !important;
        padding-bottom: 20px !important;
    }
</style>

<?php
                                    $contact = getPageData(11);
                                    
                                    
                                    ?>
<div class=" pt70">
    <div class="fluid-container">
        <iframe 
  width="100%" 
  height="450" 
  frameborder="0" 
  style="border:0" allowfullscreen
  src="https://maps.google.com/maps?q=<?php echo $contact->lat;?>,<?php echo $contact->lng;?>&hl=es;z=14&amp;output=embed"
 >
 </iframe>
       

</div>

        <div class=" pt60 pb60 ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">

                        <h3 class="text-red">
                             <?php echo ($this->session->lang == 'en' ? $contact->page_title_en : $contact->page_title_ar ); ?>

                        </h3>
                        <?php echo ($this->session->lang == 'en' ? $contact->page_description_en : $contact->page_description_ar ); ?>
                    </div>

                </div>

               <div class="row pt30 pb30">
                    

                        <div class="col-lg-4 mb30 mt30 wow fadeInUp address-div" data-wow-delay=".2s">
                            <?php echo ($this->session->lang == 'en' ? $contact->address1_en : $contact->address1_ar ); ?>
                        </div><!--/.col-->

                   <div class="col-lg-4 mb30 mt30 wow fadeInUp address-div" data-wow-delay=".2s">
                      <?php echo ($this->session->lang == 'en' ? $contact->address2_en : $contact->address2_ar ); ?>
                   </div><!--/.col-->

                        
                   <div class="col-lg-4 mb30 mt30 wow fadeInUp address-div" data-wow-delay=".2s">
                       <?php echo ($this->session->lang == 'en' ? $contact->address3_en : $contact->address3_ar ); ?>
                   </div><!--/.col-->


                   


         

                    <!--/.col-->



                </div>
                <div class="row pt30 pb30">


                    <div class="col-lg-4 mb30 mt30 wow fadeInUp address-div" data-wow-delay=".2s">



                        <?php echo ($this->session->lang == 'en' ? $contact->address4_en : $contact->address4_ar ); ?>


                    </div><!--/.col-->



                    <div class="col-lg-4 mb30 mt30 wow fadeInUp address-div" data-wow-delay=".2s">



                        <?php echo ($this->session->lang == 'en' ? $contact->address5_en : $contact->address5_ar ); ?>


                    </div><!--/.col-->


                    <div class="col-lg-4 mb30 mt30 wow fadeInUp address-div" data-wow-delay=".2s">



                        <?php echo ($this->session->lang == 'en' ? $contact->address6_en : $contact->address6_ar ); ?>


                    </div><!--/.col-->







                    <!--/.col-->



                </div>
            </div>
        </div>
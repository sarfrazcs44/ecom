<div class="bg-parallax parallax-overlay accounts-page" data-jarallax='{"speed": 0.2}' style='background-image: url("images/bg6.jpg")'>
	<div class="container">
		<div class="row pb30">
			<div class="col-lg-6 col-md-6 mr-auto ml-auto col-sm-8">
				<h4 ><span class="text-blue"><?php echo lang('verify_account');?></span><br /></h4>

				<br />
				<form action="<?php echo base_url();?>/account/verify_account" method="post" class="form-horizontal form_data" autocomplete="off" data-parsley-validate>
                    <div class="alert  validatio-msg" style="display: none;">
                    </div>
                    <div class="form-group">
						<input type="text" name="verification_code" class="form-control text-field-border" placeholder="<?php echo lang('verification_code');?>" autocomplete="off">
					</div>
					
					<div class="form-group" align="center">
						<button type="submit" class="btn btn-success btn-md"><?php echo lang('verify_account');?></button>
						<button type="button" class="btn btn-success btn-primary" onclick="resendCode(<?php echo $this->session->userdata('user_id_to_verify'); ?>);">Resend Code</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>




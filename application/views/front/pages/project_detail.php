<?php 
$page_data = getPageData($page_id);
$project_images = getPageImages($page_id);
?>
<div class=" pt90 pb90 grey">
	<div class="container">
        
		<div class="row pt90">
            <div class="col-lg-12">
				
				<h3 class="text-blue">
					<a href="<?php echo base_url()?>page/projects"><img src="<?php echo base_url('assets/front')?>/images/back.png" /></a>
				</h3>
				
			</div>
            <div class="col-lg-12">

				<h3 class="text-blue"><?php echo $page_data->page_title_en;;?></h3>
			</div>
            

			<?php if($project_images) { ?>
            <?php foreach($project_images as $project_image) { ?>
			<div class="col-lg-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s">
				<div class="entry-card">
				
                        <?php $image = base_url() . $project_image->image; ?>
                        <a data-fancybox="gallery" href="<?php echo $image; ?>"><img src="<?php echo $image; ?>" alt="" class="img-fluid"></a>
				</div>
			</div><!--/.col-->
            <?php } } ?>
		</div>
	</div>
</div>
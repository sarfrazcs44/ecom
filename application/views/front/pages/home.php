<style>

    .cbp-l-grid-mosaic-flat .cbp-l-caption-title:after {
        content: '';
        display: none !important;
        width: 40%;
        height: 1px;
        background-color: #fff;
        margin: 8px auto 0;
    }

    @media (min-width: 1600px)  and (max-width: 2000px) {

        .sticky-footer {

            position: absolute;
            padding-left: 0px !important;
            padding-right: 0px !important;
            bottom: inherit;

        }
    }

    img.img-fluid.circle.inline-display.client {
        width: 128px !important;
        height: 128px;
    }

</style>
<div class="container-fluid no-padding">

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->

        <?php if ($ads) { ?>
            <ol class="carousel-indicators">
                <?php $i = 0;
                foreach ($ads as $ad) { ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>"
                        class="<?php ($i == 0) ? 'active' : '' ?>"></li>
                    <?php $i++;
                } ?>
            </ol>
        <?php } ?>

        <!-- Wrapper for slides -->
        <?php if ($ads) { ?>
            <div class="carousel-inner">
                <?php $i = 0;
                foreach ($ads as $key => $ad) { ?>
                    <div class="item <?php echo ($i == 0 ? 'active' : ''); ?>">
                        <img class="slider-img-class" src="<?php echo base_url() . $ad['image'] ?>"
                             alt="Los Angeles" style="width:100%;">
                        <img class="overlay-class" src="<?php echo base_url() ?>assets/front/images/overlay.png"
                             style="width:100%;">
                        <div class="slider-text">
                            <h1 style="text-align:center !important;color: white;"><?php echo ($this->session->lang == 'en') ? $ad['ad_title_en'] : $ad['ad_title_ar'] ?></h1>
                            <p style="color: white;text-align:center !important;"><?php echo ($this->session->lang == 'en') ? $ad['ad_description_en'] : $ad['ad_description_ar'] ?></p>
                            <a style="text-align:center !important;"
                               href="<?php echo base_url() ?>page/product_details?id=<?php echo $ad['product_id'] ?>"
                               class="btn-btn-slider-sm btn btn-lg btn-blue-outline"><?php echo ($this->session->lang == 'en' ? 'Shop Now' : 'تسوق الآن'); ?></span></a>
                        </div>
                    </div>
                    <?php $i++;
                } ?>

                <!--                <div class="item">-->
                <!--                    <img src="-->
                <?php //echo base_url('assets/front')?><!--/img/003.jpg" alt="Chicago" style="width:100%;">-->
                <!--                </div>-->
                <!---->
                <!--                <div class="item">-->
                <!--                    <img src="-->
                <?php //echo base_url('assets/front')?><!--/img/004.jpg" alt="New york" style="width:100%;">-->
                <!--                </div>-->

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        <?php } ?>
    </div>
</div>


<!-- END REVOLUTION SLIDER -->
<?php
$profile = getPageData(1);


?>
<div class="bg-parallax parallax-overlay" data-jarallax='{"speed": 0.2}'
     style='background-image: url("<?php echo base_url('assets/front') ?>/images/bg6.jpg")'>
    <div class="container pt60 pb60">
        <div class="row">
            <div class="col-lg-12">

                <h3 class="text-blue">
                    <?php echo lang('home_top_section_heading'); ?>

                </h3>
                <!--<p><?php /*echo lang('home_top_section_paragraph'); */ ?></p>-->
                <p>
                    <?php
                    $description = $this->session->lang == 'en' ? $profile->page_description_en : $profile->page_description_ar;
                    if ($this->session->lang == 'en')
                    {
                        echo truncate_text($profile->page_description_en, 330);
                    }else{
                        // not truncating text here because it was not truncating arabic text
                        echo $profile->page_description_ar;
                    }
                    /*$description = explode('. ', $description);
                    echo $description[0].'.'; // first line
                    echo $description[1].'.'; // second line*/
                    // echo truncate_text($description, 330);
                    ?>
                </p>
                <a href="<?php echo base_url('page/who_we_are'); ?>" class="btn btn-lg btn-blue-outline">
                    <?php echo lang('home_top_section_button'); ?>
                </a>
            </div>

        </div>
    </div>
</div><!--cta-->


<div class="container pt90 pb90">


    <!--            <div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter">
                    <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
                        All <div class="cbp-filter-counter"></div>
                    </div>
                    <div data-filter=".print" class="cbp-filter-item">
                        Print <div class="cbp-filter-counter"></div>
                    </div>
                    <div data-filter=".web-design" class="cbp-filter-item">
                        Web Design <div class="cbp-filter-counter"></div>
                    </div>
                    <div data-filter=".graphic" class="cbp-filter-item">
                        Graphic <div class="cbp-filter-counter"></div>
                    </div>
                    <div data-filter=".motion" class="cbp-filter-item">
                        Motion <div class="cbp-filter-counter"></div>
                    </div>
                </div>-->

    <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat">

        <?php foreach ($homeimages as $homeimage) { ?>

            <div class="cbp-item web-design graphic">
                <a href="<?php echo($homeimage['url'] != '' ? $homeimage['url'] : 'javascript:void(0);'); ?>" <?php echo($homeimage['is_new_tab'] == 1 ? 'target="_blank"' : ''); ?>
                   class="cbp-caption" data-title="Bolt UI<br>by Tiberiu Neamu">
                    <div class="cbp-caption-defaultWrap">
                        <img src="<?php echo base_url() . $homeimage['image'] ?>" alt="">
                    </div>
                    <div class="cbp-caption-activeWrap">
                        <div class="cbp-l-caption-alignCenter">
                            <div class="cbp-l-caption-body">
                                <div class="cbp-l-caption-title"><?php echo ($this->session->lang == 'en') ? $homeimage['title_en'] : $homeimage['title_ar'] ?>
                                    <br>
                                    <small style="text-decoration: underline;"><?php echo ($this->session->lang == 'en') ? 'Read More' : 'اقرأالمزيد'; ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        <?php } ?>


    </div>


</div>


<div class="container-fluid fluid-zero">
    <!--<iframe style="width:100%;height:70vh;" src="https://www.youtube.com/embed/i_waF9r7Fmc" frameborder="0"
            allow="autoplay; encrypted-media" allowfullscreen></iframe>-->
    <iframe style="width:100%;height:70vh;" src="https://www.youtube.com/embed/<?php echo homeVideoURL(); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>


<div class=" pt90 pb90 grey">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <h3 class="text-blue"><?php echo lang('home_filter_heading'); ?></h3>
            </div>


            <div class="col-lg-3">
                <div class="mb30 mt30">
                    <form method="get" action="<?php echo base_url() ?>page/products">
                        <div class="input-group">
                            <select name="category" class="form-control main_categories"
                                    data-lang="<?php echo($this->session->lang) ?>">
                                <option value="-1"><?php echo lang('home_filter_category'); ?>...</option>
                                <?php if ($categories) {
                                    foreach ($categories as $category) { ?>
                                        <option value="<?php echo $category['category_id'] ?>">
                                            <?php echo ($this->session->lang == 'en') ? $category['title_en'] : $category['title_ar'] ?>
                                        </option>
                                    <?php }
                                } ?>
                            </select>
                        </div>

                </div><!--/col-->
            </div>

            <div class="col-lg-3">
                <div class="mb30 mt30">
                    <div class="input-group">
                        <select name="sub_category" class="form-control sub_categories">
                            <option value="-1"><?php echo lang('home_filter_subvategory'); ?>...</option>
                            <!--							--><?php //if($categories){  foreach ($categories as $category) { ?>
                            <!--                                <option value="-->
                            <?php //echo $category['category_id'] ?><!--">-->
                            <!--									--><?php //echo ($this->session->lang == 'en') ? $category['title_en'] : $category['title_ar'] ?>
                            <!--                                </option>-->
                            <!--							--><?php //} } ?>
                        </select>
                    </div>

                </div><!--/col-->
            </div>

            <div class="col-lg-3">
                <div class="mb30 mt30">

                    <div class="input-group">
                        <select name="brand_id" class="form-control">
                            <option value="-1"><?php echo lang('home_filter_brand'); ?>...</option>
                            <?php if ($brands) {
                                foreach ($brands as $brand) { ?>
                                    <option value="<?php echo $brand['brand_id'] ?>">
                                        <?php echo ($this->session->lang == 'en') ? $brand['title_en'] : $brand['title_ar'] ?>
                                    </option>
                                <?php }
                            } ?>

                        </select>
                    </div>

                </div><!--/col-->
            </div>

            <div class="col-lg-3">
                <div class="mb30 mt30">
                        <span class="input-group-btn" style="float:right;">
                            <button class="btn btn-filter btn-primary"
                                    type="submit"><?php echo lang('home_filter_button'); ?></button>
                        </span>
                </div><!--/col-->
            </div>
            </form>


            <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                    <div class="col-lg-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s">
                        <div class="entry-card">
                            <a href="<?php echo base_url() ?>page/product_details?id=<?php echo $product['product_id'] ?>"
                               class="entry-thumb product-thumb">
                                <?php if (isset($product['product_images'][0]['product_image'])) {
                                    $img_url = base_url() . $product['product_images'][0]['product_image'];
                                } else {
                                    $img_url = '';
                                }
                                ?>
                                <img src="<?php echo $img_url; ?>" alt="" class="img-fluid img-full">
                                <span class="thumb-hover ti-back-right"></span>
                            </a><!--/entry thumb-->
                            <div class="entry-content">
                                <div class="entry-meta mb5" align="center">
                                    <span><?php echo ($this->session->lang == 'en') ? $product['category_title_en'] : $product['category_title_ar'] ?></span>
                                </div>
                                <h4 class="entry-title text-capitalize" align="center">
                                    <a href="#">
                                        <?php echo ($this->session->lang == 'en') ? $product['model_en'] : $product['model_ar'] ?>
                                    </a>
                                </h4>
                                <p align="center">
                                    <a href="<?php echo base_url() ?>page/product_details?id=<?php echo $product['product_id'] ?>"
                                       class="btn font-button btn-md btn-rounded btn-success">
                                        <span><?php echo (stristr($product['price'], '.') == false) ? $product['price'] . '.00' : $product['price'] ?>
                                            SAR</span><span style="margin-left:10px !important;"></span>
                                    </a><br>
                                </p>

                            </div><!--/entry content-->
                        </div>
                    </div><!--/.col-->
                <?php } ?>
            <?php } ?>


            <div class="col-lg-12 pt0" align="center">
                <a href="<?php echo base_url() ?>page/products" class="btn btn-sm btn-blue-outline">
                    <?php echo lang('home_load_more'); ?>
                </a>
            </div>

        </div>
    </div>
</div>


<!--<div class="container pt90 pb90">
	<div class="row">
		<div class="col-lg-12">

			<h3 class="text-blue">
				<?php /*echo lang('home_ourclients');*/ ?>
			</h3>
		</div>
	</div>
	<br>
	<ul class="clients-grid mb0 items10 clearfix">
		<?php /*$images   = getPageImages(10);
              if($images){
                  foreach($images as $image){
                      if($image->image != '' && file_exists($image->image))
        */ ?>
        <li><img src="<?php /*echo base_url($image->image)*/ ?>" class="img-fluid" alt="Clients"></li>
        <?php /*} } */ ?>
		

		

	</ul>
</div>-->


<!--<div class="container pt90 pb90">
    <div class="row">
        <div class="col-lg-12">

            <h3 class="text-blue">
                <?php /*echo lang('home_ourclients');*/ ?>
            </h3>
        </div>
    </div>
    <br>
    <ul class="clients-grid mb0 items10 clearfix">
        <?php /*$images   = getPageImages(10);
        if($images){
            foreach($images as $image){
                if($image->image != '' && file_exists($image->image))
                    */ ?>
                    <li><img src="<?php /*echo base_url($image->image)*/ ?>" class="img-fluid" alt="Clients"></li>
            <?php /*} } */ ?>




    </ul>
</div>-->

<div class="container pt90 pb90" style="display: none;">
    <div class="row">
        <div class="col-lg-12">

            <h3 class="text-blue">
                <?php echo lang('home_ourclients'); ?>
            </h3>
        </div>
    </div>
    <br>
    <!-- Carousel -->
    <div class="carousel-testimonial owl-carousel owl-theme carousel-dark testimonial-direction">

        <div class="item">
            <div class=" testimonial testimonial-dark">
                <div class="row align-items-center">
                    <?php $images = getPageImages(10);
                    if ($images){
                    $i = 0;
                    foreach ($images

                    as $image){
                    if ($image->image != '' && file_exists($image->image)){

                    if ($i % 6 == 0 && $i != 0){ ?>

                </div>
            </div>
        </div>
        <div class="item">
            <div class=" testimonial testimonial-dark">
                <div class="row align-items-center">
                    <?php } ?>
                    <div class="col-lg-2" style="display:inherit;">
                        <div class="col-lg-12">
                            <!--<img src="<?php /*echo base_url($image->image); */
                            ?>" alt=""
                                 class="img-fluid circle inline-display client">-->
                            <img src="<?php echo base_url($image->image); ?>" alt=""
                                 class="inline-display client" width="70" height="80">
                        </div>
                    </div>

                    <?php $i++;
                    }
                    }
                    } ?>
                </div>
            </div>
        </div><!--item-->


    </div> <!-- /Carousel -->
</div>


<div class="container pt90 pb90">
    <div class="row">
        <div class="col-lg-12">

            <h3 class="text-blue">
                <?php echo lang('home_ourclients'); ?>
            </h3>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class='marquee' data-direction='left' dir="ltr">
                <?php
                $images = getPageImages(10);
                if ($images) {
                    foreach ($images as $image) {
                        if ($image->image != '' && file_exists($image->image)) {
                            ?>
                            <img src="<?php echo base_url($image->image); ?>" alt="" class="inline-display client" width="150">
                        <?php }
                    }
                } ?>
            </div>
        </div>
    </div>
</div>


<!--portfolio-->


<div class="bg-primary pt90 pb90 grey testimonial-direction">
    <div class="container ">
        <!-- Carousel -->
        <div class="carousel-testimonial owl-carousel owl-theme carousel-dark testimonial-direction">

            <div class="item">
                <div class=" testimonial testimonial-dark">
                    <div class="row align-items-center">

                        <?php if ($testimonials){
                        $i = 0;
                        foreach ($testimonials

                        as $testimonial){

                        if ($i % 3 == 0 && $i != 0){ ?>

                    </div>
                </div>
            </div>
            <div class="item">
                <div class=" testimonial testimonial-dark">
                    <div class="row align-items-center">
                        <?php } ?>


                        <div class="col-lg-4" style="display:inherit;">

                            <div class="col-lg-4">
                                <img src="<?php echo base_url($testimonial->image); ?>" alt=""
                                     class="img-fluid circle inline-display">
                            </div>
                            <div class="col-lg-8">
                                <h4 class="heading-testimonial"><?php echo($this->session->lang == 'en' ? $testimonial->title_en : $testimonial->title_ar); ?></h4>
                                <h6 class="sub-testimonial"><?php echo($this->session->lang == 'en' ? $testimonial->position_en : $testimonial->position_ar); ?></h6>
                                <p class="para-testimonial"><?php echo($this->session->lang == 'en' ? $testimonial->description_en : $testimonial->description_ar); ?></p>
                            </div>

                        </div>

                        <?php $i++;
                        }
                        } ?>


                    </div>
                </div>
            </div><!--item-->


        </div> <!-- /Carousel -->
    </div>
</div><!--/.testimonials-->
<!--news-->
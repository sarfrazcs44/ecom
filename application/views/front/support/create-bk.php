<div class="pt90 pb90">
	<div class="container">
    
    <?php $this->load->view('front/layouts/usertabs');?>
    
		<div class="row pt60" style="width: 60%; margin: 0 auto">
			<form action="<?php echo base_url();?>/support/create_ticket" method="post" class="GeoDetails form-horizontal form_data" autocomplete="off" onsubmit="return false;">
					<div class="col-lg-12">

						<div class="mb40 pt10" align="center">

							<h4 class="text-blue"><?php echo lang('reg_com');?></h4>

						</div><!--/col-->

					</div>
				<div class="alert  validatio-msg" style="display: none;">
				</div>

					<div class="col-lg-12 text-field-column">

						<div class="mb10 mt10">


							<div class="input-group">
								<select name="ticket_type" class="form-control ticket_type field-color">
									<option value="complaint"><?php echo lang('complain');?></option>
									<option value="visit"><?php echo lang('visit');?></option>
								</select>
							</div>
                            <!--<div class="input-group">
                                <input type="hidden" name="ticket_type" value="complaint">
                            </div>-->

						</div><!--/col-->

					</div>



					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">
							<div class="input-group">
								<input type="text" name="city" id="pac-input" class="form-control field-color required text-field-border" placeholder="<?php echo lang('customer_city');?>">
							</div>
						</div><!--/col-->
					</div>
                <?php if($complain_types){ ?>
					<div class="col-lg-12 text-field-column complain_fields">
						<div class="mb10 mt10">
							<div class="input-group">
								<select name="complaint_type_id" class="form-control field-color">
									<?php foreach ($complain_types as $type) { ?>
										<option value="<?php echo $type->complaint_type_id ?>"><?php echo $type->complaint_title_en ?></option>
									<?php } ?>
								</select>
							</div>
						</div><!--/col-->
					</div>
                    <?php } ?>
                    <?php if($visit_complain_types){ ?>
					<div class="col-lg-12 text-field-column visit_complain_fields">
						<div class="mb10 mt10">
							<div class="input-group">
								<select name="visit_complaint_type_id" class="form-control visit_complains_select field-color">
									<?php foreach ($visit_complain_types as $type) { ?>
										<option value="<?php echo $type->visit_complaint_type_id ?>"><?php echo $type->visit_complaint_title_en ?></option>
									<?php } ?>
								</select>
							</div>
						</div><!--/col-->
					</div>
                    <?php } ?>

					<!--<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">
							<div class="input-group">
								<input type="text" name="product" class="form-control field-color required text-field-border" placeholder="Product/Brand">
							</div>
						</div>
					</div>-->

					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
								<input type="text" name="location" class="form-control field-color required text-field-border" data-geo="formatted_address" placeholder="Pin your Location" data-toggle="modal" data-target="#locationModal">
							</div>

						</div><!--/col-->
					</div>

				<!--<div class="col-lg-12 text-field-column visit_complain_fields">
					<div class="mb10 mt10">

						<div class="input-group">
							<input type="text" name="apartment_no" class="form-control field-color required text-field-border" placeholder="Appartment No">
						</div>

					</div>
				</div>-->


				<!--<div class="col-lg-12 text-field-column visit_complain_fields">
						<div class="mb10 mt10">

							<div class="input-group">
								<input type="text" name="visit_time" class="form-control field-color flatpicker_time required text-field-border" placeholder="<?php /*echo lang('visit_date');*/?>">
							</div>

						</div>
					</div>-->

					<div class="col-lg-12 text-field-column">
						<div class="mb10 mt10">

							<div class="input-group">
								<textarea name="message" class="form-control field-color" rows="5" cols="70" placeholder="<?php echo lang('customer_message');?>"></textarea>
							</div>

						</div><!--/col-->
					</div>
					<input name="lat" type="hidden" data-geo="lat" value="">
					<input name="lng" type="hidden" data-geo="lng" value="">

					<div class="col-lg-12 text-field-column" align="center">

						<button class="btn btn-filter btn-success" type="submit"><?php echo lang('customer_submitbutton');?></button>

					</div>
			</form>
		</div>
	</div>
</div>



<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<input type="text" class="form-control geocomplete_map_draggable">
				<a class="btn confirm-loc-btn btn-success" data-dismiss="modal"><?php echo lang('confirm');?></a>
			</div>
			<div class="modal-body">
				<div class="address_map"></div>
			</div>
		</div>
	</div>
</div><!--/modal-->

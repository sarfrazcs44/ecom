<section>
        <div class="container">
            <div class="height-header d-flex align-items-center">
                <div class="row w-100 py-3">
                    <div class="col-md-12 text-center">
                        <h1 class="text-dark mb-3">Please Fill Registration the form</h1>
                        
                    </div>
                    <div class="col-md-8 mx-auto pt-4">
                        <!--Form with header-->
                        <form action="<?php echo base_url('niehez/shop/action');?>" method="post" class="form-horizontal form_data" autocomplete="off" enctype="multipart/form-data" onsubmit="return false;">
                        <input type="hidden" name="form_type" value="register">
                        <input type="hidden" name="package_id" value="<?php echo encode_url($package_id); ?>">
                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                            <div class="card">
                                <div class="card-body p-3">
    
                                    <!--Body-->
                                    <div class="form-group pb-3 border-bottom">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-user text-danger "></i></div>
                                            </div>
                                            <input type="text" class="form-control" id="txtCompName" name="title_en" placeholder="<?php echo lang('company_name'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-hand-o-right text-danger "></i></div>
                                            </div>
                                            <input type="text" class="form-control" id="domain_name" name="domain_name" placeholder="<?php echo lang('domain_name'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-user text-danger "></i></div>
                                            </div>
                                            <input type="text" class="form-control" id="txtUserName" name="full_name" placeholder="<?php echo lang('register_fullname'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-address-book text-danger "></i></div>
                                            </div>
                                            <input type="text" class="form-control" id="txtUsrName" name="username" placeholder="<?php echo lang('register_username'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-envelope text-danger "></i></div>
                                            </div>
                                            <input type="email" class="form-control" id="txtEmail" name="email" placeholder="<?php echo lang('register_email'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-key text-danger "></i></div>
                                            </div>
                                            <input type="password" class="form-control" id="txtPassword" name="password" placeholder="<?php echo lang('register_password'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-key text-danger "></i></div>
                                            </div>
                                            <input type="password" class="form-control" id="txtPassword" name="confirm_password" placeholder="<?php echo lang('register_confirmpassword'); ?>" required>
                                        </div>
                                    </div>
    
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-comment text-danger "></i></div>
                                            </div>
                                            <textarea class="form-control" placeholder="<?php echo lang('register_description'); ?>" name="description_en" required></textarea>
                                        </div>
                                    </div>
    
                                    <div class="text-center">
                                        <input type="submit" value="Submit" class="btn btn-success btn-rounded text-white mb-1 w-auto px-5">
                                    </div>
                                </div>
    
                            </div>
                        </form>
                        <!--Form with header-->
                    </div>
                </div>
            </div>
        </div>
    </section>
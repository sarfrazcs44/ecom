<section class="mainWrapper text-center">
      <div class="container">
        <h1 class="m-0">Compatible Packages</h1>
        <p class="m-0 text-muted">No surprises, no hidden fees or charges</p>
        <div class="mt-5 pt-5">
            <div class="row">
            	<?php if($packages){ 
            				foreach ($packages as $key => $value) { ?>
            					<div class="col-md-4">
				                    <div class="card mb-3">
				                        <div class="card-body">
				                          <h5 class="card-title text-danger"><?php echo  ($this->session->lang == 'en' ? $value->title_en : $value->title_ar ); ?></h5>
				                          <p class="card-text text-muted"><?php echo  ($this->session->lang == 'en' ? $value->description_en : $value->description_ar ); ?></p>
				                          <h1 class="text-success mt-5 mb-0"><?php echo $value->price; ?> SAR</h1>
				                          <p class="small text-success mb-4">Monthly</p>
				                          <a href="<?php echo base_url('niehez/shop/start/'.encode_url($value->package_id)); ?>" class="btn btn-success rounded text-white mb-2 w-100 d-block">Get Started</a>
				                          <a href="#" class="text-muted">Learn More</a>
				                        </div>
				                      </div>
				                </div>
            				

            	<?php } } ?>
                
                
            </div>
        </div>
      </div>
 </section>
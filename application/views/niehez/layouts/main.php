<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/niehez/images/favicon.png');?>">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url('assets/niehez/images/favicon.ico');?>">
        <title>Niehez</title>
        <!--<link rel="stylesheet" href="css/jquery-ui_1.12.1_themes.css">-->
        <!--<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" media="all">-->
        <!--<link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">-->
        <!--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">-->
        <link href="<?php echo base_url('assets/niehez/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css" media="all">
        <link href="<?php echo base_url('assets/niehez/css/ltr.css');?>" rel="stylesheet" type="text/css" media="all">
        <script src="<?php echo base_url('assets/niehez/js/jquery-2.1.3.min.js');?>"></script>
        <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false&controls=1"></script>-->
        <!--<script src="js/jquery-ui_1.12.1.js"></script>-->
    </head>
    <body class="packages text-white" style="background-image: url(<?php echo base_url('assets/niehez/images/imageBg.jpg');?>);">
    <header>
        <div class="container d-flex align-items-center justify-content-center mt-5 mb-5 pb-5">
            <a href="#." class="logo"><img src="<?php echo base_url('assets/niehez/images/niehez.svg');?>" alt="Logo" height="50" width="218.50"></a>
        </div>
    </header>
    <script>
        
        var base_url = '<?php echo base_url(); ?>';
    </script>

    <?php $this->load->view($view); ?>
    

<script src="<?php echo base_url('assets/niehez/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('assets/niehez/js/bootstrap.bundle.min.js');?>"></script>
<!--<script type="text/javascript" src="js/jquery.imgzoom-min.js"></script>-->
<!--<script type="text/javascript" src="js/jquery.fancybox.js"></script>-->
<script src="<?php echo base_url('assets/niehez/js/script.js');?>"></script>
<script src="<?php echo base_url();?>assets/js/script.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.blockUI.js"></script>
</body>
</html>
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Edit Email Template</h4>
                               
                                <form action="<?php echo base_url();?>cms/email_template/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="email_template_id" value="<?php echo $email_template_id;?>">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                
                                    <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="title_en" id="title-eng" required value="<?php echo $result->title_en; ?>">
                                     </div>

                                      <div class="form-group">
                                       <label for="title-eng">Subject Eng * :</label>
                                       <input type="text" class="form-control" name="subject_en" id="title-eng" required value="<?php echo $result->subject_en; ?>">
                                     </div>

                                     <div class="form-group">
                                       <label>Description Eng :</label>
                                        <textarea class="form-control editor" rows="5" name="description_en"><?php echo $result->description_en; ?></textarea>
                                                   
                                   </div>
                                     
                                   
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="title_ar" id="title-arb" required="" value="<?php echo $result->title_ar; ?>">
                                     </div>
                                     <div class="form-group">
                                       <label for="title-arb">Subject Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields " name="subject_ar" id="title-arb" required="" value="<?php echo $result->subject_ar; ?>">
                                     </div>

                                     <div class="form-group">
                                       <label>Description Arb : </label>
                                        <textarea class="form-control arabic-cms-fields editor_ar" rows="5" name="description_ar"><?php echo $result->description_ar; ?></textarea>
                                                   
                                   </div>
                                    
                                    <div class="form-group" style="overflow:hidden">
                                        <span style="float: left;margin-right: 15px;">Is Active&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="checkbox" id="switch7" switch="primary" <?php echo ($result->is_active == 1 ? 'checked=""' : ''); ?> name="is_active">
                                        <label style="display: inline-block;float: left;" for="switch7" data-on-label="Yes" data-off-label="No"></label>
                                       
                                    </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                    </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    

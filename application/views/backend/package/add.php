<div class="content-page" style="margin-top:10px;">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Add Package</h4>
                    
                    <form action="<?php echo base_url();?>cms/package/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                        <input type="hidden" name="form_type" value="save">
                        <div class="alert" id="validatio-msg" style="display: none;">
                        </div> 
                            <div class="form-group">
                            <label for="title-eng">Title Eng * :</label>
                            <input type="text" class="form-control" name="title_en" id="title-eng" required value="">
                            </div>
                            <div class="form-group">
                            <label>Description Eng :</label>
                            <textarea class="form-control" rows="5" name="description_en"></textarea>
                                        
                        </div>
                        <div class="form-group">
                            <label for="title-eng">Price * :</label>
                            <input type="text" class="form-control" name="price" id="price" required value="">
                            </div>
                            
                        
                        
                        <div class="form-group">
                            <label for="title-arb">Title Arb * :</label>
                            <input type="text" class="form-control arabic-cms-fields" name="title_ar" id="title-arb" required="" value="">
                            </div>
                            <div class="form-group">
                            <label>Description Arb : </label>
                            <textarea class="form-control arabic-cms-fields" rows="5" name="description_ar"></textarea>
                                        
                        </div>
                        
                        
                        <div class="form-group" style="overflow:hidden">
                            <span style="float: left;margin-right: 15px;">Is Active&nbsp;&nbsp;&nbsp;&nbsp;</span>
                            <input type="checkbox" id="switch7" switch="primary" checked="" name="is_active">
                            <label style="display: inline-block;float: left;" for="switch7" data-on-label="Yes" data-off-label="No"></label>
                            
                        </div>
                        <div class="form-group">
                            <a  data-toggle="collapse" href="#packagesList" role="button" aria-expanded="false" aria-controls="packagesList">Select Module </a>
                        </div>
                        <div class="collapse in" id="packagesList">
                            <div class="card card-body mb-4">
                                <div class="row">
                                    <?php $menu_items = getMenuItems(); 
                                             foreach ($menu_items as $key => $main_menu) { ?>
                                                <?php if($main_menu['parent_id'] == 0){ ?>
                                                 <div class="col-md-4 mb-3 py-2 position-relative haveBefore">
                                                    <label class="w-100 m-0 px-2 pb-0 text-danger" for="<?php echo $main_menu['module_id']; ?>"><input type="checkbox" id="<?php echo $main_menu['module_id']; ?>"  data-id = "<?php echo $main_menu['module_id']; ?>" class="w-auto mr-2 parent parent-<?php echo $main_menu['module_id']; ?>" name="module_id[]" value="<?php echo $main_menu['module_id']; ?>"><?php echo $main_menu['title_en']; ?></label>
                                                    <?php 
                                                    foreach ($menu_items as $sub_menu) { 
                                                                 if ($sub_menu['parent_id'] == $main_menu['module_id']) {
                                                        ?>
                                                    <label class="w-100 m-0 px-4 pb-0" for="firstChkBox_<?php echo $sub_menu['module_id']; ?>"><input type="checkbox" name="module_id[]" value="<?php echo $sub_menu['module_id']; ?>" data-parent-id="<?php echo $main_menu['module_id']; ?>" id="firstChkBox_<?php echo $sub_menu['module_id']; ?>" class="w-auto mr-2 child child-<?php echo $main_menu['module_id']; ?>"><?php echo $sub_menu['title_en']; ?></label>
                                                <?php }  } ?>
                                                </div>
                                                <?php } ?>
                                             
                                             <?php }

                                    ?>
                                   
                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                Submit
                            </button>
                            
                            
                        </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- end col -->
                
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
<script language="JavaScript">
   $(document).ready(function(){
        $('.parent').on('click',function(){
            var parent_id  = $(this).attr('data-id');

            if($(this).is(":checked")){
                $('.child-'+parent_id).prop('checked', true);

            }else{
                 $('.child-'+parent_id).prop('checked', false);

            }
        });

        $('.child').on('click',function(){
                
                var parent_id  = $(this).attr('data-parent-id');
                var $boxes = $('.child-'+parent_id+':checked');

                
                if($boxes.length > 0){

                    $('.parent-'+parent_id).prop('checked', true);

                }else{
                    $('.parent-'+parent_id).prop('checked', false);
                }

        });

   });
</script>
<style>
.collapse.in {
    display: block !important;
}
</style>
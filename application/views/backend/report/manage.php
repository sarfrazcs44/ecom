<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xs-12">
                                <form action="" method="post" style="padding-bottom: 105px;">
                                <div class="row">
                                    
                                    <div class="col-md-4">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label" for="DepositDate ">From</label>
                                            <input type="text" name="from" class="form-control datepicker" value="<?php echo (isset($post_data['from']) ? $post_data['from'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label" for="DepositDate ">To</label>
                                            <input type="text" name="to" class="form-control datepicker" value="<?php echo (isset($post_data['to']) ? $post_data['to'] : ''); ?>">
                                        </div>
                                    </div>
                                   
                                    
                                   
                                    
                                    
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                               Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            </div>
                        </div>


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Sales</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                       
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Order Track ID</th>
                                            <th>Transaction ID</th>
                                            <th>Customer Name</th>
                                            <th>Email</th>
                                            <th>Total Item</th>
                                            <th>Total Amount</th>
                                            <th>Vat</th>
                                            <th>Delivery Charges</th>
                                            
                                           
                                            
                                            <th>Created At</th>
                                           
                                             
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($orders){
                                                foreach($orders as $order){ ?>
                                                    <tr id="<?php echo $order->order_id;?>">
                                                    
                                                    <td><?php echo $order->order_track_id; ?></td>
                                                    <td><?php echo ($order->transaction_id == '' ? 'N/A' : $order->transaction_id); ?></td>
                                                    <td><?php echo $order->full_name; ?></td>
                                                    <td><?php echo $order->email; ?></td>
                                                    <td><?php echo $order->total_items; ?></td>
                                                    <td><?php echo $order->total_amount; ?></td>
                                                    <td><?php echo $order->vat_total; ?></td>
                                                    <td><?php echo $order->delivery_charges; ?></td>
                                                    
                                                    
                                                    <td><?php echo $order->order_date; ?></td>
                                                        
                                                    <td>
                                                       
                                                        <a href="<?php echo base_url('cms/order/view/'.$order->order_id);?>">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                        </td> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->

<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <h4 class="page-title">Ticket Types</h4>
                            <ol class="breadcrumb p-0 m-0">
                                
                                <li>
                                <a href="<?php echo base_url('cms/complaint_type/add').($parent_id > 0 ? '/'.$parent_id : '');?>">
                                    <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Ticket Type</button>
                                </a>
                                </li>
                                
                                <!-- <li>
                                    <a href="#">Tables </a>
                                </li>
                                <li class="active">
                                    Responsive Table
                                </li>-->
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th>Title Eng</th>
                                            <th>Title Arb</th>
                                            <th>Is Active</th>

                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($types){
                                                foreach($types as $type){ ?>
                                                    <tr id="<?php echo $type->complaint_type_id;?>">
                                                    
                                                    <th><?php echo $type->complaint_title_en; ?></th>
                                                    <th><?php echo $type->complaint_title_ar; ?></th>
                                                    <th><?php echo ($type->is_active == 1 ? 'Yes' : 'No'); ?></th> 

                                                    <th>
                                                        <?php
                                                            if ($parent_id == 0)
                                                            { ?>
                                                                <a href="<?php echo base_url('cms/complaint_type/index/'.$type->complaint_type_id);?>" title="Click to see its child ticket types">
                                                                    <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                            <?php }
                                                        ?>
                                                        <a href="<?php echo base_url('cms/complaint_type/edit/'.$type->complaint_type_id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $type->complaint_type_id;?>','cms/complaint_type/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <style>
@import url("https://fonts.googleapis.com/css?family=Cairo");                
                    table.table-bordered.dataTable tbody tr th:nth-child(2) {
                         font-family: "Cairo";
}
                </style>
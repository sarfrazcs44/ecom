<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add Ticket Type</h4>
                               
                                <form action="<?php echo base_url();?>cms/complaint_type/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="save">
                                    <input type="hidden" name="parent_id" value="<?php echo $parent_id; ?>">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                     <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="complaint_title_en" id="title-eng" required value="">
                                     </div>
                                    
                                   
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="complaint_title_ar" id="title-arb" required="" value="">
                                     </div>

                                    <?php
                                    if ($parent_id == 0)
                                    { ?>
                                        <div class="form-group">
                                            <label for="placeholder-eng">Placeholder Eng * :</label>
                                            <input type="text" class="form-control" name="placeholder_en" id="placeholder-eng" required value="">
                                        </div>
                                        <div class="form-group">
                                            <label for="placeholder-arb">Placeholder Arb * :</label>
                                            <input type="text" class="form-control arabic-cms-fields" name="placeholder_ar" id="placeholder-arb" required="" value="">
                                        </div>
                                    <?php }
                                    ?>

                                    <div class="form-group" style="overflow:hidden">
                                        <span style="float: left;margin-right: 15px;">Is Active&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="checkbox" id="switch7" switch="primary" checked="" name="is_active">
                                        <label style="display: inline-block;float: left;" for="switch7" data-on-label="Yes" data-off-label="No"></label>
                                       
                                    </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        <a href="javascript:void(0);" onclick="window.history.back();">
                                            <button type="button" class="btn btn-danger waves-effect waves-light">
                                                Cancel
                                            </button>
                                        </a>
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    

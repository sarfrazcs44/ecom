<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <h4 class="page-title">Delivery Users</h4>
                <ol class="breadcrumb p-0 m-0">

                    <li>
                        <a href="<?php echo base_url('cms/delivery_user/add');?>">
                            <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Delivery User</button>
                        </a>
                    </li>

                    <!-- <li>
                            <a href="#">Tables </a>
                        </li>
                        <li class="active">
                            Responsive Table
                        </li>-->
                </ol>
                <div class="clearfix"></div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                        <h4 class="m-t-0 header-title">&nbsp;</h4>

                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                        <table id="datatable-responsive"
                               class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>

                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact No.</th>
                                <th>Assigned Districts</th>
                                <th>Otp</th>
                                <th>Status</th>
                                <th>Created At</th>

                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if($delivery_users){
                                foreach($delivery_users as $user){ 
                                    $user_status = array('0' => 'Pending','1' => 'Approved', '2' => 'Rejected');
                                    ?>
                                    <tr id="<?php echo $user->user_id;?>">

                                        <th><?php echo $user->full_name; ?></th>
                                        <th><?php echo $user->email; ?></th>
                                        <th><?php echo $user->phone; ?></th>
                                        <th><?php echo assigned_districts($user->branch_id); ?></th>
                                        <th><?php echo $user->otp; ?></th>
                                        <th><?php echo $user_status[$user->is_approved]; ?></th>
                                        <th><?php echo $user->created_at; ?></th>

                                        <th>

                                            <a href="<?php echo base_url('cms/delivery_user/edit/'.$user->user_id);?>">
                                                <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                            <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $user->user_id;?>','cms/delivery_user/delete','')">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                            <?php if($user->is_approved == 0){ ?>    
                                                <a href="javascript:void(0);" onclick="approveUser('<?php echo $user->user_id;?>','cms/delivery_user/approveUser','',1)">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> Approve User </button></a>
                                                <a href="javascript:void(0);" onclick="approveUser('<?php echo $user->user_id;?>','cms/delivery_user/approveUser','',2)">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> Reject User </button></a>
                                            <?php }elseif($user->is_approved == 1){ ?>
                                                <a href="javascript:void(0);" onclick="approveUser('<?php echo $user->user_id;?>','cms/delivery_user/approveUser','',2)">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> Reject User </button></a>
                                            <?php }elseif($user->is_approved == 2){ ?>

                                                <a href="javascript:void(0);" onclick="approveUser('<?php echo $user->user_id;?>','cms/delivery_user/approveUser','',1)">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> Approve User </button></a>
                                            <?php } ?>   
                                        </th>
                                    </tr>
                                    <?php
                                }

                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div> <!-- container -->

    </div> <!-- content -->
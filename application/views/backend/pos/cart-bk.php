<div class="content-page" style="margin-top:10px;">
<!-- Start content -->
<div class="content">
   <div class="container-fluid">
      <div class="card-box">
         <div class="row">
            <div class="col-md-7">
               <h2 class="hedTitle">Point of Sales</h2>
               <div class="drp-Dn">
                  <select name="category" class="cAt drDn">
                     <option>Category</option>
                  </select>
                  <select name="subcategory" class="sUbcAt drDn">
                     <option>Sub Category</option>
                  </select>
                  <div class="add-to-cart">
                     <button type="button" href="#" class="cart-Btn"><i class="fa fa-shopping-cart"></i></button>
                  </div>
               </div>
         <!--Products-->
         <div class="row">
            <div class="col-md-12 pdct">
               <h2 class="hedTitle">Products</h2>
               <div class="row prdt-r">
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="products-ctalog">
                        <img src="/uploads/product_images/dummy-img.png" class="img-responsive productImg">
                        <p class="text-center pTitle">Product Title<br>Model - 220 SAR</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
            <!-- end col -->
            <div class="col-md-5 bill-invoice">
               <div class="row">
                  <div class="col-md-12">
                     <h2 class="text-left hedTitle">Bill Invoice 
                        <span class="text-right ord-no">Order No.</span>
                     </h2>
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <div class="card-boxe">
                        <table 
                           class="tbl-cart dt-responsive nowrap" cellspacing="0" width="100%">
                           <thead>
                              <tr>
                                 <th class="text-left">Products</th>
                                 <th class="text-center">Quantity</th>
                                 <th class="text-right">Total</th>
                                 <th class="text-center"></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>Item Title - Model</td>
                                 <td>
                                    <div class="qty-btns">
                                       <button type="button" class="btn-plus qty-btn">+</button>    
                                       <input type="number" name="quantity" value="1" class="qty-pdt">
                                       <button type="button" class="btn-minus qty-btn">-</button>
                                    </div>
                                 </td>
                                 <td class="text-center">20.00 SAR</td>
                                 <td class="text-center">
                                    <p class="cls-icon">x</p>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Item Title - Model</td>
                                 <td>
                                    <div class="qty-btns">
                                       <button type="button" class="btn-plus qty-btn">+</button>    
                                       <input type="number" name="quantity" value="1" class="qty-pdt">
                                       <button type="button" class="btn-minus qty-btn">-</button>
                                    </div>
                                 </td>
                                 <td class="text-center">20.00 SAR</td>
                                 <td class="text-center">
                                    <p class="cls-icon">x</p>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Item Title - Model</td>
                                 <td>
                                    <div class="qty-btns">
                                       <button type="button" class="btn-plus qty-btn">+</button>    
                                       <input type="number" name="quantity" value="1" class="qty-pdt">
                                       <button type="button" class="btn-minus qty-btn">-</button>
                                    </div>
                                 </td>
                                 <td class="text-center">20.00 SAR</td>
                                 <td class="text-center">
                                    <p class="cls-icon">x</p>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Item Title - Model</td>
                                 <td>
                                    <div class="qty-btns">
                                       <button type="button" class="btn-plus qty-btn">+</button>    
                                       <input type="number" name="quantity" value="1" class="qty-pdt">
                                       <button type="button" class="btn-minus qty-btn">-</button>
                                    </div>
                                 </td>
                                 <td class="text-center">20.00 SAR</td>
                                 <td class="text-center">
                                    <p class="cls-icon">x</p>
                                 </td>
                              </tr>
                              <tr>
                                 <td>Item Title - Model</td>
                                 <td>
                                    <div class="qty-btns">
                                       <button type="button" class="btn-plus qty-btn">+</button>    
                                       <input type="number" name="quantity" value="1" class="qty-pdt">
                                       <button type="button" class="btn-minus qty-btn">-</button>
                                    </div>
                                 </td>
                                 <td class="text-center">20.00 SAR</td>
                                 <td class="text-center">
                                    <p class="cls-icon">x</p>
                                 </td>
                              </tr>
                           </tbody>
                           <tfoot>
                              <tr class="ttl-cost" style="
                                 border: 0;
                                 ">
                                 <td colspan="4" style="
                                    padding: 0;
                                    ">
                                    <div style="
                                       display: block;
                                       width: 100%;
                                       border: 1px solid gray;
                                       border-radius: 10px;
                                       ">
                                       <table style="
                                          width: 100%;
                                          ">
                                          <tfoot>
                                             <tr class="ttl-cost" style="
                                                border: 0;
                                                ">
                                                <td>Grand Total</td>
                                                <td></td>
                                                <td></td>
                                                <td style="
                                                   text-align: revert;
                                                   ">160.00 SAR</td>
                                             </tr>
                                          </tfoot>
                                       </table>
                                    </div>
                                 </td>
                              </tr>
                           </tfoot>
                        </table>
                     </div>
                     <button type="button" class="sbmt" href="#">Submit</button>            
                  </div>
               </div>
            </div>
         </div>
         <!-- end row -->
      </div>
   </div>
   <!-- container -->
</div>
<!-- content -->
<style>
   .drp-Dn {
   display: flex;
   margin-top: 30px;
   }
   select.cAt {
   margin-right: 20px;
   }
   .drDn {
   width: 45%;
   border-radius: 20px;
   padding: 5px;
   border-color: gray;
   font-weight:500;
   padding-left: 25px;
   }
   .productImg {
   display: block;
   width: 150px;
   margin: auto;
   }
   .ord-no {
   float: right;
   font-size: 16px;
   margin-top: 15px;
   }
   table.tbl-cart {
   /*border: 1px solid;*/
   }
   table.tbl-cart thead tr th {
   padding: 12px;
   font-size: 20px;
   }
   table.tbl-cart tbody tr td {
   padding: 15px 8px;
   font-size: 15px;
   font-weight: 500;
   }
   table.tbl-cart tbody tr {
   border-bottom: 1px solid;
   }
   .cls-icon, .qty-btn {
   width: 30px;
   height: 30px;
   text-align: center;
   border: 1px solid;
   line-height: 1.8;
   border-radius: 20px;
   background: transparent;
   margin: auto;
   }
   .qty-pdt {
   background: transparent;
   border: none;
   text-align: center;
   font-size: 15px;
   width:70px;
   }
   .pTitle {
   padding-top:20px;
   font-weight: 500;
   line-height: 18px;
   }
   .hedTitle {
   margin-top:15px;
   }
   .card-boxe {
   border: 1px solid;
   padding: 20px;
   border-radius: 15px;
   }
   tr.ttl-cost {
   border: 1px solid;
   border-radius: 15px !important;
   }
   tfoot tr.ttl-cost td {
   padding: 15px;
   font-weight: 600;
   font-size: 15px;
   }    
   body {
   color: gray;
   }
   a.dropdown-item {
   display: block;
   padding: 10px;
   width: 300px;
   color: gray;
   }
   .hedTitle {
   color: gray;
   }
   .qty-btns {
   display: flex;
   }
   input[type=number]::-webkit-inner-spin-button, 
   input[type=number]::-webkit-outer-spin-button { 
   -webkit-appearance: none;
   }
   .prdt-r {
   margin-bottom: 25px;
   }
   table.tbl-cart tbody tr:last-child {
   border: 0;
   }
   select {
   /* for Firefox */
   -moz-appearance: none;
   /* for Chrome */
   -webkit-appearance: none;
   }
   /* For IE10 */
   select::-ms-expand {
   display: none;
   }
   .add-to-cart {
   display: block;
   margin-right: 20px;
   margin-left: auto;
   }
   button.cart-Btn {
   background: transparent;
   width: 100px;
   padding: 10px;
   border-radius: 30px;
   border: none;
   font-weight: 600;
   color: #fff;
   }
   .fa-shopping-cart:hover::before {
   color: #f5707a;
   }
   .fa-shopping-cart:before {
   content: "\f07a";
   font-size: 24px;
   color: #4bd396;
   }
   button.sbmt {
   margin-top: 20px;
   display: block;
   margin-right: 30px;
   margin-left: auto;
   background: transparent;
   border: none;
   background-color: #4bd396;
   padding: 10px 50px;
   color: #fff;
   font-weight: bold;
   font-size: 18px;
   border-radius: 30px;
   }
   button.sbmt:hover {
   background-color: #f5707a;
   }
   .card-box {
    height: 610px;
    overflow: hidden;
    overflow-y: scroll;
}
.bill-invoice {
    position: sticky;
    top: 0;
}
::-webkit-scrollbar {
    width: 0px;
    background: transparent; /* make scrollbar transparent */
}
   @media (max-width:1280px) {
   .pdct {
   width: 100%;
   }
   .bill-invoice {
   width: 100%;
   float:none;
   }
   }
   @media (max-width: 425px) {
   table.tbl-cart thead tr th {
   padding: 7px;
   font-size: 18px;
   }
   table.tbl-cart tbody tr td {
   padding: 5px;
   font-size: 15px;
   font-weight: 500;
   }
   .cls-icon, .qty-btn {
   width: 20px;
   height: 20px;
   text-align: center;
   border: 1px solid;
   line-height: 0.8;
   border-radius: 20px;
   background: transparent;
   margin: auto;
   }
   tfoot tr.ttl-cost td {
   padding: 10px;
   font-weight: 600;
   font-size: 16px;
   }
   .qty-pdt {
   width:30px;
   }
   button.sbmt {
   margin: 20px auto;
   }
   .cls-icon {
   line-height: 1.1;
   }
   button.btn-plus {
   padding-left: 5px;
   }
   .card-boxe {
   padding: 10px;
   }
   .drp-Dn {
   display: block;
   }
   select.cAt {
   margin-right: 20px;
   margin-bottom: 20px;
   }
   .drDn {
   width: 100%;
   }
   button.cart-Btn {
   display: block;
   margin: 10px 0px 0px auto;
   }
   }
</style>
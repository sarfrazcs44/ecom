<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Cash Flow</h4>
                               
                                <form action="<?php echo base_url();?>cms/pos/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="save">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                <div class="trs-typ">
                                    <div class="form-group trs-type">
                                        <label for="type">Trasaction Type :</label>
                                        <select id="type" name="type" class="form-control " required="">
                                            <option value="Credit">Credit</option>
                                            <option value="Debit">Debit</option>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>


                                     <div class="form-group trs-type trs-by">
                                       <label for="title-eng">Transaction By * :</label>
                                       <input type="text" class="form-control" name="transaction_user" id="transaction_user" required value="">
                                     </div>
                                    </div> 

                                     <div class="form-group">
                                       <label>Description / Purpose :</label>
                                        <textarea class="form-control" rows="5" name="description"></textarea>
                                                   
                                   </div>

                                    <div class="form-group">
                                    <label for="image"> Invoice :</label>
                                    <input type="file" class="filestyle img-frm" id="image" name="image[]" data-placeholder="No Image" required="">
                                    </div>
                                    
                                   <div class="form-group">
                                       <label for="title-eng">Total Amount * :</label>
                                       <input type="text" class="form-control" name="total_amount" id="total_amount" required value="">
                                     </div>


                                     
                                    
                                   
                                    
                                    
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light btn-clr">
                                            Submit
                                        </button>
                                        
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    

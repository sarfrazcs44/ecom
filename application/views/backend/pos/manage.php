<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xs-12">
                                <form action="" method="post" style="padding-bottom: 105px;">
                                <div class="row">
                                    
                                    <div class="col-md-4">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label" for="DepositDate ">From</label>
                                            <input type="text" name="from" class="form-control datepicker" value="<?php echo (isset($post_data['from']) ? $post_data['from'] : ''); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group label-floating is-focused">
                                            <label class="control-label" for="DepositDate ">To</label>
                                            <input type="text" name="to" class="form-control datepicker" value="<?php echo (isset($post_data['to']) ? $post_data['to'] : ''); ?>">
                                        </div>
                                    </div>
                                   
                                    
                                   
                                    
                                    
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                               Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            </div>
                        </div>


                         <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                   
                                    <ol class="breadcrumb p-0 m-0">
                                       
                                        <li>
                                        <a href="<?php echo base_url('cms/pos/add');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Entry</button>
                                        </a>
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Cashflow</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                       
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table 
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Transaction</th>
                                            <th>Done By</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                            <th>Invoice</th>
                                            <th>Grand Total</th>
                                            
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($results){
                                        $grand_total = 0;
                                                foreach($results as $result){
                                                    if($result->type == 'Debit'){
                                                        $grand_total = $grand_total - $result->total_amount;
                                                    }else{
                                                        $grand_total = $grand_total + $result->total_amount;
                                                    }

                                                 ?>
                                                    <tr id="<?php echo $result->point_of_sale_id;?>">
                                                    
                                                    <td><?php echo $result->type; ?></td>
                                                    <td><?php echo $result->transaction_user; ?></td>
                                                    <td><?php echo $result->description; ?></td>
                                                    <td>
                                                        <?php if($result->type == 'Debit'){ ?>

                                                            <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $result->point_of_sale_id;?>','cms/pos/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                                        

                                                        <?php } ?>
                                                    </td>
                                                    <td> 
                                                        <?php if($result->order_id > 0){ ?>
                                                        <a href="<?php echo base_url('cms/order/view/'.$result->order_id);?>" target="_blank">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>

                                                    <?php }else{ ?>

                                                        <a href="<?php echo base_url($result->invoice);?>" target="_blank">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>


                                                   <?php } ?>


                                                    </td>
                                                    
                                                    <td><?php echo $result->total_amount; ?> SAR</td>
                                                    
                                                        
                                                    
                                                </tr>
                                            <?php 
                                                }
                                                ?>

                                                <tr>
                                                    <td colspan="5"> Grand Total</td>
                                                    <td> <?php echo $grand_total; ?> SAR</td>
                                                </tr>


    
                                         <?php   }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->

<div class="content-page">
   <div class="content">
      <div class="container-fluid">
         <div class="page-title-box mb-0">
            <h4 class="page-title pb-0">Thank you for Choosing Niehez™ 7</h4>
            <ol class="breadcrumb p-0 m-0 d-none">
               <li>
                  <a href="https://ecom.schopfen.com/cms/product/add">
                  <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Product</button>
                  </a>
               </li>
            </ol>
            <div class="clearfix"></div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <?php if($this->session->flashdata('message')){ ?>
               <div class="col-md-12 alert alert-success"><p><?php echo $this->session->flashdata('message'); ?></p></div>
            <?php }  ?>

               <div class="border-0 ">
                  <div class="panel-body card-box pt-4 pb-5 px-4 mb-4">
                     <div class="text">
                        <h4 class="m-0 text-danger font-weight-light">Set up your Store on Niehez™ 7</h4>
                        <p class="m-0 smallFontSize lineHeight1-3">
                           <span class="m-0 text-dark"><b>Where Selling is made easier!</b></span>
                        </p>
                     </div>
                     <div class="row mt-4">
                        <div class="col-md-4">
                           <div class="w-auto d-inline-block position-relative">
                              <a href="javascript:void(0);" class="text-warning stretched-link"></a>
                              <h5 class="m-0 text-danger"><b>1.</b> <span class="text-dark">Where are you located?</span></h5>
                              <p class="m-0 lineHeight1-3">
                                 <span class="text-danger smallFontSize"><b>Set up the Cities and Branches</b></span>
                              </p>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="w-auto d-inline-block position-relative">
                              <a href="javascript:void(0);" class="text-warning stretched-link"></a>
                              <h5 class="m-0 text-danger"><span class="text-danger"><b>2.</b></span> <span class="text-dark">How Many Employees Do you have?</span></h5>
                              <p class="m-0 lineHeight1-3">
                                 <span class="text-danger smallFontSize"><b>Set Up User Roles/Rights and Create Users</b></span>
                              </p>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="w-auto d-inline-block position-relative">
                              <a href="javascript:void(0);" class="text-warning stretched-link"></a>
                              <h5 class="m-0 text-danger"><span class="text-danger"><b>3.</b></span> <span class="text-dark">What products you want to sell?</span></h5>
                              <p class="m-0 lineHeight1-3">
                                 <span class="text-danger smallFontSize"><b>Set up the Categories and Products</b></span>
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="row mt-4">
                        <div class="col-md-4">
                           <div class="w-auto d-inline-block position-relative">
                              <a href="javascript:void(0);" class="text-warning stretched-link"></a>
                              <h5 class="m-0 text-danger"><span class="text-danger"><b>4.</b></span> <span class="text-dark">Design your Website</span></h5>
                              <p class="m-0 lineHeight1-3">
                                 <span class="text-danger smallFontSize"><b>Select and Customize your website Theme</b></span>
                              </p>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="w-auto d-inline-block position-relative">
                              <a href="javascript:void(0);" class="text-warning stretched-link"></a>
                              <h5 class="m-0 text-danger"><span class="text-danger"><b>5.</b></span> <span class="text-dark">Design your Mobile Application</span></h5>
                              <p class="m-0 lineHeight1-3">
                                 <span class="text-danger smallFontSize"><b>Select and Customize your mobile app Theme</b></span>
                              </p>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="w-auto d-inline-block position-relative">
                              <a href="javascript:void(0);" class="text-warning stretched-link"></a>
                              <h5 class="m-0 text-danger"><span class="text-danger"><b>Or</b></span> <span class="text-dark">Let us do it all for you..</span></h5>
                              <p class="m-0 lineHeight1-3">
                                 <span class="text-success smallFontSize"><b>Contact Customer Support</b></span>
                              </p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
               <h6>What’s New?</h6>
               <div class="row mt-2">
                  <div class="col-sm-3 col-md-3 position-relative shadow">
                     <div class="dashNewsBgImg" style="background-image:url('<?php echo base_url() ?>assets/images/todaySales.png');"></div>
                     <div class="m-0 bg-black-light rounded py-3 px-3 h-100 d-flex align-items-center">
                        <div class="wrapper">
                           <p class="m-0 lineHeight1-1">
                              <span class="m-0 text-white smallFontSize lineHeight1-1">Today’s sales</span>
                           </p>
                           <h1 class="m-0 text-white lineHeight1-1"><?php echo $today_sales; ?> </h1>
                           <p class="m-0  text-white lineHeight1-1 smallFontSize">SAR</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3 col-md-3 position-relative shadow">
                     <div class="dashNewsBgImg" style="background-image:url('<?php echo base_url() ?>assets/images/itemsSoldToday.png');"></div>
                     <div class="m-0 bg-black-light rounded py-3 px-3 h-100 d-flex align-items-center">
                        <div class="wrapper">
                           <p class="m-0 lineHeight1-1">
                              <span class="m-0 text-white smallFontSize lineHeight1-1">Items sold today</span>
                           </p>
                           <h1 class="m-0 text-white lineHeight1-1"><?php echo $today_total_items; ?></h1>
                           <p class="m-0  text-white lineHeight1-1 smallFontSize">Items</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3 col-md-3 position-relative shadow">
                     <div class="dashNewsBgImg" style="background-image:url('<?php echo base_url() ?>assets/images/monthlySales.png');"></div>
                     <div class="m-0 bg-black-light rounded py-3 px-3 h-100 d-flex align-items-center">
                        <div class="wrapper">
                           <p class="m-0 lineHeight1-1">
                              <span class="m-0 text-white smallFontSize lineHeight1-1">Monthly sales</span>
                           </p>
                           <h1 class="m-0 text-white lineHeight1-1"><?php echo $monthly_sales; ?> </h1>
                           <p class="m-0  text-white lineHeight1-1 smallFontSize">SAR</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3 col-md-3 position-relative shadow">
                     <div class="dashNewsBgImg" style="background-image:url('<?php echo base_url() ?>assets/images/monthlyNewCus.png');"></div>
                     <div class="m-0 bg-black-light rounded py-3 px-3 h-100 d-flex align-items-center">
                        <div class="wrapper">
                           <p class="m-0 lineHeight1-1">
                              <span class="m-0 text-white smallFontSize lineHeight1-1">Monthly new customers</span>
                           </p>
                           <h1 class="m-0 text-white lineHeight1-1"><?php echo $today_users; ?></h1>
                           <p class="m-0  text-white lineHeight1-1 smallFontSize">Customers</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-12">
               <div class="row mt-3">
                  <div class="col-md-6">
                     <h6 class="mb-2">Notifications</h6>
                     <div class="card-box bg-beriosova pr-1 mb-4">
                        <div class="haveScroll slimscrollleft pr-3">
                           <ul class="p-0">
                              <?php for ($x = 0; $x <= 40; $x++) { ?>
                              <li class="d-flex py-2">
                                 <div class="imgBox mr-3"><img class="rounded-circle" src="<?php echo base_url() ?>assets/images/small/img-1.jpg" alt="product" height="45" width="45"></div>
                                 <div class="text">
                                    <p class="m-0 smallFontSize lineHeight1-3">
                                       <span class="text-danger d-block w-100">Salman Ahmed</span>
                                       <span class="text-white  d-block w-100">21 Oct, 2020 | 12:05 PM</span>
                                       <span class="text-white  d-block w-100">Added <span class="text-danger">Product title - Model here,</span> to their Shopping Cart</span>
                                    </p>
                                 </div>
                              </li>
                              <?php } ?>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <h6 class="mb-2">Top selling items</h6>
                     <div class="card-box bg-beriosova pr-1 mb-4">
                        <div class="haveScroll slimscrollleft pr-3">
                           <ul class="m-0 p-0">
                             <?php if($top_selling) {

                                        foreach ($top_selling as $key => $value) { 


                                            $images = getProductImages($value['product_id']);
                                            $image = getImage($images[0]->product_image);     
                                            


                                            ?>
                              <li class="d-flex py-2">
                                 <div class="imgBox mr-3"><img class="rounded" src="<?php echo $image; ?>" alt="product" height="45" width="45"></div>
                                 <div class="text">
                                    <p class="m-0 smallFontSize lineHeight1-3">
                                       <span class="text-danger d-block w-100"><?php echo $value['product_title_en']; ?></span>
                                       <span class="text-white  d-block w-100"><?php echo $value['price']; ?> SAR </span>
                                       <span class="text-white  d-block w-100"><?php echo $value['total']; ?> Units Sold</span>
                                    </p>
                                 </div>
                              </li>
                              <?php } }?>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <h6 class="mb-2">Lowest in Stock</h6>
                     <div class="card-box bg-beriosova pr-1 mb-4">
                     <div class="haveScroll slimscrollleft pr-3">
                        <ul class="m-0 p-0">
                           <?php for ($x = 0; $x <= 40; $x++) { ?>
                           <li class="d-flex py-2">
                              <div class="imgBox mr-3"><img class="rounded" src="<?php echo base_url() ?>assets/images/small/img-1.jpg" alt="product" height="45" width="45"></div>
                              <div class="text">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100">Product Title</span>
                                    <span class="text-white  d-block w-100">200 SAR</span>
                                    <span class="text-white  d-block w-100">0 Units Left</span>
                                 </p>
                              </div>
                           </li>
                           <?php } ?>
                        </ul>
                     </div>
                     </div>
                  </div>
               </div>
               <!-- </div>
                  <div class="col-md-12"> -->
               <div class="row">
                  <div class="col-md-9">
                     <h6 class="mb-2">Latest Orders</h6>
                     <div class="card-box bg-beriosova shadow-none p-2">
                        <div class="haveScroll slimscrollleft tableHeight">
                           <table class="table table-striped table-bordered dt-responsive nowrap dataTable m-0 no-footer dashboardLatestOrders">
                              <thead>
                                 <tr>
                                       <th>No.</th>
                                       <th>Items</th>
                                       <th>Price</th>
                                       <th>Date</th>
                                       <th>Customer</th>
                                       <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                <?php if($orders){
                                foreach($orders as $order){ ?>
                                       <tr class="smallFontSize text-white">
                                          <td>
                                             <span class="text-danger "><?php echo $order->order_track_id; ?></span>
                                          </td>   
                                          <td><?php echo $order->total_items; ?></td>
                                          <td><?php echo $order->total_amount; ?></td>
                                          <td><?php echo date('d M, Y H:i ',strtotime($order->created_at)); ?></td>
                                          <td><?php echo $order->full_name; ?></td>
                                          <td>
                                          Not Picked by
                                             <span class="text-danger ">Unassigned</span>
                                          </td>
                                       </tr>
                                 <?php } } ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                     
                  </div>
                    <div class="col-md-3">
                        <h6 class="mb-2">Top Drivers</h6>
                        <div class="card-box bg-beriosova pr-1">
                           <div class="haveScroll slimscrollleft pr-2">
                              <ul class="m-0 p-0 ">
                                    <?php foreach($drivers as $driver){ ?>
                                    <li class="d-flex py-2">
                                    <div class="imgBox mr-3"><img class="rounded-circle" src="<?php echo getImage($driver['profile_pic'],'user') ?>" alt="product" height="45" width="45"></div>
                                    <div class="text">
                                       <p class="m-0 smallFontSize  lineHeight1-3">
                                          <span class="text-danger d-block w-100"><?php echo $driver['full_name']; ?></span>
                                          <span class="text-white  d-block w-100">Branch title</span>
                                          <span class="text-white  d-block w-100"><?php echo $driver['total_deliveries']; ?> Deliveries</span>
                                       </p>
                                    </div>
                                    </li>
                                    <?php } ?>
                              </ul>
                           </div>
                        </div>
                    </div>
               </div>
            </div>
         </div>
         <div class="buttons d-flex justify-content-between">
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#negativeActionModal">Negative Action</button>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#positiveActionModal">Positive Action</button>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#doubleActionModal">Double Action</button>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="negativeActionModal" data-keyboard="false" tabindex="-1" aria-labelledby="negativeActionModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="mdWrapperEd w-100">
         <div class="d-flex justify-content-between align-items-center">
             <img src="<?php echo base_url(); ?>assets/images/msjwhite.png" width="82">
             <button type="button" class="text-white text-white bg-transparent border-0 fa-2x lineHeight1-1" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
         </div>
        <div class="modal-content p-3 rounded">
            <div class="modal-body">
                <p class="font-weight-bold text-dark mediumFontSize">Are you sure you want to perform this action</p>
            </div>
            <div class="modal-footer border-0 text-center">
                <button type="button" class="btn btn-danger px-3 mx-auto w-100">Negative Action</button>
            </div>
            </div>
      </div>
  </div>
</div>
<div class="modal" id="positiveActionModal" data-keyboard="false" tabindex="-1" aria-labelledby="positiveActionModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="mdWrapperEd w-100">
         <div class="d-flex justify-content-between align-items-center">
             <img src="<?php echo base_url(); ?>assets/images/msjwhite.png" width="82">
             <button type="button" class="text-white text-white bg-transparent border-0 fa-2x lineHeight1-1" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
         </div>
        <div class="modal-content p-3 rounded">
            <div class="modal-body">
                <p class="font-weight-bold text-dark mediumFontSize">Are you sure you want to perform this action</p>
            </div>
            <div class="modal-footer border-0 text-center">
                <button type="button" class="btn btn-success px-3 mx-auto w-100">Positive Action</button>
            </div>
            </div>
      </div>
  </div>
</div>
<div class="modal" id="doubleActionModal" data-keyboard="false" tabindex="-1" aria-labelledby="doubleActionModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-sm">
      <div class="mdWrapperEd w-100">
         <div class="d-flex justify-content-between align-items-center">
             <img src="<?php echo base_url(); ?>assets/images/msjwhite.png" width="82">
             <button type="button" class="text-white text-white bg-transparent border-0 fa-2x lineHeight1-1" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
         </div>
        <div class="modal-content p-3 rounded">
            <div class="modal-body">
                <p class="font-weight-bold text-dark mediumFontSize">Are you sure you want to perform this action</p>
            </div>
            <div class="modal-footer border-0 text-center d-flex justify-content-between">
                <button type="button" class="btn btn-success px-3 mx-auto w-auto">Positive Action</button>
                <button type="button" class="btn btn-danger px-3 mx-auto w-auto">Negative Action</button>
            </div>
            </div>
      </div>
  </div>
</div>
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<?php
$user_id = $this->session->userdata['admin']['user_id'];
$assigned_complaint_types_arr = assigned_complaint_types_arr($user_id);
?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <h4 class="page-title">Support Tickets</h4>
                <div class="clearfix"></div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                        <h4 class="m-t-0 header-title">&nbsp;</h4>

                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                        <table id="datatable-responsive"
                               class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Ticket ID</th>
                                <th>Ticket Type</th>
                                <th>Status</th>
                                <th>Closed Req</th>
                                <th>Messages</th>
                                <th>Username</th>
                                <th>Submitted Date</th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($tickets) {
                                foreach ($tickets as $ticket) {
                                    // if admin is seeing this then show him all tickets or if support type user is accessing this then filter all those tickets matching his assigned type
                                    if ($this->session->userdata['admin']['role_id'] == 1 || ($this->session->userdata['admin']['role_id'] == 2 && in_array($ticket['type_id'], $assigned_complaint_types_arr))) { ?>
                                        <tr id="<?php echo $ticket['ticket_id']; ?>">
                                            <th data-sort="desc"><?php echo $ticket['ticket_id']; ?></th>
                                            <th><?php $ticket_type_parent = ticket_type_detail($ticket['ticket_type']);
                                                echo $ticket_type_parent->complaint_title_en;; ?></th>
                                            <th>
                                                <?php $status_class = ($ticket['status'] == 'open') ? "ticket_status_open" : "ticket_status_closed" ?>
                                                <span class="<?php echo $status_class ?>"><?php echo ucfirst($ticket['status']) ?></span>
                                            </th>
                                            <?php $close_req = array('', 'Closed by admin', 'Closed by user', 'Reopen Please'); ?>
                                            <th><?php echo $close_req[$ticket['closed_request']]; ?></th>
                                            <?php if ($ticket['status'] == 'open') { ?>
                                                <?php $status_class = ($ticket['unread'] > 0) ? "ticket_status_open" : "ticket_status_closed" ?>
                                                <th>
                                                    <span class="<?php echo $status_class ?>"><?php echo $ticket['unread']; ?></span>
                                                </th>
                                            <?php } else { ?>
                                                <th>-</th>
                                            <?php } ?>


                                            <th><?php echo $ticket['full_name']; ?></th>
                                            <th><?php echo date('Y-m-d', strtotime($ticket['created_at'])); ?></th>
                                            <th>

                                                <a href="<?php echo base_url('cms/support/view/' . $ticket['ticket_id']); ?>">
                                                    <button class="btn btn-icon waves-effect waves-light btn-success m-b-5">
                                                        <i class="fa fa-eye"></i></button>
                                                </a>
                                                <a href="javascript:void(0);"
                                                   onclick="deleteRecord('<?php echo $ticket['ticket_id']; ?>','cms/support/delete','')">
                                                    <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                                        <i class="fa fa-remove"></i></button>
                                                </a>
                                            </th>
                                        </tr>
                                    <?php }

                                    ?>
                                    <?php
                                }

                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div> <!-- container -->

    </div> <!-- content -->
    <script type="text/javascript">
        function autoRefreshPage() {
            window.location = window.location.href;
        }

        setInterval('autoRefreshPage()', 10000);
    </script>
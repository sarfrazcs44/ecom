<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add Testimonial</h4>
                               
                                <form action="<?php echo base_url();?>cms/testimonial/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="save">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                     <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="title_en" id="title-eng" required value="">
                                     </div>
                                     <div class="form-group">
                                       <label for="position_en">Position Eng * :</label>
                                       <input type="text" class="form-control" name="position_en" id="position_en" required value="">
                                     </div>
                                     <div class="form-group">
	                                   <label>Description Eng : </label>
                                        <textarea class="form-control" rows="5" name="description_en"></textarea>
	                                               
	                               </div>
                                    <div class="form-group">
                                    <label for="image">Please Choose Image :</label>
                                    <input type="file" class="filestyle" id="image" name="image[]" data-placeholder="No Image" required="">
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="title_ar" id="title-arb" required="" value="">
                                     </div>
                                    <div class="form-group">
                                       <label for="position_ar">Position Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="position_ar" id="position_ar" required="" value="">
                                     </div>
                                    
                                     <div class="form-group">
	                                   <label>Description Arb : </label>
                                        <textarea class="form-control arabic-cms-fields" rows="5" name="description_ar"></textarea>
	                                               
	                               </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    

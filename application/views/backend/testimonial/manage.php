<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <h4 class="page-title">Testimonials</h4>
                            <ol class="breadcrumb p-0 m-0">
                                
                                <li>
                                <a href="<?php echo base_url('cms/testimonial/add');?>">
                                    <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Testimonial</button>
                                </a>
                                </li>
                                
                                <!-- <li>
                                    <a href="#">Tables </a>
                                </li>
                                <li class="active">
                                    Responsive Table
                                </li>-->
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Title Eng</th>
                                            <th>Title Arb</th>
                                            
                                            <th>Position Eng</th>
                                            <th>Position Arb</th>
                                             
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($testimonials){
                                                foreach($testimonials as $testimonial){ ?>
                                                    <tr id="<?php echo $testimonial->testimonial_id;?>">
                                                    <th><img src="<?php echo base_url($testimonial->image);?>" alt="image"
                                                         class="img-responsive thumb-sm"/>
                                                    </th>
                                                    <th><?php echo $testimonial->title_en; ?></th>
                                                    <th><?php echo $testimonial->title_ar; ?></th>
                                                    
                                                    <th><?php echo $testimonial->position_en; ?></th>
                                                    <th><?php echo $testimonial->position_ar; ?></th>
                                                        
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/testimonial/edit/'.$testimonial->testimonial_id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $testimonial->testimonial_id;?>','cms/testimonial/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
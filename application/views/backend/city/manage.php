<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <h4 class="page-title">Cities</h4>
                            <ol class="breadcrumb p-0 m-0">
                                <li>
                                <a href="<?php echo base_url('cms/city/add');?>">
                                    <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add City</button>
                                </a>
                                </li>
                                <!-- <li>
                                    <a href="#">Tables </a>
                                </li>
                                <li class="active">
                                    Responsive Table
                                </li>-->
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th>Title Eng</th>
                                            <th>Title Arb</th>
                                            <th>Districts</th>
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($cities){
                                                foreach($cities as $city){ ?>
                                                    <tr id="<?php echo $city->id;?>">
                                                   
                                                    <th><?php echo $city->eng_name; ?></th>
                                                    <th><?php echo $city->arb_name; ?></th>
                                                   
                                                    
                                                    <th class="text-center">
                                                        <a href="<?php echo base_url('cms/district/index/'.$city->id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> District</button></a>
                                                        </th> 
                                                    
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/city/edit/'.$city->id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $city->id;?>','cms/city/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <style>
@import url("https://fonts.googleapis.com/css?family=Cairo");                 
                    table.table-bordered.dataTable tbody tr th:nth-child(3) {
                         font-family: "Cairo";
}
                </style>
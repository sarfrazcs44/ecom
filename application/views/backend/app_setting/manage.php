<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <h4 class="page-title">App Settings</h4>
                            
                            <div class="clearfix"></div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th>Company</th>
                                            
                                            <th>Created At</th>
                                             
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($companies){
                                                foreach($companies as $value){ ?>
                                                    <tr>
                                                   
                                                    </th>
                                                    <th><?php echo company_name($value->company_id); ?></th>
                                                     
                                                    <th><?php echo $value->created_at; ?></th>
                                                        
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/app_setting/edit/'.$value->company_id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                        
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <style>
@import url("https://fonts.googleapis.com/css?family=Cairo");                
                    table.table-bordered.dataTable tbody tr th:nth-child(3) {
                         font-family: "Cairo";
}
                </style>
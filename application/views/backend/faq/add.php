<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add faq</h4>
                               
                                <form action="<?php echo base_url();?>cms/faq/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="save">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                     <div class="form-group">
                                       <label for="title-eng">Question Eng * :</label>
                                       <input type="text" class="form-control" name="question_en" id="title-eng" required value="">
                                     </div>
                                    <div class="form-group">
	                                   <label>Answer Eng : </label>
                                        <textarea class="form-control" rows="5" name="answer_en"></textarea>
	                                               
	                               </div>
                                    
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Question Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="question_ar" id="title-arb" required="" value="">
                                     </div>
                                    
                                    <div class="form-group">
	                                   <label>Answer Arb : </label>
                                        <textarea class="form-control arabic-cms-fields" rows="5" name="answer_ar"></textarea>
	                                               
	                               </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    

<div class="content-page" style="margin-top:10px;">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Edit Support User</h4>

                        <form action="<?php echo base_url();?>cms/support_user/update" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                            <div class="alert" id="validatio-msg" style="display: none;">
                            </div>
                            <input type="hidden" name="user_id" value="<?php echo $user_data->user_id; ?>">
                            <div class="form-group">
                                <label for="full_name">Full Name</label>
                                <input type="text" class="form-control" name="full_name" id="full_name" required value="<?php echo $user_data->full_name; ?>">
                            </div>

                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" name="username" id="username" required value="<?php echo $user_data->username; ?>">
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" required value="<?php echo $user_data->email; ?>">
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" class="form-control" name="phone" id="phone" required value="<?php echo $user_data->phone; ?>">
                            </div>

                            <?php $gender = $user_data->gender; ?>
                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <select class="form-control" id="gender" name="gender">
                                    <option value="male" <?php echo ($gender == 'male' ? 'selected' : ''); ?>>Male</option>
                                    <option value="female" <?php echo ($gender == 'female' ? 'selected' : ''); ?>>Female</option>
                                </select>
                                <i class="fa fa-chevron-down"></i>
                            </div>
                            <div class="form-group">
                                <label for="complaint_type_id">Support Types</label>
                                <select class="multiple_dropdown" id="complaint_type_id" name="complaint_type_id[]" multiple="multiple">
                                    <?php foreach ($complaint_types as $complaint_type)
                                    {
                                        if (in_array($complaint_type['complaint_type_id'], $user_complaint_types))
                                        {
                                            $selected = 'selected';
                                        }else{
                                            $selected = '';
                                        }
                                        ?>
                                        <option value="<?php echo $complaint_type['complaint_type_id']; ?>" <?php echo $selected; ?>><?php echo $complaint_type['complaint_title_en']; ?></option>
                                    <?php }?>
                                </select>
                                <i class="fa fa-chevron-down"></i>
                            </div>
                            <!--<span id="append_here"></span>-->
                            <?php /*$is_verified = $user_data->is_verified; */?><!--
                            <div class="form-group" style="overflow:hidden">
                                <span style="float: left;margin-right: 15px;">Is Active ? &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <input type="checkbox" id="switch7" switch="primary" checked="<?php /*echo ($is_verified == 1 ? 'checked' : ''); */?>" name="is_verified">
                                <label style="display: inline-block;float: left;" for="switch7" data-on-label="Yes" data-off-label="No"></label>

                            </div>-->

                            <div class="form-group">
                                <label for="password">Password <small style="color: red;">(Only fill up here if need to change password otherwise make sure its not filled)</small></label>
                                <input type="password" class="form-control" name="password" id="password">
                            </div>
                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        Submit
                                    </button>
                                    <a href="<?php echo base_url('cms/support_user'); ?>"><button type="button" class="btn btn-danger waves-effect waves-light">
                                            Cancel
                                        </button></a>
                                    <!--<button id="add_more" type="button">Add More</button>-->


                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- end col -->


            </div>
            <!-- end row -->



        </div> <!-- container -->

    </div> <!-- content -->

    <script>
        var html = "<select>" +
            <option></option>
        "</select>";
        /*$("#add_more").click(function(){
            alert('here');
            $("#append_here").after("<i>After</i>");
        });*/

        $(document).on('click', '#add_more', function () {
            alert('here');
        });
    </script>
    

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <h4 class="page-title">Roles</h4>
                
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive p-0">

                        <h4 class="m-t-0 header-title">&nbsp;</h4>

                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                         <form action="<?php echo base_url(); ?>cms/role/action" method="post"
                              onsubmit="return false;" class="form_data" enctype="multipart/form-data"
                              data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save_rights">
                            <div class="alert" id="validatio-msg" style="display: none;"></div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="m-0" for="RoleID">Select Role</label>
                                        <select id="RoleID" class="form-control roles" required="" name="role_id">
                                            <?php foreach($roles as $value){ ?>
                                                    <option value="<?php echo $value->role_id; ?>" <?php echo ($value->role_id == $RoleID ? 'selected' : ''); ?>><?php echo $value->role_title;?></option>
                                           <?php  } ?>
                                             
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card-box table-responsive">
                                        <h4 class="m-t-0 header-title"><?php echo lang('rights'); ?></h4>
                                        <table id="custom-table"
                               class="table table-striped table-bordered dataTable  dt-responsive nowrap" cellspacing="0">
                                        
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <div class="checkbox pl-2">
                                                            <input id="all_check" type="checkbox" data-parsley-multiple="all_check"/>
                                                            <label class="m-0" for="all_check">
                                                                 &nbsp;
                                                            </label>
                                                        </div>
                                                    </th>
                                                    <th>Title</th>
                                                    <th>View</th>
                                                    <th>Add</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php if ($results) {
                                                foreach ($results as $value) { ?>
                                                    <tr id="<?php echo $value->module_right_id; ?>">
                                                        <td>
                                                            <div class="checkbox pl-2">
                                                                <input name="IsActive" value="1"
                                                                            type="checkbox"
                                                                            id="all-<?php echo $value->module_right_id; ?>"
                                                                            data-id="<?php echo $value->module_right_id; ?>"
                                                                            data-parsley-multiple="all-<?php echo $value->module_right_id; ?>"
                                                                            class="all horizontal"/>
                                                                <label class="m-0" for="all-<?php echo $value->module_right_id; ?>">&nbsp;</label>
                                                            </div>
                                                        </td>
                                                        <td><?php echo $value->title_en; ?></td>
                                                        <td>
                                                            <div class="checkbox pl-2">
                                                                    <input id="view-<?php echo $value->module_right_id; ?>"
                                                                            class="all horizontal-<?php echo $value->module_right_id; ?>" <?php echo($value->can_view == 1 ? 'checked' : ''); ?>
                                                                            type="checkbox"
                                                                            data-parsley-multiple="view-<?php echo $value->module_right_id; ?>"
                                                                            name="can_view[<?php echo $value->module_right_id; ?>]"/>
                                                                <label class="m-0" for="view-<?php echo $value->module_right_id; ?>">
                                                                    &nbsp;
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="checkbox pl-2">
                                                                    <input id="add-<?php echo $value->module_right_id; ?>"
                                                                            class="all horizontal-<?php echo $value->module_right_id; ?>" <?php echo($value->can_add == 1 ? 'checked' : ''); ?>
                                                                            type="checkbox"
                                                                            data-parsley-multiple="add-<?php echo $value->module_right_id; ?>"
                                                                            name="can_add[<?php echo $value->module_right_id; ?>]"/>
                                                                <label class="m-0" for="add-<?php echo $value->module_right_id; ?>">
                                                                    &nbsp;
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="checkbox pl-2">
                                                                    <input id="edit-<?php echo $value->module_right_id; ?>"
                                                                            class="all horizontal-<?php echo $value->module_right_id; ?>" <?php echo($value->can_edit == 1 ? 'checked' : ''); ?>
                                                                            type="checkbox"
                                                                            data-parsley-multiple="edit-<?php echo $value->module_right_id; ?>"
                                                                            name="can_edit[<?php echo $value->module_right_id; ?>]"/>
                                                                <label class="m-0" for="edit-<?php echo $value->module_right_id; ?>">
                                                                    &nbsp;
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="checkbox pl-2">
                                                                    <input id="delete-<?php echo $value->module_right_id; ?>"
                                                                            class="all horizontal-<?php echo $value->module_right_id; ?>" <?php echo($value->can_delete == 1 ? 'checked' : ''); ?>
                                                                            type="checkbox"
                                                                            data-parsley-multiple="delete-<?php echo $value->module_right_id; ?>"
                                                                            name="can_delete[<?php echo $value->module_right_id; ?>]"/>
                                                                <label class="m-0" for="delete-<?php echo $value->module_right_id; ?>">
                                                                    &nbsp;
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }

                                            }
                                            ?>

                                            </tbody>
                                        </table>
                                        <div class="form-group text-right m-b-0">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                <?php echo lang('submit'); ?>
                                            </button>
                                            <a href="<?php echo base_url(); ?>cms/role">
                                                <button type="button" class="btn btn-default waves-effect m-l-5">
                                                    <?php echo lang('back'); ?>
                                                </button>
                                            </a>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div> <!-- container -->

    </div> <!-- content -->
<script src="<?php echo base_url(); ?>assets/js/module.js"></script>
<script>
    function reloadUser(role_id)
    {
        redirect('cms/role/rights/'+role_id);
    }
</script>
          
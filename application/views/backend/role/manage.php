<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                        <h4 class="m-t-0 header-title">&nbsp;</h4>

                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                        <table id="datatable-responsive"
                               class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                               width="100%">
                            <thead>
                                <tr>

                                    <th>title</th>

                                  


                                    <th>actions</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ ?>
                                        <tr id="<?php echo $value->role_id;?>">

                                            <td><?php echo $value->role_title; ?></td>


                                            


                                            <td>
                                               
                                                <a href="<?php echo base_url('cms/category/edit/'.$value->role_id);?>">
                                                <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>


                                                  <a href="<?php echo base_url('cms/role/rights/'.$value->role_id);?>" class="btn btn-simple btn-warning btn-icon edit">Rights</a> 
                                                   
                                            </td>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div> <!-- container -->

    </div> <!-- content -->

<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
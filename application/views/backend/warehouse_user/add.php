<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJaZ8KGubGRNlTmqjTlDaizEEMojWTsA4&libraries=places"></script>
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add Warehouse User</h4>

                                <form action="<?php echo base_url();?>cms/warehouse_user/save" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                     <div class="form-group">
                                       <label for="full_name">Full Name</label>
                                       <input type="text" class="form-control" name="full_name" id="full_name" required value="">
                                     </div>

                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" name="username" id="username" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="tel" class="form-control" name="phone" id="phone" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="gender">Gender</label>
                                        <select class="form-control" id="gender" name="gender">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="cities">City</label>
                                    <select class="multiple_dropdown" id="city_id" name="city_id" required>
                                        <option value="">Select City</option>
                                        <?php foreach ($ksa_cities as $ksa_city)
                                        { ?>
                                            <option value="<?php echo $ksa_city->id; ?>"><?php echo $ksa_city->eng_name; ?></option>
                                        <?php }?>
                                    </select>
                                    <i class="fa fa-chevron-down"></i>
                                    </div>

                                    <div class="form-group">
                                        <label for="cities">Districts</label>
                                    <select class="multiple_dropdown" id="districts" name="districts[]" multiple="multiple" required>
                                        
                                    </select>
                                    <i class="fa fa-chevron-down"></i>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label" for="Address">Address</label>
                                        <textarea  id="map-search" name="address" class="form-control" autocomplete="off" required></textarea>
                                    </div>
                            
                                <div class="form-group">
                                    <input type='hidden' name='latitude' id='lat' class="latitude">
                                    <input type='hidden' name='longitude' id='lng' class="longitude">
                                    <input type='hidden'  class="reg-input-city">
                                    
                                    
                                     <div id="map-canvas" style="height: 400px;"></div>
                                   
                                </div>
                          
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        <a href="<?php echo base_url('cms/warehouse_user'); ?>"><button type="button" class="btn btn-danger waves-effect waves-light">
                                                Cancel
                                            </button></a>
                                        <!--<button id="add_more" type="button">Add More</button>-->
                                        
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->

    <script>
            var html = "<select><option></option></select>";
            /*$("#add_more").click(function(){
                alert('here');
                $("#append_here").after("<i>After</i>");
            });*/

            $(document).on('click', '#add_more', function () {
                alert('here');
            });
    </script>
    <script>
    function initialize() {
        var mapOptions, map, marker, searchBox, city,
        infoWindow = '',
        addressEl = document.querySelector( '#map-search' ),
        latEl = document.querySelector( '.latitude' ),
        longEl = document.querySelector( '.longitude' ),
        element = document.getElementById( 'map-canvas' );
        city = document.querySelector( '.reg-input-city' );
        mapOptions = {
            // How far the maps zooms in.
            zoom: 8,
            // Current Lat and Long position of the pin/
            center: new google.maps.LatLng( 21.484716, 39.189606 ),
            // center : {
            //  lat: -34.397,
            //  lng: 150.644
            // },
            disableDefaultUI: false, // Disables the controls like zoom control on the map if set to true
            scrollWheel: true, // If set to false disables the scrolling on the map.
            draggable: true, // If set to false , you cannot move the map around.
            // mapTypeId: google.maps.MapTypeId.HYBRID, // If set to HYBRID its between sat and ROADMAP, Can be set to SATELLITE as well.
            // maxZoom: 11, // Wont allow you to zoom more than this
            // minZoom: 9  // Wont allow you to go more up.
            
        };
    /**
     * Creates the map using google function google.maps.Map() by passing the id of canvas and
     * mapOptions object that we just created above as its parameters.
     *
     */
    // Create an object map with the constructor function Map()
        map = new google.maps.Map( element, mapOptions ); // Till this like of code it loads up the map.
    /**
     * Creates the marker on the map
     *
     */
        marker = new google.maps.Marker({
            position: mapOptions.center,
            label: {text: '1' , color: 'white'},
            map: map,
            draggable: true
        });
    /**
     * Creates a search box
     */
         searchBox = new google.maps.places.SearchBox( addressEl );
    /**
     * When the place is changed on search box, it takes the marker to the searched location.
     */
    google.maps.event.addListener( searchBox, 'places_changed', function () {
        var places = searchBox.getPlaces(),
            bounds = new google.maps.LatLngBounds(),
            i, place, lat, long, resultArray,
            addresss = places[0].formatted_address;
        for( i = 0; place = places[i]; i++ ) {
            bounds.extend( place.geometry.location );
            marker.setPosition( place.geometry.location );  // Set marker position new.
        }
        map.fitBounds( bounds );  // Fit to the bound
        map.setZoom( 15 ); // This function sets the zoom to 15, meaning zooms to level 15.
        // console.log( map.getZoom() );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        latEl.value = lat;
        longEl.value = long;
        resultArray =  places[0].address_components;
        // Get the city and set the city input value to the one selected
        for( var i = 0; i < resultArray.length; i++ ) {
            if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                citi = resultArray[ i ].long_name;
                city.value = citi;
            }
        }
        // Closes the previous info window if it already exists
        if ( infoWindow ) {
            infoWindow.close();
        }
        /**
         * Creates the info Window at the top of the marker
         */
        infoWindow = new google.maps.InfoWindow({
            content: addresss
        });
        infoWindow.open( map, marker );
    } );
    /**
     * Finds the new position of the marker when the marker is dragged.
     */
    google.maps.event.addListener( marker, "dragend", function ( event ) {
        var lat, long, address, resultArray, citi;
        console.log( 'i am dragged' );
        lat = marker.getPosition().lat();
        long = marker.getPosition().lng();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { latLng: marker.getPosition() }, function ( result, status ) {
            if ( 'OK' === status ) {  // This line can also be written like if ( status == google.maps.GeocoderStatus.OK ) {
                address = result[0].formatted_address;
                resultArray =  result[0].address_components;
                // Get the city and set the city input value to the one selected
                for( var i = 0; i < resultArray.length; i++ ) {
                    if ( resultArray[ i ].types[0] && 'administrative_area_level_2' === resultArray[ i ].types[0] ) {
                        citi = resultArray[ i ].long_name;
                        console.log( citi );
                        city.value = citi;
                    }
                }
                addressEl.value = address;
                latEl.value = lat;
                longEl.value = long;
            } else {
                console.log( 'Geocode was not successful for the following reason: ' + status );
            }
            // Closes the previous info window if it already exists
            if ( infoWindow ) {
                infoWindow.close();
            }
            /**
             * Creates the info Window at the top of the marker
             */
            infoWindow = new google.maps.InfoWindow({
                content: address
            });
            infoWindow.open( map, marker );
        } );
    });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
    

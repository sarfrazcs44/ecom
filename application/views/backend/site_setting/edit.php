<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
            <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30" style="color:#90616d">Site Settings</h4>
                               
                                <form action="<?php echo base_url();?>cms/site_setting/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="id" value="<?php echo $result->id;?>">
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                
                                    
                                      <div class="form-group">
                                       <label for="vat">Vat % * :</label>
                                       <input type="text" class="form-control" name="vat" id="vat" required value="<?php echo $result->vat; ?>">
                                     </div>
                                     <div class="form-group">
                                       <label for="vat">Header Gradian Color (Comma seprated) * :</label>
                                       <input type="text" class="form-control" name="header_gradian_color" id="header_gradian_color" required value="<?php echo $result->header_gradian_color; ?>">
                                     </div>
                                     <div class="form-group">
                                       <label for="vat">Footer Gradian Color (Comma seprated) * :</label>
                                       <input type="text" class="form-control" name="footer_gradian_color" id="footer_gradian_color" required value="<?php echo $result->footer_gradian_color; ?>">
                                     </div>
                                      <?php if($result->login_screen_logo != ''){ ?>
                                    <img src="<?php echo base_url($result->login_screen_logo);?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                    <div class="form-group">
                                    <label for="image">Login Screen Logo (Only PNG) :</label>
                                    <input type="file" class="filestyle" id="login_screen_logo" name="login_screen_logo[]" data-placeholder="No Image">
                                    </div>
                                     <?php if($result->splash_screen_logo != ''){ ?>
                                    <img src="<?php echo base_url($result->splash_screen_logo);?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                    <div class="form-group">
                                    <label for="image">Splash Screen Logo (Only PNG) :</label>
                                    <input type="file" class="filestyle" id="splash_screen_logo" name="splash_screen_logo[]" data-placeholder="No Image">
                                    </div>
                                     <?php if($result->header_logo != ''){ ?>
                                    <img src="<?php echo base_url($result->header_logo);?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                    <div class="form-group">
                                    <label for="image">Header Logo (Only PNG) :</label>
                                    <input type="file" class="filestyle" id="image" name="header_logo[]" data-placeholder="No Image">
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                    </form>
                                </div>
                            </div> <!-- end col -->
                            
                        </div>
                        <!-- end row -->
                    </div> <!-- container -->
            </div> <!-- content -->
    
  </div>
               
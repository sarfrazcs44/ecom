<?php
$category_option = '<option value="">Select Category</option>';
  if($categories){
      
      foreach($categories as $category){
          $category_option .= '<option value="'.$category->category_id.'" '.($category->category_id == $result->category_id ? 'selected' : '').'>'.$category->title_en.'</option>';
      }
  }
    
?>
<?php
$brand_option = '<option value="">Select Brand</option>';
  if($brands){
      
      foreach($brands as $brand){
          $brand_option .= '<option value="'.$brand->brand_id.'" '.($brand->brand_id == $result->brand_id ? 'selected' : '').'>'.$brand->title_en.'</option>';
      }
  }
    
?>
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add Product</h4>
                               
                                <form action="<?php echo base_url();?>cms/product/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="product_id" value="<?php echo $product_id;?>">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                     <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="product_title_en" id="title-eng" required value="<?php echo $result->product_title_en; ?>">
                                     </div>
                                    
                                    <div class="form-group">
	                                   <label>Description Eng :</label>
                                        <textarea class="form-control" rows="5" name="product_description_en"><?php echo $result->product_description_en; ?></textarea>
	                                               
	                               </div>
                                    <div class="form-group">
                                       <label for="model-eng">Model Eng * :</label>
                                       <input type="text" class="form-control" name="model_en" id="model-eng" required value="<?php echo $result->model_en; ?>">
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="price">Price * :</label>
                                       <input type="text" class="form-control price" name="price" id="price" required value="<?php echo $result->price; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="brand">Choose Brand :</label>
                                        <select id="brand" name="brand_id" class="form-control" required="">
                                            <?php echo $brand_option; ?>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                    <div class="form-group">
                                        <label for="category">Choose Parent Category :</label>
                                        <select id="category" name="category_id" class="form-control" required="" onchange="getAllSubCategories(this.value);">
                                            <?php echo $category_option; ?>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="sub_category">Choose Sub Category :</label>
                                        <select id="sub_category" name="sub_category_id" class="form-control" required="">
                                           <?php echo $sub_categories; ?>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                    
                                    
                                    <!--<div class="form-group">
                                    <label for="image">Please Choose Images :</label>
                                    <input type="file" class="filestyle" id="image" name="image[]" data-placeholder="No Image" required="">
                                    </div>-->
                                    <div class="form-group m-b-20">
                                        <label>Please Choose Images : (images will save if you submit form)</label>
                                        <input type="file" name="image[]" id="filer_input1"
                                                               multiple="multiple">
                                    </div>
                                            <?php if($images){ ?>
                                    
                                                <div class="form-group clearfix">
                                                       <div class="col-sm-12 padding-left-0 padding-right-0">
                                                          <div class="jFiler jFiler-theme-dragdropbox">
                                                             <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <?php foreach($images as $image){ ?>
                                                                   <li class="jFiler-item" data-jfiler-index="1" style="" id="img-<?php echo $image->product_image_id; ?>">
                                                                      <div class="jFiler-item-container">
                                                                         <div class="jFiler-item-inner">
                                                                            <div class="jFiler-item-thumb">
                                                                               <div class="jFiler-item-status"></div>
                                                                               <div class="jFiler-item-info">                                                              </div>
                                                                               <div class="jFiler-item-thumb-image"><img src="<?php echo base_url($image->product_image);?>" draggable="false"></div>
                                                                            </div>
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                               <ul class="list-inline pull-left">
                                                                                  <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                                                               </ul>
                                                                               <ul class="list-inline pull-right">
                                                                                  <li><a class="icon-jfi-trash jFiler-item-trash-action" onclick="deleteImage('<?php echo $image->product_image_id; ?>','cms/product/action')"></a></li>
                                                                               </ul>
                                                                            </div>
                                                                         </div>
                                                                      </div>
                                                                   </li>
                                                                    <?php } ?>
                                                                   
                                                                </ul>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>
                                            
                                    
                                    
                                    <?php } ?>
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="product_title_ar" id="title-arb" required="" value="<?php echo $result->product_title_ar; ?>">
                                     </div>
                                    <div class="form-group">
	                                   <label>Description Arb : </label>
                                        <textarea class="form-control arabic-cms-fields" rows="5" name="product_description_ar"><?php echo $result->product_description_ar; ?></textarea>
	                                               
	                               </div>
                                    <div class="form-group">
                                       <label for="model-arb">Model Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="model_ar" id="model-arb" required value="<?php echo $result->model_ar; ?>">
                                    </div>
                                    
                                   
                                        <!--<label>Product Specifications :</label>
                                        <?php if($specifications){
                                                foreach($specifications as $specific){ ?>
                                                <div class="form-group fieldGroup" id="spec-<?php echo $specific->product_specification_id;?>">
                                                    <div class="input-group">
                                                        <input type="text" name="specification_title_en[]" class="form-control" placeholder="Specification Title Eng" value="<?php echo $specific->specification_title_en;?>"/>
                                                        <input type="text" name="specification_title_ar[]" class="form-control arabic-cms-fields" placeholder="Specification Title Arb" value="<?php echo $specific->specification_title_ar;?>"/>
                                                        <div class="input-group-addon"> 
                                                            <a href="javascript:void(0)" onclick ="deleteSpefication(<?php echo $specific->product_specification_id;?>);" class="btn btn-danger"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                                        </div>
                                                    </div>
                                                    </div>
                                        
                                        <?php
                                                    
                                                }
                                        }
                                        ?>
                                     <div class="form-group fieldGroup">                
                                        <div class="input-group">
                                            <input type="text" name="specification_title_en[]" class="form-control" placeholder="Specification Title Eng"/>
                                            <input type="text" name="specification_title_ar[]" class="form-control arabic-cms-fields" placeholder="Specification Title Arb"/>
                                            <div class="input-group-addon"> 
                                                <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                            </div>
                                        </div>
                                    </div>-->
                                    
                                    <div class="form-group" style="overflow:hidden">
                                        <span style="float: left;margin-right: 15px;">Is Featured</span>
                                        <input type="checkbox" id="switch6" switch="primary" <?php echo ($result->is_featured == 1 ? 'checked=""' : ''); ?> name="is_featured">
                                        <label style="display: inline-block;float: left;" for="switch6" data-on-label="Yes" data-off-label="No"></label>
                                       
                                    </div>
                                     <div class="form-group" style="overflow:hidden">
                                        <span style="float: left;margin-right: 15px;">Is Active&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="checkbox" id="switch7" switch="primary" <?php echo ($result->is_active == 1 ? 'checked=""' : ''); ?> name="is_active">
                                        <label style="display: inline-block;float: left;" for="switch7" data-on-label="Yes" data-off-label="No"></label>
                                       
                                    </div>
                                    
                                     
                                    
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    
<div class="form-group fieldGroupCopy" style="display: none;">
    <div class="input-group">
        <input type="text" name="specification_title_en[]" class="form-control" placeholder="Specification Title Eng"/>
        <input type="text" name="specification_title_ar[]" class="form-control arabic-cms-fields" placeholder="Specification Title Arb"/>
        <div class="input-group-addon"> 
            <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span></a>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    //group add limit
    var maxGroup = 100;
    
    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });
    
    //remove fields group
    $("body").on("click",".remove",function(){ 
        $(this).parents(".fieldGroup").remove();
    });
    
    
    
}); 
    
function getAllSubCategories(category_id){
        $("#sub_category").attr("disabled", true);
        $.ajax({
				type: "POST",
				url: base_url+'cms/category/getAllSubCategories',
				data: {'category_id' : category_id},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				
			     $('#sub_category').html(result.html);
                 $("#sub_category").attr("disabled", false);        
				
			}
		});
        
    }    
</script>    
    
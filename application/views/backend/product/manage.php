<?php 
$parent_category = '';
$child_category = '';
if($categories){
   foreach ($categories as $key => $category) {
      if($category->parent_id == 0){
         $parent_category .= '<option value="'.$category->category_id.'">'.$category->title_en.'</option>';
      }else{
         $child_category .= '<option style="display:none;" class="child_category child_category'.$category->parent_id.'" value="'.$category->category_id.'">'.$category->title_en.'</option>';
      }
     
   }
   } ?>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <h4 class="page-title">Products</h4>
            
                <div class="clearfix"></div>
            </div>
            <!-- end row -->
            <div class="row fixedRightColumn">
                <div class="col-md-9">
                    <div class="card-box">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="flex-grow-1 pr-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="gender">Category</label>
                                            <div class="position-relative">
                                                <select name="category" class="form-control get_products" id="category">
                                                    <option value="">Category</option>
                                                    <?php echo $parent_category; ?>
                                                </select>
                                                <i class="fa fa-chevron-down position-absolute right-0 top-0 bottom-0 left-auto my-auto height-12px mr-3"></i>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-4 px-0">
                                        <div class="form-group">
                                            <label for="gender">Sub Category</label>
                                            <div class="position-relative">
                                                <select name="subcategory" class="form-control get_products" id="sub_category">
                                                    <option value="">Sub Category</option>
                                                    <?php echo $child_category; ?>
                                                </select>
                                                <i class="fa fa-chevron-down position-absolute right-0 top-0 bottom-0 left-auto my-auto height-12px mr-3"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="gender">Search Products</label>   
                                            <input type="text" class="form-control " id="search" placeholder="Search Products">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5 position-relative"><a class="text-white stretched-link" href="javascript:void(0);"  data-toggle="modal" data-target="#addProductEdModal"><i class="fa fa-plus"></i> Add a Product</a></button>
                        </div>
                        
                        
                        <div class="prodsNewStyle position-relative" >
                            <div class="position-absolute top-0 left-0">
                                <div class="singleProduct position-relative addNewProd border-success d-flex align-items-center">
                                    <div class="wrapper w-100 text-center">
                                    <a href="<?php echo base_url('cms/product/add');?>" class="text-dark stretched-link"><button type="button" class="btn btn-primary waves-effect w-md waves-light mb-2"><i class="fa fa-plus fa-2x"></i></button>
                                        <p class="smallFontSize">Add a new Products</p></a>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div id="products_div" class=" d-flex align-items-stretch flex-wrap">
                           <?php if($products){
                                    foreach($products as $product){ 
                                        $images = getProductImages($product['product_id']);
                                        $image = base_url()."uploads/product_images/dummy-img.png";   
                                         
                                        if(file_exists($images[0]->product_image)){
                                                        
                                            $image = base_url().$images[0]->product_image;
                                        }
                                ?>
                                    <div class="singleProduct position-relative text-center search_product">
                                        <div class="dropdown">
                                            <a class="px-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v text-secondary"></i>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item" href="<?php echo base_url('cms/product/edit/'.$product['product_id']);?>">View</a>
                                                <a class="dropdown-item" href="javascript:void(0);" onclick="deleteRecord('<?php echo $product['product_id'];?>','cms/product/action','')">Delete</a>
                                            </div>
                                        </div>
                                        <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="<?php echo $image;?>" alt="product" height="83" width="83" ></div>
                                        <p class="m-0 text-danger bigger text-truncate" title="<?php echo $product['category_title_en']; ?>"><?php echo $product['category_title_en']; ?></p>
                                        <p class="text-dark my-1"><?php echo $product['product_title_en']; ?></p>
                                        <p class="m-0 text-success bigger"><?php echo $product['price']; ?> SAR</p>
                                    </div>
                                <?php
                                }

                            }
                            ?>
                            </div>
                        </div>
                        <div class="text-center py-2 mt-2">
                            <?php if(count($products) > 40){ ?>
                                <a href="javascript:void(0);" class="text-secondary load_more" data-page-no="1">Load More</a>
                           <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-box bg-beriosova shadow-none text-white text-center mb-4">
                        <div class="row">
                            <div class="col mx-2">
                                <h3 class="m-0"><?php echo $total_products[0]['total']; ?></h3>
                                <p class="m-0 lineHeight1-3 small">Products Uploaded</p>
                            </div>
                            <div class="col mx-2">
                                <h3 class="m-0"><?php echo $total_active_products[0]['total']; ?></h3>
                                <p class="m-0 lineHeight1-3 small">Active Uploaded</p>
                            </div>
                            <!--<div class="col mx-2">
                                <h3 class="m-0">33</h3>
                                <p class="m-0 lineHeight1-3 small">Out of Stock</p>
                            </div>-->
                        </div>
                    </div>
                    <h4 class="page-title mb-2">Lowest in Stock</h4>
                    <div class="card-box bg-beriosova pr-0 mb-4">
                        <div class="haveScroll slimscrollleft pr-2">
                            <ul class="m-0 p-0">
                                
                                <?php for ($x = 0; $x <= 40; $x++) { ?>
                                    <li class="d-flex py-2">
                                        <div class="imgBox mr-3"><img class="rounded" src="<?php echo base_url() ?>assets/images/small/img-1.jpg" alt="product" height="45" width="45" ></div>
                                        <div class="text">
                                            <p class="m-0 smallFontSize lineHeight1-3">
                                                <span class="text-danger d-block w-100">Product Title</span>
                                                <span class="text-white  d-block w-100">200 SAR</span>
                                                <span class="text-white  d-block w-100">3 Units Sell</span>
                                            </p>
                                        </div>
                                    </li>
                                <?php } ?>
                                    
                                
                            </ul>
                        </div>
                    </div>
                    <h4 class="page-title mb-2">Top selling items</h4>
                    <div class="card-box bg-beriosova pr-0 mb-4">
                        <div class="haveScroll slimscrollleft pr-2">
                            <ul class="m-0 p-0">
                                <?php if($top_selling) {

                                        foreach ($top_selling as $key => $value) { 


                                            $images = getProductImages($value['product_id']);
                                            $image = base_url()."uploads/product_images/dummy-img.png";     
                                            if($images)
                                            {
                                                $image = $images[0]->product_image;
                                            }


                                            ?>

                                            <li class="d-flex py-2">
                                                <div class="imgBox mr-3"><img class="rounded" src="<?php echo $image;?>" alt="product" height="45" width="45" ></div>
                                                <div class="text">
                                                    <p class="m-0 smallFontSize lineHeight1-3">
                                                        <span class="text-danger d-block w-100"><?php echo $value['product_title_en']; ?></span>
                                                        <span class="text-white  d-block w-100"><?php echo $value['price']; ?> SAR</span>
                                                        <span class="text-white  d-block w-100"><?php echo $value['total']; ?> Units Sell</span>
                                                    </p>
                                                </div>
                                            </li>
                                           
                                       <?php }

                                   }


                                 ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title">&nbsp;</h4>
                        <div class="alert" id="validatio-msg" style="display: none;"></div>

                        <table id="datatable-responsive"
                                class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                width="100%">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Title Eng</th>
                                
                                <th>Model Eng</th>
                                
                                <th>Price</th>
                                <th>Is Active</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                    
                                <th>Actions</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                        <?php if($products){
                                    foreach($products as $product){ 
                                        $images = getProductImages($product->product_id);
                                        $image = '';    
                                        if($images)
                                        {
                                            $image = $images[0]->product_image;
                                        }
                                ?>
                                        <tr id="<?php echo $product->product_id;?>">
                                        <th><img src="<?php echo base_url().$image;?>" alt="image"
                                                class="img-responsive thumb-sm"/>
                                        </th>
                                        <th><?php echo $product->product_title_en; ?></th>
                                        
                                        
                                        <th><?php echo $product->model_en; ?></th> 
                                        <th><?php echo $product->price; ?></th> 
                                        <th><?php echo ($product->is_active == 1 ? 'Yes' : 'No'); ?></th> 
                                        <th><?php echo $product->created_at; ?></th>
                                        <th><?php echo $product->updated_at; ?></th>
                                            
                                        <th>
                                            
                                            <a href="<?php echo base_url('cms/product/edit/'.$product->product_id);?>">
                                                <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                            <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $product->product_id;?>','cms/product/action','')">
                                            <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                            </th> 
                                    </tr>
                                <?php 
                                    }

                                }
                                ?>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>-->



        </div> <!-- container -->

    </div> <!-- content -->
    <style>
        .thumb-sm {
            height: 60px;
            width: 60px;
        }
    </style>
    <!-- Modal -->
<div class="modal" id="addProductEdModal" data-keyboard="false" tabindex="-1" aria-labelledby="addProductEdModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered  modal-lg">
      <div class="mdWrapperEd w-100">
        <div class="d-flex justify-content-between align-items-center">
            <h5 class="modal-title" id="addProductEdModalLabel">Add a new Product</h5>
            <button type="button" class="text-white text-white bg-transparent border-0 fa-2x lineHeight1-1" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-content p-3 rounded">
            <div class="modal-body">
                <div class="form-group px-2">
                    <label class="text-danger mb-1">Upload Photos <span class="text-secondary small font-light">upto 5 images max</span></label>
                    <div class="input-group inputFileEd">
                        <div class="custom-file position-relative d-flex align-items-end">
                            <div class="w-100 mb-4 text-center">
                                <i class="fa fa-upload text-muted position-relative fa-2x mb-2"></i>
                                <button type="button" class="input-group-text btn btn-danger px-4 " id="inputGroupFileAddon01">Upload</button >
                            </div>
                            <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label border-danger" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="form-group px-2">
                    <div class="row">
                        <div class="col">
                            <label class="text-danger mb-1">Title English</label>
                            <input type="text" class="form-control" placeholder="Write here">
                        </div>
                        <div class="col direction-rtl">
                            <label class="text-danger mb-1">عنوان</label>
                            <input type="text" class="form-control" placeholder="Arabic">
                        </div>
                    </div>
                </div>
                <div class="form-group px-2">
                    <div class="row">
                        <div class="col">
                            <label class="text-danger mb-1">Model in English</label>
                            <input type="text" class="form-control" placeholder="Write here">
                        </div>
                        <div class="col direction-rtl">
                            <label class="text-danger mb-1">Model Arabic</label>
                            <input type="text" class="form-control" placeholder="Arabic">
                        </div>
                    </div>
                </div>
                <div class="form-group px-2">
                    <div class="row">
                        <div class="col">
                            <label class="text-danger mb-1">Description in English</label>
                            <textarea class="form-control" placeholder="Write Here"></textarea>
                        </div>
                        <div class="col direction-rtl">
                            <label class="text-danger mb-1">Description in Arabic</label>
                            <textarea class="form-control" placeholder="Write Here"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group px-2">
                    <label class="text-danger mb-1">Price</label>
                    <input type="text" class="form-control" placeholder="Enter Price without Unit">
                </div>
                <div class="form-group px-2">
                    <label class="text-danger mb-1">Category</label>
                    <div class="position-relative">
                        <select name="" class="form-control bg-white text-dark get_products" id="">
                            <option value="">Select Category</option>
                        </select>
                        <i class="fa fa-chevron-down text-danger position-absolute right-0 top-0 bottom-0 left-auto my-auto height-12px mr-3"></i>
                    </div>
                </div>
                <div class="form-group px-2">
                    <label class="text-danger mb-1">Sub Category</label>
                    <div class="position-relative">
                        <select name="" class="form-control bg-white text-dark get_products" id="">
                            <option value="">Select Category</option>
                        </select>
                        <i class="fa fa-chevron-down text-danger position-absolute right-0 top-0 bottom-0 left-auto my-auto height-12px mr-3"></i>
                    </div>
                </div>
                <div class="form-group px-2">
                    <label class="text-danger mb-1">Initial Quantity</label>
                    <div class="position-relative">
                        <select name="" class="form-control bg-white text-dark get_products" id="">
                            <option value="">Select Category</option>
                        </select>
                        <i class="fa fa-chevron-down text-danger position-absolute right-0 top-0 bottom-0 left-auto my-auto height-12px mr-3"></i>
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0 text-center">
                <button type="button" class="btn btn-danger px-3 mx-auto w-50">Upload Product</button>
            </div>
            </div>
      </div>
    
  </div>
</div>
<script type="text/javascript">
   $(document).ready(function(){  
    $("#search").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $(".search_product").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
      });
      
      $('#category').on('change',function(){
         $('.child_category').hide();
         var category = $( "#category option:selected" ).val();
         $('.child_category'+category).show();
      });


      $('.get_products').on('change',function(){
         var page = parseInt(0);
         var category_id = $( "#category option:selected" ).val();
         var sub_category_id = $( "#sub_category option:selected" ).val();
         $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/product/fetch_products');?>',
            data: {'category_id' : category_id,'sub_category_id' : sub_category_id,'page' : page},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
             
                
                $('#products_div').html(result.html);
                if(result.html == ''){
                    $('.load_more').attr('data-page-no', 0);
                }else{
                    if(result.show_load_more){
                        $('.load_more').attr('data-page-no', page + 1);
                    }
                    
                }
                if(!result.show_load_more){
                    $('.load_more').hide();

                }
                
            }, complete: function () {
                
            }
        });
         
      });

      $('.load_more').on('click',function(){
        var page = parseInt($(this).attr('data-page-no'));
         var category_id = $( "#category option:selected" ).val();
         var sub_category_id = $( "#sub_category option:selected" ).val();
         $.ajax({
            type: "POST",
            url: '<?php echo base_url('cms/product/fetch_products');?>',
            data: {'category_id' : category_id,'sub_category_id' : sub_category_id,'page' : page},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
              
             
                
                $('#products_div').append(result.html);

                if(result.html == ''){
                    $('.load_more').attr('data-page-no', 0);
                }else{
                   if(result.show_load_more){
                        $('.load_more').attr('data-page-no', page + 1);
                    }
                }
                


                if(!result.show_load_more){
                    $('.load_more').hide();

                }
                
            }, complete: function () {
                
            }
        });
         
      });


    });  
</script>
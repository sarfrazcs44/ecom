 <link href="<?php echo base_url();?>assets/plugins/summernote/summernote.css" rel="stylesheet" />
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4 class="header-title m-t-0 m-b-30">Edit Event</h4>
                                        </div>
                                        
                                    </div>
                                <form action="<?php echo base_url();?>cms/pages/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="page_id" value="<?php echo $result->page_id;?>">
                                    <input type="hidden" name="parent_id" value="<?php echo $result->parent_id;?>">
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                
                                    <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="page_title_en" id="title-eng" required value="<?php echo $result->page_title_en;?>">
                                     </div>

                                    <?php if($images){ ?>

                                        <div class="form-group clearfix">
                                            <div class="col-sm-12 padding-left-0 padding-right-0">
                                                <div class="jFiler jFiler-theme-dragdropbox">
                                                    <div class="jFiler-items jFiler-row">
                                                        <ul class="jFiler-items-list jFiler-items-grid">
                                                            <?php foreach($images as $image){ ?>
                                                                <li class="jFiler-item" data-jfiler-index="1" style="" id="img-<?php echo $image->page_image_id; ?>">
                                                                    <div class="jFiler-item-container">
                                                                        <div class="jFiler-item-inner">
                                                                            <div class="jFiler-item-thumb">
                                                                                <div class="jFiler-item-status"></div>
                                                                                <div class="jFiler-item-info">                                                              </div>
                                                                                <div class="jFiler-item-thumb-image"><img src="<?php echo base_url($image->image);?>" draggable="false"></div>
                                                                            </div>
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                                <ul class="list-inline pull-left">
                                                                                    <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                                                                </ul>
                                                                                <ul class="list-inline pull-right">
                                                                                    <li><a class="icon-jfi-trash jFiler-item-trash-action" onclick="deleteImage('<?php echo $image->page_image_id; ?>','cms/pages/action')"></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <?php } ?>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    <?php } ?>
                                    <div class="form-group">
                                            <label for="image">Please Choose Images :</label>
                                        <input type="file" name="image[]" id="filer_input1" multiple="multiple">
                                    </div>
                                          
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control  arabic-cms-fields" name="page_title_ar" id="title-arb" required="" value="<?php echo $result->page_title_ar;?>">
                                     </div>
                                    
                                    
                                    
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                    </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    

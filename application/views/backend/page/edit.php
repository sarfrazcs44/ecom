 <link href="<?php echo base_url();?>assets/plugins/summernote/summernote.css" rel="stylesheet" />
 <style>
 
/************* custom styles ****************/
.pac-container {
    z-index: 10000;
}

.btn.confirm-loc-btn.btn-success {
    color: #fff;
    display: block;
    padding: 10px !important;
    width: 150px;
}
.address_map {
    width: 100%;
    height: 400px;
}
   
</style>
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4 class="header-title m-t-0 m-b-30">Edit Page</h4>
                                        </div>
                                        <?php if($result->page_id == 9){?>
                                        <div class="col-md-6">
                                            <a href="<?php echo base_url('cms/pages/index/9');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5 pull-right">Catelog Pdf's</button>
                                        </a>
                                        </div>
                                        <?php }elseif ($result->page_id == 14){ ?>
                                            <div class="col-md-6">
                                                <a href="<?php echo base_url('cms/pages/index/14');?>">
                                                    <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5 pull-right">Projects Listing</button>
                                                </a>
                                            </div>
                                        <?php }elseif ($result->page_id == 15){ ?>
                                            <div class="col-md-6">
                                                <a href="<?php echo base_url('cms/pages/index/15');?>">
                                                    <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5 pull-right">Events Listing</button>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <form action="<?php echo base_url();?>cms/pages/action" method="post" onsubmit="return false;" class="form_data <?php echo($result->page_id == 11 ? 'GeoDetails' : '');?>" enctype="multipart/form-data" data-parsley-validate="" autocomplete="off"> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="page_id" value="<?php echo $page_id;?>">
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                
                                    <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="page_title_en" id="title-eng" required value="<?php echo $result->page_title_en; ?>">
                                     </div>
                                    
                                    <?php  if($result->page_id != 10){?>
                                    <div class="form-group">
	                                   <label>Description Eng : </label>
                                        <textarea class="form-control summernote" rows="5" name="page_description_en"><?php echo $result->page_description_en; ?></textarea>
	                                               
	                                </div>
                                    <?php } ?>
                                    
                                    
                                    <?php if($result->page_id == 11){?>
                                    <div class="form-group">
	                                   <label>Pin your Location : </label>
                                        <input type="text" name="location" class="form-control field-color required text-field-border" data-geo="formatted_address" data-toggle="modal" data-target="#locationModal" value="<?php echo $result->location; ?>">
	                                               
	                                </div>
                                    
                                    <div class="form-group">
	                                   <label>Address 1 : </label>
                                        <textarea class="form-control summernote" rows="5" name="address1_en"><?php echo $result->address1_en; ?></textarea>
	                                               
	                                </div>
                                    
                                    <div class="form-group">
	                                   <label>Address 2 : </label>
                                        <textarea class="form-control summernote" rows="5" name="address2_en"><?php echo $result->address2_en; ?></textarea>
	                                               
	                                </div>
                                    
                                    <div class="form-group">
	                                   <label>Address 3 : </label>
                                        <textarea class="form-control summernote" rows="5" name="address3_en"><?php echo $result->address3_en; ?></textarea>
	                                               
	                                </div>

                                        <div class="form-group">
                                            <label>Address 4 : </label>
                                            <textarea class="form-control summernote" rows="5" name="address4_en"><?php echo $result->address4_en; ?></textarea>

                                        </div>

                                        <div class="form-group">
                                            <label>Address 5 : </label>
                                            <textarea class="form-control summernote" rows="5" name="address5_en"><?php echo $result->address5_en; ?></textarea>

                                        </div>

                                        <div class="form-group">
                                            <label>Address 6 : </label>
                                            <textarea class="form-control summernote" rows="5" name="address6_en"><?php echo $result->address6_en; ?></textarea>

                                        </div>
                                    
                                    
                                    <?php } ?>
                                    <?php
                                    $page_ids = array(4,8,5,6,9,10);
                                    ?>
                                    
                                    <?php 
                                    if($result->page_id != 7 && $result->page_id != 11){
                                    if($result->image != '' && !in_array($result->page_id,$page_ids) ){ ?>
                                    <img src="<?php echo base_url($result->image);?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                    
                                    <?php if($result->image != '' && ($result->page_id == 8 || $result->page_id == 9)){ ?>
                                    <a href="<?php echo base_url($result->image);?>" target="_blank"><img src="<?php echo base_url('assets/images/pdf.jpg');?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/></a>
                                    <?php } ?>
                                    <?php if($result->page_id !=4  && $result->page_id !=5 && $result->page_id !=6 && $result->page_id !=10 ){?>
                                        <div class="form-group">
                                            <label for="image">Please Choose <?php echo (($result->page_id == 8 || $result->page_id == 9) ? 'File' : 'Image'); ?> :</label>
                                            <input type="file" class="filestyle" id="image" name="image[]" data-placeholder="No Image">
                                        </div>
                                           <?php }else{ ?>
                                    <div class="form-group m-b-20">
                                        <label>Please Choose Images : (images will save if you submit form) <?php echo ($result->page_id == 10 ? '. Image size should be greater than 150 * 150' : ''); ?></label>
                                        <input type="file" name="image[]" id="filer_input1"
                                                               multiple="multiple">
                                    </div>
                                    <?php if($images){ ?>
                                    
                                                <div class="form-group clearfix">
                                                       <div class="col-sm-12 padding-left-0 padding-right-0">
                                                          <div class="jFiler jFiler-theme-dragdropbox">
                                                             <div class="jFiler-items jFiler-row">
                                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                                    <?php foreach($images as $image){ ?>
                                                                   <li class="jFiler-item" data-jfiler-index="1" style="" id="img-<?php echo $image->page_image_id; ?>">
                                                                      <div class="jFiler-item-container">
                                                                         <div class="jFiler-item-inner">
                                                                            <div class="jFiler-item-thumb">
                                                                               <div class="jFiler-item-status"></div>
                                                                               <div class="jFiler-item-info">                                                              </div>
                                                                               <div class="jFiler-item-thumb-image"><img src="<?php echo base_url($image->image);?>" draggable="false"></div>
                                                                            </div>
                                                                            <div class="jFiler-item-assets jFiler-row">
                                                                               <ul class="list-inline pull-left">
                                                                                  <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                                                               </ul>
                                                                               <ul class="list-inline pull-right">
                                                                                  <li><a class="icon-jfi-trash jFiler-item-trash-action" onclick="deleteImage('<?php echo $image->page_image_id; ?>','cms/pages/action')"></a></li>
                                                                               </ul>
                                                                            </div>
                                                                         </div>
                                                                      </div>
                                                                   </li>
                                                                    <?php } ?>
                                                                   
                                                                </ul>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>
                                            
                                    
                                    
                                    <?php } ?>
                                    
                                   <?php } }
                                        ?>
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control  arabic-cms-fields" name="page_title_ar" id="title-arb" required="" value="<?php echo $result->page_title_ar; ?>">
                                     </div>
                                    <?php  if($result->page_id != 10){?>
                                    <div class="form-group arb">
	                                   <label>Description Arb : </label>
                                        <textarea class="form-control summernote arabic-cms-fields" rows="5" name="page_description_ar"><?php echo $result->page_description_ar; ?></textarea>
	                                               
	                               </div>
                                    <?php } ?>
                                    
                                    
                                    <?php if($result->page_id == 11){?>
                                    <div class="form-group">
	                                   <label>Address 1 : </label>
                                        <textarea class="form-control summernote arabic-cms-fields" rows="5" name="address1_ar"><?php echo $result->address1_ar; ?></textarea>
	                                               
	                                </div>
                                    
                                    <div class="form-group">
	                                   <label>Address 2 : </label>
                                        <textarea class="form-control summernote arabic-cms-fields" rows="5" name="address2_ar"><?php echo $result->address2_ar; ?></textarea>
	                                               
	                                </div>
                                    
                                    <div class="form-group">
	                                   <label>Address 3 : </label>
                                        <textarea class="form-control summernote arabic-cms-fields" rows="5" name="address3_ar"><?php echo $result->address3_ar; ?></textarea>
	                                               
	                                </div>

                                        <div class="form-group">
                                            <label>Address 4 Arb : </label>
                                            <textarea class="form-control summernote arabic-cms-fields" rows="5" name="address4_ar"><?php echo $result->address4_ar; ?></textarea>

                                        </div>

                                        <div class="form-group">
                                            <label>Address 5 Arb : </label>
                                            <textarea class="form-control summernote arabic-cms-fields" rows="5" name="address5_ar"><?php echo $result->address5_ar; ?></textarea>

                                        </div>

                                        <div class="form-group">
                                            <label>Address 6 Arb : </label>
                                            <textarea class="form-control summernote arabic-cms-fields" rows="5" name="address6_ar"><?php echo $result->address6_ar; ?></textarea>

                                        </div>
                                    
                                    <input name="lat" type="hidden" data-geo="lat" value="<?php echo $result->lat; ?>">
					               <input name="lng" type="hidden" data-geo="lng" value="<?php echo $result->lng; ?>">
                                    <?php } ?>
                                    
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                    </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
<?php if($result->page_id == 11){?>   
<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<input type="text" class="form-control geocomplete_map_draggable" style="width:70%;float:left;">
				<a class="btn confirm-loc-btn btn-success" style="float:right;" data-dismiss="modal"><?php echo lang('confirm');?></a>
			</div>
			<div class="modal-body">
				<div class="address_map"></div>
			</div>
		</div>
	</div>
</div><!--/modal-->
<?php } ?>    
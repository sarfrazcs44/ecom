 <footer class="footer text-right">
 <?php echo date("Y"); ?> &copy; Niehez 7 | Schöpfen
                </footer>
            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
            <!-- Right Sidebar -->
 <audio id="audiotag" src="<?php echo base_url('assets/bell1.mp3'); ?>" preload="auto"></audio>
            <div class="side-bar right-bar">
                <a href="javascript:void(0);" class="right-bar-toggle">
                    <i class="mdi mdi-close-circle-outline"></i>
                </a>
                <h4 class="">Settings</h4>
                <div class="setting-list nicescroll">
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Notifications</h5>
                            <p class="text-muted m-b-0"><small>Do you need them?</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">API Access</h5>
                            <p class="m-b-0 text-muted"><small>Enable/Disable access</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Auto Updates</h5>
                            <p class="m-b-0 text-muted"><small>Keep up to date</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                    <div class="row m-t-20">
                        <div class="col-xs-8">
                            <h5 class="m-0">Online Status</h5>
                            <p class="m-b-0 text-muted"><small>Show your status to all</small></p>
                        </div>
                        <div class="col-xs-4 text-right">
                            <input type="checkbox" checked data-plugin="switchery" data-color="#7fc1fc" data-size="small"/>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Right-bar -->
        </div>
        <!-- END wrapper -->
        <script>
            var resizefunc = [];
        </script>
        <!-- jQuery  -->
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/detect.js"></script>
        <script src="<?php echo base_url();?>assets/js/fastclick.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.blockUI.js"></script>
        <script src="<?php echo base_url();?>assets/js/waves.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/switchery/switchery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
        <!-- responsive-table-->
        <script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
       <!-- <script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.responsive.min.js"></script>-->
        <script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/parsleyjs/parsley.min.js"></script>
         <script src="<?php echo base_url();?>assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
        <script src="<?php echo base_url();?>assets/pages/jquery.blog-add.init.js"></script>
        <?php if($this->uri->segment(2) == 'myChat' || $this->uri->segment(2) == 'MyChat'){?>
        <!-- Chat Include -->
        <script src="https://www.gstatic.com/firebasejs/4.0.0/firebase-app.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.0.0/firebase-messaging.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.0.0/firebase-database.js"></script>
        <script src="https://www.gstatic.com/firebasejs/4.10.0/firebase.js"></script>
        <script src="<?php echo base_url();?>assets/js/firebase_custom.js"></script>
        <?php }?>
<script src="<?php echo base_url();?>assets/plugins/summernote/summernote.min.js"></script>
 <!-- added by asif -->
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWJjL2YrT5FefvnQ71XtYtt7E73cwcFXE&libraries=places"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
 <script src="<?php echo base_url();?>assets/js/app.js"></script>
        <!-- App js -->
        <script src="<?php echo base_url();?>assets/js/jquery.core.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.app.js"></script>
        <script src="<?php echo base_url();?>assets/js/script.js"></script>
        
        <script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.js"></script>
        <script src="<?php echo base_url();?>assets/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
    tinymce.init({
        selector: '.editor',
        theme : "modern",
        mode: "exact",
        theme_advanced_toolbar_location : "top",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
        + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
        + "bullist,numlist,outdent,indent",
        theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"
        +"undo,redo,cleanup,code,separator,sub,sup,charmap",
        theme_advanced_buttons3 : "",
        height:"250px",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });
</script>
<script type="text/javascript">
    tinymce.init({
        selector: '.editor_ar',
        theme : "modern",
        mode: "exact",
        directionality: "rtl",
        theme_advanced_toolbar_location : "top",
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,separator,"
        + "justifyleft,justifycenter,justifyright,justifyfull,formatselect,"
        + "bullist,numlist,outdent,indent",
        theme_advanced_buttons2 : "link,unlink,anchor,image,separator,"
        +"undo,redo,cleanup,code,separator,sub,sup,charmap",
        theme_advanced_buttons3 : "",
        height:"250px",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });
</script>
<script src="<?php echo base_url('assets/js/jquery.inputmask.bundle.js'); ?>"></script>
<script>
    $( document ).ready( function() {
        if(("#customer_mobile_no").length > 0){
            $("#customer_mobile_no").inputmask({"mask": "+\\966999999999"});
        }
        
     $('.datepicker').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove',
                inline: true
            }
         });
      });
</script> 
<script type="text/javascript">
function refreshNotifications() {
    var unreadChatsCount = $('.unreadChatsCountForRing').text();
    $.ajax({
        type: "POST",
        url: base_url + 'cms/pages/refreshNotifications',
        data: {'unreadChatsCount': unreadChatsCount},
        dataType: "json",
        cache: false,
        //async:false,
        success: function (response) {
            $('#notification-alerts').html(response.html);
            if (response.count > 0)
            {
                $('#site_title').text('('+response.count+') MSJ Security System');
            }
            if (response.notify == true) {
                playAudio();
            }
        }
    });
}
            $(document).ready(function () {
              $('#datatable-responsive').DataTable();
              /*$('#datatable-responsive-nosort').DataTable({
                "aaSorting": []
              });*/
               $(function($) {
                    $('.price').autoNumeric('init');
               });
             $('.summernote').summernote({
                height: 200,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: false                 // set focus to editable area after initializing summernote
            });
            $('.inline-editor').summernote({
                airMode: true
            });
setInterval('refreshNotifications()', 5000);
            });
            setTimeout(function(){ $( ".arb" ).find( ".panel-body" ).css( "direction", "rtl" );  }, 1000);
        </script>
    </body>
</html>
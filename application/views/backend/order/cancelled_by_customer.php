<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <h4 class="page-title">Orders</h4>
                            <ol class="breadcrumb p-0 m-0">
                                
                                
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Order Track ID</th>
                                            <th>Status</th>
                                            <th>Assigned To</th>
                                            <th>Transaction ID</th>
                                            <th>Amount</th>
                                            
                                            <th>Created At</th>
                                            <th>Updated At</th>

                                            <?php
                                            if ($this->session->userdata['admin']['role_id'] == 1 || $this->session->userdata['admin']['role_id'] == 4) { ?>
                                                <th>Actions</th>
                                            <?php }
                                            ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($orders){
                                                foreach($orders as $order){ ?>
                                                    <tr id="<?php echo $order->order_id;?>">
                                                    
                                                    <td><?php echo $order->order_track_id; ?></td>
                                                    <td id="status_<?php echo $order->order_id;?>">Cancelled By Customer</td>
                                                    <?php
                                                            $is_assigned = checkIfOrderAssinged($order->order_id);
                                                            if (!$is_assigned)
                                                            { 

                                                                $is_assigned = 'N/A';
                                                            } 

                                                             ?>
                                                    
                                                    <td><?php echo $is_assigned;?></td>
                                                    <td><?php echo $order->transaction_id; ?></td>
                                                    <td><?php echo $order->total_amount; ?></td>
                                                    
                                                    <td><?php echo $order->created_at; ?></td>
                                                    <td><?php echo $order->updated_at; ?></td>
                                                        
                                                    <td>
                                                       
                                                        <a href="<?php echo base_url('cms/order/view/'.$order->order_id);?>">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                        
                                                        </td> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <h4 class="page-title">Orders</h4>
                <ol class="breadcrumb p-0 m-0">
                </ol>
                <div class="clearfix"></div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title">&nbsp;</h4>
                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                        <table id="datatable-responsive"
                                class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                width="100%">
                            <thead>
                            <tr>
                                <th>Order No.</th>
                                <th class="text-center px-2">Items</th>
                                <th class="text-center px-2">Price</th>
                                <!-- <th>Transaction ID</th> -->
                                <th class="text-center px-2">Date</th>
                                <th class="text-center px-2">Customer</th>
                                <th>Status</th>
                                <?php
                                if ($this->session->userdata['admin']['role_id'] == 1 || $this->session->userdata['admin']['role_id'] == 4) { ?>
                                    <th class="text-center">Actions</th>
                                <?php }
                                ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if($orders){
                                foreach($orders as $order){ ?>
                                    <tr id="<?php echo $order->order_id;?>">
                                        <td><?php echo $order->order_track_id; ?></td>
                                        <td class="text-center"><b><?php echo $order->total_items; ?></b> <br> items</td>
                                        <td class="text-center"><b><?php echo $order->total_amount; ?></b> <br> SAR</td>
                                        <!-- <td><?php echo $order->transaction_id; ?></td> -->
                                        <td class="text-center px-2"><?php echo $order->created_at; ?></td>
                                        <td class="text-center px-2"><?php echo $order->full_name; ?></td>
                                        <td id="status_<?php echo $order->order_id;?>">
                                            unassigned
                                            <br>
                                            <!-- Split dropright button -->
                                            <div class="btn-group dropright">
                                            <button type="button" class="border-0 bg-transparent text-danger pl-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Click to assign</button>
                                            <button type="button" class="border-0 bg-transparent text-dark shadow-none dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="sr-only">Toggle Dropright</span>
                                            </button>
                                            <div class="dropdown-menu bg-dark">
                                                <?php
                                                    $delivery_users = getDeliveryUsersForOrder($order->order_id);
                                                        if (count($delivery_users) > 0)
                                                        { ?>
                                                                <?php
                                                                foreach ($delivery_users as $delivery_user)
                                                                {
                                                                    $exploded_data = explode('|', $delivery_user);
                                                                    $user_id = $exploded_data[0];
                                                                    $user_name = $exploded_data[1];
                                                                    ?>
                                                                    <button id="<?php echo $user_id; ?>" onclick="assignForDelivery(<?php echo $user_id; ?>,'<?php echo $order->order_id;?>')" class="dropdown-item" type="button"><?php echo $user_name; ?></button>
                                                                <?php }
                                                                ?>
                                                        <?php }else{ ?>
                                                            <span>No Delivery Men Assgined To This Order's District</span>
                                                <?php } ?>
                                            </div>
                                            </div>
                                            <!-- <?php
                                                $delivery_users = getDeliveryUsersForOrder($order->order_id);
                                                    if (count($delivery_users) > 0)
                                                    { ?>
                                                        <select onchange="assignForDelivery(this.value,'<?php echo $order->order_id;?>')">
                                                            <option disabled="disabled" selected>Choose user for delivery...</option>
                                                            <?php
                                                            foreach ($delivery_users as $delivery_user)
                                                            {
                                                                $exploded_data = explode('|', $delivery_user);
                                                                $user_id = $exploded_data[0];
                                                                $user_name = $exploded_data[1];
                                                                ?>
                                                                <option value="<?php echo $user_id; ?>"><?php echo $user_name; ?></option>
                                                            <?php }
                                                            ?>
                                                        </select>
                                                    <?php }else{ ?>
                                                        <span>No Delivery Men Assgined To This Order's District</span>
                                            <?php } ?> -->
                                        </td>
                                        <td class="text-center">
                                        
                                            <a href="<?php echo base_url('cms/order/view/'.$order->order_id);?>">
                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                            <!--<button class="btn btn-icon waves-effect waves-light btn-secondary m-b-5" data-toggle="modal" data-target="#cancelModal"><i class="fa fa-ban"></i></button>-->
                                            <br>
                                            <!-- <?php
                                            if ($this->session->userdata['admin']['role_id'] == 1 || $this->session->userdata['admin']['role_id'] == 4) { ?>
                                                <select onchange="changeStatus(this.value,'<?php echo $order->order_id;?>')">
                                                    <option value="">Change Stauts</option>
                                                    <option value="Cancelled By Customer" <?php echo ($order->status == 'Cancelled By Customer' ? 'selected' : ''); ?>>Cancelled By Customer</option>
                                                    <option value="Cancelled By Admin" <?php echo ($order->status == 'Cancelled By Admin' ? 'selected' : ''); ?>>Cancelled By Admin</option>
                                                </select>
                                            <?php }
                                            ?> -->
                                        </td> 
                                </tr>
                            <?php 
                                }
                            }
                            ?>      
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div> <!-- container -->

    </div> <!-- content -->
    <!-- Modal -->
<div class="modal" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content p-0">
      <div class="modal-header px-3">
        <h5 class="modal-title text-danger" id="cancelModalLabel">Cancel Order!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="d-flex justify-content-between px-3">
            <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5">Cancel by Admin</button>
            <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5">Cancel by Customer</button>                
        </div>
      </div>
      
    </div>
  </div>
</div>
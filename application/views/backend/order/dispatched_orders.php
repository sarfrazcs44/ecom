<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <h4 class="page-title">Orders</h4>
                            <ol class="breadcrumb p-0 m-0">
                            </ol>
                            <div class="clearfix"></div>
                        </div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Order Track ID</th>
                                            <th>Status</th>
                                            <th>Assigned To</th>
                                            <th>Transaction ID</th>
                                            <th>Amount</th>

                                            
                                            <th>Created At</th>
                                            <th>Updated At</th>

                                            <?php
                                            if ($this->session->userdata['admin']['role_id'] == 1 || $this->session->userdata['admin']['role_id'] == 4 || $this->session->userdata['admin']['role_id'] == 5) { ?>
                                                <th>Actions</th>
                                            <?php }
                                            ?>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($orders){
                                                foreach($orders as $order){ ?>
                                                    <tr id="<?php echo $order->order_id;?>">
                                                    
                                                    <td><?php echo $order->order_track_id; ?></td>
                                                    <td id="status_<?php echo $order->order_id;?>">Out For Delivery</td>
                                                    <?php
                                                            $is_assigned = checkIfOrderAssinged($order->order_id);
                                                            if (!$is_assigned)
                                                            { 

                                                                $is_assigned = 'N/A';
                                                            } 

                                                             ?>
                                                    
                                                    <td><?php echo $is_assigned;?></td>
                                                     <td><?php echo $order->transaction_id; ?></td>
                                                    <td><?php echo $order->total_amount; ?></td>
                                                    
                                                    <td><?php echo $order->created_at; ?></td>
                                                    <td><?php echo $order->updated_at; ?></td>
                                                        
                                                    <td>
                                                       
                                                        <a href="<?php echo base_url('cms/order/view/'.$order->order_id);?>">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                        <br>
                                                          <!-- Split dropright button -->
                                                        <div class="btn-group dropright">
                                                            <button type="button" class="border-0 bg-transparent text-danger pl-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Change Status</button>
                                                            <button type="button" class="border-0 bg-transparent text-dark shadow-none dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <span class="sr-only">Toggle Dropright</span>
                                                            </button>
                                                            <div class="dropdown-menu bg-dark">
                                                                <button class="dropdown-item" type="button">Change Status</button>
                                                                <button class="dropdown-item" type="button">Cancelled By Customer</button>
                                                                <button class="dropdown-item" type="button">Cancelled By Admin</button>
                                                                    
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <?php
                                                        if ($this->session->userdata['admin']['role_id'] == 1 || $this->session->userdata['admin']['role_id'] == 4) { ?>
                                                            <select onchange="changeStatus(this.value,'<?php echo $order->order_id;?>')">
                                                                <option value="">Change Stauts</option>
                                                                <option value="Cancelled By Customer" <?php echo ($order->status == 'Cancelled By Customer' ? 'selected' : ''); ?>>Cancelled By Customer</option>
                                                                <option value="Cancelled By Admin" <?php echo ($order->status == 'Cancelled By Admin' ? 'selected' : ''); ?>>Cancelled By Admin</option>
                                                            </select>
                                                        <?php }
                                                        ?>
                                                        <?php /*
                                                        if ($this->session->userdata['admin']['role_id'] == 1 || $this->session->userdata['admin']['role_id'] == 4 || $this->session->userdata['admin']['role_id'] == 5) { ?>
                                                            <?php
                                                            $is_assigned = checkIfOrderAssinged($order->order_id);
                                                            if ($is_assigned)
                                                            { ?>
                                                                <span>This delivery is already assigned to "<?php echo $is_assigned; ?>"</span>
                                                            <?php }else{
                                                                $delivery_users = getDeliveryUsersForOrder($order->order_id);
                                                                if (count($delivery_users) > 0)
                                                                { ?>
                                                                    <select onchange="assignForDelivery(this.value,'<?php echo $order->order_id;?>')">
                                                                        <option disabled="disabled" selected>Choose user for delivery...</option>
                                                                        <?php
                                                                        foreach ($delivery_users as $delivery_user)
                                                                        {
                                                                            $exploded_data = explode('|', $delivery_user);
                                                                            $user_id = $exploded_data[0];
                                                                            $user_name = $exploded_data[1];
                                                                            ?>
                                                                            <option value="<?php echo $user_id; ?>"><?php echo $user_name; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                <?php }else{ ?>
                                                                    <span>No Delivery Men Assgined To This Order's City</span>
                                                                <?php }
                                                            } 
                                                            ?>
                                                        <?php } */
                                                        ?>
                                                        </td> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
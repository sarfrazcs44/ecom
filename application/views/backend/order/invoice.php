<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <h4 class="page-title">Order Detail</h4>
                <ol class="breadcrumb p-0 m-0">
                    <?php
                        if ($this->session->userdata['admin']['role_id'] != 2 && $order_items[0]['status'] == 'Dispatched') { ?>

                           
                        <?php } ?>
                        <li>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal">
                                    <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Order delivered to customer</button>
                                </a>
                            </li>
                    <!--<li>
                        <a href="#">
                            <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add New</button>
                        </a>
                    </li>-->
                    <li>
                        <a href="#">
                            
                            <button type="button" class="btn btn-secondary waves-effect w-md waves-light ml-3 rounded-pill"  data-toggle="modal" data-target="#staticBackdrop">Cancel Order</button>
                        </a>
                    </li>
                    
                    <!-- <li>
                        <a href="#">Tables </a>
                    </li>
                    <li class="active">
                        Responsive Table
                    </li>-->
                </ol>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3 border-0">
                        <!-- <div class="panel-heading">
                            <h4>Invoice</h4>
                        </div> -->
                        <div class="panel-body card-box">
                            <div class="row">
                                <div class="col-md-6 mt-3">
                                    <p class="m-0 largeFontSize lineHeight1-3">
                                        <span class="text-danger d-block w-100 mediumFontSize"><b>Order No.</b></span>
                                        <span class="text-dark  d-block w-100"><b><?php echo $order_items[0]['order_track_id'];?></b></span>
                                    </p>
                                </div>
                                <div class="col-md-6 mt-3">
                                    <div class="col-md-6">
                                        <p class="m-0 largeFontSize lineHeight1-3">
                                            <span class="text-danger d-block w-100 mediumFontSize"><b>Order Date</b></span>
                                            <span class="text-dark  d-block w-100"><b><?php echo date('d M, Y',strtotime($order_items[0]['order_date']));?> | <?php echo date('H:i',strtotime($order_items[0]['order_date']));?></b></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-6 mt-3">
                                <p class="m-0 largeFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100 mediumFontSize"><b>Customer info</b></span>
                                    <span class="text-dark  d-block w-100"><b><?php echo $order_items[0]['full_name'];?></b></span>
                                    <span class="text-dark  d-block w-100"><b><?php echo $order_items[0]['phone'];?></b></span>
                                    <span class="text-dark  d-block w-100"><b><?php echo $order_items[0]['email'];?></b></span>
                                </p>
                                </div>
                                <div class="col-md-6 mt-3">
                                <div class="col-md-6">
                                    <p class="m-0 largeFontSize lineHeight1-3">
                                        <span class="text-danger d-block w-100 mediumFontSize"><b>Delivered By</b></span>
                                        <span class="text-dark  d-block w-100"><b><?php echo $order_items[0]['driver_name'];?></b></span>
                                        <span class="text-dark  d-block w-100"><b><?php echo $order_items[0]['driver_phone'];?></b></span>
                                        <span class="text-dark  d-block w-100"><b><?php echo $order_items[0]['driver_email'];?></b></span>
                                    </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-6 mt-3">
                                <p class="m-0 largeFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100 mediumFontSize"><b>Payment Method</b></span>
                                    <span class="text-dark  d-block w-100"><b>Online Payment</b></span>
                                
                                </p>
                                </div>
                                <div class="col-md-6 mt-3">
                                <div class="col-md-6">
                                    <p class="m-0 largeFontSize lineHeight1-3">
                                        <span class="text-danger d-block w-100 mediumFontSize"><b>Transaction ID</b></span>
                                        <span class="text-dark  d-block w-100"><b><?php echo $order_items[0]['transaction_id'];?></b></span>
                                    
                                    </p>
                                    </div>
                                </div>
                            </div>

                            <?php if($order_items[0]['address_id'] > 0){ ?>
                            <div class="row mt-5">
                                <div class="col-md-12">
                                <p class="m-0 largeFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100 mediumFontSize"><b>Delivery Address</b></span>
                                    <span class="text-success  d-block w-100"><b>Mobile: <span class="text-dark"><?php echo $order_items[0]['phone'];?> </span></b></span>
                                    <span class="text-success  d-block w-100"><b>Building no: <span class="text-dark"><?php echo $order_items[0]['building_no'];?></span></b></span>
                                    <span class="text-success  d-block w-100"><b>Floor no: <span class="text-dark"><?php echo $order_items[0]['floor_no'];?> </span></b></span>
                                    <span class="text-success  d-block w-100"><b>Apartment no: <span class="text-dark"><?php echo $order_items[0]['apartment_no'];?></span></b></span>
                                    <span class="text-success  d-block w-100"><b>District: <span class="text-dark"><?php echo $order_items[0]['district'];?> </span></b></span>
                                    <span class="text-success  d-block w-100"><b>Address: <span class="text-dark"><?php echo $order_items[0]['location'];?></span></b></span>

                                        
                                </p>
                                </div>
                            </div>
                            <?php 
                        } ?>
                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <p class="m-0 largeFontSize lineHeight1-3">
                                        <span class="text-danger d-block w-100 mediumFontSize"><b>Status</b></span>
                                    </p>
                                    <div class="w-100">
                                        <?php 
                                        $status_image = '';
                                        if($order_items[0]['status'] == 'Received' && $order_items[0]['driver_name'] == ''){ 
                                             $status_image = 'orderPlaced.svg';
                                        }else if($order_items[0]['status'] == 'Delivered'){
                                            $status_image = 'deliverToCustomer.svg';


                                        }else if($order_items[0]['status'] == 'Received' && $order_items[0]['driver_name'] != ''){
                                            $status_image = 'assignToDriver.svg';


                                        }else if($order_items[0]['status'] == 'Dispatched'){
                                            $status_image = 'pcikByDriver.svg';


                                        }else if($order_items[0]['status'] == 'Cancelled By Customer' && $order_items[0]['driver_name'] != ''){
                                            $status_image = 'cancelByCustomer_afterAssign.svg';


                                        }else if($order_items[0]['status'] == 'Cancelled By Customer' && $order_items[0]['driver_name'] == ''){
                                            $status_image = 'cancelByCustomer_step_1.svg';


                                        }else if($order_items[0]['status'] == 'Cancelled By Admin'){
                                            $status_image = 'cancelByCustomer_step_1.svg';


                                        }

                                        ?>
                                        <img src="<?php echo base_url(); ?>assets/images/<?php echo $status_image; ?>">
                                        <!-- 
                                            order images

                                            assignToDriver.svg
                                            cancelByCustomer_afterAssign.svg
                                            cancelByCustomer_step_1.svg
                                            cancelByCustomer_step2.svg
                                            deliverToCustomer.svg
                                            orderPlaced.svg
                                            pcikByDriver.svg
                                            returnRequested.svg
                                            returnToWarehouse.svg

                                         -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3 border-0">
                        <!-- <div class="panel-heading">
                            <h4>Invoice</h4>
                        </div> -->
                        
                        <div class="panel-body card-box pb-0">
                            <p class="m-0 largeFontSize lineHeight1-3 px-2">
                                <span class="text-danger d-block w-100 mediumFontSize"><b>Invoice / Receipt</b></span>
                            </p>
                            <div class="d-flex py-2">
                            <div class="table-responsive  haveScroll slimscrollleft w-100">
                                <table class="table table-borderless w-100">
                                    <tbody>
                                         <?php 
                                                $total = 0;
                                                $ordered_itmes = getOrderItems($order_items[0]['order_id']);
                                                foreach($ordered_itmes as $key => $value){
                                                    $total = $total + ($value['palce_order_price']  * $value['quantity']);?>
                                                <tr>
                                                    <td style="width:52px">
                                                        <div class="imgBox"><img class="rounded" src="https://ecom.schopfen.com/assets/images/small/img-1.jpg" alt="product" height="45" width="45"></div>
                                                    </td>
                                                    <td>
                                                        <div class="text">
                                                            <p class="m-0 smallFontSize lineHeight1-3 ">
                                                                <span class="text-dark  d-block w-100"><b><?php echo ($value['model_en']);?></b></span>
                                                                <span class="text-dark small d-block w-100"><a href="<?php echo base_url('cms/product/edit/'.$value['product_id']);?>" class="text-dark" target="_blank"><?php echo $value['product_title_en'];?></a></span>
                                                                <span class="text-dark small d-block w-100">Price per unit <?php echo ($value['palce_order_price']);?> SAR</span>
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="text text-center">
                                                            <p class="m-0 smallFontSize lineHeight1-3">
                                                                <span class="text-dark  d-block w-100"><b>Quantity</b></span>
                                                                <span class="text-success small d-block w-100"><?php echo $value['quantity']; ?></span>
                                                                <span class="text-success small d-block w-100">Unit</span>
                                                            </p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="text text-center">
                                                            <p class="m-0 smallFontSize lineHeight1-3">
                                                                <span class="text-dark  d-block w-100"><b>Cost</b></span>
                                                                <span class="text-success small d-block w-100"><?php echo ($value['palce_order_price']  * $value['quantity']);?></span>
                                                                <span class="text-success small d-block w-100">SAR</span>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            </div> 
                        
                            <div class="tbody card-box shadow row">
                                <div class="row d-flex justify-content-end w-100">
                                    <div class="col-md-6">
                                        <p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark border-bottom border-secondary d-flex justify-content-between"><strong>Total</strong><strong><?php echo $total; ?> SAR</strong></p>
                                        <p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark border-bottom border-secondary d-flex justify-content-between"><strong>Delivery Charges</strong><strong><?php echo ($order_items[0]['delivery_charges'] == '' ? 'N/A' : $order_items[0]['delivery_charges'].' SAR'); ?> SAR</strong></p>
                                        <p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark border-bottom border-secondary d-flex justify-content-between"><strong>Tax (VAT 15%)</strong><strong><?php echo ($order_items[0]['vat_total'] == '' ? 'N/A' : $order_items[0]['vat_total'].' SAR'); ?> SAR</strong></p>
                                        <p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger border-bottom border-secondary d-flex justify-content-between"><strong>Grand Total</strong><strong><?php echo ($order_items[0]['total_amount'] == '' ? 'N/A' : $order_items[0]['total_amount'].' SAR'); ?> SAR</strong></p>
                                    </div>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->
<!--onclick="markAsDelivered(<?php echo $order_items[0]['order_id'];?>);"-->
<div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order Delivery OTP form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?php echo base_url();?>cms/order/mark_as_delivered" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
        
      <div class="modal-body">
       <div class="alert" id="validatio-msg" style="display: none;"></div>
            <input type="hidden" name="order_id" value="<?php echo $order_items[0]['order_id']; ?>">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Enter OTP:</label>
            <input type="text" class="form-control" id="otp" required name="otp" value="">
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
  </form>
    </div>
  </div>
</div>              
<style>
    .slimScrollDiv {width:100% !important;}
</style>


<!-- Modal -->
<div class="modal" id="staticBackdrop" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
      <div class="mdWrapperEd mx-auto">
          <div class=""><img src="<?php echo base_url(); ?>assets/images/msjwhite.png" width="82"></div>
    <div class="modal-content p-3 rounded">
        <form action="<?php echo base_url();?>cms/order/cancel_order" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
             <input type="hidden" name="order_id" value="<?php echo $order_items[0]['order_id']; ?>">
      <div class="modal-body py-2">
        <p class="text-danger largeFontSize"><strong>Are you sure you want to cancel this order?</strong></p>
        <div class="from-group">
            <label for="" class="text-danger mb-1 smallFontSize">Cancel Request by</label>
            <div class="row">
                <div class="col-sm-6">
                    <div class="radio radio-danger  pl-1">
                        <input type="radio" name="status" id="radio6" value="Cancelled By Customer">
                        <label for="radio6" class="text-dark">
                            Customer?
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="radio radio-danger  pl-1">
                        <input type="radio" name="status" id="radio7" value="Cancelled By Admin" checked>
                        <label for="radio7" class="text-dark">
                            Admin
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="" class="text-danger mb-1 smallFontSize">Reason?`</label>
            <textarea name="cancel_reason" id="" cols="30" rows="4" class="form-control" spellcheck="false"></textarea>
        </div>
        <button type="submit" class="btn btn-danger w-100" >Cancel Order</button>
      </div>
  </form>
    </div>
      </div>
  </div>
</div>
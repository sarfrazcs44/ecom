<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container-fluid">
			<div class="page-title-box">
				<h4 class="page-title" style="margin-right: 20px; ">Application Details</h4>
				<div class="clearfix"></div>
			</div>
			<!-- end row -->


			<div class="row">
				<div class="col-sm-12">

						<div class="col-lg-8">
                            <div class="card-box">
							<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0">
								<thead>
                                    <tr>
                                        <th colspan="2" class="th_heading">Details</th>
                                    </tr>
								</thead>
								<tbody>
								<?php if($career) { ?>
									<tr>
										<td style="width: 120px;">Full Name</td>
										<td><?php echo ucfirst($career->full_name); ?></td>
									</tr>
                                    <tr>
                                        <td style="width: 120px;">Email Address</td>
                                        <td><?php echo ucfirst($career->email); ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px;">Phone</td>
                                        <td><?php echo ucfirst($career->phone); ?></td>
                                    </tr>
									<tr>
										<td>Nationality</td>
										<td><?php echo ucfirst($career->nationality); ?></td>
									</tr>

                                    <tr>
                                        <td>Attachment</td>
                                        <td><a href="<?php echo base_url() . $career->file ?>" download>Download</a></td>
                                    </tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
                    </div>
				</div>
			</div>

		</div> <!-- container -->

	</div> <!-- content -->

	<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="map_with_pin"></div>
				</div>
			</div>
		</div>
	</div><!--/modal-->



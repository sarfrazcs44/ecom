<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Edit Home Image</h4>
                               
                                <form action="<?php echo base_url();?>cms/homeimage/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="homeimage_id" value="<?php echo $homeimage_id;?>">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                   
                                    <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="title_en" id="title-eng" required value="<?php echo $result->title_en; ?>">
                                     </div>
                                    <div class="form-group">
                                       <label for="title-url">Url :</label>
                                       <input type="text" class="form-control" name="url" id="title-url"  value="<?php echo $result->url; ?>">
                                     </div>
                                     
                                    <?php if($result->image != ''){ ?>
                                    <img src="<?php echo base_url($result->image);?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
                                    <div class="form-group">
                                    <label for="image">Please Choose Image : (380 * 380) OR (760 * 380) OR (380 * 760)</label>
                                    <input type="file" class="filestyle" id="image" name="image[]" data-placeholder="No Image">
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="title_ar" id="title-arb" required="" value="<?php echo $result->title_ar; ?>">
                                     </div>
                                    <div class="form-group" style="overflow:hidden">
                                        <span style="float: left;margin-right: 15px;">Open in new tab&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="checkbox" id="switch8" switch="primary" <?php echo ($result->is_new_tab == 1 ? 'checked=""' : ''); ?> name="is_new_tab">
                                        <label style="display: inline-block;float: left;" for="switch8" data-on-label="Yes" data-off-label="No"></label>
                                       
                                    </div>
                                    <div class="form-group" style="overflow:hidden">
                                        <span style="float: left;margin-right: 15px;">Is Active&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="checkbox" id="switch7" switch="primary" <?php echo ($result->is_active == 1 ? 'checked=""' : ''); ?> name="is_active">
                                        <label style="display: inline-block;float: left;" for="switch7" data-on-label="Yes" data-off-label="No"></label>
                                       
                                    </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                    </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    

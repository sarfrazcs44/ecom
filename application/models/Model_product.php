<?php
Class Model_product extends Base_Model
{
	public function __construct()
	{
		parent::__construct("products");
		
	}
    
    
  public function getAllProducts($data , $limit = false)
  {
      
      $this->db->select('p.*,c.title_en as category_title_en,c.title_ar as category_title_ar,c.image as category_image,sb.image as sub_category_image,sb.title_en as sub_category_title_en,sb.title_ar as sub_category_title_ar,b.title_en as brand_title_en,b.title_ar as brand_title_ar,b.image as brand_image');
      $this->db->from('products p');
      $this->db->join('categories c','p.category_id = c.category_id','left');
      $this->db->join('categories sb','p.sub_category_id = sb.category_id','left');
      $this->db->join('brands b','p.brand_id = b.brand_id','left');
      $this->db->where('p.is_active',1);
      $this->db->where('c.is_active',1);
      $this->db->where('sb.is_active',1);
      
      if(isset($data['category_id'])){
          $this->db->where('p.category_id',$data['category_id']);
      }
      if(isset($data['product_id'])){
          $this->db->where('p.product_id',$data['product_id']);
      }
      if(isset($data['sub_category_id'])){
          $this->db->where('p.sub_category_id',$data['sub_category_id']);
      }
      if(isset($data['brand_id'])){
          $this->db->where('p.brand_id',$data['brand_id']);
      }
      
      if(isset($data['is_featured'])){
          $this->db->where('p.is_featured',$data['is_featured']);
      }

      if(isset($data['company_id'])){
          $this->db->where('p.company_id',$data['company_id']);
      }

      if ($limit)
      {
          $this->db->limit(4, 0);
      }
      
      
      return $this->db->get()->result_array();
          
      
  }

  public function getAllProductsForBackend($data , $limit = false,$start = 0)
  {
      
      $this->db->select('p.*,c.title_en as category_title_en,c.title_ar as category_title_ar,c.image as category_image,sb.image as sub_category_image,sb.title_en as sub_category_title_en,sb.title_ar as sub_category_title_ar,b.title_en as brand_title_en,b.title_ar as brand_title_ar,b.image as brand_image');
      $this->db->from('products p');
      $this->db->join('categories c','p.category_id = c.category_id','left');
      $this->db->join('categories sb','p.sub_category_id = sb.category_id','left');
      $this->db->join('brands b','p.brand_id = b.brand_id','left');
      
      
      if(isset($data['category_id'])){
          $this->db->where('p.category_id',$data['category_id']);
      }
      if(isset($data['product_id'])){
          $this->db->where('p.product_id',$data['product_id']);
      }
      if(isset($data['sub_category_id'])){
          $this->db->where('p.sub_category_id',$data['sub_category_id']);
      }
      if(isset($data['brand_id'])){
          $this->db->where('p.brand_id',$data['brand_id']);
      }
      
      if(isset($data['is_featured'])){
          $this->db->where('p.is_featured',$data['is_featured']);
      }

      if(isset($data['company_id'])){
          $this->db->where('p.company_id',$data['company_id']);
      }

      if ($limit)
      {
          $this->db->limit($limit, $start);
      }
      
      
      return $this->db->get()->result_array();
          
      
  }


  public function getCountProducts($where = false)
  {
      
      $this->db->select('Count(products.product_id) as total');
      $this->db->from('products');
      if($where){
        $this->db->where($where);
      }
      
      return $this->db->get()->result_array();
          
      
  }

  public function gettopSelling($where = false)
  {
      
      $this->db->select('Count(order_items.product_id) as total,products.product_title_en,products.price,products.product_id');
      $this->db->from('order_items');
      $this->db->join('products','products.product_id = order_items.product_id');
      if($where){
        $this->db->where($where);
      }

      $this->db->group_by('products.product_id');
      $this->db->order_by('total','DESC');
      
      return $this->db->get()->result_array();
          
      
  }
	
		
}
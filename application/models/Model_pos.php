<?php
Class Model_pos extends Base_Model
{
	public function __construct()
	{
		parent::__construct("point_of_sales");
		
	}


	public function getData($where = false)
    {
        $this->db->select('*');
        $this->db->from('point_of_sales');
        if($where){
        	$this->db->where($where);
        }
        
        return $this->db->get()->result();
    }
    
    
    
	
		
}
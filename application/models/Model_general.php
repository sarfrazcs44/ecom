<?php

Class Model_general extends CI_Model

{

    public function __construct()

    {

        parent::__construct();

    }


    public function getRow($id, $table_name, $as_array = false,$field = false)

    {
        if($field){
            $result = $this->db->get_where($table_name, array($field => $id));
        }else{
            $result = $this->db->get_where($table_name, array('id' => $id));
        }
        

        //echo $this->db->last_query(); exit();

        if ($result->num_rows() > 0) {

            $row = $result->row_array();


            if ($as_array) {

                return $row;

            }


            foreach ($row as $col => $val) {

                $this->$col = $val;

            }


            return $this;

        } else {

            return false;

        }

    }


    public function getSingleRow($table_name, $field, $as_array = false)

    {

        $result = $this->db->get_where($table_name, $field);


        if ($result->num_rows() > 0) {

            $row = $result->row_array();


            if ($as_array) {

                return $row;

            }


            foreach ($row as $col => $val) {

                $this->$col = $val;

            }


            return $this;

        } else {

            return false;

        }

    }


    public function getAll($table_name, $as_array = false, $idOrderBy = 'asc', $orderBy = 'id')

    {


        $this->db->order_by($orderBy, $idOrderBy);


        $result = $this->db->get($table_name);


        if ($as_array) {

            return $result->result_array();

        }


        return $result->result();

    }


    public function getMultipleRows($table_name, $fields, $as_array = false, $idOrderBy = 'asc', $orderBy = 'id')

    {


        $this->db->order_by($orderBy, $idOrderBy);


        $result = $this->db->get_where($table_name, $fields);


        //echo $this->db->last_query(); exit();


        if ($result->num_rows() > 0) {


            if ($as_array) {

                return $result->result_array();

            }


            return $result->result();

        } else {

            return false;

        }

    }

    /* Insert user */

    public function save($table, $data)
    {
        $this->db->set($data);
        $this->db->insert($table);
        $insertId = $this->db->insert_id();
        if ($insertId > 0) {

            return $insertId;
        } else {
            return false;
        }
    }

    public function updateRow($table_name, $data, $id,$field = false)
    {

        if($field){
            $this->db->where($field, $id);
        }else{
            $this->db->where('id', $id);
        }
        
        $this->db->update($table_name, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    public function approveProject($id)
    {

        $query = $this->db->query("UPDATE projects SET active_status = 1 WHERE  id = '" . $id . "'");

        return true;


    }


    public function verifyrequests()
    {

        $query = $this->db->query("SELECT * FROM  users where is_verified = 1");

        return $query->result();


    }


    public function deleteRow($table_name, $id,$field = false)
    {
        if($field){
            $this->db->where($field, $id);
        }else{
            $this->db->where('id', $id);
        }
        
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }

    public function deleteMultipleRow($table_name, $field, $id)
    {
        $this->db->where($field, $id);
        $this->db->delete($table_name);
        if ($this->db->affected_rows() > 0) {
            return TRUE;

        } else {
            return false;
        }
    }


    public function validateLogin($username, $password)
    {
        $q = "select * from users where username = '" . $username . "' and password = '" . md5($password) . "' and status = 'active' and user_type_id != 1";
        $query = $this->db->query($q);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }


    function getRecordsCount($table_name)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->num_rows();
    }
	
	function getRecordsCountWhere($table_name, $fields)
    {

        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->where($fields);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getAllWarehouseUsers($as_array = false,$company_id = false)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id','4');
        if($company_id){
            $this->db->where('company_id',$company_id);

        }
        if($as_array){
            return $this->db->get()->result_array();
        }
        return $this->db->get()->result();
    }

    public function getAllDeliveryUsers($where = false)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id','5');
        if($where){
            $this->db->where($where);
        }
        return $this->db->get()->result();
    }


     public function getUserDistricts($where = false)
    {
        $this->db->select('districts.*');
        $this->db->from('districts');
        $this->db->join('user_districts','user_districts.district_id = districts.district_id');
        if($where){
            $this->db->where($where);
        }
        return $this->db->get()->result_array();
    }


}
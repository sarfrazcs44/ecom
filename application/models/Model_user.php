<?php
Class Model_user extends Base_Model
{
	public function __construct()
	{
		parent::__construct("users");
		
	}
    
    
    
   /* public function getUser($data){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$data['email']);
        $this->db->where('password',$data['password']);
        $this->db->where('role_id !=',3);
        $result = $this->db->get();
        if($result->num_rows > 0){
            return $result->result();
        }else
        {
            return false;
        }
    }*/

   public function checkIfUsernameAvailable($username)
   {
       $this->db->select('*');
       $this->db->from('users');
       $this->db->where('username',$username);
       $this->db->where('role_id',3);
       $result = $this->db->get();
       return $result->num_rows();
   }

    public function checkIfEmailAvailable($email)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$email);
        $this->db->where('role_id',3);
        $result = $this->db->get();
        return $result->num_rows();
    }

    public function getUsers($where)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function getDrivers($where)
    {
        $this->db->select('COUNT(assigned_orders_for_delivery.user_id) as total_deliveries,users.*');
        $this->db->from('users');
        $this->db->join('assigned_orders_for_delivery','assigned_orders_for_delivery.user_id = users.user_id');
        $this->db->where($where);
        $this->db->order_by('total_deliveries','DESC');
        $this->db->group_by('users.user_id');
        $result = $this->db->get();
        return $result->result_array();
    }

   // checkIfEmailAvailable
	
		
}
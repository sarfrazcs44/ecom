<?php

Class Model_support_tickets extends Base_Model
{
    public function __construct()
    {
        parent::__construct("support_tickets");

    }


    public function getUserData($ticket_id)
    {
        $this->db->select('users.*,support_tickets.*');
        $this->db->from('users');
        $this->db->join('support_tickets', 'support_tickets.user_id = users.user_id ');
        $this->db->where('support_tickets.ticket_id', $ticket_id);
        return $this->db->get()->row_array();

    }

    public function getTickets($user_id)
    {
        $sql = "SELECT * FROM support_tickets WHERE user_id = $user_id ORDER BY ticket_id DESC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function getOpenTickets()
    {
        $sql = "SELECT * FROM support_tickets WHERE closed_request = 0 ORDER BY ticket_id DESC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function getClosedTickets()
    {
        $sql = "SELECT * FROM support_tickets WHERE (closed_request = 1 OR closed_request = 2) ORDER BY ticket_id DESC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function getReOpenedTickets()
    {
        $sql = "SELECT support_tickets.*, users.full_name as ticket_last_closed_by FROM support_tickets LEFT JOIN users ON support_tickets.last_closed_by = users.user_id WHERE closed_request = 3 ORDER BY ticket_id DESC";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }


}
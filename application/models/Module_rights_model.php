<?php
Class Module_rights_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("modules_rights");

    }
    
    
    
    
    public function getModulesWithRights($role_id,$where = false){
        
            
            $this->db->select('modules.*,modules_rights.*');
            $this->db->from('modules');
            
            $this->db->join('modules_rights','modules.module_id = modules_rights.module_id');
            $this->db->join('roles','roles.role_id = modules_rights.role_id');
            
            
            
            
            $this->db->where('modules.Hide','0');
            $this->db->where('roles.role_id',$role_id);
            
            
            if($where){
                $this->db->where($where);
            }
            
            $this->db->group_by('modules.module_id');
            
            $this->db->order_by('modules.sort_order','ASC');
            $result = $this->db->get();
           //echo $this->db->last_query();exit;
            return $result->result();
    }

    
}
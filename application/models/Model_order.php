<?php

Class Model_order extends Base_Model
{
    public function __construct()
    {
        parent::__construct("orders");
    }

    public function getDetail($order_id)
    {
        $this->db->select('users.full_name,users.email,,address.*,orders.*,city.eng_name as city,districts.eng_name as district,districts.delivery_charges as district_delivery_charges,assigned_orders_for_delivery.user_id as assign_to,driver_user.full_name as driver_name,driver_user.profile_pic as driver_profile_pic');
        $this->db->from('orders');
        $this->db->join('address', 'address.address_id = orders.address_id','left');
        $this->db->join('assigned_orders_for_delivery assigned_orders_for_delivery', 'assigned_orders_for_delivery.order_id = orders.order_id','left');
        $this->db->join('users driver_user', 'assigned_orders_for_delivery.user_id = driver_user.user_id','left');
         $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        $this->db->join('users', 'users.user_id = orders.user_id');

        $this->db->where('orders.order_id', $order_id);
        return $this->db->get()->result_array();
    }

    public function getOrderItems($order_id)
    {
        $this->db->select('order_items.quantity,order_items.price as palce_order_price,products.*,product_images.product_image');
        $this->db->from('order_items');
        $this->db->join('products', 'products.product_id = order_items.product_id');
        $this->db->join('product_images', 'products.product_id = product_images.product_id','left');
        $this->db->where('order_items.order_id', $order_id);
        $this->db->group_by('products.product_id');
        return $this->db->get()->result_array();
    }

    public function getOrderDetail($order_id,$company_id = false)
    {
        $this->db->select('users.full_name,users.email,users.phone,orders.*,orders.created_at as order_date,order_items.quantity,order_items.price as palce_order_price,products.*,address.*,city.eng_name as city,districts.eng_name as district,districts.delivery_charges as district_delivery_charges,driver.full_name as driver_name,driver.email as driver_email,driver.phone as driver_phone');
        $this->db->from('orders');
        $this->db->join('order_items', 'order_items.order_id = orders.order_id');
        $this->db->join('products', 'products.product_id = order_items.product_id');
        $this->db->join('users', 'users.user_id = orders.user_id');
        $this->db->join('address', 'address.address_id = orders.address_id');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        $this->db->join('assigned_orders_for_delivery', 'assigned_orders_for_delivery.order_id = orders.order_id','left');
        $this->db->join('users driver', 'driver.user_id = assigned_orders_for_delivery.user_id','left');
        $this->db->where('orders.order_id', $order_id);
        if($company_id){
            $this->db->where('orders.company_id', $company_id);

        }
        return $this->db->get()->result_array();
    }


    public function getOrders($where = false)
    {
        $this->db->select('users.full_name,users.email,orders.*,orders.created_at as order_date,order_items.quantity,order_items.price as palce_order_price,products.*,address.*,city.eng_name as city,districts.eng_name as district,districts.delivery_charges as district_delivery_charges');
        $this->db->from('orders');
        $this->db->join('order_items', 'order_items.order_id = orders.order_id');
        $this->db->join('products', 'products.product_id = order_items.product_id');
        $this->db->join('users', 'users.user_id = orders.user_id');
        $this->db->join('address', 'address.address_id = orders.address_id');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        if($where){
             $this->db->where($where);

        }
       
        return $this->db->get()->result();
    }

    public function getAllCurrentOrdersForUser($user_id)
    {
        $this->db->select('users.full_name,users.email,orders.*,address.*,city.eng_name as city,districts.eng_name as district,districts.delivery_charges as district_delivery_charges');
        $this->db->from('orders');
        $this->db->join('address', 'address.address_id = orders.address_id','left');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        $this->db->join('users', 'users.user_id = orders.user_id');
        $this->db->where('orders.status !=', 'Delivered');
        $this->db->where('orders.user_id', $user_id);
        return $this->db->get()->result_array();
    }


    public function getAllOrdersForUser($user_id,$where = false)
    {
        $this->db->select('orders.*,orders.created_at as order_date,city.eng_name as city,districts.eng_name as district,assigned_orders_for_delivery.user_id as assign_to,driver_user.full_name as driver_name,driver_user.profile_pic as driver_profile_pic');
        $this->db->from('orders');
        $this->db->join('address', 'address.address_id = orders.address_id','left');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        $this->db->join('users', 'users.user_id = orders.user_id');
        $this->db->join('assigned_orders_for_delivery', 'assigned_orders_for_delivery.order_id = orders.order_id','left');
        $this->db->join('users driver_user', 'assigned_orders_for_delivery.user_id = driver_user.user_id','left');
        if($where){
            $this->db->where($where);
        }
        //$this->db->where('orders.status !=', 'Delivered');
        $this->db->where('orders.user_id', $user_id);
        return $this->db->get()->result_array();
    }

    public function getAllHistoryOrdersForUser($user_id)
    {
        $this->db->select('users.full_name,users.email,orders.*,address.*,city.eng_name as city,districts.eng_name as district');
        $this->db->from('orders');
        $this->db->join('address', 'address.address_id = orders.address_id','left');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        $this->db->join('users', 'users.user_id = orders.user_id');
        $this->db->where('orders.status', 'Delivered');
        $this->db->where('orders.user_id', $user_id);
        return $this->db->get()->result_array();
    }

    public function getAllCurrentOrdersDetailsForUser($user_id)
    {
        $this->db->select('users.full_name,users.email,orders.*,orders.created_at as order_date,order_items.quantity,order_items.price as palce_order_price,products.*,address.*');
        $this->db->from('orders');
        $this->db->join('order_items', 'order_items.order_id = orders.order_id');
        $this->db->join('products', 'products.product_id = order_items.product_id');
        $this->db->join('users', 'users.user_id = orders.user_id');
        $this->db->join('address', 'address.address_id = orders.address_id');
        $this->db->where('orders.status !=', 'Delivered');
        $this->db->where('orders.user_id', $user_id);
        return $this->db->get()->result_array();
    }

    public function getAllHistoryOrdersDetailsForUser($user_id)
    {
        $this->db->select('users.full_name,users.email,orders.*,orders.created_at as order_date,order_items.quantity,order_items.price as palce_order_price,products.*,address.*,city.eng_name as city,districts.eng_name as district');
        $this->db->from('orders');
        $this->db->join('order_items', 'order_items.order_id = orders.order_id');
        $this->db->join('products', 'products.product_id = order_items.product_id');
        $this->db->join('users', 'users.user_id = orders.user_id');
        $this->db->join('address', 'address.address_id = orders.address_id');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        $this->db->where('orders.status', 'Delivered');
        $this->db->where('orders.user_id', $user_id);
        return $this->db->get()->result_array();
    }



    public function getOrdersNotPicked($order_status = false, $districts = array(), $user_id = '',$return_type = false,$company_id = false) // 3rd parameter to get orders assigned to this delivery user only
    {

        $this->db->select('orders.*,orders.created_at as order_date,city.eng_name as city,districts.eng_name as district');
        $this->db->from('orders');
        $this->db->join('assigned_orders_for_delivery assigned_orders_for_delivery', 'assigned_orders_for_delivery.order_id = orders.order_id');
        $this->db->join('address', 'address.address_id = orders.address_id');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        if($order_status){
            $this->db->where('orders.status', $order_status);
        }

        if($company_id){
            $this->db->where('orders.company_id', $company_id);
        }
        
        if ($user_id > 0)
        {
            $this->db->where('assigned_orders_for_delivery.user_id', $user_id);
        }
        if(!empty($districts)){
             $this->db->where_in('address.district_id', $districts);
        }
       
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        if($return_type){
            return $result->result_array();
        }
        return $result->result();
    }

    public function getOrdersForWarehouseOrDeliveryUser($order_status = false, $districts, $user_id = '',$return_type = false,$where = false) // 3rd parameter to get orders assigned to this delivery user only
    {

        $this->db->select('orders.*,orders.created_at as order_date,city.eng_name as city,districts.eng_name as district,assigned_orders_for_delivery.user_id as assign_to,driver_user.full_name as driver_name,driver_user.profile_pic as driver_profile_pic');
        $this->db->from('orders');
        $this->db->join('assigned_orders_for_delivery assigned_orders_for_delivery', 'assigned_orders_for_delivery.order_id = orders.order_id','left');
        $this->db->join('users driver_user', 'assigned_orders_for_delivery.user_id = driver_user.user_id','left');
        $this->db->join('address', 'address.address_id = orders.address_id');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        if($order_status){
            $this->db->where('orders.status', $order_status);
        }

        if($where){
            $this->db->where($where);
        }
        
        if ($user_id > 0)
        {
            $this->db->where('assigned_orders_for_delivery.user_id', $user_id);
        }
        $this->db->where_in('address.district_id', $districts);
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        if($return_type){
            return $result->result_array();
        }
        return $result->result();
    }


    public function getOrdersOfPos($where = false) // 3rd parameter to get orders assigned to this delivery user only
    {

        $this->db->select('orders.*,orders.created_at as order_date');
        $this->db->from('orders');
        
       
        
        $this->db->where('orders.status', 'Delivered');
        $this->db->where('orders.is_pos_order', 1);
        if($where){
            $this->db->where($where);
        }
       
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
       
        return $result->result();
    }



    public function getOrdersForWarehouseOrDeliveryUserReceivedNotAssigned($order_status = false, $districts = array(), $user_id = '',$return_type = false,$company_id = false,$limit = false) // 3rd parameter to get orders assigned to this delivery user only
    {

        $this->db->select('orders.*,customer.full_name,orders.created_at as order_date,city.eng_name as city,districts.eng_name as district');
        $this->db->from('orders');
        $this->db->join('assigned_orders_for_delivery assigned_orders_for_delivery', 'assigned_orders_for_delivery.order_id = orders.order_id','left');
        $this->db->join('users customer', 'customer.user_id = orders.user_id','left');
        $this->db->join('address', 'address.address_id = orders.address_id');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        if($order_status){
            $this->db->where('orders.status', $order_status);
        }

        if($company_id){
             $this->db->where('orders.company_id', $company_id);
        }
        
        $this->db->where('assigned_orders_for_delivery.user_id', NULL,true);
        if(!empty($districts)){
             $this->db->where_in('address.district_id', $districts);
        }

        if($limit){
            $this->db->limit($limit);
        }

        $this->db->order_by('orders.order_id','DESC');
       
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
        if($return_type){
            return $result->result_array();
        }
        return $result->result();
    }


    public function getTodaySaleSum($where = false) // 3rd parameter to get orders assigned to this delivery user only
    {

        $this->db->select('SUM(orders.total_amount) as today_total');
        $this->db->from('orders');
        
        if($where){
            $this->db->where($where);
        }

        $this->db->where('orders.status !=', 'Cancelled By Customer');
        $this->db->where('orders.status !=', 'Cancelled By Admin');
       
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
       
        if($result->result()[0]->today_total != ''){
            return $result->result()[0]->today_total;
        }else{
            return 0;
        }
    }

    public function getTodayItemSum($where = false) // 3rd parameter to get orders assigned to this delivery user only
    {

        $this->db->select('SUM(orders.total_items) as today_total');
        $this->db->from('orders');
        
        if($where){
            $this->db->where($where);
        }

        $this->db->where('orders.status !=', 'Cancelled By Customer');
        $this->db->where('orders.status !=', 'Cancelled By Admin');
       
        $result = $this->db->get();
        //echo $this->db->last_query();exit();
       
        if($result->result()[0]->today_total != ''){
            return $result->result()[0]->today_total;
        }else{
            return 0;
        }
    }


}
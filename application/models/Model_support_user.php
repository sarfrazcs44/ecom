<?php
Class Model_support_user extends Base_Model
{
	public function __construct()
	{
		parent::__construct("support_users");
		
	}

	public function getAllSupportUsers($company_id = false)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id','2');
        if($company_id){
        	$this->db->where('company_id',$company_id);

        }
        return $this->db->get()->result();
    }
}
<?php
Class Model_address extends Base_Model
{
	public function __construct()
	{
		parent::__construct("address");
		
	}



	public function getAllAddress($where = false){

		$this->db->select('address.*,city.eng_name as city_eng_name,city.arb_name as city_arb_name,districts.eng_name as district_eng_name,districts.arb_name as district_arb_name,districts.delivery_charges as district_delivery_charges');
        $this->db->from('address');
        $this->db->join('city', 'address.city_id = city.id','left');
        $this->db->join('districts', 'address.district_id = districts.district_id','left');
        if($where){
        	$this->db->where($where);
        }
        
        return $this->db->get()->result_array();
	}
    
    
    
	
		
}
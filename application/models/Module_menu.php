<?php
Class Module_menu extends Base_Model
{
	public function __construct()
	{
		parent::__construct("modules");
		
	}

	public function getModules($where = false)
    {
        $this->db->select('*');
        $this->db->from('modules');
        if($where){
        	$this->db->where($where);
        }
        $this->db->order_by('modules.sort_order','ASC');
        return $this->db->get()->result_array();
    }
}
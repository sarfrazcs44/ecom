<?php

/*
 * English language
 */

$lang['text_rest_invalid_api_key'] = 'Invalid API key %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';
$lang['home_top_section_heading'] = 'ShopBox';



$lang['nav_whoweare'] = 'Who We Are';
$lang['nav_ourbrands'] = 'Our Brands';
$lang['nav_forcustomers'] = 'For Customers';
$lang['nav_language'] = 'Language';
$lang['home_top_section_heading'] = 'MSJ Security System';
$lang['home_top_section_paragraph'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.';
$lang['home_top_section_button'] = 'Continue Reading';
$lang['home_filter_heading'] = 'Product Range';
$lang['home_filter_category'] = 'Categories';
$lang['home_filter_brand'] = 'Brands';
$lang['home_filter_subvategory'] = 'Sub Categories';
$lang['home_filter_button'] = 'Filter';
$lang['home_load_more'] = 'Load More';
$lang['home_ourclients'] = 'Our Clients';
$lang['home_testimonialname'] = 'Jameel Al Manea';
$lang['home_testimonialdesignation'] = 'CEO Al Jamea';
$lang['home_testimonialcontent'] = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.';
$lang['home_newsletter'] = 'Subscribe our Newsletter';
$lang['home_newsletterplaceholder'] = 'Enter your Email';
$lang['home_newsletterbutton'] = 'Subscribe';
$lang['home_footercopyright'] = 'Copyright | Niehez';
$lang['home_faq'] = 'FAQs';
$lang['home_becomeareseller'] = 'Become a Reseller';
$lang['home_downloadcenter'] = 'Download Center';
$lang['home_contactus'] = 'Contact Us';
$lang['home_careers'] = 'Careers';
$lang['terms_and_conditions'] = 'Terms And Conditions';

/* All products */

$lang['allproducts_heading'] = 'All Products';
$lang['allproducts_button'] = 'Find a Product';


/* Categories */

$lang['categories_upper_heading'] = 'Products';
$lang['categories_content'] = '<b>MSJ Security Systems</b> has the advantage of high-quality industry leading products, which meets all the needs of our clients.  We provide all kinds of security systems and solutions such as';
$lang['categories_lower_heading'] = 'Categories';
$lang['categories_button'] = 'Find a Product';


/* Contact */


$lang['contact_heading'] = 'Contact Us';
$lang['contact_content'] = 'Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.';
$lang['contact_mainbranch'] = 'Head Office';
$lang['contact_branch1'] = 'Branch Office Riyadh';
$lang['contact_branch2'] = 'Branch Office Jeddah';
$lang['contact_branch3'] = 'Branch Office Jeddah';
$lang['contact_address'] = 'MSJ Tower Al-Olaya , Riyadh';
$lang['contact_pobox'] = 'P.O box 12321 , Riyadh 14121 ,
<br/>Kingdom of SaudiArabia';


/* Customer Service */

$lang['customer_heading'] = 'Customer Service';
$lang['customer_subheading'] = 'Contact our Toll free Number';
$lang['customer_content'] = 'Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.';
$lang['customer_sendafeedback'] = 'Send a feedback';
$lang['customer_submitbutton'] = 'Submit';
$lang['customer_fullname'] = 'Full Name';
$lang['customer_email'] = 'Email';
$lang['customer_city'] = 'City';
$lang['customer_mobile'] = 'Mobile';
$lang['customer_message'] = 'Message';
$lang['complain'] = 'Complain';
$lang['visit'] = 'Visit';
$lang['reg_com'] = 'Register Complain';
$lang['confirm'] = 'Confirm';
$lang['visit_date'] = 'Please Select Date And Time';



/* Downloads*/


$lang['download_companyprofileheading'] = 'Company Profile';
$lang['download_companyprofilesubheading1'] = 'MSJ Company';
$lang['download_companyprofilesubheading2'] = 'Profile 2017';
$lang['download_brandcatalogue'] = 'Brand Catalogue';
$lang['download_samsungCCTV'] = 'Samsung CCTV';
$lang['download_catalogue'] = 'Catalogue';

/* Login */


$lang['Login_heading'] = 'Log in';
$lang['Login_subheading'] = 'To access your information';
$lang['Login_button'] = 'Login';
$lang['Login_username'] = 'User Name';
$lang['Login_password'] = 'Password';
$lang['Login_forgot'] = 'Forgot my password';
$lang['Login_noaccount'] = 'Dont have an account';
$lang['Login_register'] = 'Register Now';
$lang['register_heading'] = 'Register';
$lang['register_fullname'] = 'Full Name';
$lang['register_username'] = 'User Name';
$lang['register_email'] = 'Email';
$lang['register_phone'] = 'Phone';
$lang['register_city'] = 'City';
$lang['register_country'] = 'Country';
$lang['register_password'] = 'Password';
$lang['register_confirmpassword'] = 'Confirm Password';
$lang['register_male'] = 'Male';
$lang['register_female'] = 'Female';
$lang['register_submit'] = 'Submit';
$lang['register_description'] = 'About';
$lang['company_name'] = 'Company Name';
$lang['domain_name'] = 'Domain Name';





/* Product Detail */

$lang['productdetail_button'] = 'Find a Product';
$lang['productdetail_description'] = 'Product Description';
$lang['productdetail_usage'] = 'Usage';
$lang['productdetail_related'] = 'Related Products';


/* Product */

$lang['product_button'] = 'Find a Product';


/* Subcategories */

$lang['subcategories_button'] = 'Find a Product';


/* Who we are */

$lang['whoweare_heading'] = 'Who we are';



/* Questions */

$lang['question_heading'] = 'Frequent Questions';
$lang['question_field'] = 'Search a Question';




/* Ticketing */

$lang['ticketing_new'] = 'File a new complaint';
$lang['ticketing_completed'] = 'Completed Tickets';
$lang['ticketing_current'] = 'Current Tickets';
$lang['ticketing_chat'] = 'Chat';


/* Live Chat */

$lang['chat_livechat'] = 'Live Chat';
$lang['chat_subject'] = 'Subject';




/* Become a Reseller */

$lang['reseller_heading'] = 'Become A Seller';

$lang['reseller_form_companyname'] = 'Company Name';
$lang['reseller_form_directorname'] = 'Director Name';
$lang['reseller_form_mobile'] = 'Mobile';
$lang['reseller_form_email'] = 'E-mail';
$lang['reseller_form_tel'] = 'Tel';
$lang['reseller_form_fax'] = 'Fax';
$lang['reseller_form_file_heading'] = 'In order to complete the request,kindly, upload PDF file contains: CR, dealing request, company profile';
$lang['reseller_form_sendbutton'] = 'Send';

$lang['reseller_heading_resellerprogram'] = 'Reseller Program:';
$lang['reseller_content_resellerprogram'] = "Qualified system integrators and reseller can apply for our dedicated partner's program membership with following details:";

$lang['reseller_heading_keybenefits'] = 'Key Benefits:';
$lang['reseller_li1_keybenefits'] = 'Support in System design & identifying proper solutions';
$lang['reseller_li2_keybenefits'] = 'Exclusive pricing support for large scale projects';
$lang['reseller_li3_keybenefits'] = 'Dedicated technical support & online remote assistance';
$lang['reseller_li4_keybenefits'] = 'Free periodical training programs on latest products';
$lang['reseller_li5_keybenefits'] = 'On-demand training program for project based solutions';
$lang['reseller_li6_keybenefits'] = 'Large stock for immediate delivery on order';
$lang['reseller_li7_keybenefits'] = 'Spare parts stock for quick after sales support';


$lang['reseller_heading_whocanapply'] = 'Who can apply?';
$lang['reseller_li1_whocanapply'] = 'Security system integrators & resellers';
$lang['reseller_li2_whocanapply'] = 'Low current systems';
$lang['reseller_li3_whocanapply'] = 'Computer and network firms';
$lang['reseller_li4_whocanapply'] = 'Electro mechanical contractors & Main contractors in the projects';

$lang['reseller_heading_requirement'] = 'Requirements for registration:';
$lang['reseller_li1_requirement'] = 'Duly updated company profile';
$lang['reseller_li2_requirement'] = 'Full contact information with postal address';
$lang['reseller_li3_requirement'] = 'A copy of valid Commercial registration';


$lang['account_not_verified'] = 'Your account is not verified.Otp is sent to your mobile number.';
$lang['account_not_verified1'] = 'Your account is not verified.';
$lang['account_not_approved'] = 'Your account is not approved';

$lang['verify_account'] = 'Verify Account';
$lang['incorrect_code'] = 'Incorrect Code';
$lang['account_verify'] = 'Your account is verified now';
$lang['verification_code'] = 'Verification Code';


$lang['company_name'] = 'Company Name';
$lang['directory_name'] = 'Director Name';
$lang['tel'] = 'Tel';
$lang['fax'] = 'Fax';
$lang['mobile'] = 'Mobile';
$lang['send'] =   'Send';
$lang['pdf_upload_bocome'] = 'In order to complete the request,kindly, upload PDF file contains: CR, dealing request, company profile';

$lang['admin_close_ticket_request'] =   'Admin closed this ticket reopen it ?';


$lang['add_to_cart'] =   'Add to cart';

$lang['login_first'] =   'Please login first';

$lang['add_successfully'] = 'Added Successfully';
$lang['deleted_successfully'] = 'Deleted Successfully';

$lang['updated_successfully'] = 'Updated Successfully';
$lang['something_went_wrong'] = 'Something went wrong';


$lang['po_box'] = 'P.O Box';
$lang['street'] = 'Street';
$lang['block']  = 'Block No';
$lang['city']   = 'City';
$lang['district'] = 'District';
$lang['zip']    = 'Zip Code';
$lang['flat'] = 'Flat No';
$lang['country'] = 'Country';
$lang['welcome'] = 'Welcome';
$lang['add_address'] = 'Add Address';
$lang['submit'] = 'Submit';

$lang['phone'] = 'Phone';
$lang['address'] = 'Addresses';
$lang['use_this_address'] = 'Use This Address';
$lang['set_successfully'] = 'Set default successfully';
$lang['logout'] = 'Logout';
$lang['account'] = 'Account';
$lang['my_account'] = 'My Account';
$lang['proceed_to_checkout'] = 'Proceed to checkout';
$lang['total_cost'] = 'Total Cost';
$lang['delete_message'] = 'Are you sure you want to delete?';
$lang['proceed'] = 'Proceed';
$lang['set_default_address'] = 'Please set default address';
$lang['no_product_in_cart'] = 'There is no product in cart';
$lang['thank_you'] = 'Thanks for placing the order.';
$lang['will_back'] = 'We Will get back to you as possible.';
$lang['track_id'] = 'Order Track ID';
$lang['pin_your_location'] = 'Pin your location';
$lang['orders'] = 'Orders';

$lang['mobile_no_verified'] = 'Your mobile no is verified successfully.';
$lang['mobile_no_failed_to_verified'] = 'Your mobile no verification has failed.';
$lang['appartment_no'] = 'Appartment No';

$lang['package_does_not_exist'] = 'Package does not exist';
$lang['registered_successfully'] = 'Registration is completed.Please wait you are redirecting...';
















<?php

class Base_Model extends CI_Model
{

    public $table;

    public function __construct($table = "")
    {
        parent::__construct();

        if (!empty ($table)) {
            $this->table = $table;

            $fields = $this->db->list_fields($table);

            foreach ($fields as $field) {
                $this->$field = NULL;
            }
        }

    }

    /**
     * Inserts a row into the table and returns the row id
     * @param data Array
     * @return insert_id int
     */
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function insert_batch($data)
	{
		return $this->db->insert_batch($this->table, $data);
	}
    public function update_batch($data,$update_by)
	{
		 return $this->db->update_batch($this->table, $data,$update_by);
		
	}
    /**
     *
     * @param array $data
     * @param array $search
     */
    public function update($data, $search)
    {
        $this->db->update($this->table, $data, $search);
        $this->db->last_query();
    }
    
    
    public function getJoinedData($as_array=false,$join_field,$second_table,$where = false)// please for now not usable function in text model 
	{
		
		$first_field = $this->table.'.'.$join_field;
		$second_field = $second_table.'.'.$join_field;
		$this->db->select($second_table.'.*, ' . $this->table . '.*');
		$this->db->join($second_table,$first_field.' = '.$second_field,'left');
		
		if($where)
		{
			$this->db->where($where);
		}
		
		//$this->db->order_by($this->table.'_cod.Code','asc');
		$result = $this->db->get($this->table);
		//echo $this->db->last_query();exit();
		if($as_array)
		{
			// return $result->result_array();
			return $data =  $result->result_array();
		}
		
		$data = $result->result();

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";*/

		/*foreach ($data as $row) {
			if($as_array) {
				if($row['Code'] == ''){
					$row['Code'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
				}
			} else {
				if($row->Code == ''){
					$row->Code = $this->GetDefaultLangData($join_field , $row->$join_field);
				}
			}
		
		}*/
		
		return $data;
		//return $result->result();
	}

    /**
     * Get an instance of the model and initialize the properties with table row for supplied ID.
     * @param unknown $id
     * @return Base_Model|boolean
     */
    public function get($id, $as_array = false, $field_name = false)
    {
        if ($field_name) {
            $result = $this->db->get_where($this->table, array($field_name => $id));

        } else {
            $result = $this->db->get_where($this->table, array('id' => $id));

        }
        if ($result->num_rows() > 0) {
            $row = $result->row_array();

            if ($as_array) {
                return $row;
            }

            foreach ($row as $col => $val) {
                $this->$col = $val;
            }

            return $this;
        } else {
            return false;
        }
    }

    public function getMultipleRows($fields, $as_array = false, $idOrderBy = 'asc', $limit = false, $start = 0)
    {


        if ($idOrderBy == 'desc')
            $this->db->order_by('id', 'desc');

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        if($this->table == 'users'){
            $this->db->select('users.*,city.eng_name as city');
            $this->db->join('city','city.id = users.city_id','left');
        }

        $result = $this->db->get_where($this->table, $fields);

        //$this->db->last_query(); exit();

        if ($result->num_rows() > 0) {


            if ($as_array) {
                return $result->result_array();
            }

            return $result->result();
        } else {
            return false;
        }
    }

    public function getWithMultipleFields($fields, $as_array = false)
    {


        $result = $this->db->get_where($this->table, $fields);

        if ($result->num_rows() > 0) {
            $row = $result->row_array();

            if ($as_array) {
                return $row;
            }

            foreach ($row as $col => $val) {
                $this->$col = $val;
            }


            return $this;
        } else {
            return false;
        }

    }

    /**
     * Gets all the records from the table.
     */
    public function getAll($as_array = false,$order = 'asc',$order_by = false)
    {
        if($order_by){
            $this->db->order_by($order_by,$order);
        }
        $result = $this->db->get($this->table);

        if ($as_array) {
            return $result->result_array();
        }

        return $result->result();
    }

    public function deleteIn($key, $valuesArr)
    {

        $this->db->where_in($key, $valuesArr);
        $this->db->delete($this->table);

    }

    public function delete($search)
    {
        $this->db->delete($this->table, $search);

    }


    public function getFields()
    {
        $fields = $this->db->list_fields($this->table);
        return $fields;
    }
}
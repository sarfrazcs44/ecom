<?php
class Base_Controller extends CI_Controller
{
	
	
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('lang'))
		{
			
            $this->session->set_userdata('lang','en');
            //$this->session->userdata('lang');
			// $this->language = $this->session->userdata('languageID');
		}
	}
	
	public function changeLanguage($language)
	{
		
		$this->session->set_userdata('lang',$language);
	    redirect($_SERVER['HTTP_REFERER']);
	}
	
	
	
	
	
}
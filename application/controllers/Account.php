<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_user');
        $this->load->model('Model_temp_order');
        $this->load->model('Api_model');


    }

    public function index()
    {
        if ($this->session->userdata('user')) {
            redirect(base_url('orders'));

        }

        redirect(base_url('page/login'));
    }


    public function signup()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->signupValidation();

        $siteKey = $post_data["g-recaptcha-response"];
        unset($post_data["g-recaptcha-response"]);
        // captcha verification function
        $res = captchaVerify($siteKey);
        if ($res->success != true) {
            $errors['error'] = 'Please use captcha';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else{
            unset($post_data['confirm_password']);
            $original_password = $post_data['password'];
            $post_data['password'] = md5($original_password);
            $post_data['verification_code'] = RandomString();
            $post_data['is_verified'] = 0;
            $post_data['phone'] = substr($post_data['phone'], 1);
            $post_data['phone'] = '966' . $post_data['phone'];
            $insertId = $this->Model_user->save($post_data);
            if ($insertId > 0) {
                $this->session->set_userdata('user_id_to_verify', $insertId);
                $sms_data = array();
                $sms_data['to'] = $post_data['phone'];
                $sms_data['sms'] = 'Verification Code ' . $post_data['verification_code'] . '';
                sendSMS($sms_data);
                $this->sendWelcomeEmail($insertId, $original_password);

                //update cart function

                if (get_cookie('temp_order_key')) {
                    $temp_user_id = get_cookie('temp_order_key');
                    $cart_products = $this->Model_temp_order->getJoinedData(false, 'product_id', 'products', 'temp_orders.user_id = ' . $temp_user_id . '');
                    if ($cart_products) {
                        $update = array();
                        $update['user_id'] = $insertId;
                        $update_by['user_id'] = get_cookie('temp_order_key');
                        $this->Model_temp_order->update($update, $update_by);
                    }
                    delete_cookie('temp_order_key');

                }


                // end update cart function

                $errors['error'] = "false";
                $errors['success'] = 'Registered Successfully.Please check your verification code on your mobile';
                $errors['redirect'] = true;
                $errors['url'] = 'page/verify_account';
                echo json_encode($errors);
                exit;
            } else {
                $errors['error'] = "Something went wrong";
                $errors['success'] = "false";
                echo json_encode($errors);
                exit;
            }
        }

    }

    public function checkLogin()
    {
        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUser($post_data);

        if ($checkUser != true) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = 'Email or password incorrect.';
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data['success'] = 'Login Successfully';
            $data['error'] = 'false';
            $data['redirect'] = true;
            if ($this->session->userdata('url')) {
                $data['url'] = $this->session->userdata('url');
            } else {
                $data['url'] = 'support';
            }


            //update cart function

            if (get_cookie('temp_order_key')) {
                $temp_user_id = get_cookie('temp_order_key');
                $cart_products = $this->Model_temp_order->getJoinedData(false, 'product_id', 'products', 'temp_orders.user_id = ' . $temp_user_id . '');
                if ($cart_products) {
                    foreach ($cart_products as $product) {
                        $fetch_by = array();
                        $fetch_by['user_id'] = $this->session->userdata['user']['user_id'];
                        $fetch_by['product_id'] = $product->product_id;

                        $already_exist = $this->Model_temp_order->getWithMultipleFields($fetch_by);
                        if ($already_exist) {
                            $update = array();
                            $update['product_quantity'] = $already_exist->product_quantity + $product->product_quantity;
                            $this->Model_temp_order->update($update, $fetch_by);
                            $deleted_by = array();
                            $deleted_by['temp_order_id'] = $product->temp_order_id;
                            $this->Model_temp_order->delete($deleted_by);
                        } else {
                            $update = array();
                            $update['user_id'] = $this->session->userdata['user']['user_id'];
                            $update_by['temp_order_id'] = $product->temp_order_id;
                            $this->Model_temp_order->update($update, $update_by);

                        }


                    }
                }
                delete_cookie('temp_order_key');

            }


            // end update cart function

            echo json_encode($data);
            exit();
        }
    }

    public function verify_account()
    {
        $data = array();
        $code = $this->input->post('verification_code');
        if ($code == '')
        {
            $data['success'] = 'false';
            $data['error'] = 'Please provide verification code sent in sms to you to verify your provided details.';
            echo json_encode($data);
            exit();
        }else{
            $checkUser = $this->Model_user->get($code, false, 'verification_code');

            if ($checkUser != true) {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = lang('incorrect_code');
                echo json_encode($data);
                exit();
            } else {
                $data = array();
                $update_data = array();
                $update_data['verification_code'] = '';
                $update_data['is_verified'] = 1;
                $update_data_by = array();
                $update_data_by['user_id'] = $checkUser->user_id;

                $this->Model_user->update($update_data, $update_data_by);


                $data['success'] = lang('account_verify');
                $data['error'] = 'false';
                $data['redirect'] = true;
                $data['url'] = 'page/login';
                echo json_encode($data);
                exit();
            }
        }
    }

    private function loginValidation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data)
    {
        $post_data['password'] = md5($post_data['password']);
        $post_data['role_id'] = 3; // role is client
        $user = $this->Model_user->getWithMultipleFields($post_data);

        if (!empty($user)) {
            /*if ($user->role_id == '2' || $user->role_id == '1' || $user->role_id == '4' || $user->role_id == '5') {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'You cannot get login from here!';
                echo json_encode($data);
                exit();

            } else*/
            if ($user->is_verified == 0) {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = lang('account_not_verified1');
                echo json_encode($data);
                exit();
            } elseif ($user->status == 'inactive'){
                $data = array();
                $data['success'] = 'false';
                $data['error'] = 'Your account is suspended. Please contact admin for further details & action.';
                echo json_encode($data);
                exit();
            }
            $user = (array)$user;
            $this->session->set_userdata('user', $user);
            // $this->session->userdata['user']['email']
            return true;
        } else {
            return false;
        }

    }

    public function forgotPassword()
    {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('old_password', 'Old Password', 'required');
        $this->form_validation->set_rules('new_password', 'New Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit();
        } else {
            $posted_data = $this->input->post();
            $posted_old_password = md5($posted_data['old_password']);
            $new_password = $posted_data['new_password'];
            $confirm_password = $posted_data['confirm_password'];
            $userdata = $this->session->userdata('user');
            $db_old_password = $userdata['password'];
            if ($posted_old_password !== $db_old_password) {
                $errors['error'] = "Entered old password is incorrect.";
                $errors['success'] = "false";
                echo json_encode($errors);
                exit();
            } elseif ($new_password != $confirm_password) {
                $errors['error'] = "New password and confirm password didn't match.";
                $errors['success'] = "false";
                echo json_encode($errors);
                exit();
            } else {
                $update['password'] = md5($new_password);
                $update_by['user_id'] = $userdata['user_id'];
                $this->Model_user->update($update, $update_by);
                $errors['error'] = "false";
                $errors['success'] = 'Password changed successfully. Please use new password next time you login.';
                $errors['reset'] = true;
                echo json_encode($errors);
                exit();

            }
        }

    }


    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('user');
        //$arr_update['id'] = $user['id'];
        //$this->Model_user->update($data,$arr_update);
        $this->session->unset_userdata('user');
        /*unset($_SESSION['userdata']);
        unset($_SESSION['loggedin_user_id']);
         unset($_SESSION['user']);
        unset($_SESSION['FBID']);
        unset($_SESSION['FULLNAME']);
        unset($_SESSION['EMAIL']);
        unset($_SESSION['FBRLH_state']);
        session_destroy();*/
        redirect($this->config->item('base_url') . 'page/login');

    }


    private function signupValidation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('full_name', 'Full Name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|is_unique[users.phone]');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    public function checkIfUsernameAvailable()
    {
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $username_count = $this->Model_user->checkIfUsernameAvailable($username);
        $email_count = $this->Model_user->checkIfEmailAvailable($email);
        if ($username_count > 0 && $email_count > 0) {
            $response['status'] = false;
            $response['message'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['status'] = false;
            $response['message'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['status'] = false;
            $response['message'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            $response['status'] = true;
            $response['message'] = '';
            echo json_encode($response);
            exit();
        }
    }

    public function sendWelcomeEmail($user_id, $password)
    {
        $user = $this->Model_user->get($user_id, true,'user_id');
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MSJ Secutiry Systems</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}


#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>



<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi '.$user['full_name'].' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Thank you for registering with MSJ Security Systems. We hope to provide you the best we can.</p>
<p style="font-family:sans-serif;font-size:14px;">Your login details are:</p>
<p style="font-family:sans-serif;font-size:14px;">Email: '.$user['email'].'</p>
<p style="font-family:sans-serif;font-size:14px;">Password: '.$password.'</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="'.base_url().'" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="'.base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">MSJ Security Systems</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="'.base_url().'" style="color:grey;font-size:10px;text-decoration: none;">www.msj.com.sa</a>
</span>
</td>
</tr>
</table>


</td>
</tr>
</table>

</body>
</html>
';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <noreply@msj.com.sa>' . "\r\n";
        mail($user['email'], 'Welcome at MSJ Security Systems', $message, $headers);

    }

    public function sendForgotPasswordEmail()
    {
        $search['email'] = $this->input->post('email');
        $this->form_validation->set_rules('email', 'Email Address', 'required');
        if ($this->form_validation->run() == FALSE) {
            $response['status'] = false;
            $response['message'] = validation_errors();
            echo json_encode($response);
            exit();
        }else{
            $countRow = $this->Api_model->getRowsCount('users', $search);
            if ($countRow == 1) {
                $user = $this->Api_model->getSingleRow('users', $search);
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers .= 'From: <noreply@msj.com.sa>' . "\r\n";

                $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Msj</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}


#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>



<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi '.$user->full_name.' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Forgot Your Password ?</p>
<p style="font-family:sans-serif;font-size:14px;">No worries , lets get you a new one</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
      <a href="'.base_url() . 'forgotpassword/index/' . $user->user_id.'"
style="background-color:#10b26a;border-radius:100px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:30px;text-align:center;text-decoration:none;width:250px;-webkit-text-size-adjust:none;">Set a New Password</a>
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="'.base_url().'" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="'.base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">MSJ Security Systems</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="'.base_url().'" style="color:grey;font-size:10px;text-decoration: none;">www.msj.com.sa</a>
</span>
</td>
</tr>
</table>


</td>
</tr>
</table>

</body>
</html>
';

                mail($user->email, 'Forgot Password', $message, $headers);
                $response['status'] = true;
                $response['message'] = "Email sent to your email account\nتم ارسال الرسالة لبريدك الاكتروني";
            } else {
                $response['status'] = false;
                $response['message'] = "Email doesn't exist\nالبريد الالكتروني خاطئ";
            }
            echo json_encode($response);
            exit;
        }
    }

    public function resendVerificationCode()
    {
        $user_id = $this->input->post('user_id');
        $user = $this->Model_user->get($user_id, false,'user_id');
        if ($user->phone != '' && $user->verification_code != '')
        {
            $sms_data           = array();
            $sms_data['to']     = convertToEnNumbers($user->phone); // converting arabic character numbers to english format
            $sms_data['sms']    = 'Verification Code : '.$user->verification_code;
            sendSMS($sms_data);
            echo 1;exit();
        }else{
            echo 0;exit();
        }

    }


}
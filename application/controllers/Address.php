<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkUserSession();
        $this->load->model('Model_user');
        $this->load->model('Model_address');
        $this->load->model('Model_general');


    }

    public function index()
    {
        $data = array();
        $user = $this->session->userdata('user');
        $data['user'] = $user;
        $fetch_by = array();
        $fetch_by['user_id'] = $user['user_id'];

        $data['address'] = $this->Model_address->getMultipleRows($fetch_by);


        $data['user_id'] = $user['user_id'];

        $data['view'] = 'front/address/index';
        $data['active'] = 'address';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function add()
    {

        $data['view'] = 'front/address/add';
        $data['active'] = 'address';


        $this->load->view('front/layouts/default', $data);
    }


    public function edit($ad_id)
    {

        $fetch_by = array();
        $fetch_by['user_id'] = $this->session->userdata['user']['user_id'];
        $fetch_by['address_id'] = $ad_id;
        $data['result'] = $this->Model_address->getWithMultipleFields($fetch_by);

        if (!$data['result']) {
            redirect(base_url('address'));
        }
        $data['view'] = 'front/address/edit';
        $data['active'] = 'address';


        $data['address_id'] = $ad_id;

        $data['districts']  = $this->Model_general->getMultipleRows('districts', array('city_id' => $data['result']->city_id),false,'asc','eng_name');

        $this->load->view('front/layouts/default', $data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save';
                $this->validate();
                $this->save();
                break;
            case 'update';
                $this->validate();
                $this->update();
                break;
            case 'delete';
                //$this->validate();
                $this->delete();
                break;
        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        //$this->form_validation->set_rules('phone', lang('phone'), 'required');
        $this->form_validation->set_rules('location', lang('pin_your_location'), 'required');
        
        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }

    private function save()
    {
        $post_data = $this->input->post();
       // $exploded_data = explode('|', $post_data['city']);
        //$post_data['city_id'] = $exploded_data[0]; // city id
       // $post_data['city'] = $exploded_data[1]; // city name
        /*if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }*/

        $from_checkout = $post_data['from_checkout'];
        unset($post_data['form_type']);
        unset($post_data['from_checkout']);
        $post_data['user_id'] = $this->session->userdata['user']['user_id'];
        $post_data['created_at'] = date('Y-m-d H:i:s');
        $post_data['updated_at'] = date('Y-m-d H:i:s');


        $insert_id = $this->Model_address->save($post_data);
        if ($insert_id > 0) {
            /*$address_count = $this->Model_address->getRowsCount(array('user_id' => $post_data['user_id']));
            if ($address_count == 1)
            {

            }*/
            $success['error'] = 'false';
            $success['success'] = lang('add_successfully');
            $success['redirect'] = true;

            // here a check
            if ($from_checkout == 1)
            {
                $success['url'] = 'checkout/proceed?checkout';
            }else{
                $success['url'] = 'address';
            }

            echo json_encode($success);
            exit;
        } else {
            $errors['success'] = 'false';
            $errors['error'] = lang('something_went_wrong');
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        $post_data = $this->input->post();
        //$exploded_data = explode('|', $post_data['city']);
       // $post_data['city_id'] = $exploded_data[0]; // city id
       // $post_data['city'] = $exploded_data[1]; // city name


        $post_data['updated_at'] = date('Y-m-d H:i:s');


        $update_by = array();
        $update_by['address_id'] = $post_data['address_id'];
        $from_checkout = $post_data['from_checkout'];
        unset($post_data['form_type']);
        unset($post_data['from_checkout']);
        $this->Model_address->update($post_data, $update_by);

        $success['error'] = 'false';
        $success['success'] = lang('updated_successfully');
        $success['redirect'] = true;

        // here a check
        if ($from_checkout == 1)
        {
            $success['url'] = 'checkout/proceed?checkout';
        }else{
            $success['url'] = 'address';
        }

        echo json_encode($success);
        exit;
    }


    public function set_default()
    {
        $post_data = $this->input->post();
        $update_by = array();
        $update = array();
        $update_by['user_id'] = $this->session->userdata['user']['user_id'];
        $update['is_default'] = 0;
        $this->Model_address->update($update, $update_by);

        $update['updated_at'] = date('Y-m-d H:i:s');


        $update['is_default'] = 1;
        $update_by['address_id'] = $post_data['address_id'];


        $this->Model_address->update($update, $update_by);
        $response['status'] = 'TRUE';
        $response['message'] = lang('set_successfully');
        //$response['reload']  = true;


        echo json_encode($response);
        exit();
    }


    public function getDistricts()
    {
        $fetch_by = array();
        $fetch_by['city_id'] = $this->input->post('city_id');
        $districts = $this->Model_general->getMultipleRows('districts',$fetch_by,false,'asc','eng_name');
        $option = '<option value="">Please select your nearest district... *</option>';
        if($districts){
            foreach($districts as $value){
                $option .= '<option value="'.$value->district_id.'">'.$value->eng_name.'</option>';
            }
        }
        //echo $options;exit;
         $success['html'] = $option;
         echo json_encode($success);
         exit;
    }


    public function delete()
    {


        $deleted_by = array();
        $deleted_by['address_id'] = $this->input->post('id');
        $this->Model_address->delete($deleted_by);
        $success['error'] = 'false';
        $success['reload'] = TRUE;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }


}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Forgotpassword extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_general');
        $this->load->model('Api_model');
        $this->load->model('Model_user');
    }

    public function index()
    {
        $user_id = $this->uri->segment(3);
        if ($user_id != '') {
            $user = $this->Model_general->getSingleRow('users', array('user_id' => $user_id));
            if ($user) {
                $data['user_id'] = $user_id;
                $data['view'] = 'front/pages/forgot_password';
                $data['active'] 	 = 'address';
                $this->load->view('front/layouts/default',$data);
            } else {
                show_404();
            }
        } else {
            show_404();
        }

    }

    public function save()
    {
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['status'] = false;
            $errors['message'] = validation_errors();
            echo json_encode($errors);
            exit();
        } else {
            $posted_data = $this->input->post();
            $password = $posted_data['password'];
            $confirm_password = $posted_data['confirm_password'];
            if ($password != $confirm_password) {
                $errors['status'] = false;
                $errors['message'] = "Password and confirm password didn't match.";
                echo json_encode($errors);
                exit();
            } else {
                $user_id = $this->input->post('user_id');
                $update['password'] = md5($password);
                $update_by['user_id'] = $user_id;
                $this->Model_user->update($update, $update_by);
                $errors['status'] = true;
                $errors['message'] = "Password changed successfully. Please use new password for login.";
                echo json_encode($errors);
                exit();

            }
        }

    }

}


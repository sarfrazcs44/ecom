<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Package_model');
        $this->load->model('Company_model');
        $this->load->model('Model_user');
        $this->load->model('Module_rights_model');
        $this->load->model('Role_model');
        $this->load->model('App_setting_model');
        


    }

    public function package()
    {
        $fetch_by = array();
        $fetch_by['hide'] = 0;
        $fetch_by['is_active'] = 1;
        
        $this->data['packages'] = $this->Package_model->getMultipleRows($fetch_by);
        $this->data['view'] = 'niehez/package';
        $this->load->view('niehez/layouts/main', $this->data);
    }

    public function start($package_id){

        $package_id = decode_url($package_id);

        $package_data = $this->Package_model->get($package_id,false,'package_id');
        if($package_data){

            $this->data['package_id'] = $package_id;
            $this->data['view'] = 'niehez/register';
            $this->load->view('niehez/layouts/main', $this->data);


        }else{
            redirect('niehez/shop/package');
        }

    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'register';
                $this->validate();
                $this->register();
          break; 
                 
        }
    }

    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('title_en', lang('company_name'), 'required|is_unique[companies.title_en]');
        $this->form_validation->set_rules('domain_name', lang('domain_name'), 'required|is_unique[companies.domain_name]');
        $this->form_validation->set_rules('full_name', lang('register_fullname'), 'required');
        $this->form_validation->set_rules('username', lang('register_username'), 'trim|required|is_unique[users.username]');
        $this->form_validation->set_rules('email', lang('register_email'), 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', lang('register_password'), 'required|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', lang('register_confirmpassword'), 'required');
        
        $this->form_validation->set_rules('description_en', lang('register_description'), 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }


    private function register(){

        $post_data = $this->input->post();
        $package_id = decode_url($post_data['package_id']);


        $package_data = $this->Package_model->get($package_id,false,'package_id');
        if(!$package_data){
            $errors['error'] = lang('package_does_not_exist');
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }



        // save company information 

        $company_data = array();
        $comapny_data['title_en'] = $post_data['title_en'];
        $comapny_data['domain_name'] = $post_data['domain_name'];
        $comapny_data['description_en'] = $post_data['description_en'];
        $comapny_data['package_id'] = $package_id;
        $comapny_data['module_ids'] = $package_data->module_id;
        $comapny_data['created_at'] = date('Y-m-d H:i:s');  

        $company_id = $this->Company_model->save($comapny_data);
        if($company_id > 0){




        $company_app_setting_data = array();
        $company_app_setting_data['company_id'] = $company_id;
        $company_app_setting_data['created_at'] = $company_app_setting_data['updated_at'] = date('Y-m-d H:i:s');  
        $this->App_setting_model->save($company_app_setting_data); 
        mkdir("uploads/images/" . $company_id, 0777);


        $modules = explode(',', $package_data->module_id);
        $other_data = array();

        $roles = $this->Role_model->getAll();

        foreach ($modules as $key => $value) {


            foreach ($roles as  $role) {
                if($role->role_id != -1){
                        $other_data[] = [
                        'module_id' => $value,
                        'role_id' => $role->role_id,
                        'company_id' => $company_id,
                        'can_view' => 1,
                        'can_add' => 1,
                        'can_edit' => 1,
                        'can_delete' => 1,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];

                }

                

               
                 
                
            }

                 
        }

        $this->Module_rights_model->insert_batch($other_data); 





            $user_data = array();
            $user_data['full_name'] = $post_data['full_name'];
            $user_data['username'] = $post_data['username'];
            $user_data['email'] = $post_data['email'];
            $user_data['password'] = md5($post_data['password']);
            $user_data['role_id'] = 1;
            $user_data['company_id'] = $company_id;
            $user_data['is_verified'] = 1;
            $user_data['is_approved'] = 1;
            $user_id = $this->Model_user->save($user_data);
            if($user_id > 0){
                $success['error'] = 'false';
                $success['success'] = lang('registered_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/account/login';
                echo json_encode($success);
                exit;


            }else{
                $errors['error'] = lang('something_went_wrong');
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;

            }





        }else{
             $errors['error'] = lang('something_went_wrong');
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;

        }




        //


        
       

    }


}
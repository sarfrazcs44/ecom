<?php


defined('BASEPATH') OR exit('No direct script access allowed');


// This can be removed if you use __autoload() in config.php OR use Modular Extensions

require APPPATH . '/libraries/REST_Controller.php';

date_default_timezone_set('UTC');


/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller
{


    function __construct()

    {

        // Construct the parent class

        parent::__construct();


        // Configure limits on our controller methods

        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php

        $this->methods['user_get']['limit'] = 50000; // 500 requests per hour per user/key

        $this->methods['user_post']['limit'] = 10000; // 100 requests per hour per user/key

        $this->methods['user_delete']['limit'] = 5000; // 50 requests per hour per user/key

        $this->load->model('Model_user');

        $this->load->model('Model_category');
        $this->load->model('Model_brand');
        $this->load->model('Model_product');
        $this->load->model('Model_product_specification');
        $this->load->model('Model_product_image');
        $this->load->model('Model_complaint_type');
        $this->load->model('Model_ad');
        $this->load->model('Model_support_tickets');
        $this->load->model('Model_ticket_comments');
        $this->load->model('Model_chat_request');
        $this->load->model('Model_address');
        $this->load->model('Model_temp_order');
        $this->load->model('Model_order');
        $this->load->model('Model_order_item');
        $this->load->model('Api_model');
        $this->load->model('Model_support_user');
        $this->load->model('Model_general');
        $this->load->model('Model_pos');
        $this->load->model('Site_setting_model');
        $this->load->model('App_setting_model');
    }


    public function userRegistration_post()
    {

        $data = array();

        $data_fetch = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {

            if (isset($post_data['temp_order_key']) && $post_data['temp_order_key'] != '') {
                $temp_order_key = $post_data['temp_order_key'];
                unset($post_data['temp_order_key']);
            }

            $message = '';
            $user = false;
            $check_for_exist = false;
            if (isset($post_data['email'])) {
                $data_fetch['email'] = $post_data['email'];
                $user = $this->Model_user->getMultipleRows($data_fetch, true);
                if ($user) {
                    $check_for_exist = true;
                }
                $message = 'Email already exist';

            }

            if (isset($post_data['username'])) {
                $email_yes = false;
                if ($user) {
                    $email_yes = true;
                }
                $data_fetch_username = array();
                $data_fetch_username['username'] = $post_data['username'];
                $user = $this->Model_user->getMultipleRows($data_fetch_username, true);
                if ($user) {
                    $check_for_exist = true;
                }
                if ($email_yes && $user) {
                    $message = 'Email already exist and Username already exist';
                }
                if (!($email_yes) && $user) {
                    $message = 'Username already exist';
                }


            }


            if ($check_for_exist) {


                $this->response([

                    'status' => FALSE,

                    'message' => $message

                ], REST_Controller::HTTP_OK);


            } else {


                $user_data = array();
                foreach ($post_data as $key => $value) {
                    $user_data[$key] = $value;

                }

                $user_data['created_at'] = date('Y-m-d H:i:s');
                $original_password = $post_data['password'];
                $user_data['password'] = md5($original_password);
                $user_data['chat_request_id'] = '-1';
                $user_data['verification_code'] = RandomString();
                $user_data['is_verified'] = 0;
                 $user_data['is_approved'] = 0;
                $file_name = '';
                if (isset($_FILES['profile_pic']) && $_FILES['profile_pic']['name'] != '') {

                    $user_data['profile_pic'] = $this->uploadImage("profile_pic", "uploads/images/");
                    //$file_name = "uploads/images/" . $file_name;
                }

                if (isset($_FILES['driving_license_img']) && $_FILES['driving_license_img']['name'] != '') {

                    $user_data['driving_license_img'] = $this->uploadImage("driving_license_img", "uploads/images/");
                    //$file_name = "uploads/images/" . $file_name;
                }

                if (isset($_FILES['registration_license_img']) && $_FILES['registration_license_img']['name'] != '') {

                    $user_data['registration_license_img'] = $this->uploadImage("registration_license_img", "uploads/images/");
                    //$file_name = "uploads/images/" . $file_name;
                }
                //$user_data['image'] = $file_name;
                //unset($user_data['districts']);
                $insert_id = $this->Model_user->save($user_data);

                /*if(isset($post_data['districts'])){
                    $districts = explode(',',$post_data['districts']);
                    foreach ($districts as $district) {
                        $city_data['user_id'] = $insert_id;
                        $city_data['district_id'] = $district; // Actually cities label change to districts
                        $city_data['user_type'] = 'delivery';
                        $this->Model_general->save('user_districts', $city_data);
                    }
                }
*/
                // updating temp user id to this user inserted id in temp orders
                if (isset($post_data['temp_order_key']) && $post_data['temp_order_key'] != '') {
                    $temp_orders_for_user = $this->Model_temp_order->getWithMultipleFields(array('user_id' => $temp_order_key));
                    if ($temp_orders_for_user) {
                        $this->Model_temp_order->update(array('user_id' => $insert_id), array('user_id' => $temp_order_key));
                    }
                }

                $this->sendWelcomeEmail($insert_id, $original_password);

                $user = $this->Model_user->get($insert_id, true, 'user_id');
                $city_data = $this->Model_general->getRow($user['city_id'],'city');
                if($city_data){
                    $user['city_title'] = $city_data->eng_name;
                }else{
                    $user['city_title'] = '';
                }



                $sms_data = array();
                $sms_data['to'] = convertToEnNumbers($user_data['phone']); // converting arabic character numbers to english format
                $sms_data['sms'] = 'Verification Code : ' . $user_data['verification_code'] . '';
                sendSMS($sms_data);


                $this->response([

                    'status' => TRUE,

                    'user_info' => $user

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }

    public function getUserData_get()
    {
        $post_data = $this->input->get(); 

        $user = $this->Model_user->get($post_data['user_id'], true, 'user_id');
        $city_data = $this->Model_general->getRow($user['city_id'],'city');
        if($city_data){
            $user['city_title'] = $city_data->eng_name;
        }else{
             $user['city_title'] = '';
        }
        //$user['districts'] = $this->Model_general->getUserDistricts('user_districts.user_id = '.$post_data['user_id'].'');

                $this->response([

                    'status' => TRUE,

                    'user_info' => $user

                ], REST_Controller::HTTP_OK);
    }


     public function verifyOTP_post()
    {
        $post_data = $this->input->post(); // UserID, OTP
        
        if (!empty($post_data)) {
            $user = $this->Model_user->get($post_data['user_id'], false, 'user_id');
            //print_r($user);exit;
            if ($post_data['otp'] == $user->verification_code) {
                $this->Model_user->update(array('is_verified' => 1, 'verification_code' => ''), array('user_id' => $post_data['user_id']));
                $this->response([
                    'status' => TRUE,
                    'message' => lang('mobile_no_verified')
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => lang('mobile_no_failed_to_verified')
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => lang('something_went_wrong')
            ], REST_Controller::HTTP_OK);
        }
    }

    public function addTicket_post()

    {
        $this->checkVersion();
        $data = array();


        $post_data = $this->input->post();


        if (!empty($post_data)) {

            $save_ticket = array();


            $save_ticket['ticket_id'] = time();
            $save_ticket['user_id'] = $post_data['user_id'];
            $save_ticket['status'] = $post_data['status'];
            $save_ticket['ticket_type'] = $post_data['ticket_type']; // first dropdown ID
            $save_ticket['full_name'] = $post_data['full_name'];
            $save_ticket['email'] = $post_data['email'];
            $save_ticket['city'] = $post_data['city'];
            $save_ticket['phone'] = $post_data['phone'];
            $save_ticket['type_id'] = $post_data['type_id']; // second dropdown ID
            $save_ticket['product'] = $post_data['product'];
            $save_ticket['apartment_no'] = $post_data['apartment_no'];
            $save_ticket['visit_time'] = $post_data['visit_time'];
            $save_ticket['location'] = $post_data['location'];
            $save_ticket['message'] = $post_data['message'];
            $save_ticket['lat'] = $post_data['lat'];
            $save_ticket['lng'] = $post_data['lng'];
            $save_ticket['timestamp'] = $post_data['timestamp'];
            $save_ticket['created_at'] = date('Y-m-d H:i:s');

            $insert_id = $this->Model_support_tickets->save($save_ticket);

            if ($insert_id > 0) {

                $get_ticket = $this->Model_support_tickets->get($insert_id, true);
                $ticket_type_parent = ticket_type_detail($get_ticket['ticket_type']);
                $ticket_type_child = ticket_type_detail($get_ticket['type_id']);

                $get_ticket['ticket_type_parent_en'] = $ticket_type_parent->complaint_title_en;
                $get_ticket['ticket_type_parent_ar'] = $ticket_type_parent->complaint_title_ar;
                $get_ticket['ticket_type_child_en'] = $ticket_type_child->complaint_title_en;
                $get_ticket['ticket_type_child_ar'] = $ticket_type_child->complaint_title_ar;

                $save_ticket_comment = array();
                $save_ticket_comment['ticket_id'] = $save_ticket['ticket_id'];
                $save_ticket_comment['user_id'] = $post_data['user_id'];
                $save_ticket_comment['message'] = $post_data['message'];
                $save_ticket_comment['created_at'] = date('Y-m-d H:i:s');
                $save_ticket_comment['timestamp'] = $post_data['timestamp'];
                $this->Model_ticket_comments->save($save_ticket_comment);

                // check complaint type by ticket type and send email to all support users against this ticket type in support_users table.
                $fetch_by['complaint_type_id'] = $get_ticket['type_id']; // complaint child id

                // fetching all support users against this ticket type
                $support_users = $this->Model_support_user->getMultipleRows($fetch_by);
                if ($support_users) { // if any support users found for this type of ticket
                    // sending email to all support users against this ticket
                    $this->sendEmailToSupportUsers($support_users, $get_ticket['ticket_id'], $get_ticket['user_id']);
                } else {
                    // sending email to admins against this ticket.
                    $this->sendEmailToAdmins($get_ticket['ticket_id'], $get_ticket['user_id']);
                }
                $this->sendPushNotificationToAdminsAndSupportUsers($insert_id);


                $this->response([

                    'status' => True,

                    'ticket_data' => $get_ticket

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function addTicketComment_post()

    {
        $this->checkVersion();
        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {

            $save_ticket_comment = array();
            $save_ticket_comment['ticket_id'] = $post_data['ticket_id'];
            $save_ticket_comment['user_id'] = $post_data['user_id'];
            $save_ticket_comment['message'] = $post_data['message'];
            $save_ticket_comment['timestamp'] = $post_data['timestamp'];
            $save_ticket_comment['created_at'] = date('Y-m-d H:i:s');
            $insert_id = $this->Model_ticket_comments->save($save_ticket_comment);
            if ($insert_id > 0) {
                $getUserData = $this->Model_support_tickets->getUserData($post_data['ticket_id']);
                $get_ticket = $this->Model_ticket_comments->get($insert_id, true, 'comment_id');
                $get_ticket_user = $this->Model_user->get($post_data['user_id'], true, 'user_id');

                // if commnet is done by support type user and ticket is not already taken
                if ($get_ticket_user['role_id'] == 2 && $getUserData['ticket_taken_by'] == 0) {
                    $this->Model_support_tickets->update(array('ticket_taken_by' => $post_data['user_id']), array('ticket_id' => $post_data['ticket_id']));
                }

                // pusher call here
                pusher($get_ticket, 'ticket_' . $post_data['ticket_id'], 'ticket_' . $post_data['ticket_id'], false);
                $this->sendTicketCommentReplyNotificationToAdminUsers($insert_id);

                $this->response([

                    'status' => True,

                    'ticket_comment_data' => $get_ticket

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function addAddress_post()

    {

        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {


            $insert_id = $this->Model_address->save($post_data);


            $post_data['created_at'] = $post_data['updated_at'] = date('Y-m-d H:i:s');
            $address = $this->Model_address->get($insert_id, true, 'address_id');


            if ($insert_id > 0) {


                $this->response([

                    'status' => True,

                    'address_info' => $address

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function updateAddress_post()

    {

        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {

            $update_by = array();
            $update_by['address_id'] = $post_data['address_id'];
            $post_data['updated_at'] = date('Y-m-d H:i:s');
            $this->Model_address->update($post_data, $update_by);


            $address = $this->Model_address->getAllAddress('address.address_id = '.$post_data['address_id']);


            $this->response([

                'status' => True,

                'address_info' => $address[0]

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function getAddress_get()

    {

        $data = array();

        $post_data = $this->input->get();


        if (!empty($post_data)) {


            $address = $this->Model_address->getAllAddress('address.address_id = '.$post_data['address_id']);


            if ($address) {


                $this->response([

                    'status' => True,

                    'address_info' => $address[0]

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function getSettings_get()

    {

        $result = $this->Site_setting_model->get(1,true);
         $this->response([

                'status' => True,

                'setting_detail' => $result

            ], REST_Controller::HTTP_OK);


    }


    public function getAppSettings_get()
    {

        $post_data = $this->input->get();
        $fetch_by = array();

        if(isset($post_data['company_id'])){
            $fetch_by['company_id']  = $post_data['company_id'];

        }else{
            $fetch_by['app_setting_id'] = 1;
        }

        $result = $this->App_setting_model->getWithMultipleFields($fetch_by);
         $this->response([

                'status' => True,

                'setting_detail' => $result

            ], REST_Controller::HTTP_OK);


    }


    public function getAllAddress_get()

    {

        $data = array();

        $post_data = $this->input->get();


        if (!empty($post_data)) {

            $return_array = array();

            

            $address = $this->Model_address->getAllAddress('address.user_id = '.$post_data['user_id']);

            if (!empty($address)) {
                $return_array = $address;
            }


            $this->response([

                'status' => True,

                'address_info' => $return_array

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function deleteAddress_post()

    {

        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {

            $return_array = array();

            $deleted_by = array();
            $deleted_by['address_id'] = $post_data['address_id'];

            $this->Model_address->delete($deleted_by);


            $this->response([

                'status' => True,

                'message' => 'Deleted Successfully'

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function addToCart_post()
    {
        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {


            $fetch_by = array();
            $fetch_by['user_id'] = $post_data['user_id'];
            $fetch_by['product_id'] = $this->input->post('product_id');
            if(isset($post_data['company_id'])){
                $fetch_by['company_id'] = $post_data['company_id'];

            }

            $already_added = $this->Model_temp_order->getWithMultipleFields($fetch_by);
            if ($already_added) {
                $update = array();
                $update_by = array();

                $update['product_quantity'] = $already_added->product_quantity + $post_data['product_quantity'];
                $update_by['temp_order_id'] = $already_added->temp_order_id;

                $this->Model_temp_order->update($update, $update_by);


                $insert_id = $already_added->temp_order_id;

            } else {
                $insert_id = $this->Model_temp_order->save($post_data);
            }


            $order_data = $this->Model_temp_order->getJoinedData(true, 'product_id', 'products', 'temp_orders.temp_order_id = ' . $insert_id . '');


            if ($insert_id > 0) {


                $this->response([

                    'status' => True,

                    'order_item' => $order_data

                ], REST_Controller::HTTP_OK);


            } else {


                $this->response([

                    'status' => false,

                    'message' => 'There is something went worng'

                ], REST_Controller::HTTP_OK);


            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }


    public function updateCartItem_post() // it adds newly sent quanitty to already existing one
    {
        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {

            $update = array();
            $update_by = array();
            $fetch_by = array();

            //$update['product_quantity'] = $post_data['product_quantity'];
            $update_by['temp_order_id'] = $post_data['temp_order_id'];
            $fetch_by['temp_order_id'] = $post_data['temp_order_id'];

            $already_added = $this->Model_temp_order->getWithMultipleFields($fetch_by);
            $update['product_quantity'] = $already_added->product_quantity + $post_data['product_quantity'];

            $this->Model_temp_order->update($update, $update_by);

            $order_data = $this->Model_temp_order->getJoinedData(true, 'product_id', 'products', 'temp_orders.temp_order_id = ' . $post_data['temp_order_id'] . '');

            $this->response([

                'status' => True,

                'cart_item' => $order_data

            ], REST_Controller::HTTP_OK);

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }

    public function updateCart_post()
    {
        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {


            $update = array();
            $update_by = array();

            $update['product_quantity'] = $post_data['product_quantity'];
            $update_by['temp_order_id'] = $post_data['temp_order_id'];

            $this->Model_temp_order->update($update, $update_by);


            $order_data = $this->Model_temp_order->getJoinedData(true, 'product_id', 'products', 'temp_orders.temp_order_id = ' . $post_data['temp_order_id'] . '');

            $this->response([

                'status' => True,

                'cart_item' => $order_data

            ], REST_Controller::HTTP_OK);

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }


    public function deleteOrderItem_post()

    {

        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {

            $return_array = array();

            $deleted_by = array();
            $deleted_by['temp_order_id'] = $post_data['temp_order_id'];

            $this->Model_temp_order->delete($deleted_by);


            $this->response([

                'status' => True,

                'message' => 'Deleted Successfully'

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function assignOrder_post()
    {
        $post_data = $this->input->post();
        $post_data['assigned_at'] = date('Y-m-d H:i:s');
        // save this data in assigned orders table
        $assgined = $this->Model_general->getSingleRow('assigned_orders_for_delivery', array('order_id' => $post_data['order_id'])); // check if already assigned

        if (!$assgined) {

            $inserted = $this->Model_general->save('assigned_orders_for_delivery', $post_data);
            if ($inserted > 0) {
                // if assigned, send sms and email to deliver man order assigned to.
                $order_details = $this->Model_general->getSingleRow('orders', array('order_id' => $post_data['order_id']));
                $user_details = $this->Model_general->getSingleRow('users', array('user_id' => $post_data['user_id']));
                if($user_details->phone != ''){

                    $sms_data = array();
                    $sms_data['to'] = $user_details->phone;
                    $sms_data['sms'] = 'An order is assigned to you with order no. ' . $order_details->order_track_id;
                    sendSMS($sms_data);
                    
                }
                if($user_details->email != '') {
                    $email['body'] = 'An order is assigned to you with order no. ' . $order_details->order_track_id;
                    $email['to'] = $user_details->email;
                    $email['from'] = 'noreply@Niehez.com.sa';
                    $email['subject'] = 'Order Assigned At Niehez';
                    sendEmail($email);
                   
                }


                $customer_details = $this->Model_user->get($order_details->user_id,false,'user_id');
                

                if ($customer_details->phone != ''){

                        $otp = RandomString();
            
                        $this->Model_general->updateRow('orders',array('otp' => $otp),$post_data['order_id'],'order_id');

                        $sms_data = array();
                        $sms_data['to'] = $customer_details->phone;
                        $sms_data['sms'] = 'Your order is assign to driver with delivery OTP :  ' . $otp;
                        sendSMS($sms_data);
                        

                       
                }



            }
        }

        // update status in orders table to Dispatched
       // $this->Model_order->update(array('status' => 'Dispatched'), array('order_id' => $post_data['order_id']));

        $this->response([

                'status' => True,

                'message' => 'Assigned Successfully'

            ], REST_Controller::HTTP_OK);
    }



    public function getBranches_get()
    {// actually ware house users

        $data = array();

        $return_array = array();

        $users = $this->Model_general->getAllWarehouseUsers(true);

        if($users){
            foreach ($users as $key => $value) {

                $value['branch_districts'] = $this->Model_general->getUserDistricts('user_districts.user_id = '.$value['user_id'].' AND user_districts.user_type = "warehouse"');

                $return_array[] = $value;
                
            }
        }



            $this->response([

                'status' => True,

                'branches' => $return_array

            ], REST_Controller::HTTP_OK);


        


    }


    public function getAllCartItem_get()

    {

        $data = array();

        $post_data = $this->input->get();


        if (!empty($post_data)) {

            $return_array = array();


            $products = $this->Model_temp_order->getCartData($post_data['user_id']);

            if ($products) {
                foreach ($products as $product) {
                    $images = array();
                    $res = getProductImages($product['product_id']);
                    if ($res) {
                        $images = $res;
                    }
                    $product['images'] = $images;
                    $return_array[] = $product;
                }


            }


            $this->response([

                'status' => True,

                'cart_item' => $return_array

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function placeOrder_post()
    {
        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {


            $data = array();


            $fetch_by = array();
            $fetch_by['user_id'] = $post_data['user_id'];

            $cart_products = $this->Model_temp_order->getJoinedData(false, 'product_id', 'products', 'temp_orders.user_id = ' . $post_data['user_id'] . '');

            if ($cart_products) {

                $address_id = $this->input->post('address_id');
                $order_data = array();

                $order_data['user_id'] = $post_data['user_id'];
                $order_data['address_id'] = $address_id;
                $order_data['created_at'] = $order_data['updated_at'] = date('Y-m-d H:i:s');
                $order_data['order_track_id'] = RandomString();

                $order_data['vat_total'] = $post_data['vat_total'];
                $order_data['delivery_charges'] = $post_data['delivery_charges'];
                $order_data['total_items'] = $post_data['total_items'];
                $order_data['total_amount'] = $post_data['total_amount'];
                $order_data['transaction_id'] = $post_data['transaction_id'];
                
                $insert_id = $this->Model_order->save($order_data);
                if ($insert_id > 0) {
                    $update_order = array();
                    $update_order_by = array();
                    $update_order_by['order_id'] = $insert_id;
                    $update_order['order_track_id'] = $order_data['order_track_id'] . $insert_id;
                    $this->Model_order->update($update_order, $update_order_by);
                    $this->sendThankyouEmailToCustomer($insert_id);
                    $return_array = array();
                    $return_array = $this->Model_order->get($insert_id, true, 'order_id');
                    $order_item_data = array();
                    foreach ($cart_products as $product) {

                        $order_item_data[] = ['order_id' => $insert_id,
                            'product_id' => $product->product_id,
                            'quantity' => $product->product_quantity,
                            'price' => $product->price
                        ];

                    }

                    $this->Model_order_item->insert_batch($order_item_data);
                    $deleted_by = array();
                    $deleted_by['user_id'] = $post_data['user_id'];
                    $this->Model_temp_order->delete($deleted_by);

                    $order_items = array();

                    $items = $this->Model_order->getOrderItems($insert_id);

                    // pos entry
                    $user_data = $this->Model_user->get($post_data['user_id'],false,'user_id');
                    $pos_data = array();
                    $pos_data['transaction_user'] = $user_data->full_name;
                    $pos_data['type'] = 'Credit';
                    $pos_data['description'] = 'Online Purchase';
                    $pos_data['total_amount'] = $post_data['total_amount'];
                    $pos_data['created_at'] = date('Y-m-d H:i');
                    $pos_data['order_id'] = $insert_id;
                    $this->Model_pos->save($pos_data);

                    if (!empty($items)) {
                        $order_items = $items;
                    }
                    $return_array['order_items'] = $order_items;
                    $this->response([

                        'status' => True,

                        'order_data' => $return_array

                    ], REST_Controller::HTTP_OK);

                } else {

                    $this->response([

                        'status' => false,

                        'message' => 'There is something went worng'

                    ], REST_Controller::HTTP_OK);
                }


            } else {
                $success['status'] = false;
                $success['message'] = lang('something_went_wrong');

                echo json_encode($success);
                exit;
            }

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }


    public function changeStatus_post()
    {
        $post_data = $this->input->post();


        

         if (!empty($post_data)) {

            $post_data['updated_at'] = date('Y-m-d H:i:s');


            $update_by = array();
            $update_by['order_id'] = $post_data['order_id'];


            if($post_data['status'] == 'Delivered'){

                 $order_details = $this->Model_general->getSingleRow('orders', array('order_id' => $post_data['order_id']));

                if($order_details->otp != $post_data['otp']){
                    $this->response([

                        'status' => FALSE,

                        'message' => "OTP does not match. Order status will not change."

                    ], REST_Controller::HTTP_OK);

                }

                $post_data['otp'] = '';


            }

            $this->Model_order->update($post_data, $update_by);
            $this->response([

                        'status' => True,

                        'message' => "Updated Successfully"

                    ], REST_Controller::HTTP_OK);


            
        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }



    public function orderDetail_get()
    {

        $post_data = $this->input->get();
         $return_array = array();


        

         if (!empty($post_data)) {

            
            $orders = $this->Model_order->getDetail($post_data['order_id']);
            if ($orders) {
               $return_array = $orders[0];
            }else{

                $this->response([

                        'status' => False,

                        'message' => 'Order not found'

                    ], REST_Controller::HTTP_OK);

            }

           $order_items = array();

            $items = $this->Model_order->getOrderItems($post_data['order_id']);

            if (!empty($items)) {
                $order_items = $items;
            }
            $return_array['order_items'] = $order_items;
            $this->response([

                        'status' => True,

                        'order_data' => $return_array

                    ], REST_Controller::HTTP_OK);


            
        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }

        

    }


    public function getAllUserOrders_get()

    {

        $data = array();

        $post_data = $this->input->get();


        if (!empty($post_data)) {

            $return_array = array();
            $return_array2 = array();
            $where = '1 = 1';

            if(isset($post_data['status'])){

                if($post_data['status'] == 'Cancelled'){
                     $where .= ' AND (orders.status = "Cancelled By Customer" OR orders.status = "Cancelled By Admin")';
                }else{
                    $where .= ' AND orders.status = "'.$post_data['status'].'"';
                    $order_status = $post_data['status'];
                }
                
                
            }

            


            if($post_data['user_type'] == 'customer'){

                $orders = $this->Model_order->getAllOrdersForUser($post_data['user_id'], $where);


            }else if($post_data['user_type'] == 'driver'){
                $user_data = $this->Model_user->get($post_data['user_id'],true,'user_id');

            
                $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_data['branch_id']));
                foreach ($user_districts as $district) {
                    $districts[] = $district->district_id;
                }

                
                //for now no need of status
                if ($post_data['type'] == 'un_assigned') {
                     $where .= ' AND assigned_orders_for_delivery.user_id is NULL';
                    $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUser($order_status, $districts,'',true,$where);
                    
                } else if($post_data['type'] == 'assigned') {
                    // Show all orders assigned to him.
                    $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUser($order_status, $districts, $post_data['user_id'],true,$where); // 3rd parameter to get orders assigned to this delivery user only
                }
            }

            

            if ($orders) {

                //$return_array = $products;
                foreach ($orders as $key => $order) {
                    $order['order_date'] = strtotime($order['order_date']);
                    $return_array2[$key] = $order;
                    $order_items = array();

                    $items = $this->Model_order->getOrderItems($order['order_id']);

                    if (!empty($items)) {
                        $order_items = $items;
                    }
                    $return_array2[$key]['order_items'] = $order_items;
                }

                $return_array = $return_array2;


                $this->response([

                    'status' => True,

                    'order_data' => $return_array

                ], REST_Controller::HTTP_OK);

            } else {
                $this->response([

                    'status' => False,

                    'message' => 'There is no order for this user'

                ], REST_Controller::HTTP_OK);
            }


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }





    public function getTicketInfo_get()
    {
        $this->checkVersion();
        $data = array();

        $post_data = $this->input->get();


        if (!empty($post_data)) {

            $ticket_info = $this->Model_support_tickets->getUserData($post_data['ticket_id']);

            $ticket_type_parent = ticket_type_detail($ticket_info['ticket_type']);
            $ticket_type_child = ticket_type_detail($ticket_info['type_id']);

            $ticket_info['ticket_type_parent_en'] = $ticket_type_parent->complaint_title_en;
            $ticket_info['ticket_type_parent_ar'] = $ticket_type_parent->complaint_title_ar;
            $ticket_info['ticket_type_child_en'] = $ticket_type_child->complaint_title_en;
            $ticket_info['ticket_type_child_ar'] = $ticket_type_child->complaint_title_ar;

            $this->response([
                'status' => True,
                'ticket_info' => $ticket_info
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }

    public function updateComment_post()

    {

        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {

            $save_ticket_comment = array();
            $save_ticket_comment['ticket_id'] = $post_data['ticket_id'];
            $save_ticket_comment['user_id'] = $post_data['user_id'];

            $comments = $this->Model_ticket_comments->getMultipleRows(['ticket_id' => $post_data['ticket_id']]);
            // update is_read status if i am not the user who posted the comment
            foreach ($comments as $item) {
                // update is_read status if i am not the user who posted the comment
                if ($item->user_id != $post_data['user_id']) {
                    $update_data['is_read'] = 1;
                    $update_by['comment_id'] = $item->comment_id;
                    $this->Model_ticket_comments->update($update_data, $update_by);
                }
            }
            $this->response([
                'status' => TRUE,
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function updateTicket_post()

    {

        $data = array();

        $post_data = $this->input->post();


        if (!empty($post_data)) {


            $update_data_by = array();
            $update_data_by['ticket_id'] = $post_data['ticket_id'];
            $this->Model_support_tickets->update($post_data, $update_data_by);

            if ($post_data['closed_request'] == 3) {
                $where = 'support_tickets.ticket_id = ' . $post_data['ticket_id'] . ' ';
                $getUserData = $this->Model_support_tickets->getJoinedData(true, 'user_id', 'users', $where);
                $getUserData = $getUserData[0];

                // sending email to all admins that a ticket is reopened
                $this->sendEmailToAdminsForReopenedTicket($getUserData, $post_data['ticket_id']);
                $this->sendReopenedTicketNotificationToAdminUsers($getUserData['id']);
            }


            $this->response([
                'status' => True,
                'message' => 'Updated Successfully'
            ], REST_Controller::HTTP_OK);

        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function createChatRequest_get()
    {
        $this->checkVersion();
        //$this->sendEmail('createChatRequest called');
        $post_data = $this->input->get();
        $field['user_id'] = $post_data['user_id'];

        $user = $this->Model_user->getWithMultipleFields($field, true);
        if ($user) {
            $save_array = array();
            $save_array['username'] = ($user['full_name'] != '' ? $user['full_name'] : 'No Name');
            $save_array['email'] = $user['email'];

            $insert_id = $this->Model_chat_request->save($save_array);

            $total_count = $this->Model_chat_request->getCount($insert_id);

            pusher(array('refresh' => 1), 'admin_chat', 'admin_chat', false);

            sendChatRequestNotificationToAdminSupportUsers($insert_id);


            $this->response([
                'status' => TRUE,
                'chat_id' => $insert_id,
                'queued' => $total_count
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'User does not exist.'
            ], REST_Controller::HTTP_OK);
        }


    }

    public function getQueuedChats_get()
    {
        // $this->sendEmail('getQueuedChats called');
        $post_data = $this->input->get();
        $chat_id = $post_data['chat_id'];

        $total_count = $this->Model_chat_request->getCount($chat_id);


        $this->response([
            'status' => TRUE,
            'queued' => $total_count
        ], REST_Controller::HTTP_OK);

    }


    public function getUserInfo_get()
    {
        $post_data = $this->input->get();
        $field['user_id'] = $post_data['user_id'];

        $user = $this->Model_user->getWithMultipleFields($field, true);
        if (!$user)
        {
            $this->response([
                'status' => FALSE,
                'message' => "User not found with this ID",
                'user_info' => FALSE
            ], REST_Controller::HTTP_OK);
        }
        if ($user['role_id'] == 1) // admin
        {
            $user['view_chat'] = 1;
            $user['view_tickets'] = 1;
            $user['view_reopened_tickets'] = 1;
        } elseif ($user['role_id'] == 2) // support user
        {
            $assigned_complaint_types = assigned_complaint_types($user['user_id']);
            if ($assigned_complaint_types == 'None') { // if no ticket types assigned to support user
                $user['view_chat'] = 1;
                $user['view_tickets'] = 0;
            } else {
                $user['view_chat'] = 0;
                $user['view_tickets'] = 1;
            }
            $user['view_reopened_tickets'] = 0;
        } else {
            $user['view_chat'] = 0;
            $user['view_tickets'] = 0;
            $user['view_reopened_tickets'] = 0;
        }


        $this->response([
            'status' => TRUE,
            'user_info' => $user
        ], REST_Controller::HTTP_OK);

    }


    public function closeUserChat_get()
    {
        // $this->sendEmail('closeUserChat called');
        $post_data = $this->input->get();
        $field['id'] = $post_data['chat_id'];

        //$user = $this->Model_chat_request->delete($field);
        $update = $this->Model_chat_request->update(array("is_closed" => 1), $field);

        //echo '<script src="'.base_url().'assets/js/firebase_custom.js"></script><script>closeChatFromApi('.$field['id'].')</script>';


        $this->response([
            'status' => TRUE
        ], REST_Controller::HTTP_OK);

    }


    public function getUserTickets_get()
    {
        // $this->checkVersion();
        $post_data = $this->input->get();
        $tickets = array();
        // $fetch_by = array();
        // $fetch_by['user_id'] = $post_data['user_id'];

        $tickets = $this->Model_support_tickets->getTickets($post_data['user_id']);
        $return_array = array();
        if ($tickets && count($tickets) > 0) {
            $return_array = $tickets;
        }

        if (count($return_array) > 0) {
            foreach ($return_array as $key => $ticket) {
                $ticket_type_parent = ticket_type_detail($ticket['ticket_type']);
                $ticket_type_child = ticket_type_detail($ticket['type_id']);

                $return_array[$key]['ticket_type_parent_en'] = $ticket_type_parent->complaint_title_en;
                $return_array[$key]['ticket_type_parent_ar'] = $ticket_type_parent->complaint_title_ar;
                $return_array[$key]['ticket_type_child_en'] = $ticket_type_child->complaint_title_en;
                $return_array[$key]['ticket_type_child_ar'] = $ticket_type_child->complaint_title_ar;

                $return_array[$key]['unread'] = $this->check_unread_comments($ticket['ticket_id'], $post_data['user_id']);
            }
        }


        $this->response([
            'status' => TRUE,
            'tickets' => $return_array
        ], REST_Controller::HTTP_OK);

    }


    public function getTicketComments_get()
    {
        $post_data = $this->input->get();
        $tickets = array();
        $fetch_by = array();
        $fetch_by['ticket_id'] = $post_data['ticket_id'];

        $tickets = $this->Model_ticket_comments->getMultipleRows($fetch_by, true);
        $return_array = array();
        if ($tickets) {
            $return_array = $tickets;
        }


        $this->response([
            'status' => TRUE,
            'ticket_comments' => $return_array
        ], REST_Controller::HTTP_OK);

    }

    public function categories_get()
    {
        $post_data = $this->input->get();
        $categories = array();
        $fetch_by = array();
        $fetch_by['parent_id'] = 0;
        $fetch_by['is_active'] = 1;
        if (isset($post_data['parent_id'])) {
            $fetch_by['parent_id'] = $post_data['parent_id'];
        }

        if (isset($post_data['company_id']) && $post_data['company_id'] > 0) {
            $fetch_by['company_id'] = $post_data['company_id'];
        }
        $categories = $this->Model_category->getMultipleRows($fetch_by, true);
        $return_array = array();
        if ($categories) {
            $return_array = $categories;
        }


        $this->response([
            'status' => TRUE,
            'categories' => $return_array
        ], REST_Controller::HTTP_OK);

    }

    public function brands_get()
    {

        $fetch_by = array();

        $fetch_by['is_active'] = 1;
        $return_array = array();
        if (isset($post_data['company_id']) && $post_data['company_id'] > 0) {
            $fetch_by['company_id'] = $post_data['company_id'];
        }
        $brands = $this->Model_brand->getMultipleRows($fetch_by, true);
        if ($brands) {
            $return_array = $brands;
        }

        $this->response([
            'status' => TRUE,
            'brands' => $return_array
        ], REST_Controller::HTTP_OK);

    }


    public function complaint_type_get()
    {
        $fetch_by = array();
        $return_array = array();
        $fetch_by['is_active'] = 1;
        $types = $this->Model_complaint_type->getMultipleRows($fetch_by, true);
        if ($types) {
            $return_array = $types;
        }
        $this->response([
            'status' => TRUE,
            'compaint_types' => $return_array
        ], REST_Controller::HTTP_OK);

    }

    public function ads_get()
    {
        $fetch_by = array();
        $return_array = array();
        $fetch_by['is_active'] = 1;
        $ads = $this->Model_ad->getMultipleRows($fetch_by, true);
        if ($ads) {
            $return_array = $ads;
        }
        $this->response([
            'status' => TRUE,
            'ads' => $return_array
        ], REST_Controller::HTTP_OK);

    }

    public function products_get()
    {

        $post_data = $this->input->get();
        $results = $this->Model_product->getAllProducts($post_data);
        $products = array();
        if (!empty($results)) {
            foreach ($results as $result) {
                $fetch_by = array();
                $fetch_by['product_id'] = $result['product_id'];
                $images = $this->Model_product_image->getMultipleRows($fetch_by, true);
                if ($images) {
                    $result['product_images'] = $images;
                } else {
                    $result['product_images'] = array();
                }

                $specifications = $this->Model_product_specification->getMultipleRows($fetch_by, true);

                if ($specifications) {
                    $result['product_specifications'] = $specifications;
                } else {
                    $result['product_specifications'] = array();
                }


                $products[] = $result;
            }
        }

        $this->response([
            'status' => TRUE,
            'products' => $products
        ], REST_Controller::HTTP_OK);

    }

    public function searchUsers_post()

    {

        $data = array();
        $post_data = $this->input->post();


        if (!empty($post_data)) {


            $users = $this->Model_user->searchUsers($post_data);

            $this->response([

                'status' => TRUE,

                'user_info' => $users

            ], REST_Controller::HTTP_OK);


        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }


    public function login_post()

    {

        $data = array();

        $data_fetch = array();

        $data = $this->input->post(); // email, password, temp_order_key

        if (!empty($data)) {

            if (isset($data['temp_order_key']) && $data['temp_order_key'] != '') {
                $temp_order_key = $data['temp_order_key'];
                unset($data['temp_order_key']);
            }

            if (isset($data['facebook_id'])) {
                $data_fetch_by_fb_id['facebook_id'] = $data['facebook_id'];
                $arr = $this->Model_user->getMultipleRows($data_fetch_by_fb_id, true);
                $user = $arr[0];
                $city_data = $this->Model_general->getRow($user['city_id'],'city');
                

                if ($user) {

                    if($city_data){
                    $user['city_title'] = $city_data->eng_name;
                }else{
                    $user['city_title'] = '';
                }
                    //$this->updateUserLoginStatus($user['id']);


                    if ($user['role_id'] != 3 && $user['role_id'] != 5) {
                        $this->response([

                            'status' => FALSE,

                            'message' => 'You cannot get login from here!'

                        ], REST_Controller::HTTP_OK);
                    } elseif ($user['is_verified'] == 0) {

                        $sms_data = array();
                        $sms_data['to'] = convertToEnNumbers($user['phone']); // converting arabic character numbers to english format
                        $sms_data['sms'] = 'Verification Code : ' . $user['verification_code'] . '';
                        sendSMS($sms_data);
                        $this->response([

                            'status' => TRUE,

                            'message' => lang('account_not_verified'),
                            'user_info' => $user

                        ], REST_Controller::HTTP_OK);
                    } elseif ($user['status'] == 'inactive') {
                        $this->response([

                            'status' => FALSE,

                            'message' => 'Your account is suspended. Please contact admin for further details & action'

                        ], REST_Controller::HTTP_OK);
                    } else {

                        // updating temp user id to this user inserted id in temp orders
                        if (isset($data['temp_order_key']) && $data['temp_order_key'] != '') {
                            $temp_orders_for_user = $this->Temp_order_model->getWithMultipleFields(array('user_id' => $temp_order_key));
                            if ($temp_orders_for_user) {
                                $this->Temp_order_model->update(array('user_id' => $user['user_id']), array('user_id' => $temp_order_key));
                            }
                        }

                        $this->response([

                            'status' => TRUE,

                            'user_info' => $user

                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([

                        'status' => FALSE,

                        'message' => 'User does not exist with this facebook id.please sign up this user first.'

                    ], REST_Controller::HTTP_OK);
                }
            } elseif (isset($data['google_id'])) {
                $data_fetch_by_google_id['google_id'] = $data['google_id'];
                $arr = $this->Model_user->getMultipleRows($data_fetch_by_google_id, true);
                $user = $arr[0];
                $city_data = $this->Model_general->getRow($user['city_id'],'city');
                

                if ($user) {
                    if($city_data){
                        $user['city_title'] = $city_data->eng_name;
                    }else{
                        $user['city_title'] = '';
                    }

                    if (isset($data['device_type']) || isset($data['device_token'])) {
                        $update_user = array();
                        $update_user_by = array();
                        $update_user_by['user_id'] = $user['user_id'];

                        if (isset($data['device_type'])) {
                            $update_user['device_type'] = $data['device_type'];
                        }

                        if (isset($data['device_token'])) {
                            $update_user['device_token'] = $data['device_token'];
                        }


                        $this->Model_user->update($update_user, $update_user_by);
                    }
                    // $this->updateUserLoginStatus($user['id']);

                    if ($user['role_id'] != 3 && $user['role_id'] != 5) {
                        $this->response([

                            'status' => FALSE,

                            'message' => 'You cannot get login from here!'

                        ], REST_Controller::HTTP_OK);
                    } elseif ($user['is_verified'] == 0) {

                        $sms_data = array();
                        $sms_data['to'] = convertToEnNumbers($user['phone']); // converting arabic character numbers to english format
                        $sms_data['sms'] = 'Verification Code : ' . $user['verification_code'] . '';
                        sendSMS($sms_data);
                        $this->response([

                            'status' => TRUE,

                            'message' => lang('account_not_verified'),
                            'user_info' => $user

                        ], REST_Controller::HTTP_OK);
                    }elseif ($user['status'] == 'inactive') {
                        $this->response([

                            'status' => FALSE,

                            'message' => 'Your account is suspended. Please contact admin for further details & action'

                        ], REST_Controller::HTTP_OK);
                    } else {

                        // updating temp user id to this user inserted id in temp orders
                        if (isset($data['temp_order_key']) && $data['temp_order_key'] != '') {
                            $temp_orders_for_user = $this->Temp_order_model->getWithMultipleFields(array('user_id' => $temp_order_key));
                            if ($temp_orders_for_user) {
                                $this->Temp_order_model->update(array('user_id' => $user['user_id']), array('user_id' => $temp_order_key));
                            }
                        }

                        $this->response([

                            'status' => TRUE,

                            'user_info' => $user

                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([

                        'status' => FALSE,

                        'message' => 'User does not exist with this google id.please sign up this user first.'

                    ], REST_Controller::HTTP_OK);
                }
            } else {


                $data_fetch['email'] = $data['email'];
                $data_fetch['password'] = md5($data['password']);
                $user = $this->Model_user->getWithMultipleFields($data_fetch, true);
                $city_data = $this->Model_general->getRow($user['city_id'],'city');
                
                if ($user) {
                    if($city_data){
                            $user['city_title'] = $city_data->eng_name;
                        }else{
                            $user['city_title'] = '';
                        }
                    if (isset($data['device_type']) || isset($data['device_token'])) {
                        $update_user = array();
                        $update_user_by = array();
                        $update_user_by['user_id'] = $user['user_id'];

                        if (isset($data['device_type'])) {
                            $update_user['device_type'] = $data['device_type'];
                            $user['device_type'] = $data['device_type'];
                        }

                        if (isset($data['device_token'])) {
                            $update_user['device_token'] = $data['device_token'];
                            $user['device_token'] = $data['device_token'];
                        }


                        $this->Model_user->update($update_user, $update_user_by);
                    }

                    //$this->updateUserLoginStatus($user['id']);


                    if ($user['role_id'] != 3 && $user['role_id'] != 5) {
                        $this->response([

                            'status' => FALSE,

                            'message' => 'You cannot get login from here!'

                        ], REST_Controller::HTTP_OK);
                    } elseif ($user['is_verified'] == 0) {

                        $sms_data = array();
                        $sms_data['to'] = convertToEnNumbers($user['phone']); // converting arabic character numbers to english format
                        $sms_data['sms'] = 'Verification Code : ' . $user['verification_code'] . '';
                        sendSMS($sms_data);
                        $this->response([

                            'status' => TRUE,

                            'message' => lang('account_not_verified'),
                            'user_info' => $user

                        ], REST_Controller::HTTP_OK);
                    } elseif ($user['is_approved'] == 0) {
                        $this->response([

                            'status' => FALSE,

                            'message' => lang('account_not_approved')

                        ], REST_Controller::HTTP_OK);
                    } elseif ($user['status'] == 'inactive') {
                        $this->response([

                            'status' => FALSE,

                            'message' => 'Your account is suspended. Please contact admin for further details & action'

                        ], REST_Controller::HTTP_OK);
                    } else {

                        // updating temp user id to this user inserted id in temp orders
                        if (isset($data['temp_order_key']) && $data['temp_order_key'] != '') {
                            $temp_orders_for_user = $this->Temp_order_model->getWithMultipleFields(array('user_id' => $temp_order_key));
                            if ($temp_orders_for_user) {
                                $this->Temp_order_model->update(array('user_id' => $user['user_id']), array('user_id' => $temp_order_key));
                            }
                        }

                        $this->response([

                            'status' => TRUE,

                            'user_info' => $user

                        ], REST_Controller::HTTP_OK);
                    }


                } else {

                    $this->response([

                        'status' => FALSE,

                        'message' => 'Email or password incorrect'

                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

                }
            }
        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


    }

    public function adminLogin_post()
    {
        $data_fetch = array();
        $data = $this->input->post(); // email, password, temp_order_key
        if (!empty($data)) {
            $data_fetch['email'] = $data['email'];
            $data_fetch['password'] = md5($data['password']);
            $user = $this->Model_user->getWithMultipleFields($data_fetch, true);
            if ($user) {
                if (isset($data['device_type']) || isset($data['device_token'])) {
                    $update_user = array();
                    $update_user_by = array();
                    $update_user_by['user_id'] = $user['user_id'];
                    if (isset($data['device_type'])) {
                        $update_user['device_type'] = $data['device_type'];
                        $user['device_type'] = $data['device_type'];
                    }
                    if (isset($data['device_token'])) {
                        $update_user['device_token'] = $data['device_token'];
                        $user['device_token'] = $data['device_token'];
                    }
                    $this->Model_user->update($update_user, $update_user_by);
                }

                if ($user['role_id'] == 1 || $user['role_id'] == 2) { // only admin and support type users can login from here
                    if ($user['role_id'] == 1) // admin
                    {
                        $user['view_chat'] = 1;
                        $user['view_tickets'] = 1;
                        $user['view_reopened_tickets'] = 1;
                    } elseif ($user['role_id'] == 2) // support user
                    {
                        $assigned_complaint_types = assigned_complaint_types($user['user_id']);
                        if ($assigned_complaint_types == 'None') { // if no ticket types assigned to support user
                            $user['view_chat'] = 1;
                            $user['view_tickets'] = 0;
                        } else {
                            $user['view_chat'] = 0;
                            $user['view_tickets'] = 1;
                        }
                        $user['view_reopened_tickets'] = 0;
                    } else {
                        $user['view_chat'] = 0;
                        $user['view_tickets'] = 0;
                        $user['view_reopened_tickets'] = 0;
                    }
                    $this->response([
                        'status' => TRUE,
                        'user_info' => $user
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'You cannot login from here!'
                    ], REST_Controller::HTTP_OK);
                }
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Email or password incorrect'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

    public function uploadImage($key, $path)

    {


        $file_name = '';

        $file_name = rand(999, 999999) . date('Ymdhsi') . $_FILES[$key]['name'];

        $file_size = $_FILES[$key]['size'];

        $file_tmp = $_FILES[$key]['tmp_name'];

        $file_type = $_FILES[$key]['type'];

        move_uploaded_file($file_tmp, $path . $file_name);

        return $path.$file_name;


    }


    public function uploadMultipleImages($key, $path, $i)

    {


        $file_name = '';

        $file_name = date('Ymdhsi') . $_FILES[$key]['name'][$i];

        $file_size = $_FILES[$key]['size'][$i];

        $file_tmp = $_FILES[$key]['tmp_name'][$i];

        $file_type = $_FILES[$key]['type'][$i];

        move_uploaded_file($file_tmp, $path . $file_name);

        return $file_name;


    }


    public function updateUserLoginStatus($id)

    {

        $data = array();

        $arr_update = array();

        $data['last_login'] = date('Y-m-d H:i:s');

        //$user = $this->session->userdata('user');

        $arr_update['id'] = $id;

        $this->Model_user->update($data, $id);

        return true;

        //redirect($this->config->item('base_url') . 'user');


    }


    public function forgotPassword_post()
    {
        $data = $this->input->post();
        if ($data) {
            $fetch_by['email'] = $data['email'];
            $user = $this->Model_user->getWithMultipleFields($fetch_by);
            if ($user) {
                $update = array();
                $update_by = array();
                $new_pass = RandomString();
                $update['password'] = md5($new_pass);

                $update_by['user_id'] = $user->user_id;

                $this->Model_user->update($update, $update_by);

                $mail_sent = $this->sendForgotPasswordEmail($user->user_id);
                if ($mail_sent) {
                    $this->response([
                        'status' => TRUE,
                        'message' => 'your new password is sent at your email address'
                    ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
                }

            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No user found with this email'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {

            $this->response([
                'status' => FALSE,
                'message' => 'No data found'
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code

        }

    }


    public function users_get()

    {

        // Users from a data store e.g. database

        $users = [

            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],

            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],

            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],

        ];


        $id = $this->get('id');


        // If the id parameter doesn't exist return all the users


        if ($id === NULL) {

            // Check if the users data store contains users (in case the database result returns NULL)

            if ($users) {

                // Set the response and exit

                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

            } else {

                // Set the response and exit

                $this->response([

                    'status' => FALSE,

                    'message' => 'No users were found'

                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

            }

        }


        // Find and return a single record for a particular user.


        $id = (int)$id;


        // Validate the id.

        if ($id <= 0) {

            // Invalid id, set the response and exit.

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


        // Get the user from the array, using the id as key for retreival.

        // Usually a model is to be used for this.


        $user = NULL;


        if (!empty($users)) {

            foreach ($users as $key => $value) {

                if (isset($value['id']) && $value['id'] === $id) {

                    $user = $value;

                }

            }

        }


        if (!empty($user)) {

            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code

        } else {

            $this->set_response([

                'status' => FALSE,

                'message' => 'User could not be found'

            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code

        }

    }


    public function users_post()

    {

        // $this->some_model->update_user( ... );

        $message = [

            'id' => 100, // Automatically generated by the model

            'name' => $this->post('name'),

            'email' => $this->post('email'),

            'message' => 'Added a resource'

        ];


        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code

    }


    public function users_delete()

    {

        $id = (int)$this->get('id');


        // Validate the id.

        if ($id <= 0) {

            // Set the response and exit

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }


        // $this->some_model->delete_something($id);

        $message = [

            'id' => $id,

            'message' => 'Deleted the resource'

        ];


        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code

    }


    public function saveContact_post()
    {
        $data = $this->input->post();


        if (!empty($data)) {
            $fetch_key['user_id'] = $data['user_id'];
            $fetch_key['key'] = $data['X-API-KEY'];

            $check_user_key = checkKeyForThisUser($fetch_key);

            if ($check_user_key) {

                $data['created_at'] = date('Y-m-d H:i:s');
                unset($data['X-API-KEY']);
                $insert_id = $this->Model_contact->save($data);
                $this->response([

                    'status' => TRUE,

                    'message' => 'Contact Saved Successfully'

                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Invalid User Key'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (200) being the HTTP response code
            }
        } else {

            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code

        }
    }

    public function updateUser_post()
    {
        $post_data = $this->input->post();
        if (!empty($post_data)) {
            $update_by['user_id'] = $post_data['user_id'];
            if (isset($post_data['password'])) {
                $post_data['password'] = md5($post_data['password']);
                if(isset($post_data['old_password'])){
                     $user_data = $this->Model_user->get($post_data['user_id'], true, 'user_id');

                    if(md5($post_data['old_password']) != $user_data['password']){

                        $this->response([
                            'status' => FALSE,
                            'message' => 'Old password does not match'
                        ], REST_Controller::HTTP_OK);

                     }


                     unset($post_data['old_password']);
                }
               


                
                
            }
            

            if (isset($_FILES['profile_pic']) && $_FILES['profile_pic']['name'] != '') {

                    $post_data['profile_pic'] = $this->uploadImage("profile_pic", "uploads/images/");
                    //$file_name = "uploads/images/" . $file_name;
                }

                if (isset($_FILES['driving_license_img']) && $_FILES['driving_license_img']['name'] != '') {

                    $post_data['driving_license_img'] = $this->uploadImage("driving_license_img", "uploads/images/");
                    //$file_name = "uploads/images/" . $file_name;
                }

                if (isset($_FILES['registration_license_img']) && $_FILES['registration_license_img']['name'] != '') {

                    $post_data['registration_license_img'] = $this->uploadImage("registration_license_img", "uploads/images/");
                    //$file_name = "uploads/images/" . $file_name;
                }


               

                /*if(isset($post_data['districts'])){
                    $this->Model_general->deleteMultipleRow('user_districts', 'user_id', $post_data['user_id']);
                    $districts = explode(',',$post_data['districts']);
                    foreach ($districts as $district) {
                        $city_data['user_id'] = $post_data['user_id'];
                        $city_data['district_id'] = $district; // Actually cities label change to districts
                        $city_data['user_type'] = 'delivery';
                        $this->Model_general->save('user_districts', $city_data);
                    }
                }  
             unset($post_data['districts']);  */   
            $this->Model_user->update($post_data, $update_by);
            $user = $this->Model_user->get($update_by['user_id'], true, 'user_id');
            $city_data = $this->Model_general->getRow($user['city_id'],'city');
                if($city_data){
                    $user['city_title'] = $city_data->eng_name;
                }else{
                    $user['city_title'] = '';
                }
            $this->response([
                'status' => TRUE,
                'user_info' => $user
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }


    private function check_unread_comments($ticket_id, $user_id)
    {
        if ($ticket_id) {
            $sql = "SELECT count(comment_id) AS unread FROM ticket_comments WHERE ticket_id = $ticket_id AND is_read = 0 AND user_id <> $user_id ";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['unread'];
        }
    }

    private function sendEmail($txt)
    {
        $to = "bilal.e@zynq.net";
        $subject = "Niehez Test";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <no-reply@zynq.net>' . "\r\n";
        mail($to, $subject, $txt, $headers);
    }

    public function ksaCities_get()
    {
        $ksa_cities = getAllKsaCities();
        if ($ksa_cities) {
            $response['status'] = true;
            $response['cities'] = $ksa_cities;
        } else {
            $response['status'] = false;
            $response['cities'] = array();
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        exit();
    }


    public function getDistricts_get()
    {
        $districts = getAllDistricts($this->input->get('city_id'));
        if ($districts) {
            $response['status'] = true;
            $response['districts'] = $districts;
        } else {
            $response['status'] = false;
            $response['districts'] = array();
        }
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
        exit();
    }


    

    public function sendWelcomeEmail($user_id, $password)
    {
        $user = $this->Model_user->get($user_id, true, 'user_id');
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Niehez </title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}


#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>



<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user['full_name'] . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Thank you for registering with Niehez. We hope to provide you the best we can.</p>
<p style="font-family:sans-serif;font-size:14px;">Your login details are:</p>
<p style="font-family:sans-serif;font-size:14px;">Email: ' . $user['email'] . '</p>
<p style="font-family:sans-serif;font-size:14px;">Password: ' . $password . '</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Dkakeen</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>

</td>
</tr>
</table>


</td>
</tr>
</table>

</body>
</html>
';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <noreply@Niehez.com.sa>' . "\r\n";
        mail($user['email'], 'Welcome at Niehez ', $message, $headers);

    }

    public function sendForgotPasswordEmail($user_id)
    {
        $search['user_id'] = $user_id;
        $user = $this->Api_model->getSingleRow('users', $search);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <noreply@Niehez.com.sa>' . "\r\n";

        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Niehez</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;	
}

h4{
	
margin-bottom:20px;

	
}


#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>



<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user->full_name . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Forgot Your Password ?</p>
<p style="font-family:sans-serif;font-size:14px;">No worries , lets get you a new one</p>
<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
      <a href="' . base_url() . 'forgotpassword/index/' . $user->user_id . '"
style="background-color:#10b26a;border-radius:100px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:13px;font-weight:bold;line-height:30px;text-align:center;text-decoration:none;width:250px;-webkit-text-size-adjust:none;">Set a New Password</a>
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Dkakeen</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>

</td>
</tr>
</table>


</td>
</tr>
</table>

</body>
</html>
';

        mail($user->email, 'Forgot Password', $message, $headers);
        return true;
    }

    public function resendVerificationCode_post()
    {
        $user_id = $this->input->post('user_id');
        $user = $this->Model_user->get($user_id, false, 'user_id');
        $sms_data = array();
        $sms_data['to'] = convertToEnNumbers($user->phone); // converting arabic character numbers to english format
        $sms_data['sms'] = 'Verification Code : ' . $user->verification_code;
        sendSMS($sms_data);
        $this->response([

            'status' => TRUE,

            'user_info' => $user

        ], REST_Controller::HTTP_OK);
    }

    private function sendThankyouEmailToCustomer($order_id)
    {
        $order_details = $this->Model_order->getDetail($order_id);
        $data['order_details'] = $order_details;
        $message = $this->load->view('front/emails/order_confirmation', $data, true);
        $email_data['to'] = $order_details[0]['email'];
        $email_data['subject'] = 'Order received at Dkakeen : Order # ' . $order_details[0]['order_track_id'];
        $email_data['from'] = 'noreply@dkakeen.com.sa';
        $email_data['body'] = $message;
        sendEmail($email_data);
        return true;
    }

    public function sendThankyouEmail_get()
    {
        /*$data['content'] = '<h4>Hi Bilal Ejaz,</h4>
                                                <p>Thank you for Purchasing from Niehez Security Systems.</p>
                                                <p> Your order # 123123123 has been placed successfully.</p>';
        $message = $this->load->view('front/emails/general_email', $data, true);
        echo $message;exit();*/
        $email_data['body'] = "Dear Bilal Ejaz , Your account is suspended at Niehez. Please contact admin for details and further action.";
        $email_body = emailTemplate($email_data['body']);
        echo $email_body;
        exit();
        $order_id = $this->input->get('order_id');
        $order_details = $this->Model_order->getDetail($order_id);
        $data['order_details'] = $order_details;
        $message = $this->load->view('front/emails/order_confirmation', $data, true);
        echo $message;
        exit();
        $email_data['to'] = 'bilal.e@zynq.net';
        $email_data['subject'] = 'Order received at Dkakeen : Order # ' . $order_details[0]['order_track_id'];
        $email_data['from'] = 'noreply@dkakeen.com.sa';
        $email_data['body'] = $message;
        sendEmail($email_data);
        return true;
    }

    public function getCartCount_get()
    {
        $user_id = $this->input->get('user_id');
        $count = getTotalProduct($user_id);
        $this->response([

            'status' => TRUE,

            'cart_count' => $count > 0 ? (int)$count : 0

        ], REST_Controller::HTTP_OK);
    }

    public function getComplaintTypes_get()
    {
        // complaint types
        $parent_id = 0;
        if (isset($_GET['complaint_type_id']) && $_GET['complaint_type_id'] > 0) {
            $parent_id = $_GET['complaint_type_id'];
        }
        $complain_types_new = $this->Model_complaint_type->getMultipleRows(array('parent_id' => $parent_id, 'is_active' => 1), true);
        $this->response([
            'status' => TRUE,
            'complaint_types' => $complain_types_new ? $complain_types_new : array()
        ], REST_Controller::HTTP_OK);
    }

    public function getChatRequests_get()
    {
        $chat_requests = $this->Model_chat_request->getMultipleRows(array('is_closed' => 0, 'is_in_progress' => 0), true);
        $this->response([
            'status' => TRUE,
            'chat_requests' => ($chat_requests ? $chat_requests : array())
        ], REST_Controller::HTTP_OK);
    }

    public function getTickets_get()
    {
        $this->checkVersion();
        $tickets = array();
        $tickets_response = array();
        $type = $this->input->get('type'); // 1(open), 2(closed), 3(reopened)
        $user_id = $this->input->get('user_id');
        $user = $this->Model_user->get($user_id, true, 'user_id');
        if ($type == 1) {
            $tickets = $this->Model_support_tickets->getOpenTickets();
        } elseif ($type == 2) {
            $tickets = $this->Model_support_tickets->getClosedTickets();
        } elseif ($type == 3) {
            $tickets = $this->Model_support_tickets->getReOpenedTickets();
        }

        if (count($tickets) > 0) {
            $i = 0;
            $assigned_complaint_types_arr = assigned_complaint_types_arr($user_id);
            foreach ($tickets as $key => $ticket) {
                // checking if user is admin or it user is support type user then check if this ticket type is assigned to him or not and if the ticket is unopened or is already opened by this specific support user
                if ($user['role_id'] == 1 || ($user['role_id'] == 2 && in_array($ticket['type_id'], $assigned_complaint_types_arr) && ($ticket['ticket_taken_by'] == 0 || $ticket['ticket_taken_by'] == $user_id))) {
                    $tickets_response[$i] = $ticket;
                    $tickets_response[$i]['unread'] = $this->check_unread_comments($ticket['ticket_id'], $user_id);
                    $ticket_type_parent = ticket_type_detail($ticket['ticket_type']);
                    $ticket_type_child = ticket_type_detail($ticket['type_id']);
                    $tickets_response[$i]['ticket_type'] = $ticket_type_parent->complaint_title_en;
                    $tickets_response[$i]['type_id'] = $ticket_type_child->complaint_title_en;
                    $i++;
                }
            }
        }
        // dump($tickets_response);
        $this->response([
            'status' => TRUE,
            'tickets' => $tickets_response
        ], REST_Controller::HTTP_OK);

    }

    public function ticketDetail_get()
    {
        $this->checkVersion();
        $ticket_id = $this->input->get('ticket_id');
        $user_id = $this->input->get('user_id');
        $ticket = $this->Model_support_tickets->get($ticket_id, true, 'ticket_id');
        $comments = $this->Model_ticket_comments->getMultipleRows(['ticket_id' => $ticket_id]);
        $ticket_type_parent = ticket_type_detail($ticket['ticket_type']);
        $ticket_type_child = ticket_type_detail($ticket['type_id']);
        $ticket['ticket_type'] = $ticket_type_parent->complaint_title_en;
        $ticket['type_id'] = $ticket_type_child->complaint_title_en;
        // update is_read status if i am not the user who posted the comment
        if ($comments) {
            foreach ($comments as $item) {
                // update is_read status if i am not the user who posted the comment
                if ($item->user_id != $user_id) {
                    $update_data['is_read'] = 1;
                    $update_by['comment_id'] = $item->comment_id;
                    $this->Model_ticket_comments->update($update_data, $update_by);
                }
            }
        }

        $this->response([
            'status' => TRUE,
            'ticket_detail' => $ticket,
            'comments' => $comments
        ], REST_Controller::HTTP_OK);
    }

    public function sendMessage_post()
    {
        $this->checkVersion();
        $post_data = $this->input->post(); // message, ticket_id, user_id
        if (!empty($post_data['message']) && !empty($post_data['ticket_id']) && !empty($post_data['user_id'])) {
            $store_data['message'] = trim($post_data['message']);
            $store_data['user_id'] = $post_data['user_id'];
            $store_data['timestamp'] = $post_data['timestamp'];
            $store_data['ticket_id'] = $post_data['ticket_id'];
            $insert_id = $this->Model_ticket_comments->save($store_data);
            if ($insert_id > 0) {
                $getUserData = $this->Model_support_tickets->getUserData($post_data['ticket_id']);
                $get_comment = $this->Model_ticket_comments->get($insert_id, true, 'comment_id');
                $get_ticket_user = $this->Model_user->get($post_data['user_id'], true, 'user_id');
                // if commnet is done by support type user and ticket is not already taken
                if ($get_ticket_user['role_id'] == 2 && $getUserData['ticket_taken_by'] == 0) {
                    $this->Model_support_tickets->update(array('ticket_taken_by' => $get_comment['user_id']), array('ticket_id' => $post_data['ticket_id']));
                }

                $getUserData = $this->Model_support_tickets->getUserData($post_data['ticket_id']);
                if (!empty($getUserData)) {
                    if ($getUserData['device_type'] != '' && $getUserData['device_token'] != '') {
                        $push_data = array();
                        $push_data['device_type'] = $getUserData['device_type'];
                        $push_data['device_token'] = $getUserData['device_token'];
                        $push_data['title'] = 'Admin Reply : Ticket ' . $post_data['ticket_id'];
                        $push_data['id'] = $insert_id;
                        $push_data['type'] = 'post';
                        $push_data['message'] = trim($post_data['message']);
                        sendPushNotification($push_data, $post_data['ticket_id']);
                    }

                    // send email to that user as a notification
                    $body = '';
                    $body .= '<p>Hi, ' . $getUserData['full_name'] . '</p>';
                    $body .= '<p>Niehez admin has replied to you against your ticket no ' . $post_data['ticket_id'] . ' .</p>';
                    $body .= '<p>' . trim($post_data['message']) . '</p>';
                    $message = emailTemplate($body);
                    $email_data = array();
                    $email_data['to'] = $getUserData['email'];
                    $email_data['subject'] = 'Admin Reply : Ticket ' . $post_data['ticket_id'];
                    $email_data['from'] = 'no_reply@Niehez.com';
                    $email_data['body'] = $message;
                    sendEmail($email_data);
                    // end Email  send

                    // send SMS
                    $sms_data = array();
                    $sms_data['to'] = $getUserData['phone'];
                    $sms_data['sms'] = 'Admin Reply : Ticket ' . $post_data['ticket_id'] . '! ' . trim($post_data['message']) . '';
                    sendSMS($sms_data);
                    //end sms functionality
                }

                $msg = $this->Model_ticket_comments->get($insert_id, true, 'comment_id');
                // $formatted_message = formatted_message($msg, $post_data['user_id']);
                // $response['status'] = 'TRUE';
                // $response['message'] = $formatted_message;

                // pusher call here
                pusher($msg, 'ticket_' . $post_data['ticket_id'], 'ticket_' . $post_data['ticket_id'], false);

                $this->response([
                    'status' => TRUE,
                    'message' => "",
                    'message_data' => $msg
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Something went wrong while saving the message",
                    'message_data' => array()
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "Posted data is not correct",
                'message_data' => array()
            ], REST_Controller::HTTP_OK);
        }
    }

    public function updateUserDeviceToken_post()
    {
        $post_data = $this->input->post(); // user_id, device_token, device_type (ios, android)
        if (!empty($post_data)) {
            $update_user['device_type'] = $post_data['device_type'];
            $update_user['device_token'] = $post_data['device_token'];
            $update_user_by['user_id'] = $post_data['user_id'];
            $this->Model_user->update($update_user, $update_user_by);
            $this->response([
                'status' => TRUE,
                'message' => "Token updated successfully",
                'message_data' => array()
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "Posted data is not correct",
                'message_data' => array()
            ], REST_Controller::HTTP_OK);
        }
    }

    public function checkIfChatExist_get()
    {
        $post_data = $this->input->get(); // chat_id
        $chat = $this->Model_chat_request->get($post_data['chat_id'], true);
        if ($chat) {
            if ($chat['is_closed'] == 1) {
                $this->response([
                    'status' => FALSE,
                    'message' => "Chat is already closed",
                    'chat' => $chat
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => TRUE,
                    'message' => "Chat exists",
                    'chat' => $chat
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "Chat doesn't exist"
            ], REST_Controller::HTTP_OK);
        }
    }

    public function updateChatInProgress_get()
    {
        $post_data = $this->input->get(); // chat_id, is_in_progress
        $update['is_in_progress'] = $post_data['is_in_progress'];
        $update_by['id'] = $post_data['chat_id'];
        $this->Model_chat_request->update($update, $update_by);
        $this->response([
            'status' => TRUE,
            'message' => "Updated successfully"
        ], REST_Controller::HTTP_OK);
    }

    public function closeTicket_get()
    {
        $post_data = $this->input->get(); // ticket_id
        $update_data['status'] = 'closed';
        $update_data['closed_request'] = 1;// admin want to close this ticket
        $update_data['last_closed_by'] = $post_data['user_id']; // id of the user who is closing this ticket
        $update_by['ticket_id'] = $post_data['ticket_id'];
        $this->Model_support_tickets->update($update_data, $update_by);
        $where = 'support_tickets.ticket_id = ' . $post_data['ticket_id'] . ' ';
        $getUserData = $this->Model_support_tickets->getJoinedData(true, 'user_id', 'users', $where);
        $getUserData = $getUserData[0];
        if ($getUserData['device_type'] != '' && $getUserData['device_token'] != '') {
            $push_data = array();
            $push_data['device_type'] = $getUserData['device_type'];
            $push_data['device_token'] = $getUserData['device_token'];
            $push_data['title'] = 'Ticket Closed : Ticket ' . $post_data['ticket_id'];
            $push_data['id'] = $getUserData['id'];
            $push_data['type'] = 'post';
            $push_data['message'] = 'Dkakeen admin closed your ticket no ' . $post_data['ticket_id'];;
            sendPushNotification($push_data, $post_data['ticket_id'], 1);
        }

        // send email to that user as a notification
        $body = '';
        $body .= '<p>Hi, ' . $getUserData['full_name'] . '</p>';
        $body .= '<p>Dkakeen admin closed your ticket no ' . $post_data['ticket_id'] . ' .</p>';
        $message = emailTemplate($body);
        $email_data = array();
        $email_data['to'] = $getUserData['email'];
        $email_data['subject'] = 'Ticket Closed : Ticket ' . $post_data['ticket_id'];
        $email_data['from'] = 'no_reply@dkakeen.com';
        $email_data['body'] = $message;
        sendEmail($email_data);
        // end Email send

        // send SMS
        $sms_data = array();
        $sms_data['to'] = $getUserData['phone'];
        $sms_data['sms'] = 'Dkakeen admin closed your ticket no ' . $post_data['ticket_id'];
        sendSMS($sms_data);
        // end sms functionality

        $this->response([
            'status' => TRUE,
            'message' => "Ticket closed successfully"
        ], REST_Controller::HTTP_OK);
    }

    private function sendEmailToSupportUsers($support_users, $ticket_id, $user_id)
    {
        $user_details = $this->Model_user->get($user_id, true, 'user_id');
        $body = '';
        $body .= '<p>Hi, Admin</p>';
        $body .= '<p>Dkakeen user, ' . $user_details['full_name'] . ' has created a new support ticket with ticket no ' . $ticket_id . ' . Please have a look.</p>';
        $message = emailTemplate($body);

        foreach ($support_users as $support_user) {
            $support_user_details = $this->Model_user->get($support_user->user_id, false, 'user_id');
            $email_data['to'] = $support_user_details->email;
            $email_data['subject'] = 'Support Request : Ticket ' . $ticket_id;
            $email_data['from'] = $user_details['email'];
            $email_data['body'] = $message;
            sendEmail($email_data);
        }
    }

    private function sendEmailToAdmins($ticket_id, $user_id)
    {
        $user_details = $this->Model_user->get($user_id, true, 'user_id');
        $admins = $this->Model_user->getMultipleRows(array('role_id' => 1)); // fetching all admin users
        $body = '';
        $body .= '<p>Hi, Admin</p>';
        $body .= '<p>Niehez user, ' . $user_details['full_name'] . ' has created a new support ticket with ticket no ' . $ticket_id . ' . Please have a look.</p>';
        $message = emailTemplate($body);

        foreach ($admins as $admin) {
            $email_data['to'] = $admin->email;
            $email_data['subject'] = 'Support Request : Ticket ' . $ticket_id;
            $email_data['from'] = $user_details['email'];
            $email_data['body'] = $message;
            sendEmail($email_data);
        }
    }

    public function sendPushNotificationToAdminsAndSupportUsers($ticket_id)
    {
        $get_ticket = $this->Model_support_tickets->get($ticket_id, true);
        $admin_users = $this->Model_user->getMultipleRows(array('role_id' => 1), true); // fetching admin users
        // sending notifications to all admin type users
        foreach ($admin_users as $admin_user) {
            $title = "Ticket Received : Ticket " . $get_ticket['ticket_id'];
            $message = "Dear " . $admin_user['full_name'] . "\nA new ticket is generated at Niehez with ticket no " . $get_ticket['ticket_id'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'new_ticket_request';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }

        // fetching support type users for this ticket type
        $support_users = $this->Model_support_user->getMultipleRows(array('complaint_type_id' => $get_ticket['type_id']));
        // sending notification
        foreach ($support_users as $support_user) {
            $admin_user = $this->Model_user->get($support_user->user_id, true, 'user_id');
            $title = "Ticket Received : Ticket " . $get_ticket['ticket_id'];
            $message = "Dear " . $admin_user['full_name'] . "\nA new ticket is generated at Niehez with ticket no " . $get_ticket['ticket_id'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'new_ticket_request';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }


        return true;
    }

    public function logout_get()
    {
        $post_data = $this->input->get(); // id
        if (!empty($post_data)) {
            // did this following because of different keys coming from IOS and android and builds were already sent so had to do this
            if (isset($post_data['id']) && $post_data['id'] > 0) // for IOS
            {
                $this->Model_user->update(array('device_token' => ''), array('user_id' => $post_data['id']));
            } elseif (isset($post_data['user_id']) && $post_data['user_id'] > 0) // for android
            {
                $this->Model_user->update(array('device_token' => ''), array('user_id' => $post_data['user_id']));
            }
            $this->response([
                'status' => TRUE,
                'message' => "User logged out successfully\nتسجيل خروج المستخدم بنجاح",
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "No posted data found\nلم يتم العثور على بيانات منشورة"
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    private function sendEmailToAdminsForReopenedTicket($user_details, $ticket_id)
    {
        $admins = $this->Model_user->getMultipleRows(array('role_id' => 1)); // fetching all admin users
        $body = '';
        $body .= '<p>Hi, Admin</p>';
        $body .= '<p>Niehez user, ' . $user_details['full_name'] . ' want to reopen his ticket no ' . $ticket_id . ' . Please have a look.</p>';
        $message = emailTemplate($body);

        foreach ($admins as $admin) {
            $email_data['to'] = $admin->email;
            $email_data['subject'] = 'Reopen Request : Ticket ' . $ticket_id;
            $email_data['from'] = $user_details['email'];
            $email_data['body'] = $message;
            sendEmail($email_data);
        }
    }

    public function sendReopenedTicketNotificationToAdminUsers($ticket_id)
    {
        $get_ticket = $this->Model_support_tickets->get($ticket_id, true);
        $admin_users = $this->Model_user->getMultipleRows(array('role_id' => 1), true); // fetching admin users
        // sending notifications to all admin type users
        foreach ($admin_users as $admin_user) {
            $title = "Ticket Reopen Request : Ticket " . $get_ticket['ticket_id'];
            $message = "Dear " . $admin_user['full_name'] . "\nA request to reopen ticket is generated at Niehez for ticket no " . $get_ticket['ticket_id'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'ticket_reopened';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }

        // fetching support type users for this ticket type
        $support_users = $this->Model_support_user->getMultipleRows(array('complaint_type_id' => $get_ticket['type_id']));
        // sending notification
        foreach ($support_users as $support_user) {
            $admin_user = $this->Model_user->get($support_user->user_id, true, 'user_id');
            $title = "Ticket Reopen Request : Ticket " . $get_ticket['ticket_id'];
            $message = "Dear " . $admin_user['full_name'] . "\nA request to reopen ticket is generated at Niehez for ticket no " . $get_ticket['ticket_id'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'ticket_reopened';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }


        return true;
    }

    public function sendTicketCommentReplyNotificationToAdminUsers($comment_id)
    {
        $get_comment = $this->Model_ticket_comments->get($comment_id, true, 'comment_id');
        $get_ticket = $this->Model_support_tickets->get($get_comment['ticket_id'], true);
        if ($get_comment['user_id'] == $get_ticket['user_id']) {
            $admin_users = $this->Model_user->getMultipleRows(array('role_id' => 1), true); // fetching admin users
            // sending notifications to all admin type users
            foreach ($admin_users as $admin_user) {
                $title = "User Reply : Ticket " . $get_ticket['ticket_id'];
                $message = $get_comment['message'];
                $data['ticket_id'] = $get_ticket['id'];
                $data['type'] = 'ticket_reopened';
                sendNotification($title, $message, $data, $admin_user['user_id']);
            }

            if ($get_ticket['status'] == 'open' && ($get_ticket['closed_request'] == 0 || $get_ticket['closed_request'] == 1 || $get_ticket['closed_request'] == 2)) {
                // fetching support type users for this ticket type
                $support_users = $this->Model_support_user->getMultipleRows(array('complaint_type_id' => $get_ticket['type_id']));
                // sending notification
                foreach ($support_users as $support_user) {
                    $admin_user = $this->Model_user->get($support_user->user_id, true, 'user_id');
                    $title = "User Reply : Ticket " . $get_ticket['ticket_id'];
                    $message = $get_comment['message'];
                    $data['ticket_id'] = $get_ticket['id'];
                    $data['type'] = 'ticket_reopened';
                    sendNotification($title, $message, $data, $admin_user['user_id']);
                }
            }
        } elseif ($get_comment['user_id'] !== $get_ticket['user_id']) {
            $title = "Admin Reply : Ticket " . $get_ticket['ticket_id'];
            $message = $get_comment['message'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'ticket_reopened';
            sendNotification($title, $message, $data, $get_ticket['user_id']);
        }

        return true;
    }

    public function checkVersion()
    {
        return true;// for now we don't need it
        $this->load->model('Model_general');
        $site = $this->Model_general->getSingleRow('site_settings', array('id' => 1));
        if (isset($_REQUEST['app_type']) && $_REQUEST['app_type'] != '' &&
            (
                isset($_REQUEST['ios_app_version']) ||
                isset($_REQUEST['android_app_version']) ||
                isset($_REQUEST['ios_team_app_version']) ||
                isset($_REQUEST['android_team_app_version'])
            )
        ) {
            if (
                (strtolower($_REQUEST['app_type']) == 'ios' && $_REQUEST['ios_app_version'] == $site->ios_app_version) ||
                (strtolower($_REQUEST['app_type']) == 'android' && $_REQUEST['android_app_version'] == $site->android_app_version) ||
                (strtolower($_REQUEST['app_type']) == 'team_ios' && $_REQUEST['ios_team_app_version'] == $site->ios_team_app_version) ||
                (strtolower($_REQUEST['app_type']) == 'team_android' && $_REQUEST['android_team_app_version'] == $site->android_team_app_version)
            ) {
                /*$this->response([
                    'status' => TRUE,
                    'message' => "All ok to continue\nكل طيب للمتابعة",
                    'ios_app_version' => $site->ios_app_version,
                    'android_app_version' => $site->android_app_version
                ], REST_Controller::HTTP_OK);*/
                // all ok
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Sorry for inconvenience. Please update your app to continue using it\nآسف على الإزعاج. يرجى تحديث تطبيقك لمتابعة استخدامه",
                    'ios_app_version' => $site->ios_app_version,
                    'android_app_version' => $site->android_app_version,
                    'ios_team_app_version' => $site->ios_team_app_version,
                    'android_team_app_version' => $site->android_team_app_version,
                    'version_ok' => FALSE
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => "Sorry for inconvenience. Please update your app to continue using it\nآسف على الإزعاج. يرجى تحديث تطبيقك لمتابعة استخدامه",
                'ios_app_version' => $site->ios_app_version,
                'android_app_version' => $site->android_app_version,
                'ios_team_app_version' => $site->ios_team_app_version,
                'android_team_app_version' => $site->android_team_app_version,
                'version_ok' => FALSE
            ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function updateTicketOpenedByUser_get()
    {
        $this->checkVersion();
        $data = array();
        $post_data = $this->input->get(); // ticket_id, user_id
        if (!empty($post_data)) {
            $ticket = $this->Model_support_tickets->get($post_data['ticket_id'], true, 'id');
            if ($ticket) {
                $this->Model_support_tickets->update(array('ticket_taken_by' => $post_data['user_id']), array('ticket_id' => $post_data['ticket_id']));
                $this->response([
                    'status' => TRUE,
                    'message' => 'Ticket updated successfully',
                    'ticket_info' => $ticket
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => "Ticket not found with this ticket id\nلم يتم العثور على تذكرة مع معرف البطاقة هذا",
                    'ticket_info' => $ticket
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }
    }

}


<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');

class App_setting extends Base_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        checkAdminSession();
        $this->load->model('App_setting_model');
        $this->load->model('Model_product');
        
       // $this->data['language'] = $this->language;
    }

    public function index() {

        if($this->session->userdata['admin']['company_id'] > 0){
            $this->edit($this->session->userdata['admin']['company_id']);
        }else{
            $this->data['view'] = 'backend/app_setting/manage';
            $this->data['companies'] = $this->App_setting_model->getAll();
            $this->load->view('backend/layouts/default',$this->data);
        }
        
    }
    
    public function edit($company_id) {


        $fetch_by = array();

        if($this->session->userdata['admin']['company_id'] > 0){
            $company_id = $this->session->userdata['admin']['company_id'];
        }

        $fetch_by['company_id'] = $company_id;


        $this->data['result'] = $this->App_setting_model->getWithMultipleFields($fetch_by);
        $this->data['products'] = $this->Model_product->getAllProductsForBackend($fetch_by,4,0);

        if (!$this->data['result']) {
            redirect(base_url('cms/user'));
        }
        $this->data['view'] = 'backend/app_setting/edit';


        $this->data['company_id'] = $company_id;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {

            case 'update';
                //$this->validate();
                $this->update();
                break;
        }
    }

    

    private function update() {
        $post_data = $this->input->post();
        $update_by['app_setting_id'] = decode_url($post_data['id']);

        $app_setting_data = $this->App_setting_model->get($update_by['app_setting_id'],false,'app_setting_id');
        $filename = "uploads/images/" . $app_setting_data->company_id . "/";
        if (!file_exists($filename)) {
            mkdir("uploads/images/" . $app_setting_data->company_id , 0777);
            exit;
        } 

        if($app_setting_data->company_id > 0){
            $images_path = "uploads/images/".$app_setting_data->company_id."/";
        }else{
            $images_path = "uploads/images/";
        }


        if(isset($post_data['show_actual_products'])){
            $post_data['show_actual_products'] = 1;
        }else{
            $post_data['show_actual_products'] = 0;
        }

        


        
        unset($post_data['form_type']);
        unset($post_data['id']);
        

        if (isset($_FILES['splash_logo']["name"][0]) && $_FILES['splash_logo']["name"][0] != '') {
            $post_data['splash_logo']        = $this->uploadImage("splash_logo", "uploads/images/".$app_setting_data->company_id."");
        }
        if (isset($_FILES['splash_background_image']["name"][0]) && $_FILES['splash_background_image']["name"][0] != '') {
            $post_data['splash_background_image']        = $this->uploadImage("splash_background_image", $images_path);
        }
         if (isset($_FILES['login_background_image']["name"][0]) && $_FILES['login_background_image']["name"][0] != '') {
            $post_data['login_background_image']        = $this->uploadImage("login_background_image", $images_path);
        }

        if (isset($_FILES['login_logo']["name"][0]) && $_FILES['login_logo']["name"][0] != '') {
            $post_data['login_logo']        = $this->uploadImage("login_logo", $images_path);
        }


        if (isset($_FILES['main_elements_background_image']["name"][0]) && $_FILES['main_elements_background_image']["name"][0] != '') {
            $post_data['main_elements_background_image']        = $this->uploadImage("main_elements_background_image", $images_path);
        }


        if (isset($_FILES['tab_image_home_icon']["name"][0]) && $_FILES['tab_image_home_icon']["name"][0] != '') {
            $post_data['tab_image_home_icon']        = $this->uploadImage("tab_image_home_icon", $images_path);
        }


        if (isset($_FILES['tab_image_catalouge_icon']["name"][0]) && $_FILES['tab_image_catalouge_icon']["name"][0] != '') {
            $post_data['tab_image_catalouge_icon']        = $this->uploadImage("tab_image_catalouge_icon", $images_path);
        }


        if (isset($_FILES['tab_image_orders_icon']["name"][0]) && $_FILES['tab_image_orders_icon']["name"][0] != '') {
            $post_data['tab_image_orders_icon']        = $this->uploadImage("tab_image_orders_icon", $images_path);
        }


        if (isset($_FILES['tab_image_setting_icon']["name"][0]) && $_FILES['tab_image_setting_icon']["name"][0] != '') {
            $post_data['tab_image_setting_icon']        = $this->uploadImage("tab_image_setting_icon", $images_path);
        }


        if (isset($_FILES['tab_image_home_hover_icon']["name"][0]) && $_FILES['tab_image_home_hover_icon']["name"][0] != '') {
            $post_data['tab_image_home_hover_icon']        = $this->uploadImage("tab_image_home_hover_icon", $images_path);
        }


        if (isset($_FILES['tab_image_catalouge_hover_icon']["name"][0]) && $_FILES['tab_image_catalouge_hover_icon']["name"][0] != '') {
            $post_data['tab_image_catalouge_hover_icon']        = $this->uploadImage("tab_image_catalouge_hover_icon", $images_path);
        }


        if (isset($_FILES['tab_image_orders_hover_icon']["name"][0]) && $_FILES['tab_image_orders_hover_icon']["name"][0] != '') {
            $post_data['tab_image_orders_hover_icon']        = $this->uploadImage("tab_image_orders_hover_icon", $images_path);
        }


        if (isset($_FILES['tab_image_setting_hover_icon']["name"][0]) && $_FILES['tab_image_setting_hover_icon']["name"][0] != '') {
            $post_data['tab_image_setting_hover_icon']        = $this->uploadImage("tab_image_setting_hover_icon", $images_path);
        }


        if (isset($_FILES['tab_image_home_selected_icon']["name"][0]) && $_FILES['tab_image_home_selected_icon']["name"][0] != '') {
            $post_data['tab_image_home_selected_icon']        = $this->uploadImage("tab_image_home_selected_icon", $images_path);
        }


        if (isset($_FILES['tab_image_catalouge_selected_icon']["name"][0]) && $_FILES['tab_image_catalouge_selected_icon']["name"][0] != '') {
            $post_data['tab_image_catalouge_selected_icon']        = $this->uploadImage("tab_image_catalouge_selected_icon", $images_path);
        }


        if (isset($_FILES['tab_image_orders_selected_icon']["name"][0]) && $_FILES['tab_image_orders_selected_icon']["name"][0] != '') {
            $post_data['tab_image_orders_selected_icon']        = $this->uploadImage("tab_image_orders_selected_icon", $images_path);
        }

        if (isset($_FILES['tab_image_setting_selected_icon']["name"][0]) && $_FILES['tab_image_setting_selected_icon']["name"][0] != '') {
            $post_data['tab_image_setting_selected_icon']        = $this->uploadImage("tab_image_setting_selected_icon", $images_path);
        }



        

        // echo date_default_timezone_get();exit();

       
        
        $this->App_setting_model->update($post_data, $update_by);

        $success['error'] = 'false';
       // $success['reload'] = true;
        $success['success'] = 'Updated Successfully';
        echo json_encode($success);
        exit;
    }

    private function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
          $data = array();
         $extension=array("png","jpg","jpeg");
         foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $path.$file_name;
                        }
                        /*$data['DestinationID'] = $id; 
                        $data['ImagePath'] = $path.$file_name; 
                        $this->Model_destination_image->save($data);*/
                    
                }else{
                    $success['error'] = 'false';
                    $success['success'] = 'Only PNG Allowed';
                    echo json_encode($success);
                    exit;

                }
               
            }
            return true;


    }

    
}

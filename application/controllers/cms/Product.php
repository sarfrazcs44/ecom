<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_product');
        $this->load->model('Model_category');
        $this->load->model('Model_brand');
        $this->load->model('Model_product_image');
        $this->load->model('Model_product_specification');
       // $this->load->model('Model_category_text');
         checkModuleRights($this->uri->segment(3),10);
		
	}
	 
    
    public function index()
	{
        
        $fetch_by = array();
        $where = false;
        if($this->session->userdata['admin']['company_id'] > 0){
            
            $fetch_by['company_id']  = $this->session->userdata['admin']['company_id'];

            $where = 'products.company_id = '.$this->session->userdata['admin']['company_id'];
            //$this->data['products'] = $this->Model_product->getMultipleRows($fetch_by);
        }else{

            //$this->data['products'] = $this->Model_product->getAll();
        }


        $this->data['products'] = $this->Model_product->getAllProductsForBackend($fetch_by,40,0);

        $this->data['total_products']  = $this->Model_product->getCountProducts($where);


        if($where){
            $where .= ' AND products.is_active = 1';
        }else{
            $where = 'products.is_active = 1';
        }

        $this->data['total_active_products'] = $this->Model_product->getCountProducts($where);
        $this->data['top_selling'] = $this->Model_product->gettopSelling('products.company_id = '.$this->session->userdata['admin']['company_id']);

       

		
        $this->data['view'] = 'backend/product/manage';

        
        $fetch_by['is_active']       = 1;
        

        $this->data['categories']    = $this->Model_category->getMultipleRows($fetch_by);


        
        
        $this->load->view('backend/layouts/default',$this->data);
	}

    public function fetch_products(){
        $post_data = $this->input->post();

        $fetch_by = array();
       
        if(isset($post_data['category_id']) && $post_data['category_id'] != ''){
            $fetch_by['category_id']       = $post_data['category_id'];
        }
        if(isset($post_data['sub_category_id']) && $post_data['sub_category_id'] != ''){
            $fetch_by['sub_category_id']       = $post_data['sub_category_id'];
        }

        $limit = 40;

        $start_index = $limit * $post_data['page'];

        $products      = $this->Model_product->getAllProductsForBackend($fetch_by,$limit,$start_index);

        $html = '';

        $success = array();

        $success['show_load_more']   = false;

        if($products){ 
            foreach($products as $product){ 
                  $images = getProductImages($product['product_id']);
                   $image = base_url()."uploads/product_images/dummy-img.png";    
                   if($images)
                   {
                        if(file_exists($images[0]->product_image)){
                                            
                           $image = base_url().$images[0]->product_image;
                        }
                   }


                    

                 $html .= '<div class="singleProduct position-relative text-center search_product">
                                        <div class="dropdown">
                                            <a class="px-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v text-secondary"></i>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item" href="'.base_url('cms/product/edit/'.$product['product_id']).'">View</a>
                                                <a class="dropdown-item" href="javascript:void(0);" onclick="deleteRecord("'.$product['product_id'].'","cms/product/action","")">Delete</a>
                                            </div>
                                        </div>
                                        <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="'.base_url().$image.'" alt="product" height="83" width="83" ></div>
                                        <p class="m-0 text-danger bigger text-truncate" title="'.$product['category_title_en'].'">'.$product['category_title_en'].'</p>
                                        <p class="text-dark my-1">'.$product['product_title_en'].'</p>
                                        <p class="m-0 text-success bigger">'.$product['price'].' SAR</p>
                                    </div>';

             }  

             if(count($products) == $limit){
                $success['show_load_more']   = true;
            }

         }

        


         $success['html']   = $html;
         echo json_encode($success);
         exit;

        

    }
    
	public function add()
	{
		$fetch_by = array();
		$fetch_by['is_active']       = 1;
        $this->data['brands']    = $this->Model_brand->getMultipleRows($fetch_by);
        $fetch_by['parent_id']       = 0;
        $this->data['categories']    = $this->Model_category->getMultipleRows($fetch_by);
       
        
        $this->data['view'] = 'backend/product/add';
        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function edit($product_id)
	{
        $fetch_by = array();
        $fetch_by['product_id'] = $product_id;
        if($this->session->userdata['admin']['company_id'] > 0){
                $fetch_by['company_id']  = $this->session->userdata['admin']['company_id'];
        }
        
        $this->data['result']		        = $this->Model_product->getWithMultipleFields($fetch_by);
		$fetch_by = array();
        $fetch_by['is_active']       = 1;
        if($this->session->userdata['admin']['company_id'] > 0){
                $fetch_by['company_id']  = $this->session->userdata['admin']['company_id'];
        }
        $this->data['brands']    = $this->Model_brand->getMultipleRows($fetch_by);
        $fetch_by['parent_id']       = 0;

        $this->data['categories']    = $this->Model_category->getMultipleRows($fetch_by);
        $fetch_sub_by = array();
        $fetch_sub_by['parent_id'] = array();
        $fetch_sub_by['parent_id'] = $this->data['result']->category_id;
        $fetch_sub_by['is_active'] = 1;

        $sub_categories            = $this->Model_category->getMultipleRows($fetch_sub_by);
        $this->data['sub_categories'] = '<option value="">Select Sub Category</option>';
        if($sub_categories){
            foreach($sub_categories as $sub_category){
                $this->data['sub_categories'] .= '<option value="'.$sub_category->category_id.'" '.($sub_category->category_id == $this->data['result']->sub_category_id ? 'selected' : '' ).'>'.$sub_category->title_en.'</option>';
            }
        }
        
        $fetch_by = array();
		$fetch_by['product_id']             = $product_id;
        $this->data['images']               = $this->Model_product_image->getMultipleRows($fetch_by);
        $this->data['specifications']        = $this->Model_product_specification->getMultipleRows($fetch_by);
        
        if(!$this->data['result']){
           redirect(base_url('cms/product')); 
        }
        $this->data['view'] = 'backend/product/edit';
        
		
		
		
        
		
		$this->data['product_id'] 	 = $product_id;
		$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->validate();
                $this->update();
          break;
            case 'delete':
                //$this->validate();
                $this->delete();
          break; 
          case 'delete_specification':  
                $this->deleteSpecification();
          break;
                
          case 'delete_image':  
                $this->deleteImage();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('product_title_en', 'Eng Title', 'required');
        $this->form_validation->set_rules('product_title_ar', 'Arabic Title', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();
	
		
		
		$save_data['product_title_en']          = $post_data['product_title_en'];
		$save_data['product_title_ar']          = $post_data['product_title_ar'];
		$save_data['product_description_en']    = $post_data['product_description_en'];
		$save_data['product_description_ar']    = $post_data['product_description_ar'];
		$save_data['model_en']                  = $post_data['model_en'];
		$save_data['model_ar']                  = $post_data['model_ar'];
		$save_data['price']                     = $post_data['price'];
        $save_data['brand_id']                  = $post_data['brand_id'];
		$save_data['category_id']               = $post_data['category_id'];
		$save_data['sub_category_id']           = $post_data['sub_category_id'];
        
        $save_data['is_featured']               = (isset($post_data['is_featured']) ? 1 : 0);
        $save_data['is_active']                 = (isset($post_data['is_active']) ? 1 : 0);
		
		
		$save_data['created_at']          = date('Y-m-d H:i:s');		
		$save_data['updated_at']          = date('Y-m-d H:i:s');
        if($this->session->userdata['admin']['company_id'] > 0){
                $save_data['company_id'] = $this->session->userdata['admin']['company_id'];
        }
		
        
		$insert_id = $this->Model_product->save($save_data);
		if($insert_id > 0)
		{
			$this->uploadImage("image", "uploads/product_images/",$insert_id,true);
            /*$specifications_title_en = $this->input->post('specification_title_en');
            $specifications_title_ar = $this->input->post('specification_title_ar');
            $save_specification = array();
            foreach($specifications_title_en as $key => $value){
                if($specifications_title_en[$key] != '' || $specifications_title_ar[$key] != ''){
                   $save_specification[] = [
                    'product_id' => $insert_id,
                    'specification_title_en' => $specifications_title_en[$key] ,
                    'specification_title_ar' => $specifications_title_ar[$key]
                    ]; 
                }
                
            }
            if(!empty($save_specification)){
                $this->Model_product_specification->insert_batch($save_specification);   
            }
            */
            
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
			$success['url'] = 'cms/product';
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
    private function update()
	{
		$post_data = $this->input->post();
	
		
		
		$save_data['product_title_en']          = $post_data['product_title_en'];
		$save_data['product_title_ar']          = $post_data['product_title_ar'];
		$save_data['product_description_en']    = $post_data['product_description_en'];
		$save_data['product_description_ar']    = $post_data['product_description_ar'];
		$save_data['model_en']                  = $post_data['model_en'];
		$save_data['model_ar']                  = $post_data['model_ar'];
		$save_data['price']                     = $post_data['price'];
        $save_data['brand_id']                  = $post_data['brand_id'];
		$save_data['category_id']               = $post_data['category_id'];
		$save_data['sub_category_id']           = $post_data['sub_category_id'];
        
        $save_data['is_featured']               = (isset($post_data['is_featured']) ? 1 : 0);
        $save_data['is_active']                 = (isset($post_data['is_active']) ? 1 : 0);
		
		
		$save_data['created_at']          = date('Y-m-d H:i:s');		
		$save_data['updated_at']          = date('Y-m-d H:i:s');
        if($this->session->userdata['admin']['company_id'] > 0){
                $save_data['company_id'] = $this->session->userdata['admin']['company_id'];
        }
		
        $update_by = array();
        $update_by['product_id'] = $post_data['product_id'];
		$this->Model_product->update($save_data,$update_by);
		if(isset($_FILES['image']["name"][0]) && $_FILES['image']["name"][0] != ''){
            $this->uploadImage("image", "uploads/product_images/",$post_data['product_id'],true);
        }
        /*$deleted_by = array();
        $deleted_by['product_id'] = $post_data['product_id'];
        $this->Model_product_specification->delete($deleted_by);
        $specifications_title_en = $this->input->post('specification_title_en');
        $specifications_title_ar = $this->input->post('specification_title_ar');
        $save_specification = array();
        foreach($specifications_title_en as $key => $value){
            if($specifications_title_en[$key] != '' || $specifications_title_ar[$key] != ''){
               $save_specification[] = [
                'product_id' => $post_data['product_id'],
                'specification_title_en' => $specifications_title_en[$key] ,
                'specification_title_ar' => $specifications_title_ar[$key]
                ]; 
            }

        }
        if(!empty($save_specification)){
            $this->Model_product_specification->insert_batch($save_specification);   
        }*/


        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/product';
        echo json_encode($success);
        exit;
	}
    
    private function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
		  $data = array();
		 $extension=array("jpeg","jpg","png","gif");
		 foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $file_name;
                        }else{
                            $image_data     = array();
                            $image_data['product_id'] = $id;
                            $image_data['product_image'] = $path.$file_name;
                            $this->Model_product_image->save($image_data);
                            
                        }
                        /*$data['DestinationID'] = $id; 
					    $data['ImagePath'] = $path.$file_name; 
					    $this->Model_destination_image->save($data);*/
                    
                }
               
            }
			return true;


    }
    
    
    private function deleteSpecification(){
        $deleted_by = array();
        $deleted_by['product_specification_id'] = $this->input->post('id');
        $this->Model_product_specification->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
        
    }
    
    private function deleteImage(){
        $deleted_by = array();
        $deleted_by['product_image_id'] = $this->input->post('id');
        $image = $this->Model_product_image->get($this->input->post('id'),false,'product_image_id');
        if(file_exists($image->product_image)) {
            unlink($image->product_image);
        }
        $this->Model_product_image->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
        
    }
    
    private function delete(){
        
        $deleted_by = array();
        $deleted_by['product_id'] = $this->input->post('id');
        $images = $this->Model_product_image->getMultipleRows($deleted_by);
        if($images){
           foreach($images as $image){
                if(file_exists($image->product_image)) {
                    unlink($image->product_image);
                } 
            } 
        }
        
        
      
        
        $this->Model_product->delete($deleted_by);
        $this->Model_product_image->delete($deleted_by);
        $this->Model_product_specification->delete($deleted_by);
        
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

}
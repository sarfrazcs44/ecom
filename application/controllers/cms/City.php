<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        checkModuleRights($this->uri->segment(3),16);
        $this->load->model('Model_general');
		
	}
	 
    
    public function index()
	{
		
        $this->data['view'] = 'backend/city/manage';
        
        $this->data['cities'] = $this->Model_general->getAll('city');
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	public function add($parent_id = 0)
	{
		
        $this->data['view'] = 'backend/city/add';
        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function edit($city_id)
	{
        
        $this->data['result']		 = $this->Model_general->getRow($city_id,'city');
		
        if(!$this->data['result']){
           redirect(base_url('cms/city')); 
        }
        $this->data['view'] = 'backend/city/edit';
        
		
		
		
        
		
		$this->data['id'] 	 = $city_id;
		$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'update';
                $this->validate();
                $this->update();
          break;
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('eng_name', 'Eng Title', 'required');
        $this->form_validation->set_rules('arb_name', 'Arabic Title', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();
	
		
		
		
		unset($post_data['form_type']);
		
		$insert_id = $this->Model_general->save('city',$post_data);
		if($insert_id > 0)
		{
			
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
            $success['url'] = 'cms/city';  
            
			
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
    private function update()
	{
		$post_data = $this->input->post();
	
        unset($post_data['form_type']);
        $this->Model_general->updateRow('city',$post_data,$post_data['id']);
		
        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/city';  
        echo json_encode($success);
        exit;
	}
    
   
    
    
    public function getAllSubCategories()
    {
        $fetch_by = array();
        $fetch_by['is_active'] = 1;
        $fetch_by['parent_id'] = $this->input->post('category_id');
        $sub_categories = $this->Model_category->getMultipleRows($fetch_by);
        $option = '<option value="">Select Sub Category</option>';
        if($sub_categories){
            foreach($sub_categories as $category){
                $option .= '<option value="'.$category->category_id.'">'.$category->title_en.'</option>';
            }
        }
        
         $success['html'] = $option;
         echo json_encode($success);
         exit;
    }
    
    
    private function delete(){
        
        $this->Model_general->deleteRow('city',$this->input->post('id'));
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

}
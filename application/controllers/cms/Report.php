<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        checkModuleRights($this->uri->segment(3),33);
        if ($this->session->userdata['admin']['role_id'] == 2) {
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_order');
        $this->load->model('Model_general');
    }


    public function index()
    {

        $this->data['view'] = 'backend/report/manage';

        $where = 'orders.status = "Delivered"';

        if($this->input->post()){

            $post_data = $this->input->post();

            if(isset($post_data['from']) && $post_data['from'] != ''){
                $where .= ' AND DATE(orders.created_at) >= "'.date('Y-m-d',strtotime($post_data['from'])).'"';
            }
            if(isset($post_data['to']) && $post_data['to'] != ''){
                $where .= ' AND DATE(orders.created_at) <= "'.date('Y-m-d',strtotime($post_data['to'])).'"';
            }

            $this->data['post_data'] = $post_data;

        }

        $this->data['orders'] = $this->Model_order->getOrders($where);
        //echo $this->db->last_query();exit;
        $this->load->view('backend/layouts/default', $this->data);
    }

    


}
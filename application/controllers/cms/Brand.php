<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        checkModuleRights($this->uri->segment(3),12);
        if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_brand');
       // $this->load->model('Model_category_text');
		
	}
	 
    
    public function index()
	{
		
        $this->data['view'] = 'backend/brand/manage';
        if($this->session->userdata['admin']['company_id'] > 0){
            $fetch_by = array();
            $fetch_by['company_id']  = $this->session->userdata['admin']['company_id'];
            $this->data['brands'] = $this->Model_brand->getMultipleRows($fetch_by);
        }else{
            $this->data['brands'] = $this->Model_brand->getAll();
        }
        
        
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	public function add()
	{
		
        $this->data['view'] = 'backend/brand/add';
        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function edit($brand_id)
	{
        $fetch_by = array();
        $fetch_by['brand_id'] = $brand_id;
        if($this->session->userdata['admin']['company_id'] > 0){
                $fetch_by['company_id']  = $this->session->userdata['admin']['company_id'];
        }
        
        $this->data['result']		 = $this->Model_brand->getWithMultipleFields($fetch_by);
		
        if(!$this->data['result']){
           redirect(base_url('cms/brand')); 
        }
        $this->data['view'] = 'backend/brand/edit';
        
		
		
		
        
		
		$this->data['brand_id'] 	 = $brand_id;
		$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'update';
                $this->validate();
                $this->update();
          break;
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('title_en', 'Eng Title', 'required');
        $this->form_validation->set_rules('title_ar', 'Arabic Title', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();
	
		
		if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }
		
		unset($post_data['form_type']);
		$post_data['created_at']    = date('Y-m-d H:i:s');		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
        if($this->session->userdata['admin']['company_id'] > 0){
                $post_data['company_id'] = $this->session->userdata['admin']['company_id'];
        }
		
        $post_data['image']         = $this->uploadImage("image", "uploads/brand_images/");
		$insert_id = $this->Model_brand->save($post_data);
		if($insert_id > 0)
		{
			
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
			$success['url'] = 'cms/brand';
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
    private function update()
	{
		$post_data = $this->input->post();
	
		
		if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		
       unset($post_data['form_type']);
        if(isset($_FILES['image']["name"][0]) && $_FILES['image']["name"][0] != ''){
            $post_data['image']         = $this->uploadImage("image", "uploads/brand_images/");
        }
        $update_by = array();
		$update_by['brand_id'] = $post_data['brand_id'];
        unset($post_data['form_type']);
        $this->Model_brand->update($post_data,$update_by);
		
        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/brand';
        echo json_encode($success);
        exit;
	}
    
    private function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
		  $data = array();
		 $extension=array("jpeg","jpg","png","gif");
		 foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $path.$file_name;
                        }
                        /*$data['DestinationID'] = $id; 
					    $data['ImagePath'] = $path.$file_name; 
					    $this->Model_destination_image->save($data);*/
                    
                }
               
            }
			return true;


    }
    
    
    
    private function delete(){
        
        $get_data = $this->Model_brand->get($this->input->post('id'),false,'brand_id');
        if(file_exists($get_data->image)) {
              unlink($get_data->image);
        }
      
        $deleted_by = array();
        $deleted_by['brand_id'] = $this->input->post('id');
        $this->Model_brand->delete($deleted_by);
        
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

}
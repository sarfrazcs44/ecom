<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support_user extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Model_user');
        $this->load->model('Model_support_user');
        $this->load->model('Model_complaint_type');
        checkModuleRights($this->uri->segment(3),19);
    }

    public function index()
    {
        $this->data['view'] = 'backend/support_user/manage';
        $company_id = false;
        if($this->session->userdata['admin']['company_id'] > 0){
            $company_id = $this->session->userdata['admin']['company_id'];
        }
        $this->data['support_users'] = $this->Model_support_user->getAllSupportUsers($company_id);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        $this->data['complain_types_new'] = $this->Model_complaint_type->getMultipleRows(array('parent_id !=' => 0), true);
        $this->data['view'] = 'backend/support_user/add';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function save()
    {
        $posted_data = $this->input->post();
        $complaint_type_ids = $posted_data['complaint_type_id'];
        unset($posted_data['complaint_type_id']);

        /*if (isset($posted_data['is_verified'])) {
            $posted_data['is_verified'] = 1;
        } else {
            $posted_data['is_verified'] = 0;
        }*/
        $posted_data['is_verified'] = 1;

        $username_count = $this->Model_user->checkIfUsernameAvailable($posted_data['username']);
        $email_count = $this->Model_user->checkIfEmailAvailable($posted_data['email']);
        if ($username_count > 0 && $email_count > 0) {
            $response['error'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['error'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['error'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            $posted_data['password'] = md5($posted_data['password']);
            $posted_data['role_id'] = 2;
            $posted_data['created_at'] = date('Y-m-d H:i:s');
            if($this->session->userdata['admin']['company_id'] > 0){
                $posted_data['company_id'] = $this->session->userdata['admin']['company_id'];
            }
            $user_id = $this->Model_user->save($posted_data);
            foreach ($complaint_type_ids as $complaint_type_id) {
                $support_user_data['complaint_type_id'] = $complaint_type_id;
                $support_user_data['user_id'] = $user_id;
                $this->Model_support_user->save($support_user_data);
            }
            $response['error'] = 'false';
            $response['success'] = 'Support user created successfully.';
            $response['redirect'] = true;
            $response['url'] = 'cms/support_user';
            echo json_encode($response);
            exit();
        }

    }

    public function edit($user_id)
    {
        $user_complaint_types = array();
        $this->data['complaint_types'] = $this->Model_complaint_type->getMultipleRows(array('parent_id !=' => 0), true);
        $this->data['view'] = 'backend/support_user/edit';
        $fetch_by = array();
        $fetch_by['user_id'] = $user_id;
        if($this->session->userdata['admin']['company_id'] > 0){
                $fetch_by['company_id'] = $this->session->userdata['admin']['company_id'];
        }
        $this->data['user_data'] = $this->Model_user->getWithMultipleFields($fetch_by);
        $support_user_types = $this->Model_support_user->getMultipleRows(array('user_id' => $user_id));
        foreach ($support_user_types as $support_user_type) {
            $user_complaint_types[] = $support_user_type->complaint_type_id;
        }
        $this->data['user_complaint_types'] = $user_complaint_types;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function update()
    {
        $posted_data = $this->input->post();
        $user_id = $this->input->post('user_id');
        $user_data = $this->Model_user->get($user_id, false, 'user_id');
        $complaint_type_ids = $posted_data['complaint_type_id'];
        unset($posted_data['complaint_type_id']);

        /*if (isset($posted_data['is_verified'])) {
            $posted_data['is_verified'] = 1;
        } else {
            $posted_data['is_verified'] = 0;
        }*/

        if ($user_data->email != $posted_data['email']) {
            $email_count = $this->Model_user->checkIfEmailAvailable($posted_data['email']);
        }

        if ($user_data->username != $posted_data['username']) {
            $username_count = $this->Model_user->checkIfUsernameAvailable($posted_data['username']);
        }

        if ($username_count > 0 && $email_count > 0) {
            $response['error'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['error'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['error'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            if (isset($posted_data['password']) && $posted_data['password'] != '') {
                $posted_data['password'] = md5($posted_data['password']);
            }
            // dump($posted_data);
            $this->Model_support_user->delete(array('user_id' => $user_id));
            $this->Model_user->update($posted_data, array('user_id' => $user_id));
            foreach ($complaint_type_ids as $complaint_type_id) {
                $support_user_data['complaint_type_id'] = $complaint_type_id;
                $support_user_data['user_id'] = $user_id;
                $this->Model_support_user->save($support_user_data);
            }
            $response['error'] = 'false';
            $response['success'] = 'Support user updated successfully.';
            $response['redirect'] = true;
            $response['url'] = 'cms/support_user';
            echo json_encode($response);
            exit();
        }

    }

    public function delete()
    {
        $user_id = $this->input->post('id');
        $this->Model_support_user->delete(array('user_id' => $user_id));
        $this->Model_user->delete(array('user_id' => $user_id));
        $response['error'] = 'false';
        $response['success'] = 'Support user deleted successfully.';
        $response['redirect'] = true;
        $response['url'] = 'cms/support_user';
        echo json_encode($response);
        exit();
    }
}
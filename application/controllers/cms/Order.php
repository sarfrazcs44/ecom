<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller
{
    public $data = array();


    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        if ($this->session->userdata['admin']['role_id'] == 2) {
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_order');
        $this->load->model('Model_general');
        $this->load->model('Model_user');
        
    }


    public function index()
    {

        $this->received_orders();
        /*$this->data['view'] = 'backend/order/manage';
        if($this->session->userdata['admin']['company_id'] > 0){
            $fetch_by = array();
            $fetch_by['company_id']  = $this->session->userdata['admin']['company_id'];
            $this->data['orders'] = $this->Model_order->getMultipleRows($fetch_by);
        }else{
            $this->data['orders'] = $this->Model_order->getAll();
        }

       
        $this->load->view('backend/layouts/default', $this->data);*/
    }

    public function received_orders() // for warehouse, admin
    {

         checkModuleRights('view',2);



        $orders = array();
        $order_status = 'Received';
        $company_id = false;

        if($this->session->userdata['admin']['company_id'] > 0){
            $company_id = $this->session->userdata['admin']['company_id'];
        }


        if ($this->session->userdata['admin']['role_id'] == 1) { // for admin
            $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUserReceivedNotAssigned($order_status,array(),'',false,$company_id);
        } elseif ($this->session->userdata['admin']['role_id'] == 4) { // for warehouse
            $user_id = $this->session->userdata['admin']['user_id'];
            $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
            foreach ($user_districts as $district) {
                $districts[] = $district->district_id;
            }
            $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUserReceivedNotAssigned($order_status, $districts,'',false,$company_id);
        }

        
        $this->data['view'] = 'backend/order/received_orders';
        $this->data['orders'] = $orders;
        $this->load->view('backend/layouts/default', $this->data);
    }



    public function not_picked() // for warehouse, delivery, admin
    {

        checkModuleRights('view',3);
        $orders = array();
        $order_status = 'Received';
        $company_id = false;

        if($this->session->userdata['admin']['company_id'] > 0){
            $company_id = $this->session->userdata['admin']['company_id'];
        }
        if ($this->session->userdata['admin']['role_id'] == 1) { // for admin
            $orders = $this->Model_order->getOrdersNotPicked($order_status,array(),'',false,$company_id);//$this->Model_order->getMultipleRows(array('status' => $order_status));
        } elseif ($this->session->userdata['admin']['role_id'] == 4 || $this->session->userdata['admin']['role_id'] == 5) { // for warehouse and delivery user
            $user_id = $this->session->userdata['admin']['user_id'];
            if($this->session->userdata['admin']['role_id'] == 5){
                $user_id = $this->session->userdata['admin']['branch_id'];
            }
            $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
            foreach ($user_districts as $district) {
                $districts[] = $district->district_id;
            }
            if ($this->session->userdata['admin']['role_id'] == 4) {
                $orders = $this->Model_order->getOrdersNotPicked($order_status, $districts,'',false,$company_id);
            } else {
                // Show all orders assigned to him.
                $orders = $this->Model_order->getOrdersNotPicked($order_status, $districts, $user_id,false,$company_id); // 3rd parameter to get orders assigned to this delivery user only
            }
        }

        $this->data['view'] = 'backend/order/notpicked_orders';
        $this->data['orders'] = $orders;
        $this->load->view('backend/layouts/default', $this->data);
    }



    public function cancelled_by_customer() // for warehouse, delivery, admin
    {
         checkModuleRights('view',6);
        $orders = array();
        $order_status = 'Cancelled By Customer';
        $fetch_by = array();
        $company_id = false;
        if($this->session->userdata['admin']['company_id'] > 0){
            $fetch_by['company_id'] = $company_id = $this->session->userdata['admin']['company_id'];
        }
        if ($this->session->userdata['admin']['role_id'] == 1) { // for admin

            $fetch_by['status'] = $order_status;

            $orders = $this->Model_order->getMultipleRows($fetch_by);
        } elseif ($this->session->userdata['admin']['role_id'] == 4) { // for warehouse
            $user_id = $this->session->userdata['admin']['user_id'];
            $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
            foreach ($user_districts as $district) {
                $districts[] = $district->district_id;
            }

            $where = false;

            if($company_id){
                $where = 'orders.company_id = '.$company_id;
            }
            $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUser($order_status, $districts,'',false,$where);
        }

        
        $this->data['view'] = 'backend/order/cancelled_by_customer';
        $this->data['orders'] = $orders;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function cancelled_by_admin() // for warehouse, delivery, admin
    {
         checkModuleRights('view',7);
        $orders = array();
        $order_status = 'Cancelled By Admin';
        $company_id = false;
        $fetch_by  = array();
        if($this->session->userdata['admin']['company_id'] > 0){
            $fetch_by['company_id'] = $company_id = $this->session->userdata['admin']['company_id'];
        }


        if ($this->session->userdata['admin']['role_id'] == 1) { // for admin
            $fetch_by['status'] = $order_status;

            $orders = $this->Model_order->getMultipleRows($fetch_by);
        } elseif ($this->session->userdata['admin']['role_id'] == 4) { // for warehouse
            $user_id = $this->session->userdata['admin']['user_id'];
            $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
            foreach ($user_districts as $district) {
                $districts[] = $district->district_id;
            }

            $where = false;

            if($company_id){
                $where = 'orders.company_id = '.$company_id;
            }

            $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUser($order_status, $districts,'',false,$where);
        }

        
        $this->data['view'] = 'backend/order/cancelled_by_admin';
        $this->data['orders'] = $orders;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function dispatched_orders() // for warehouse, delivery, admin
    {
         checkModuleRights('view',4);
        $orders = array();
        $order_status = 'Dispatched';

        $company_id = false;
        $fetch_by  = array();
        if($this->session->userdata['admin']['company_id'] > 0){
            $fetch_by['company_id'] = $company_id = $this->session->userdata['admin']['company_id'];
        }




        if ($this->session->userdata['admin']['role_id'] == 1) { // for admin

             $fetch_by['status'] = $order_status;


            $orders = $this->Model_order->getMultipleRows($fetch_by);
        } elseif ($this->session->userdata['admin']['role_id'] == 4 || $this->session->userdata['admin']['role_id'] == 5) { // for warehouse and delivery user
            $user_id = $this->session->userdata['admin']['user_id'];
            if($this->session->userdata['admin']['role_id'] == 5){
                $user_id = $this->session->userdata['admin']['branch_id'];
            }
            $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
            foreach ($user_districts as $district) {
                $districts[] = $district->district_id;
            }

            $where = false;

            if($company_id){
                $where = 'orders.company_id = '.$company_id;
            }


            if ($this->session->userdata['admin']['role_id'] == 4) {
                $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUser($order_status, $districts,'',false,$where);
            } else {
                // Show all orders assigned to him.
                $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUser($order_status, $districts, $user_id,false,$where); // 3rd parameter to get orders assigned to this delivery user only
            }
        }
        $this->data['view'] = 'backend/order/dispatched_orders';
        $this->data['orders'] = $orders;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function delivered_orders() // for warehouse, delivery, admin
    {
         checkModuleRights('view',5);
        $orders = array();
        $order_status = 'Delivered';

        $company_id = false;
        $fetch_by  = array();
        if($this->session->userdata['admin']['company_id'] > 0){
            $fetch_by['company_id'] = $company_id = $this->session->userdata['admin']['company_id'];
        }







        if ($this->session->userdata['admin']['role_id'] == 1) { // for admin

            $fetch_by['status'] = $order_status;
            $orders = $this->Model_order->getMultipleRows($fetch_by);


        } elseif ($this->session->userdata['admin']['role_id'] == 4 || $this->session->userdata['admin']['role_id'] == 5) { // for warehouse and delivery user
            $user_id = $this->session->userdata['admin']['user_id'];
            if($this->session->userdata['admin']['role_id'] == 5){
                $user_id = $this->session->userdata['admin']['branch_id'];
            }
            $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
            foreach ($user_districts as $district) {
                $districts[] = $district->district_id;
            }


            $where = false;

            if($company_id){
                $where = 'orders.company_id = '.$company_id;
            }



            if ($this->session->userdata['admin']['role_id'] == 4) {
                $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUser($order_status, $districts,'',false,$where);
            } else {
                // Show all orders assigned to him.
                $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUser($order_status, $districts, $user_id,false,$where); // 3rd parameter to get orders assigned to this delivery user only
            }
        }
        $this->data['view'] = 'backend/order/delivered_orders';
        $this->data['orders'] = $orders;
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function pos_orders() // for warehouse, delivery, admin
    {
         checkModuleRights('view',8);
        $orders = array();

        $where = false;
        
        if($this->session->userdata['admin']['company_id'] > 0){
            $where = 'orders.company_id = '.$this->session->userdata['admin']['company_id'];
        }
        
        $orders = $this->Model_order->getOrdersOfPos($where);
        $this->data['view'] = 'backend/order/pos_orders';
        $this->data['orders'] = $orders;
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function view($order_id)
    {
         checkModuleRights('view',1);
        $company_id = false;
        
        if($this->session->userdata['admin']['company_id'] > 0){
            $company_id = $this->session->userdata['admin']['company_id'];
        }

        $this->data['order_items'] = $this->Model_order->getOrderDetail($order_id,$company_id);

        if (!$this->data['order_items']) {
            redirect(base_url('cms/order'));
        }
        $this->data['view'] = 'backend/order/invoice';


        $this->data['order_id'] = $order_id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function change_status()
    {
         checkModuleRights('edit',1);
        $post_data = $this->input->post();


        $post_data['updated_at'] = date('Y-m-d H:i:s');


        $update_by = array();
        $update_by['order_id'] = $post_data['order_id'];

        $this->Model_order->update($post_data, $update_by);


        if($post_data['status'] == 'Dispatched'){

             $order_details = $this->Model_general->getSingleRow('orders', array('order_id' => $post_data['order_id']));

            $title = "Order Picked";
            $message = "Dear Customer, your order has been picked by the captain \n عزيزي العميل، تم استلام طلبك من قبل السائق";
            
            sendNotification($title, $message, $post_data, $order_details->user_id);
        }

        $success['error'] = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/brand';
        echo json_encode($success);
        exit;
    }

     public function cancel_order()
    {
         checkModuleRights('edit',1);
        $post_data = $this->input->post();


        $post_data['updated_at'] = date('Y-m-d H:i:s');


        $update_by = array();
        $update_by['order_id'] = $post_data['order_id'];

        $this->Model_order->update($post_data, $update_by);


        

        $success['error'] = 'false';
        $success['success'] = 'Updated Successfully';
        $success['reload'] = true;
        echo json_encode($success);
        exit;
    }

    public function assign_order()
    {
         checkModuleRights('edit',1);
        $post_data = $this->input->post();
        $post_data['assigned_at'] = date('Y-m-d H:i:s');
        // save this data in assigned orders table
        $assgined = $this->Model_general->getSingleRow('assigned_orders_for_delivery', array('order_id' => $post_data['order_id'])); // check if already assigned
        $otp = RandomString();
                 $this->Model_order->update(array('otp' => $otp),array('order_id' => $post_data['order_id']));
        if (!$assgined) {
            $inserted = $this->Model_general->save('assigned_orders_for_delivery', $post_data);
            if ($inserted > 0) {
                // if assigned, send sms and email to deliver man order assigned to.
                $order_details = $this->Model_general->getSingleRow('orders', array('order_id' => $post_data['order_id']));
                $user_details = $this->Model_general->getSingleRow('users', array('user_id' => $post_data['user_id']));
                if ($user_details->phone != '') {
                    $this->SendSms($user_details->phone, $order_details->order_track_id);
                }
                if ($user_details->email != '') {
                   // $this->SendEmail($user_details->email, $order_details->order_track_id);
                }


                $customer_details = $this->Model_user->get($order_details->user_id,false,'user_id');
                
                if ($customer_details->phone != ''){
                        
                        $sms_data = array();
                        $sms_data['to'] = $customer_details->phone;
                        $sms_data['sms'] = 'Your order is assign to captain with delivery OTP :  ' . $otp;
                        sendSMS($sms_data);

                        $email = array();

                        $email['body'] = 'Your order is assign to captain with delivery OTP :  ' . $otp;
                        $email['to'] = $customer_details->email;;
                        $email['from'] = 'noreply@niehez.com.sa';
                        $email['subject'] = 'Order Assigned At niehez';
                        sendEmail($email);

                       
                }


                $title = "Order Assigned";
                $message = "Dear Customer, your order has been assigned to captain \n عزيزي العميل، تم ارسال طلبك للسائق";
                
                sendNotification($title, $message, $post_data, $customer_details->user_id);



            }
        }

        // update status in orders table to Dispatched
       // $this->Model_order->update(array('status' => 'Dispatched'), array('order_id' => $post_data['order_id']));

        $success['error'] = 'false';
        $success['success'] = 'Order assigned  successfully.';
        $success['redirect'] = true;
        $success['url'] = 'cms/order/not_picked';
        echo json_encode($success);
        exit;
    }

    public function mark_as_delivered()
    {
         checkModuleRights('edit',1);
        $order_id = $this->input->post('order_id');
        $otp = $this->input->post('otp');
        $order_details = $this->Model_general->getSingleRow('orders', array('order_id' => $order_id));

        if($order_details->otp != $otp){
            $success['error'] = 'Otp does not match.Order will not mark as delivered';
            $success['success'] = 'false';
            echo json_encode($success);
            exit;

        }


        $this->Model_order->update(array('status' => 'Delivered','otp' => ''), array('order_id' => $order_id));
        //$this->Model_general->deleteMultipleRow('assigned_orders_for_delivery', array('order_id' => $order_id));

        $order_details = $this->Model_general->getSingleRow('orders', array('order_id' => $post_data['order_id']));

        $title = "Order Delivered";
        $message = "Dear Customer, your order has been delivered successfully, please shop again with us \n عزيزي العميل، تم توصيل طلبك بنجاح، نحن في انتظارك مره اخرى";
        
        sendNotification($title, $message, $post_data, $order_details->user_id);
        $success['error'] = 'false';
        $success['success'] = 'Order status changed to delivered successfully.';
        $success['redirect'] = true;
        $success['url'] = 'cms/order/delivered_orders';
        echo json_encode($success);
        exit;
    }

    private function SendSms($phone, $order_no)
    {
        $sms_data = array();
        $sms_data['to'] = $phone;
        $sms_data['sms'] = 'An order is assigned to you with order no. ' . $order_no;
        sendSMS($sms_data);
    }

    private function SendEmail($email, $order_no)
    {
        $email['body'] = 'An order is assigned to you with order no. ' . $order_no;
        $email['to'] = $email;
        $email['from'] = 'noreply@shopbox.com.sa';
        $email['subject'] = 'Order Assigned At Shopbox';
        sendEmail($email);
    }




}
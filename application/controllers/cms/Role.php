<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends Base_Controller
{
    public $data = array();

    public function __construct()
    {

        parent::__construct();
        checkAdminSession();
        checkModuleRights($this->uri->segment(3),68);

        $this->load->Model([
            'Role_model',
            'Module_rights_model',
            'Module_menu'


        ]);




    }


    public function index()
    {


       
        $this->data['view'] = 'backend/role/manage';
        $this->data['results'] = $this->Role_model->getAll();
        $this->load->view('backend/layouts/default',$this->data);
    }

    


    public function rights($role_id = '')
    {

       
        if ($role_id == ''){
            $role_id = $this->session->userdata['admin']['role_id'];
        }
        
        $this->data['RoleID'] = $role_id;
        $this->data['view'] = 'backend/role/rights';
        $where = false;
        if($this->session->userdata['admin']['company_id'] > 0){
            $where = 'modules_rights.company_id = '.$this->session->userdata['admin']['company_id'];
        }
        $this->data['results'] = $this->Module_rights_model->getModulesWithRights($role_id,$where);



        if (empty($this->data['results'])) {

            $this->session->set_flashdata('message', lang('please_add_module_first'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $this->data['roles'] = $this->Role_model->getAll();

        if (empty($this->data['roles'])) {

            $this->session->set_flashdata('message', lang('please_add_role_first'));
            redirect(base_url('cms/role'));
        }


        $this->load->view('backend/layouts/default', $this->data);


    }

    public function edit($id = '')
    {

        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data['RoleID'] = $id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                //$this->validate();
                $this->update();
                break;
            case 'delete':
                //$this->validate();
                $this->delete();
                break;
            case 'save_rights':
                $this->saveRights();
                break;

        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Title', lang('title'), 'required|is_unique[' . $this->data['Table'] . '_text.Title]');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    


    private function saveRights()
    {


        
        $role_id = $this->input->post('role_id');
        $where = false;
        if($this->session->userdata['admin']['company_id'] > 0){
            $where = 'modules_rights.company_id = '.$this->session->userdata['admin']['company_id'];
        }

        $modules = $this->Module_rights_model->getModulesWithRights($role_id, $where);

        $can_view = $this->input->post('can_view');
        $can_add = $this->input->post('can_add');
        $can_edit = $this->input->post('can_edit');
        $can_delete = $this->input->post('can_delete');

        if (!empty($modules)) {

            foreach ($modules as $module) {

                $update_data[] = [
                    'module_right_id' => $module->module_right_id,
                    'can_view' => (isset($can_view[$module->module_right_id]) ? 1 : 0),
                    'can_add' => (isset($can_add[$module->module_right_id]) ? 1 : 0),
                    'can_edit' => (isset($can_edit[$module->module_right_id]) ? 1 : 0),
                    'can_delete' => (isset($can_delete[$module->module_right_id]) ? 1 : 0),
                    'updated_at' => date('Y-m-d H:i:s'),
                    'updated_by' => $this->session->userdata['admin']['user_id']

                ];
            }

            $this->Module_rights_model->update_batch($update_data, 'module_right_id');
            $success['error'] = 'false';
            $success['success'] = 'Updated Successfully';
            $success['reload'] = true;

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }


    }


   


}
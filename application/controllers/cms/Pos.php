<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pos extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        checkModuleRights($this->uri->segment(3),32);
       
        $this->load->model('Model_order');
        $this->load->model('Model_pos');
        $this->load->model('Model_product');
        $this->load->model('Model_category');
        $this->load->model('Model_temp_order');
        $this->load->model('Site_setting_model');
        $this->load->model('Model_user');
        $this->load->model('Model_order_item');
    }


    public function index()
    {

        $this->data['view'] = 'backend/pos/manage';

        $where = '1 = 1';



        if($this->input->post()){

            $post_data = $this->input->post();

            if(isset($post_data['from']) && $post_data['from'] != ''){
                $where .= ' AND DATE(created_at) >= "'.date('Y-m-d',strtotime($post_data['from'])).'"';
            }
            if(isset($post_data['to']) && $post_data['to'] != ''){
                $where .= ' AND DATE(created_at) <= "'.date('Y-m-d',strtotime($post_data['to'])).'"';
            }

            $this->data['post_data'] = $post_data;

        }else{

            $where .= ' AND Month(created_at) = "'.date('m').'" AND Year(created_at) = "'.date('Y').'"';

        }




        $this->data['results'] = $this->Model_pos->getData($where);
        //echo $this->db->last_query();exit;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        
        $this->data['view'] = 'backend/pos/add';
        $this->load->view('backend/layouts/default',$this->data);
    }
    
     public function cart()
    {
         

        $fetch_by = array();
        $fetch_by['is_active']       = 1;
        $where = false;

        if($this->session->userdata['admin']['company_id'] > 0){
            
            $fetch_by['company_id']  = $this->session->userdata['admin']['company_id'];

            $where = 'products.company_id = '.$this->session->userdata['admin']['company_id'];
            //$this->data['products'] = $this->Model_product->getMultipleRows($fetch_by);
        }else{

            //$this->data['products'] = $this->Model_product->getAll();
        }


        $this->data['products'] = $this->Model_product->getAllProductsForBackend($fetch_by,40,0);

        $this->data['total_products']  = $this->Model_product->getCountProducts($where);








       
        $this->data['categories']    = $this->Model_category->getMultipleRows($fetch_by);
        $this->data['cart_products']     = $this->Model_temp_order->getCartData($this->session->userdata['admin']['user_id']);

        $this->data['vat_setting'] = $this->Site_setting_model->get(1);

        
        $this->data['view'] = 'backend/pos/cart';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function fetch_products(){
        $post_data = $this->input->post();

        $fetch_by = array();
       
        if(isset($post_data['category_id']) && $post_data['category_id'] != ''){
            $fetch_by['category_id']       = $post_data['category_id'];
        }
        if(isset($post_data['sub_category_id']) && $post_data['sub_category_id'] != ''){
            $fetch_by['sub_category_id']       = $post_data['sub_category_id'];
        }

        $limit = 40;

        $start_index = $limit * $post_data['page'];

        $products      = $this->Model_product->getAllProductsForBackend($fetch_by,$limit,$start_index);


        $html = '';

        $success = array();

        $success['show_load_more']   = false;

        if($products){ 
            foreach($products as $product){ 
                  $images = getProductImages($product['product_id']);
                   $image = base_url()."uploads/product_images/dummy-img.png";    
                   if($images)
                   {
                        if(file_exists($images[0]->product_image)){
                                            
                           $image = base_url().$images[0]->product_image;
                        }
                   }


                    

                 $html .= '<div class="singleProduct position-relative text-center search_product">
                                        <div class="dropdown">
                                            <a class="px-2" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v text-secondary"></i>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <a class="dropdown-item" href="'.base_url('cms/product/edit/'.$product['product_id']).'">View</a>
                                                <a class="dropdown-item" href="javascript:void(0);" onclick="deleteRecord("'.$product['product_id'].'","cms/product/action","")">Delete</a>
                                            </div>
                                        </div>
                                        <div class="imgBox d-flex w-auto mx-auto mb-1 justify-content-center"><img class="rounded" src="'.$image.'" alt="product" height="83" width="83" ></div>
                                        <p class="m-0 text-danger bigger text-truncate" title="'.$product['category_title_en'].'">'.$product['category_title_en'].'</p>
                                        <p class="text-dark my-1">'.$product['product_title_en'].'</p>
                                        <p class="m-0 text-success bigger">'.$product['price'].' SAR</p>
                                        <div class="text-center"><input type="button" class="add_to_cart btn btn-primary" data-product-id="'.$product['product_id'].'" value="Add to cart"></div>
                                    </div>';

             }  

             if(count($products) == $limit){
                $success['show_load_more']   = true;
            }

         }

        


         $success['html']   = $html;
         echo json_encode($success);
         exit;

        

    }


    public function add_to_cart(){
        
        $data = array();
        $vat_setting = $this->Site_setting_model->get(1);

        $post_data = $this->input->post();
        $post_data['product_quantity'] = 1;


        


        $fetch_by = array();
        $fetch_by['user_id'] = $post_data['user_id'] = $this->session->userdata['admin']['user_id'];
        $fetch_by['product_id'] = $this->input->post('product_id');

        $already_added = $this->Model_temp_order->getWithMultipleFields($fetch_by);
        if ($already_added) {
            $update = array();
            $update_by = array();

            $update['product_quantity'] = $already_added->product_quantity + $post_data['product_quantity'];
            $update_by['temp_order_id'] = $already_added->temp_order_id;

            $this->Model_temp_order->update($update, $update_by);


            $insert_id = $already_added->temp_order_id;

        } else {
            $insert_id = $this->Model_temp_order->save($post_data);
        }


        


        if ($insert_id > 0) {


            $cart_products     = $this->Model_temp_order->getCartData($this->session->userdata['admin']['user_id']);

            $html = '';
            $total = 0;
            $pop_up_html = '';


            



            $html .='<div class="haveScroll slimscrollleft" style="max-height:520px;">
                  <ul class="m-0 p-0">';
                              
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['price'] * $value['product_quantity'];
                                          $images = getProductImages($value['product_id']);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($images)
                                           {
                                                if(file_exists($images[0]->product_image)){
                                                                    
                                                   $image = base_url().$images[0]->product_image;
                                                }
                                           }
                                           
                    $html .= '<li class="d-flex py-2">
                        <div class="imgBox mr-3"><img class="rounded" src="'.$image.'" alt="product" height="45" width="45" ></div>
                        <div class="text">
                           <p class="m-0 smallFontSize lineHeight1-3">
                              <span class="text-danger d-block w-100 strong bold">'.$value['model_en'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">'.$value['product_title_en'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">Price per unit '.$value['price'].' SAR</span>
                           </p>
                        </div>
                        <div class="dropdown  dropleft">
                           <a class="px-2" href="#" role="button" id="deleteCart_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-ellipsis-v text-secondary"></i>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="deleteCart_1" style="width:auto;">
                              <a class="dropdown-item w-100 delete_cart" href="javascript:void(0);" data-temp-order-id = "'.$value['temp_order_id'].'">Delete</a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="d-flex justify-content-between align-items-center mt-2 mb-4 px-1 pb-2 plusMinusPrice">
                           <div class="form-group mb-0 mr-3">
                              <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected position-relative">



                                 <span class="input-group-btn input-group-prepend">
                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto left-0 largeFontSize bootstrap-touchspin-down btn-minus" data-temp-order-id="'.$value['temp_order_id'].'" data-product-id="'.$value['product_id'].'" type="button"><i class="typcn typcn-minus"></i></button>
                                 </span>


                                 <input id="demo0" type="text"  name="quantity" value="'.$value['product_quantity'].'"  data-bts-min="0" data-bts-max="100" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-primary" data-bts-button-up-class="btn btn-primary" class="form-control text-center strong bold largeFontSize text-dark bg-light px-4 quantity'.$value['product_id'].'">
                                 <span class="input-group-btn input-group-append">



                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto right-0 largeFontSize bootstrap-touchspin-up btn-plus" data-temp-order-id="'.$value['temp_order_id'].'" data-product-id="'.$value['product_id'].'" type="button"><i class="typcn typcn-plus"></i></button>
                                 </span>
                              </div>
                           </div>
                           <div class="shadow bg-white rounded-pill text-center text-success">
                              <p class="m-0 ">
                                    <span class="d-block largeFontSize w-100 strong bold">'.($value['price'] * $value['product_quantity']).'</span>
                                    <span class="d-block smallerFontSize  w-100 ">SAR</span>
                              </p>
                           </div>
                        </div>
                        
                     </li>';
                     } 

                      $html .= '</ul>
               </div><div class="row">
                  <div class="tbody card-box shadow w-100 m-0 px-3">
                     <div class="px-3">
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$total.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center border-bottom border-success">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Tax (VAT 15%)</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$vat_setting->vat.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>Grand Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>'.($vat_setting->vat + $total).' SAR</strong></p></div>
                        </div>
                     </div>
                     <button type="button" class="btn btn-primary waves-effect w-md waves-light mb-0 mt-2 w-100 get_popup" >Check Out</button>
                  </div>
               </div>';



               $pop_up_html .= '<div class="epmtyClass">
                              <div class="">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Cashier </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.$this->session->userdata['admin']['full_name'].'</b></span>
                                 </p>
                              </div>
                              
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Items </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.count($cart_products).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Purchased on</b></span>
                                    <span class="text-dark largeFontSize  d-block w-100 "><b>'.(date('d, M Y')).' </b></span>
                                    <span class="text-dark  d-block w-100"><b>'.(date('H:m:i')).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-4">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Bill</b></span>
                                 <table class="w-100">
                                    <tr>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-dark  d-block w-100"><b>Total</b></span>
                                                <span class="text-dark  d-block w-100"><b>Tax (VAT 15%)</b></span>
                                                <span class="text-dark  d-block w-100"><b>Grand Total</b></span>
                                             </p>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-success  d-block w-100"><b>'.$total.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.$vat_setting->vat.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.($vat_setting->vat + $total).' SAR</b></span>
                                             </p>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 </p>
                              </div>
                           </div>';



                 }


                    




               



               


            



            $success['html']   = $html;
            $success['pop_up_html']   = $pop_up_html;
            echo json_encode($success);
            exit;


            


        } else {


        $errors['error'] = 'There is something went wrong';
        $errors['success'] = 'false';
        echo json_encode($errors);
        exit;


        }

        
    }



    public function get_popup_detail(){

        $data = array();
        $vat_setting = $this->Site_setting_model->get(1);
        $post_data = $this->input->post();

        

       


        


            $cart_products     = $this->Model_temp_order->getCartData($this->session->userdata['admin']['user_id']);

            
            $total = 0;
            $pop_up_html = '';


            



            
                              
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['price'] * $value['product_quantity'];
                                          $images = getProductImages($value['product_id']);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($images)
                                           {
                                                if(file_exists($images[0]->product_image)){
                                                                    
                                                   $image = base_url().$images[0]->product_image;
                                                }
                                           }
                                           
                    
                                }


               $pop_up_html .= '<div class="wrap">
                              <div class="">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Cashier </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.$this->session->userdata['admin']['full_name'].'</b></span>
                                 </p>
                              </div>
                              
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Items </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.count($cart_products).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Purchased on</b></span>
                                    <span class="text-dark largeFontSize  d-block w-100 "><b>'.(date('d, M Y')).' </b></span>
                                    <span class="text-dark  d-block w-100"><b>'.(date('H:m:i')).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-4">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Bill</b></span>
                                 <table class="w-100">
                                    <tr>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-dark  d-block w-100"><b>Total</b></span>
                                                <span class="text-dark  d-block w-100"><b>Tax (VAT 15%)</b></span>
                                                <span class="text-dark  d-block w-100"><b>Grand Total</b></span>
                                             </p>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-success  d-block w-100"><b>'.$total.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.$vat_setting->vat.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.($vat_setting->vat + $total).' SAR</b></span>
                                             </p>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 </p>
                              </div>
                           </div>';



                 

             }


                    




               



               


            



           
            $success['pop_up_html']   = $pop_up_html;
            echo json_encode($success);
            exit;


            


        

    }


    public function update_cart(){

        $data = array();
        $vat_setting = $this->Site_setting_model->get(1);
        $post_data = $this->input->post();

        $update = array();
        $update_by = array();
        $fetch_by = array();

        //$update['product_quantity'] = $post_data['product_quantity'];
        $update_by['temp_order_id'] = $post_data['temp_order_id'];
        $fetch_by['temp_order_id'] = $post_data['temp_order_id'];

        
        $update['product_quantity'] = $post_data['product_quantity'];

        $this->Model_temp_order->update($update, $update_by);

       


        if ($post_data['temp_order_id'] > 0) {


            $cart_products     = $this->Model_temp_order->getCartData($this->session->userdata['admin']['user_id']);

            $html = '';
            $total = 0;
            $pop_up_html = '';


            



            $html .='<div class="haveScroll slimscrollleft" style="max-height:520px;">
                  <ul class="m-0 p-0">';
                              
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['price'] * $value['product_quantity'];
                                          $images = getProductImages($value['product_id']);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($images)
                                           {
                                                if(file_exists($images[0]->product_image)){
                                                                    
                                                   $image = base_url().$images[0]->product_image;
                                                }
                                           }
                                           
                    $html .= '<li class="d-flex py-2">
                        <div class="imgBox mr-3"><img class="rounded" src="'.$image.'" alt="product" height="45" width="45" ></div>
                        <div class="text">
                           <p class="m-0 smallFontSize lineHeight1-3">
                              <span class="text-danger d-block w-100 strong bold">'.$value['model_en'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">'.$value['product_title_en'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">Price per unit '.$value['price'].' SAR</span>
                           </p>
                        </div>
                        <div class="dropdown  dropleft">
                           <a class="px-2" href="#" role="button" id="deleteCart_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-ellipsis-v text-secondary"></i>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="deleteCart_1" style="width:auto;">
                              <a class="dropdown-item w-100 delete_cart" href="javascript:void(0);" data-temp-order-id = "'.$value['temp_order_id'].'">Delete</a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="d-flex justify-content-between align-items-center mt-2 mb-4 px-1 pb-2 plusMinusPrice">
                           <div class="form-group mb-0 mr-3">
                              <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected position-relative">



                                 <span class="input-group-btn input-group-prepend">
                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto left-0 largeFontSize bootstrap-touchspin-down btn-minus" data-temp-order-id="'.$value['temp_order_id'].'" data-product-id="'.$value['product_id'].'" type="button"><i class="typcn typcn-minus"></i></button>
                                 </span>


                                 <input id="demo0" type="text"  name="quantity" value="'.$value['product_quantity'].'"  data-bts-min="0" data-bts-max="100" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-primary" data-bts-button-up-class="btn btn-primary" class="form-control text-center strong bold largeFontSize text-dark bg-light px-4 quantity'.$value['product_id'].'">
                                 <span class="input-group-btn input-group-append">



                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto right-0 largeFontSize bootstrap-touchspin-up btn-plus" data-temp-order-id="'.$value['temp_order_id'].'" data-product-id="'.$value['product_id'].'" type="button"><i class="typcn typcn-plus"></i></button>
                                 </span>
                              </div>
                           </div>
                           <div class="shadow bg-white rounded-pill text-center text-success">
                              <p class="m-0 ">
                                    <span class="d-block largeFontSize w-100 strong bold">'.($value['price'] * $value['product_quantity']).'</span>
                                    <span class="d-block smallerFontSize  w-100 ">SAR</span>
                              </p>
                           </div>
                        </div>
                        
                     </li>';
                     } 

                      $html .= '</ul>
               </div><div class="row">
                  <div class="tbody card-box shadow w-100 m-0 px-3">
                     <div class="px-3">
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$total.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center border-bottom border-success">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Tax (VAT 15%)</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$vat_setting->vat.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>Grand Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>'.($vat_setting->vat + $total).' SAR</strong></p></div>
                        </div>
                     </div>
                     <button type="button" class="btn btn-primary waves-effect w-md waves-light mb-0 mt-2 w-100 get_popup">Check Out</button>
                  </div>
               </div>';



               $pop_up_html .= '<div class="w-100">
                              <div class="">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Cashier </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.$this->session->userdata['admin']['full_name'].'</b></span>
                                 </p>
                              </div>
                              
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Items </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.count($cart_products).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Purchased on</b></span>
                                    <span class="text-dark largeFontSize  d-block w-100 "><b>'.(date('d, M Y')).' </b></span>
                                    <span class="text-dark  d-block w-100"><b>'.(date('H:m:i')).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-4">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Bill</b></span>
                                 <table class="w-100">
                                    <tr>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-dark  d-block w-100"><b>Total</b></span>
                                                <span class="text-dark  d-block w-100"><b>Tax (VAT 15%)</b></span>
                                                <span class="text-dark  d-block w-100"><b>Grand Total</b></span>
                                             </p>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-success  d-block w-100"><b>'.$total.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.$vat_setting->vat.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.($vat_setting->vat + $total).' SAR</b></span>
                                             </p>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 </p>
                              </div>
                           </div>';



                 }


                    




               



               


            



            $success['html']   = $html;
            $success['pop_up_html']   = $pop_up_html;
            echo json_encode($success);
            exit;


            


        } else {


        $errors['error'] = 'There is something went wrong';
        $errors['success'] = 'false';
        echo json_encode($errors);
        exit;


        }

    }

    public function delete_cart_item(){
        $vat_setting = $this->Site_setting_model->get(1);
        $post_data = $this->input->post();
        

        $deleted_by = array();
        $deleted_by['temp_order_id'] = $post_data['temp_order_id'];
        $this->Model_temp_order->delete($deleted_by);


        $cart_products     = $this->Model_temp_order->getCartData($this->session->userdata['admin']['user_id']);

            $html = '';
            $total = 0;
            $pop_up_html = '';


            



            $html .='<div class="haveScroll slimscrollleft" style="max-height:520px;">
                  <ul class="m-0 p-0">';
                              
                              if($cart_products){
                                       foreach ($cart_products as $key => $value) { 
                                          $total += $value['price'] * $value['product_quantity'];
                                          $images = getProductImages($value['product_id']);
                                           $image = base_url()."uploads/product_images/dummy-img.png";    
                                           if($images)
                                           {
                                                if(file_exists($images[0]->product_image)){
                                                                    
                                                   $image = base_url().$images[0]->product_image;
                                                }
                                           }
                                           
                    $html .= '<li class="d-flex py-2">
                        <div class="imgBox mr-3"><img class="rounded" src="'.$image.'" alt="product" height="45" width="45" ></div>
                        <div class="text">
                           <p class="m-0 smallFontSize lineHeight1-3">
                              <span class="text-danger d-block w-100 strong bold">'.$value['model_en'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">'.$value['product_title_en'].'</span>
                              <span class="text-dark d-block w-100 smallerFontSize font-weight-bold">Price per unit '.$value['price'].' SAR</span>
                           </p>
                        </div>
                        <div class="dropdown  dropleft">
                           <a class="px-2" href="#" role="button" id="deleteCart_1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="fa fa-ellipsis-v text-secondary"></i>
                           </a>
                           <div class="dropdown-menu" aria-labelledby="deleteCart_1" style="width:auto;">
                              <a class="dropdown-item w-100 delete_cart" href="javascript:void(0);" data-temp-order-id = "'.$value['temp_order_id'].'">Delete</a>
                           </div>
                        </div>
                     </li>
                     <li>
                        <div class="d-flex justify-content-between align-items-center mt-2 mb-4 px-1 pb-2 plusMinusPrice">
                           <div class="form-group mb-0 mr-3">
                              <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected position-relative">



                                 <span class="input-group-btn input-group-prepend">
                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto left-0 largeFontSize bootstrap-touchspin-down btn-minus" data-temp-order-id="'.$value['temp_order_id'].'" data-product-id="'.$value['product_id'].'" type="button"><i class="typcn typcn-minus"></i></button>
                                 </span>


                                 <input id="demo0" type="text"  name="quantity" value="'.$value['product_quantity'].'"  data-bts-min="0" data-bts-max="100" data-bts-init-val="" data-bts-step="1" data-bts-decimal="0" data-bts-step-interval="100" data-bts-force-step-divisibility="round" data-bts-step-interval-delay="500" data-bts-prefix="" data-bts-postfix="" data-bts-prefix-extra-class="" data-bts-postfix-extra-class="" data-bts-booster="true" data-bts-boostat="10" data-bts-max-boosted-step="false" data-bts-mousewheel="true" data-bts-button-down-class="btn btn-primary" data-bts-button-up-class="btn btn-primary" class="form-control text-center strong bold largeFontSize text-dark bg-light px-4 quantity'.$value['product_id'].'">
                                 <span class="input-group-btn input-group-append">



                                    <button class="btn text-danger shadow-none strong bold position-absolute top-0 bottom-0 border-0 my-auto right-0 largeFontSize bootstrap-touchspin-up btn-plus" data-temp-order-id="'.$value['temp_order_id'].'" data-product-id="'.$value['product_id'].'" type="button"><i class="typcn typcn-plus"></i></button>
                                 </span>
                              </div>
                           </div>
                           <div class="shadow bg-white rounded-pill text-center text-success">
                              <p class="m-0 ">
                                    <span class="d-block largeFontSize w-100 strong bold">'.($value['price'] * $value['product_quantity']).'</span>
                                    <span class="d-block smallerFontSize  w-100 ">SAR</span>
                              </p>
                           </div>
                        </div>
                        
                     </li>';
                     } 

                      $html .= '</ul>
               </div><div class="row">
                  <div class="tbody card-box shadow w-100 m-0 px-3">
                     <div class="px-3">
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$total.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center border-bottom border-success">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>Tax (VAT 15%)</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-dark"><strong>'.$vat_setting->vat.' SAR</strong></p></div>
                        </div>
                        <div class="row text-center">
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>Grand Total</strong></p></div>
                           <div class="col-md-6"><p class="mb-0 px-2 py-1 smallFontSize lineHeight1-3 text-danger"><strong>'.($vat_setting->vat + $total).' SAR</strong></p></div>
                        </div>
                     </div>
                     <button type="button" class="btn btn-primary waves-effect w-md waves-light mb-0 mt-2 w-100 get_popup" >Check Out</button>
                  </div>
               </div>';



               $pop_up_html .= '<div class="w-100">
                              <div class="">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Cashier </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.$this->session->userdata['admin']['full_name'].'</b></span>
                                 </p>
                              </div>
                              
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Items </b></span>
                                    <span class="text-dark largeFontSize  d-block w-100"><b>'.count($cart_products).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-3">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Purchased on</b></span>
                                    <span class="text-dark largeFontSize  d-block w-100 "><b>'.(date('d, M Y')).' </b></span>
                                    <span class="text-dark  d-block w-100"><b>'.(date('H:m:i')).'</b></span>
                                 </p>
                              </div>
                              <div class=" mt-4">
                                 <p class="m-0 smallFontSize lineHeight1-3">
                                    <span class="text-danger d-block w-100"><b>Bill</b></span>
                                 <table class="w-100">
                                    <tr>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-dark  d-block w-100"><b>Total</b></span>
                                                <span class="text-dark  d-block w-100"><b>Tax (VAT 15%)</b></span>
                                                <span class="text-dark  d-block w-100"><b>Grand Total</b></span>
                                             </p>
                                          </div>
                                       </td>
                                       <td>
                                          <div class="text">
                                             <p class="m-0 largeFontSize  lineHeight1-3">
                                                <span class="text-success  d-block w-100"><b>'.$total.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.$vat_setting->vat.' SAR</b></span>
                                                <span class="text-success  d-block w-100"><b>'.($vat_setting->vat + $total).' SAR</b></span>
                                             </p>
                                          </div>
                                       </td>
                                    </tr>
                                 </table>
                                 </p>
                              </div>
                           </div>';



                 }


                    




               



               


            



            $success['html']   = $html;
            $success['pop_up_html']   = $pop_up_html;
            echo json_encode($success);
            exit;





    }


    public function place_order(){
        $data = array();

        $post_data = $this->input->post();
        $vat_setting = $this->Site_setting_model->get(1);


        


           $get_user_data = $this->Model_user->getJoinedData(false,'user_id','address','users.phone = "'.$post_data['mobile'].'"');
           if($get_user_data){


                $post_data['user_id'] = $get_user_data[0]->user_id;
                $address_id = $get_user_data[0]->address_id;
                $user_name = $get_user_data[0]->full_name;
                if($address_id == ''){
                    $address_id = -1;
                }
           }else{
                $post_data['user_id'] = -1;
                $address_id = -1;
                $user_name = 'Guest';

           }


            

            $cart_products = $this->Model_temp_order->getJoinedData(false, 'product_id', 'products', 'temp_orders.user_id = ' . $this->session->userdata['admin']['user_id'] . '');

            if ($cart_products) {
               

                

                 

                
                $order_data = array();

                $order_data['user_id'] = $post_data['user_id'];
                $order_data['address_id'] = $address_id;
                $order_data['created_at'] = $order_data['updated_at'] = date('Y-m-d H:i:s');
                $order_data['order_track_id'] = RandomString();
                $order_data['is_pos_order'] = 1;
                $order_data['status'] = 'Delivered';
                $order_data['additional_note'] = $post_data['additional_note'];

                $order_data['vat_total'] = $vat_setting->vat;
                $order_data['delivery_charges'] = 0;
                $order_data['total_items'] = count($cart_products);
                $order_data['total_amount'] = 0;
                $order_data['transaction_id'] = '';
                
                $insert_id = $this->Model_order->save($order_data);
                if ($insert_id > 0) {
                    
                    $post_data['total_amount'] = 0;
                    $return_array = array();
                    $return_array = $this->Model_order->get($insert_id, true, 'order_id');
                    $order_item_data = array();
                    foreach ($cart_products as $product) {
                    $post_data['total_amount'] = $post_data['total_amount'] + ($product->price * $product->product_quantity);

                        $order_item_data[] = ['order_id' => $insert_id,
                            'product_id' => $product->product_id,
                            'quantity' => $product->product_quantity,
                            'price' => $product->price
                        ];

                    }

                    $post_data['total_amount'] =  $post_data['total_amount'] + $vat_setting->vat;

                    $this->Model_order_item->insert_batch($order_item_data);


                    $update_order = array();
                    $update_order_by = array();
                    $update_order_by['order_id'] = $insert_id;
                    $update_order['order_track_id'] = $order_data['order_track_id'] . $insert_id;

                    $update_order['total_amount'] = $post_data['total_amount'];
                    $this->Model_order->update($update_order, $update_order_by);
                    




                    
                    $deleted_by = array();
                    $deleted_by['user_id'] = $this->session->userdata['admin']['user_id'];
                    $this->Model_temp_order->delete($deleted_by);

                   

                    // pos entry
                    //$user_data = $this->Model_user->get($post_data['user_id'],false,'user_id');
                    $pos_data = array();
                    $pos_data['transaction_user'] = $user_name;
                    $pos_data['type'] = 'Credit';
                    $pos_data['description'] = 'Pos Purchase';
                    $pos_data['total_amount'] = $post_data['total_amount'];
                    $pos_data['created_at'] = date('Y-m-d H:i');
                    $pos_data['order_id'] = $insert_id;
                    
                    $this->Model_pos->save($pos_data);


                    $success['error']   = 'false';
                    $success['success'] = 'Order Place Successfully';
                    $success['reload'] = true;
                    
                    
                    echo json_encode($success);
                    exit;

                    

                } else {
                    $errors['error'] = 'There is something went wrong';
                    $errors['success'] = 'false';
                    echo json_encode($errors);
                    exit;

                   
                }


            } else {

                $errors['error'] = 'There is something went wrong';
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            }

        
    }

    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('transaction_user', 'Done By', 'required');
        $this->form_validation->set_rules('total_amount', 'Total Amount', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        $post_data = $this->input->post();
    
        
        
        unset($post_data['form_type']);
        $post_data['created_at']    = date('Y-m-d H:i:s');      
        
        if (isset($_FILES['image']["name"][0]) && $_FILES['image']["name"][0] != '') {
            $post_data['invoice']         = $this->uploadImage("image", "uploads/pos/");
        }
        
        $insert_id = $this->Model_pos->save($post_data);
        if($insert_id > 0)
        {
            
            $success['error']   = 'false';
            $success['success'] = 'Save Successfully';
            $success['redirect'] = true;
            $success['url'] = 'cms/pos'; 
            
            
            echo json_encode($success);
            exit;
            
            
        }else
        {
            $errors['error'] = 'There is something went wrong';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }


    private function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
          $data = array();
         $extension=array("jpeg","jpg","png","gif");
         foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $path.$file_name;
                        }
                        /*$data['DestinationID'] = $id; 
                        $data['ImagePath'] = $path.$file_name; 
                        $this->Model_destination_image->save($data);*/
                    
                }
               
            }
            return true;


    }

     private function delete(){
        
        $get_data = $this->Model_pos->get($this->input->post('id'),false,'point_of_sale_id');
        if(file_exists($get_data->invoice)) {
              unlink($get_data->invoice);
        }
      
        $deleted_by = array();
        $deleted_by['point_of_sale_id'] = $this->input->post('id');
        $this->Model_pos->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

    


}
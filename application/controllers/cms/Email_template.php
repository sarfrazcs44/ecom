<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_template extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        checkModuleRights($this->uri->segment(3),29);
        if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_email_template');
		
	}
	 
    
    public function index()
	{
		
        $this->data['view'] = 'backend/email_template/manage';
        
        $this->data['results'] = $this->Model_email_template->getAll();
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	public function add()
	{
		
        $this->data['view'] = 'backend/email_template/add';
       
        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function edit($email_template_id)
	{
        
        $this->data['result']		 = $this->Model_email_template->get($email_template_id,false,'email_template_id');
		
        if(!$this->data['result']){
           redirect(base_url('cms/email_template')); 
        }
        $this->data['view'] = 'backend/email_template/edit';
        
		
		
		
        
		
		$this->data['email_template_id'] 	 = $email_template_id;
		$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'update';
                $this->validate();
                $this->update();
          break;
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('title_en', 'Eng Title', 'required');
        $this->form_validation->set_rules('title_ar', 'Arabic Title', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();
	
		if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }
		
		
		unset($post_data['form_type']);
		$post_data['created_at']    = date('Y-m-d H:i:s');		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		
       
		$insert_id = $this->Model_email_template->save($post_data);
		if($insert_id > 0)
		{
			
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
            $success['url'] = 'cms/email_template'; 
			
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
    private function update()
	{
		$post_data = $this->input->post();
	
		
		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }
       unset($post_data['form_type']);
        
        $update_by = array();
		$update_by['email_template_id'] = $post_data['email_template_id'];
        unset($post_data['form_type']);
        $this->Model_email_template->update($post_data,$update_by);
		
        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/email_template'; 
        
        echo json_encode($success);
        exit;
	}
    
    
    private function delete(){
        
       
        
      
        $deleted_by = array();
        $deleted_by['email_template_id'] = $this->input->post('id');
        $this->Model_email_template->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

}
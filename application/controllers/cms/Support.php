<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Model_user');
        $this->load->model('Model_complaint_type');
        $this->load->model('Model_support_tickets');
        $this->load->model('Model_ticket_comments');

    }

    public function index()
    {
        $this->data['view'] = 'backend/support/manage';
        $tickets = $this->Model_support_tickets->getAll(true, 'DESC', 'ticket_id');
        $admin = $this->session->userdata('admin');

        foreach ($tickets as $key => $ticket) {
            if ($admin['role_id'] == 1 || ($admin['role_id'] == 2 && ($ticket['ticket_taken_by'] == 0 || $ticket['ticket_taken_by'] == $admin['user_id']))) {
                $tickets[$key]['unread'] = $this->check_unread_comments($ticket['ticket_id'], $admin['user_id']);
            }
        }

        // echo "<pre>"; print_r($tickets); exit;

        $this->data['tickets'] = $tickets;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function open_tickets()
    {
        $this->data['view'] = 'backend/support/manage';
        $tickets = $this->Model_support_tickets->getOpenTickets(); // 0,1
        $admin = $this->session->userdata('admin');

        foreach ($tickets as $key => $ticket) {
            if ($admin['role_id'] == 1 || ($admin['role_id'] == 2 && ($ticket['ticket_taken_by'] == 0 || $ticket['ticket_taken_by'] == $admin['user_id']))) {
                $tickets[$key]['unread'] = $this->check_unread_comments($ticket['ticket_id'], $admin['user_id']);
            }
        }

        // echo "<pre>"; print_r($tickets); exit;

        $this->data['tickets'] = $tickets;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function closed_tickets()
    {
        $this->data['view'] = 'backend/support/manage';
        $tickets = $this->Model_support_tickets->getClosedTickets();
        $admin = $this->session->userdata('admin');

        foreach ($tickets as $key => $ticket) {
            if ($admin['role_id'] == 1 || ($admin['role_id'] == 2 && ($ticket['ticket_taken_by'] == 0 || $ticket['ticket_taken_by'] == $admin['user_id']))) {
                $tickets[$key]['unread'] = $this->check_unread_comments($ticket['ticket_id'], $admin['user_id']);
            }
        }

        // echo "<pre>"; print_r($tickets); exit;

        $this->data['tickets'] = $tickets;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function reopened_tickets()
    {
        $this->data['view'] = 'backend/support/manage';
        $tickets = $this->Model_support_tickets->getReOpenedTickets();
        $admin = $this->session->userdata('admin');

        foreach ($tickets as $key => $ticket) {
            if ($admin['role_id'] == 1 || ($admin['role_id'] == 2 && ($ticket['ticket_taken_by'] == 0 || $ticket['ticket_taken_by'] == $admin['user_id']))) {
                $tickets[$key]['unread'] = $this->check_unread_comments($ticket['ticket_id'], $admin['user_id']);
            }
        }

        // echo "<pre>"; print_r($tickets); exit;

        $this->data['tickets'] = $tickets;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function view($ticket_id)
    {
        if (!$ticket_id) redirect($this->config->item('base_url') . 'cms/support');
        $this->data['ticket_id'] = $ticket_id;
        $admin = $this->session->userdata('admin');
        $ticket = $this->Model_support_tickets->get($ticket_id, true, 'ticket_id');

        // if support type user is logged in and he is trying to view the ticket and ticket is already taken be someone else
        if ($admin['role_id'] == 2 && $ticket['ticket_taken_by'] > 0 && $ticket['ticket_taken_by'] !== $admin['user_id']) {
            redirect($this->config->item('base_url') . 'cms/support');
        }

        $comments = $this->Model_ticket_comments->getMultipleRows(['ticket_id' => $ticket_id]);

        // update is_read status if i am not the user who posted the comment
        if ($comments) {
            foreach ($comments as $item) {
                // update is_read status if i am not the user who posted the comment
                if ($item->user_id != $admin['user_id']) {
                    $update_data['is_read'] = 1;
                    $update_by['comment_id'] = $item->comment_id;
                    $this->Model_ticket_comments->update($update_data, $update_by);
                }
            }
        }

        // get visit/complain complain type
        $complain = "";
        $this->data['comments'] = $comments;
        $fetch_by['ticket_id'] = $ticket_id;
        $chat = $this->Model_ticket_comments->getMultipleRows($fetch_by, false);

        //echo "<pre>"; print_r($complain); exit;


        $this->data['ticket'] = (object)$ticket;
        $this->data['complain'] = $complain;
        $this->data['chat'] = $chat;
        $this->data['admin'] = $admin;
        $this->data['view'] = 'backend/support/view';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function store_message()
    {
        $post_data = $this->input->post();
        if (!empty($post_data['message']) && !empty($post_data['ticket_id'])) {
            $admin = $this->session->userdata('admin');

            $ticket = $this->Model_support_tickets->get($post_data['ticket_id'], true, 'ticket_id');
            // if support type user is logged in and he is trying to view the ticket and ticket is already taken be someone else
            if ($admin['role_id'] == 2 && $ticket['ticket_taken_by'] > 0 && $ticket['ticket_taken_by'] !== $admin['user_id']) {
                redirect($this->config->item('base_url') . 'cms/support');
            }

            $store_data['message'] = trim($post_data['message']);
            $store_data['user_id'] = $admin['user_id'];
            $store_data['timestamp'] = strtotime(date('Y-m-d H:i:s'));
            $store_data['ticket_id'] = $post_data['ticket_id'];

            $insert_id = $this->Model_ticket_comments->save($store_data);
            if ($insert_id > 0) {
                $getUserData = $this->Model_support_tickets->getUserData($post_data['ticket_id']);

                $get_comment = $this->Model_ticket_comments->get($insert_id, true, 'comment_id');
                $get_ticket_user = $this->Model_user->get($get_comment['user_id'], true, 'user_id');

                // if commnet is done by support type user and ticket is not already taken
                if ($get_ticket_user['role_id'] == 2 && $getUserData['ticket_taken_by'] == 0) {
                    $this->Model_support_tickets->update(array('ticket_taken_by' => $get_comment['user_id']), array('ticket_id' => $post_data['ticket_id']));
                }

                if (!empty($getUserData)) {

                    pusher($getUserData, 'ticket_' . $post_data['ticket_id'], 'ticket_' . $post_data['ticket_id'], false);

                    if ($getUserData['device_type'] != '' && $getUserData['device_token'] != '') {
                        $push_data = array();

                        $push_data['device_type'] = $getUserData['device_type'];
                        $push_data['device_token'] = $getUserData['device_token'];
                        $push_data['title'] = 'Admin Reply : Ticket ' . $post_data['ticket_id'];
                        $push_data['id'] = $insert_id;
                        $push_data['type'] = 'post';
                        $push_data['message'] = trim($post_data['message']);
                        sendPushNotification($push_data, $post_data['ticket_id']);
                    }

                    /*$title = 'Admin Reply : Ticket '.$post_data['ticket_id'];
                    $message = trim($post_data['message']);
                    $data['ticket_id'] = $getUserData['id'];
                    $data['type'] = 'ticket_reopened';
                    sendNotification($title, $message, $data, $getUserData['user_id']);*/

                    // send email to that user as a notification 
                    $body = '';
                    $body .= '<p>Hi, ' . $getUserData['full_name'] . '</p>';
                    $body .= '<p>MSJ admin reply to you against your ticket no ' . $post_data['ticket_id'] . ' .</p>';
                    $body .= '<p>' . trim($post_data['message']) . '</p>';

                    $message = emailTemplate($body);


                    $email_data = array();
                    $email_data['to'] = $getUserData['email'];
                    $email_data['subject'] = 'Admin Reply : Ticket ' . $post_data['ticket_id'];
                    $email_data['from'] = 'no_reply@msj.com';
                    $email_data['body'] = $message;


                    sendEmail($email_data);


                    // end Email  send

                    // send SMS 
                    $sms_data = array();
                    $sms_data['to'] = $getUserData['phone'];
                    $sms_data['sms'] = 'Admin Reply : Ticket ' . $post_data['ticket_id'] . '! ' . trim($post_data['message']) . '';
                    sendSMS($sms_data);
                    //end sms functionality


                }

                $msg = $this->Model_ticket_comments->get($insert_id, true, 'comment_id');

                // pusher call here
                pusher($msg, 'ticket_' . $post_data['ticket_id'], 'ticket_' . $post_data['ticket_id'], false);

                $formatted_message = $this->formatted_message($msg, $admin['user_id']);
                $response['status'] = 'TRUE';
                $response['message'] = $formatted_message;
            } else {
                $response['status'] = 'FALSE';
            }
            echo json_encode($response);
            exit;
        }

    }


    public function check_new_message()
    {
        $post_data = $this->input->post();
        if (!empty($post_data['lastCommentId']) && !empty($post_data['ticket_id'])) {
            $admin = $this->session->userdata('admin');
            $ticket_id = $post_data['ticket_id'];
            $last_comment_id = $post_data['lastCommentId'];
            $sql = "select * from ticket_comments where comment_id = (select min(comment_id) from ticket_comments where comment_id > $last_comment_id) AND ticket_id='$ticket_id' ";
            $query = $this->db->query($sql);
            $new_messages = $query->result_array();
            if ($new_messages) {
                $new_message = $new_messages[0];
                $formatted_message = $this->formatted_message($new_message, $admin['user_id']);
                $response['status'] = 'TRUE';
                $response['message'] = $formatted_message;
            } else {
                $response['status'] = 'FALSE';
            }
            echo json_encode($response);
            exit;
        }
    }

    public function close_status()
    {
        $post_data = $this->input->post();
        $admin = $this->session->userdata('admin');
        $update_data['status'] = 'closed';
        $update_data['closed_request'] = 1;// admin want to close this ticket
        $update_data['last_closed_by'] = $admin['user_id']; // id of the user who is closing this ticket
        $update_by['ticket_id'] = $post_data['ticket_id'];
        $this->Model_support_tickets->update($update_data, $update_by);
        $where = 'support_tickets.ticket_id = ' . $post_data['ticket_id'] . ' ';
        $getUserData = $this->Model_support_tickets->getJoinedData(true, 'user_id', 'users', $where);

        $getUserData = $getUserData[0];

        if ($getUserData['device_type'] != '' && $getUserData['device_token'] != '') {
            $push_data = array();

            $push_data['device_type'] = $getUserData['device_type'];
            $push_data['device_token'] = $getUserData['device_token'];
            $push_data['title'] = 'Ticket Closed : Ticket ' . $post_data['ticket_id'];
            $push_data['id'] = $getUserData['id'];
            $push_data['type'] = 'post';
            $push_data['message'] = 'MSJ admin closed your ticket no ' . $post_data['ticket_id'];;
            sendPushNotification($push_data, $post_data['ticket_id'], 1);
        }


        // send email to that user as a notification
        $body = '';
        $body .= '<p>Hi, ' . $getUserData['full_name'] . '</p>';
        $body .= '<p>MSJ admin closed your ticket no ' . $post_data['ticket_id'] . ' .</p>';
        //  $body .= '<p>'.trim($post_data['message']).'</p>';

        $message = emailTemplate($body);


        $email_data = array();
        $email_data['to'] = $getUserData['email'];
        $email_data['subject'] = 'Ticket Closed : Ticket ' . $post_data['ticket_id'];
        $email_data['from'] = 'no_reply@msj.com';
        $email_data['body'] = $message;


        sendEmail($email_data);


        // end Email  send

        // send SMS 
        $sms_data = array();
        $sms_data['to'] = $getUserData['phone'];
        $sms_data['sms'] = 'MSJ admin closed your ticket no ' . $post_data['ticket_id'];
        sendSMS($sms_data);
        //end sms functionality


        $response['status'] = 'TRUE';
        echo json_encode($response);
        exit;
    }


    private function check_unread_comments($ticket_id, $user_id)
    {
        if ($ticket_id) {
            $sql = "SELECT count(comment_id) AS unread FROM ticket_comments WHERE ticket_id = $ticket_id AND is_read = 0 AND user_id <> $user_id ";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['unread'];
        }
    }


    private function formatted_message($message, $user_id)
    {
        $message['created_at'] = date("h:i a", strtotime($message['created_at']));
        $message['who_said'] = ($message['user_id'] == $user_id) ? "Support: " : "Client: ";
        $message['even_odd'] = ($message['user_id'] == $user_id) ? "even" : "odd";
        return $message;
    }


    public function delete()
    {


        $deleted_by = array();
        $deleted_by['ticket_id'] = $this->input->post('id');
        $this->Model_support_tickets->delete($deleted_by);
        $this->Model_ticket_comments->delete($deleted_by);

        $success['error'] = 'false';
        $success['success'] = 'Deleted Successfully';

        echo json_encode($success);
        exit;
    }

    public function share_chat($ticket_id)
    {
        if (!$ticket_id) redirect($this->config->item('base_url') . 'cms/support');
        $this->data['ticket_id'] = $ticket_id;
        $admin = $this->session->userdata('admin');
        $ticket = $this->Model_support_tickets->get($ticket_id, false, 'ticket_id');

        $comments = $this->Model_ticket_comments->getMultipleRows(['ticket_id' => $ticket_id]);

        // update is_read status if i am not the user who posted the comment
        if ($comments) {
            foreach ($comments as $item) {
                // update is_read status if i am not the user who posted the comment
                if ($item->user_id != $admin['user_id']) {
                    $update_data['is_read'] = 1;
                    $update_by['comment_id'] = $item->comment_id;
                    $this->Model_ticket_comments->update($update_data, $update_by);
                }
            }
        }

        // get visit/complain complain type
        $complain = "";
        $this->data['comments'] = $comments;
        $fetch_by['ticket_id'] = $ticket_id;
        $chat = $this->Model_ticket_comments->getMultipleRows($fetch_by, false);

        //echo "<pre>"; print_r($complain); exit;


        $this->data['ticket'] = $ticket;
        $this->data['complain'] = $complain;
        $this->data['chat'] = $chat;
        $this->data['admin'] = $admin;
        $this->load->view('backend/emails/template_for_view', $this->data);
    }

    public function email_chat()
    {
        $ticket_id = $this->input->post('ticket_id');
        $posted_email = $this->input->post('email');
        if (!$ticket_id) redirect($this->config->item('base_url') . 'cms/support');
        $this->data['ticket_id'] = $ticket_id;
        $admin = $this->session->userdata('admin');
        $ticket = $this->Model_support_tickets->get($ticket_id, false, 'ticket_id');

        $comments = $this->Model_ticket_comments->getMultipleRows(['ticket_id' => $ticket_id]);

        // update is_read status if i am not the user who posted the comment
        if ($comments) {
            foreach ($comments as $item) {
                // update is_read status if i am not the user who posted the comment
                if ($item->user_id != $admin['user_id']) {
                    $update_data['is_read'] = 1;
                    $update_by['comment_id'] = $item->comment_id;
                    $this->Model_ticket_comments->update($update_data, $update_by);
                }
            }
        }

        // get visit/complain complain type
        $complain = "";
        $this->data['comments'] = $comments;
        $fetch_by['ticket_id'] = $ticket_id;
        $chat = $this->Model_ticket_comments->getMultipleRows($fetch_by, false);

        $this->data['ticket'] = $ticket;
        $this->data['complain'] = $complain;
        $this->data['chat'] = $chat;
        $this->data['admin'] = $admin;
        $this->data['view'] = 'backend/support/view';
        $message = $this->load->view('backend/emails/template_for_email', $this->data, true);

        $subject = 'Support chat for ticket no. ' . $ticket_id;
        $headers = "MIME-Version: 1.0" . "\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
        $headers .= 'From: ' . $admin['full_name'] . '<no-reply@msj.com.sa>' . "\n";
        mail($posted_email, $subject, $message, $headers);
        echo 1;
        exit();

    }

    public function pusher_data()
    {
        $post_data = $this->input->post();
        $msg = json_decode($post_data['response_json']);
        $admin = $this->session->userdata('admin');
        $even_odd = ($msg->user_id == $admin['user_id']) ? "even" : "odd";
        $support_client = ($even_odd == "even" ? "Support: " : "Client: ");
        $html = '<li class="item_message clearfix ' . $even_odd . '" data-ticket_id="' . $msg->ticket_id . '" data-comment_id="' . $msg->comment_id . '">
                                        <div class="chat-avatar">
                                            <span><strong>' . $support_client . '</strong></span>
                                            <i>' . date("h:i a", strtotime($msg->created_at)) . '</i>
                                        </div>
                                        <div class="conversation-text">
                                            <div class="ctext-wrap">
                                                <p>' . $msg->message . '</p>
                                            </div>
                                        </div>
                                    </li>';
                                    
        if($msg->comment_id) {                            
            echo $html;
        }
        exit();
    }
}
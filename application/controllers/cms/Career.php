<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {
	public 	$data = array();

	public function __construct()
	{
		parent::__construct();
		checkAdminSession();
		checkModuleRights($this->uri->segment(3),37);
		if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_user');
		$this->load->model( 'Model_career' );

	}

	public function index()
	{
		$this->data['view'] = 'backend/career/manage';
		$fetch_by = array();
                $fetch_by['request_for'] = 0;
                $this->data['careers'] = $this->Model_career->getMultipleRows($fetch_by);

		 //echo "<pre>"; print_r($this->data['careers']); exit;

		$this->load->view('backend/layouts/default',$this->data);
	}

	public function view($id)
	{
		if(!$id) redirect($_SERVER['HTTP_REFERER']);
		$this->data['id'] = $id;

		$this->Model_career->update(array('to_be_notified' => 'no'), array('id' => $id));
		$career = $this->Model_career->get($id,false,'id');

		//echo "<pre>"; print_r($career); exit;


		$this->data['career'] = $career;
		$this->data['view'] = 'backend/career/view';
		$this->load->view('backend/layouts/default',$this->data);
	}




	private function formatted_message($message, $user_id)
	{
		$message['created_at'] = date("h:i a", strtotime($message['created_at']));
		$message['who_said'] = ($message['user_id'] == $user_id) ? "Support: " : "Client: ";
		$message['even_odd'] = ($message['user_id'] == $user_id) ? "even" : "odd";
		return $message;
	}
    
    public function delete(){
        
        $get_data = $this->Model_career->get($this->input->post('id'),false);
        if(file_exists($get_data->file)) {
              unlink($get_data->file);
        }
      
        $deleted_by = array();
        $deleted_by['id'] = $this->input->post('id');
        $this->Model_career->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }
}
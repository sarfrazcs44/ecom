<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public 	$data = array();
	public  $company_id;
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }

        $this->company_id = false;

        if($this->session->userdata['admin']['company_id'] > 0){
            $this->company_id = $this->session->userdata['admin']['company_id'];
        }
    	$this->load->model('Model_order');
    	$this->load->model('Model_user');
    	$this->load->model('Model_product');
		
	}
	 
    
    public function index()
	{

		$this->data['orders'] = $this->getLatestOrders();
		$this->data['drivers'] = $this->getTopDrivers();
		$this->data['top_selling'] = $this->getTopSellings();
		$where = '';
		if($this->company_id){
			$where = ' AND orders.company_id = '.$this->company_id;
		}

		$this->data['today_sales'] = $this->Model_order->getTodaySaleSum('Date(orders.created_at) = "'.date('Y-m-d').'"'.$where);
		$this->data['today_total_items'] = $this->Model_order->getTodayItemSum('Date(orders.created_at) = "'.date('Y-m-d').'"'.$where);


		$start_date = date('Y-m-01');
		$end_date = date('Y-m-t');



		$this->data['monthly_sales'] = $this->Model_order->getTodaySaleSum('Date(orders.created_at) BETWEEN "'.$start_date.'" AND "'.$end_date.'"'.$where);

		$today_users = $this->Model_user->getUsers('users.role_id = 3 AND Date(users.created_at) BETWEEN "'.$start_date.'" AND "'.$end_date.'"'.$where);
 		$this->data['today_users']  = Count($today_users);

		
        $this->data['view'] = 'backend/dashboard/manage';
        $this->load->view('backend/layouts/default',$this->data);
	}


	private function getTopSellings(){
		$where = false;
		if($this->company_id){
			$where = 'products.company_id = '.$this->company_id;
		}
		$top_selling = $this->Model_product->gettopSelling($where);

		return $top_selling;
	}



	private function getTopDrivers(){

		$where = 'users.role_id = 5';

		if($this->company_id){
			$where .= ' AND users.company_id = '.$this->company_id;
		}
		$users = $this->Model_user->getDrivers($where);
		return $users;
	}




	private function getLatestOrders(){
		$orders = array();
        $order_status = 'Received';
        
        $limit = 10;

        
       


        if ($this->session->userdata['admin']['role_id'] == 1) { // for admin
            $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUserReceivedNotAssigned($order_status,array(),'',false,$this->company_id,$limit);
        } elseif ($this->session->userdata['admin']['role_id'] == 4) { // for warehouse
            $user_id = $this->session->userdata['admin']['user_id'];
            $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
            foreach ($user_districts as $district) {
                $districts[] = $district->district_id;
            }
            $orders = $this->Model_order->getOrdersForWarehouseOrDeliveryUserReceivedNotAssigned($order_status, $districts,'',false,$this->company_id,$limit);
        }


        return $orders;
	}
    

}
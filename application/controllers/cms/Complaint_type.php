<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint_type extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        checkModuleRights($this->uri->segment(3),39);
        if ($this->session->userdata['admin']['role_id'] == 2) {
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_complaint_type');
        // $this->load->model('Model_category_text');

    }


    public function index($parent_id = 0)
    {
        $this->data['view'] = 'backend/complaint_type/manage';
        $this->data['parent_id'] = $parent_id;
        $this->data['types'] = $this->Model_complaint_type->getMultipleRows(array('parent_id' => $parent_id));
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add($parent_id = 0)
    {
        $this->data['view'] = 'backend/complaint_type/add';
        $this->data['parent_id'] = $parent_id;
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function edit($complaint_type_id)
    {

        $this->data['result'] = $this->Model_complaint_type->get($complaint_type_id, false, 'complaint_type_id');

        if (!$this->data['result']) {
            redirect(base_url('cms/complaint_type'));
        }
        $this->data['view'] = 'backend/complaint_type/edit';


        $this->data['complaint_type_id'] = $complaint_type_id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save';
                $this->validate();
                $this->save();
                break;
            case 'update';
                $this->validate();
                $this->update();
                break;
            case 'delete';
                //$this->validate();
                $this->delete();
                break;
        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('complaint_title_en', 'Eng Title', 'required');
        $this->form_validation->set_rules('complaint_title_ar', 'Arabic Title', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {
        $post_data = $this->input->post();

        if (isset($post_data['is_active'])) {
            $post_data['is_active'] = 1;
        } else {
            $post_data['is_active'] = 0;
        }

        unset($post_data['form_type']);
        $insert_id = $this->Model_complaint_type->save($post_data);
        if ($insert_id > 0) {

            $success['error'] = 'false';
            $success['success'] = 'Save Successfully';
            $success['redirect'] = true;
            if ($post_data['parent_id'] > 0) {
                $success['url'] = 'cms/complaint_type/index/' . $post_data['parent_id'];
            } else {
                $success['url'] = 'cms/complaint_type';
            }
            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = 'There is something went wrong';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        $post_data = $this->input->post();

        if (isset($post_data['is_active'])) {
            $post_data['is_active'] = 1;
        } else {
            $post_data['is_active'] = 0;
        }

        $post_data['updated_at'] = date('Y-m-d H:i:s');

        unset($post_data['form_type']);

        $update_by = array();
        $update_by['complaint_type_id'] = $post_data['complaint_type_id'];
        unset($post_data['form_type']);
        $this->Model_complaint_type->update($post_data, $update_by);

        $c_type = $this->Model_complaint_type->get($post_data['complaint_type_id'], true, 'complaint_type_id');

        $success['error'] = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        if ($c_type['parent_id'] > 0) {
            $success['url'] = 'cms/complaint_type/index/' . $c_type['parent_id'];
        } else {
            $success['url'] = 'cms/complaint_type';
        }
        echo json_encode($success);
        exit;
    }


    private function delete()
    {

        $deleted_by = array();
        $deleted_by['complaint_type_id'] = $this->input->post('id');
        $this->Model_complaint_type->delete($deleted_by);

        $success['error'] = 'false';
        $success['success'] = 'Deleted Successfully';

        echo json_encode($success);
        exit;
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Package extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_user');
       // $this->load->model('Model_system_language');
        $this->load->model('Package_model');
        $this->load->model('Module_menu');
       // $this->load->model('Model_category_text');
        checkModuleRights($this->uri->segment(3),67);
		
	}
	 
    
    public function index()
	{
       
		
        $this->data['view'] = 'backend/package/manage';
        $fetch_by = array();
        $fetch_by['hide'] = 0;
        
        $this->data['packages'] = $this->Package_model->getMultipleRows($fetch_by);
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	public function add()
	{
		
        $this->data['view'] = 'backend/package/add';

        $fetch_by = array();
        $fetch_by['hide'] = 0;
        $fetch_by['is_active'] = 1;
        
        $this->data['modules'] = $this->Module_menu->getMultipleRows($fetch_by);



        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function edit($package_id)
	{
        
        $this->data['result']		 = $this->Package_model->get($package_id,false,'package_id');
		
        if(!$this->data['result']){
           redirect(base_url('cms/package')); 
        }
        $this->data['view'] = 'backend/package/edit';
        
		
		
		
        
		
		$this->data['package_id'] 	 = $package_id;
		$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'update';
                $this->validate();
                $this->update();
          break;
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('title_en', 'Eng Title', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('title_ar', 'Arabic Title', 'required');
        
        



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();


        if(!isset($post_data['module_id']) || empty($post_data['module_id'])){
            $errors['error'] = 'Please select modules for this package';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else{
           $post_data['module_id'] = implode(',', $post_data['module_id']); 
        }
	
		if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }



		
		
		unset($post_data['form_type']);
		$post_data['created_at']    = date('Y-m-d H:i:s');		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		
        
		$insert_id = $this->Package_model->save($post_data);
		if($insert_id > 0)
		{
			
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
            $success['url'] = 'cms/package';  
			
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
    private function update()
	{
		$post_data = $this->input->post();

        if(!isset($post_data['module_id']) || empty($post_data['module_id'])){
            $errors['error'] = 'Please select modules for this package';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else{
           $post_data['module_id'] = implode(',', $post_data['module_id']); 
        }
	
		
		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }
       unset($post_data['form_type']);
        
        $update_by = array();
		$update_by['package_id'] = $post_data['package_id'];
        unset($post_data['form_type']);
        $this->Package_model->update($post_data,$update_by);
		
        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/package';
        echo json_encode($success);
        exit;
	}
    
   
    
    
    private function delete(){
        
        
        $deleted_by = array();
        $deleted_by['package_id'] = $this->input->post('id');
        $this->Package_model->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

}
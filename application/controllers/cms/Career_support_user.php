<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career_support_user extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        checkModuleRights($this->uri->segment(3),23);
        $this->load->model('Model_user');
        $this->load->model('Model_general');
    }

    public function index()
    {
        $this->data['view'] = 'backend/career_support_user/manage';
        $fetch_by = array();
        $fetch_by['role_id'] = 6;
        if($this->session->userdata['admin']['company_id'] > 0){
            $fetch_by['company_id'] = $this->session->userdata['admin']['company_id'];
        }
        $this->data['career_support_user'] = $this->Model_user->getMultipleRows($fetch_by);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        $this->data['view'] = 'backend/career_support_user/add';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function save()
    {
        $posted_data = $this->input->post();
        $posted_data['is_verified'] = 1;

        $username_count = $this->Model_user->checkIfUsernameAvailable($posted_data['username']);
        $email_count = $this->Model_user->checkIfEmailAvailable($posted_data['email']);
        if ($username_count > 0 && $email_count > 0) {
            $response['error'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['error'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['error'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            $posted_data['password'] = md5($posted_data['password']);
            $posted_data['role_id'] = 6;
            $posted_data['created_at'] = date('Y-m-d H:i:s');
            if($this->session->userdata['admin']['company_id'] > 0){
                $posted_data['company_id'] = $this->session->userdata['admin']['company_id'];
            }
            $user_id = $this->Model_user->save($posted_data);
            $response['error'] = 'false';
            $response['success'] = 'Career support user created successfully.';
            $response['redirect'] = true;
            $response['url'] = 'cms/career_support_user';

            echo json_encode($response);
            exit();
        }

    }

    public function edit($user_id)
    {
        $fetch_by = array(); 
        $fetch_by['user_id'] = $user_id;
        if($this->session->userdata['admin']['company_id'] > 0){
                $fetch_by['company_id'] = $this->session->userdata['admin']['company_id'];
        }
        $this->data['user_data'] =$this->Model_user->getWithMultipleFields($fetch_by);
        $this->data['view'] = 'backend/career_support_user/edit';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function update()
    {
        $posted_data = $this->input->post();
        $user_id = $this->input->post('user_id');
        $user_data = $this->Model_user->get($user_id, false, 'user_id');

        if ($user_data->email != $posted_data['email']) {
            $email_count = $this->Model_user->checkIfEmailAvailable($posted_data['email']);
        }

        if ($user_data->username != $posted_data['username']) {
            $username_count = $this->Model_user->checkIfUsernameAvailable($posted_data['username']);
        }

        if ($username_count > 0 && $email_count > 0) {
            $response['error'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['error'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['error'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            $this->Model_user->update($posted_data, array('user_id' => $user_id));
            $response['error'] = 'false';
            $response['success'] = 'Career suppport user updated successfully.';
            $response['redirect'] = true;
            $response['url'] = 'cms/career_support_user';
            echo json_encode($response);
            exit();
        }

    }

    public function delete()
    {
        $user_id = $this->input->post('id');
        $this->Model_user->delete(array('user_id' => $user_id));
        $response['error'] = 'false';
        $response['success'] = 'Career suppport user deleted successfully.';
        $response['redirect'] = true;
        $response['url'] = 'cms/career_support_user';
        echo json_encode($response);
        exit();
    }
}
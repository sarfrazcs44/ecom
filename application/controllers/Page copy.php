<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Base_Controller {

	 function __construct() {
        parent::__construct();
		 $this->load->model('Model_user');
		 $this->load->model( 'Model_career' );
		 $this->load->model('Model_product');
		 $this->load->model('Model_product_specification');
		 $this->load->model('Model_product_image');
		 $this->load->model('Model_category');
		 $this->load->model('Model_brand');
		 $this->load->model('Model_ad');
         $this->load->model('Model_chat_request');
         $this->load->model('Model_testimonial');
		  $this->load->model('Model_newsletter');
		  $this->load->model('Model_temp_order');

		// $this->session->set_userdata('lang','en');
	 }

	 public function index()
	 {
	 	$this->home();
	 }

	 public function home() {
		$data = array();

		$products = $this->get_all_products();
		$data['categories'] = $this->categories_get();
		$data['brands'] = $this->brands_get();
		$data['products'] = $products;
		$data['ads'] = $this->ads_get();
        $data['testimonials'] = $this->Model_testimonial->getAll();
		// echo "<pre>"; print_r($data['ads']); exit;

        $data['view'] = 'front/pages/home';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default',$data);
	}

	public function login()
	{
		$data = array();
		$data['view'] = 'front/pages/login';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
	}
    
    public function verify_account()
	{
		$data = array();
		$data['view'] = 'front/pages/verify_account';
		$this->load->view('front/layouts/default',$data);
	}
    
    public function faq()
	{
		$data = array();
		$data['view'] = 'front/pages/faq';
		$this->load->view('front/layouts/default',$data);
	}

	public function products()
	{
		$data = array();
		$get = $this->input->get();
		$fetch_by = [];
		$bread_crumb = [];
		if(!empty($get['category']) && $get['category'] != '-1'){
			$fetch_by['category_id'] = $get['category'];
			$bread_crumb['cat'] = $this->getCategoryById($fetch_by['category_id']);
		}
		if(!empty($get['sub_category']) && $get['sub_category'] != '-1'){
			$fetch_by['sub_category_id'] = $get['sub_category'];
			$bread_crumb['sub_cat'] = $this->getCategoryById($fetch_by['sub_category_id']);
		}
		if(!empty($get['brand_id']) && $get['brand_id'] != '-1'){
			$fetch_by['brand_id'] = $get['brand_id'];
			$bread_crumb['brand'] = $this->getBrandById($fetch_by['brand_id']);
		}



		$products = $this->get_all_products($fetch_by);

		//echo "<pre>"; print_r($products); exit;
        $data['categories'] = $this->categories_get();
		$data['brands'] = $this->brands_get();
		$data['view'] = 'front/pages/products';
		$data['isLogin'] = TRUE;
		$data['products'] = $products;
		$data['bread_crumb'] = $bread_crumb;
		$this->load->view('front/layouts/default',$data);
	}

	public function product_details()
	{
		// redirect back if not id
		$product_id = $this->input->get('id');

		if(!$product_id) redirect($this->config->item('base_url').'page/products');

		$product = $this->get_all_products(['product_id'=>$product_id]);

		// if not product redirect back
		if(!$product) redirect($this->config->item('base_url').'page/products');

		$fetch_by['category_id'] = $product[0]['category_id'];
		$related_products = $this->get_related_products_by_category($fetch_by, $product_id);

		//echo "<pre>"; print_r($related_products); exit;

		$data = array();
		$data['product'] = $product[0];
		$data['related'] = $related_products;
		$data['view'] = 'front/pages/product_detail';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
	}

	public function categories()
	{
		$data = array();
		$data['categories'] = $this->categories_get();
		$data['view'] = 'front/pages/categories';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
	}

	public function sub_categories()
	{
		$data = array();
		$_get = $this->input->get();
		if (empty($_get['category'])) redirect($this->config->item('base_url').'page/');
		$category_id = $_get['category'];

		$category = $this->getCategoryById($category_id);

		$data['sub_cats'] = $this->sub_categories_get($category_id);
		$data['category'] = $category;
		$data['view'] = 'front/pages/sub_categories';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
	}

	public function who_we_are()
	{
		$data = array();
		$data['view'] = 'front/pages/who_we_are';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
	}
    
    
    public function download_center()
	{
		$data = array();
		$data['view'] = 'front/pages/download_center';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
	}
    
    public function contact_us()
	{
		$data = array();
		$data['view'] = 'front/pages/contact_us';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
	}


	public function career_apply()
	{
		$post_data = $this->input->post();
		$this->validate();

		// upload file
		if(isset($_FILES['file']["name"][0]) && $_FILES['file']["name"][0] != ''){
			$post_data['file']   = $this->uploadImage("file", "uploads/careers/");
		}
        
        
        $this->sendCareerEmailToAdmin($post_data);

		$insert_id = $this->Model_career->save($post_data);
		if($insert_id > 0) {
			// save message in comments
			$success['error']   = 'false';
			$success['success'] = 'Applied successfully.';
			$success['redirect'] = true;
			$success['url'] = 'page/who_we_are';
			echo json_encode($success);
			exit;
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}

	}
        
    public function become_a_seller_apply()
	{
		$post_data = $this->input->post();
		$this->validate(true);

		// upload file
		if(isset($_FILES['file']["name"][0]) && $_FILES['file']["name"][0] != ''){
			$post_data['file']   = $this->uploadImage("file", "uploads/careers/");
		}
        
               $post_data['request_for'] = 1;
        
		$insert_id = $this->Model_career->save($post_data);
		if($insert_id > 0) {
			// save message in comments
			$success['error']   = 'false';
			$success['success'] = 'Applied successfully.';
			$success['redirect'] = true;
			$success['url'] = 'page/become_a_seller';
			echo json_encode($success);
			exit;
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}

	}    
	
	
    
    
    public function become_a_seller(){
        $data = array();
		$data['view'] = 'front/pages/become_a_seller';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
    } 
	
	
	public function newsletter()
	{
		$post_data = $this->input->post();
		

$existing = $this->Model_newsletter->get($post_data['email'],true, 'email');

if(empty($existing)){
        
		$insert_id = $this->Model_newsletter->save($post_data);
		if($insert_id > 0) {
			// save message in comments
			$success['error']   = 'false';
			$success['success'] = 'Applied successfully.';
			echo json_encode($success);
			exit;
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
		
}

else{

		$exist['error']   = 'false';
		$exist['success'] = 'Already Registered.';
		echo json_encode($exist);
		exit;
	
	
	}

	}    
	

	
	
	
	
	
	
	  public function profile_edit(){
        $data = array();
		$data['view'] = 'front/pages/profile_edit';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
    } 
	
	
	
	
	
    private function sendCareerEmailToAdmin($data,$files = false){
                        $body = '';
					 
					  $body .= '<h3>Dear Admin,</h3>';
					  $body .= '<h3>'.date('d-m-Y H:i:s').'</h3><br>';
				  	  $body .= '<p>You have received a career request</p>';
        
                      $body .= '<table><tbody><tr><td>Name </td><td>'.$data['full_name'].' </td></tr><tr><td>Email </td><td>'.$data['email'].' </td></tr><tr><td>Phone </td><td>'.$data['phone'].' </td></tr></tbody></table>';
					  
				      $message = emailTemplate($body);
					  
					  
					 
					  $email_data = array();
					  $email_data['to'] = 'jameel@msj.com.sa,Sales@msj.com.sa';
					  $email_data['subject'] = 'Career Email';
					  $email_data['from'] = 'no_reply@msj.com';
					  $email_data['body'] = $message;
					 if(isset($data['file'])){
                         $files = array($data['file']);
                     }
					   
					  sendEmail($email_data,$files);
    }

	private function validate($become_seller = false){
		$errors = array();
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if($become_seller){
           $this->form_validation->set_rules('company_name', lang('company_name'), 'required');
           $this->form_validation->set_rules('director_name', lang('directory_name'), 'required');
           $this->form_validation->set_rules('email', lang('email'), 'required');
            
        }else{
           $this->form_validation->set_rules('full_name', 'Full Name', 'required');
           $this->form_validation->set_rules('email', 'Email Address', 'required');
           $this->form_validation->set_rules('phone', 'Phone', 'required');
           $this->form_validation->set_rules('nationality', 'Nationality', 'required'); 
        }
		


		if ($this->form_validation->run() == FALSE)
		{
			$errors['error'] = validation_errors();
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}else
		{
			return true;
		}
	}


	private function uploadImage($file_key, $path,$id=false,$multiple = false)
	{
		$data = array();
		foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
		{
			$file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
			$file_tmp=$_FILES[$file_key]["tmp_name"][$key];
			$ext=pathinfo($file_name,PATHINFO_EXTENSION);
				move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
				if(!$multiple){
					return $path.$file_name;
				}

		}
		return true;


	}


	private function get_all_products($post_data=array())
	{
		$results = $this->Model_product->getAllProducts($post_data);
		$products = array();
		if(!empty($results)){
			foreach($results as $result){
				$fetch_by = array();
				$fetch_by['product_id'] = $result['product_id'];
				$images = $this->Model_product_image->getMultipleRows($fetch_by,true);
				if($images){
					$result['product_images'] = $images;
				}else{
					$result['product_images'] = array();
				}

				$specifications = $this->Model_product_specification->getMultipleRows($fetch_by,true);

				if($specifications){
					$result['product_specifications'] = $specifications;
				}else{
					$result['product_specifications'] = array();
				}
				$products[] = $result;
			}
		}
		return $products;
	}

	private function get_related_products_by_category($post_data=array(), $product_id)
	{
		$results = $this->Model_product->getAllProducts($post_data);
		$products = array();

		if(!empty($results)){
			foreach($results as $result){
				if($result['category_id'] == $post_data['category_id']
				   && $result['product_id'] == $product_id) continue; // don't who product that we are currently viewing.
				$fetch_by = array();
				$fetch_by['product_id'] = $result['product_id'];
				$images = $this->Model_product_image->getMultipleRows($fetch_by,true);
				if($images){
					$result['product_images'] = $images;
				}else{
					$result['product_images'] = array();
				}
				$products[] = $result;
			}
		}
		return $products;
	}


	private function getCategoryById($category_id)
	{
		if($category_id){
			$category = $this->Model_category->get($category_id,true, 'category_id');
			if($category){
				return $category;
			}else {
				return false;
			}
		}
		return false;
	}

	private function getBrandById($brand_id)
	{
		if($brand_id){
			$brand = $this->Model_brand->get($brand_id,true, 'brand_id');
			if($brand){
				return $brand;
			}else {
				return false;
			}
		}
		return false;
	}

	public function categories_get()
	{
		$post_data = $this->input->post();
		$categories = array();
		$fetch_by = array();
		$fetch_by['parent_id'] = 0;
		$fetch_by['is_active'] = 1;
		if(isset($post_data['parent_id'])){
			$fetch_by['parent_id'] = $post_data['parent_id'];
		}
		$categories = $this->Model_category->getMultipleRows($fetch_by,true);

		if(!empty($post_data['ajax_call'])){
			if($categories){
				$response['status'] = 'TRUE';
				$response['data']   = $categories;
			}else {
				$response['status'] = 'FALSE';
			}
			echo json_encode($response);
			exit;
		}
		// no ajax call
		if($categories){
			return $categories;
		}

		return false;

	}

	public function sub_categories_get($parent_id)
	{
		if($parent_id){
			$fetch_by = array();
			$fetch_by['parent_id'] = $parent_id;
			$fetch_by['is_active'] = 1;
			$categories = $this->Model_category->getMultipleRows($fetch_by,true);
			if($categories){
				return $categories;
			}
			return false;
		}
	}

	public function brands_get()
	{
		$fetch_by = array();

		$fetch_by['is_active'] = 1;
		$return_array = array();
		$brands = $this->Model_brand->getMultipleRows($fetch_by,true);
		if($brands){
			return $brands;
		}
		return false;
	}

	public function ads_get()
	{
		$fetch_by = array();
		$fetch_by['is_active'] = 1;
		$ads = $this->Model_ad->getMultipleRows($fetch_by,true);
		if($ads){
			return $ads;
		}
		return false;
	}

	public function creat_new_chat()
	{
		$user = $this->input->post();
		$save_array = array();
        $save_array['username'] = $user['name'];
        $save_array['email'] = $user['email'];
        $save_array['subject'] = $user['subject'];
        $save_array['message'] = $user['message'];

        $insert_id   = $this->Model_chat_request->save($save_array);

        $total_count = $this->Model_chat_request->getCount($insert_id);

        $response['success'] = true;
        $response['chat_id'] = $insert_id;
        $response['total_count'] = 'You are in the position of '.$total_count.' in que';


        echo json_encode($response);
        exit();
	}

	public function change_status_closed()
    {
        $user_id = $this->input->post('chat_request_id');
        $update = $this->Model_chat_request->update(array("is_closed"=>'1'), array("id"=>$user_id));
        $Chat_request = $this->Model_chat_request->getMultipleRows(array('is_admin' => '0', 'is_closed'=>'0'), true);
        if($Chat_request[0]['id'] != '')
        {
            $response['receiver'] = $Chat_request[0]['id'];    
        }
        else{
            $response['receiver'] = 0;
        }
        $response['success'] = 1;
        echo json_encode($response);   
    }
    
    
    
    public function add_to_cart(){
        if($this->session->userdata('user')){
            $fetch_by = array();
            $fetch_by['user_id']    = $this->session->userdata['user']['user_id'];
            $fetch_by['product_id'] = $this->input->post('product_id');
            
            $already_added = $this->Model_temp_order->getWithMultipleFields($fetch_by);
            if($already_added){
                $update    = array();
                $update_by = array();
                
                $update['product_quantity']     = $already_added->product_quantity + 1;
                $update_by['temp_order_id']    = $already_added->temp_order_id;
                
                $this->Model_temp_order->update($update,$update_by);
                
                
                $response['status']  = 'TRUE';
                $response['message']  = lang('add_successfully');
                $response['reload']  = true;
                

                echo json_encode($response);
                exit();
                
            }else{
                $save_data = array();
                $save_data['user_id']          = $this->session->userdata['user']['user_id'];
                $save_data['product_quantity'] = 1;
                $save_data['product_id'] = $this->input->post('product_id');
                $this->Model_temp_order->save($save_data);
                
                $response['status']  = 'TRUE';
                $response['message']  = lang('add_successfully');
                $response['reload']  = true;
                

                echo json_encode($response);
                exit();
            }
            
            
            
        }else{
            $url = 'page/product_details?id='.$this->input->post('product_id');
            $this->session->set_userdata('url',$url);
            $response['status']  = false;
            $response['message']  = lang('login_first');
            $response['redirect'] = true;
            $response['url'] = 'page/login';


            echo json_encode($response);
            exit();
            
        }
    }

}

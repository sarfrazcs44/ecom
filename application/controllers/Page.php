<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Base_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_user');
        $this->load->model('Model_career');
        $this->load->model('Model_product');
        $this->load->model('Model_product_specification');
        $this->load->model('Model_product_image');
        $this->load->model('Model_category');
        $this->load->model('Model_brand');
        $this->load->model('Model_homeimage');
        $this->load->model('Model_ad');
        $this->load->model('Model_chat_request');
        $this->load->model('Model_testimonial');
        $this->load->model('Model_newsletter');
        $this->load->model('Model_temp_order');
        $this->load->model('Model_general');

        // $this->session->set_userdata('lang','en');
    }

    public function index()
    {
        //redirect('cms/account/login');
        $this->home();
    }

    public function home()
    {
        $data = array();
        $fetch_by['is_featured'] = 1;
        $products = $this->get_all_products($fetch_by);
        $data['categories'] = $this->categories_get();
        $data['brands'] = $this->brands_get();
        $data['homeimages'] = $this->homeimages_get();
        $data['products'] = $products;
        $data['ads'] = $this->ads_get();
        $data['testimonials'] = $this->Model_testimonial->getAll();
        // echo "<pre>"; print_r($data['ads']); exit;

        $data['view'] = 'front/pages/home';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function login()
    {
        $data = array();
        $data['view'] = 'front/pages/login';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function verify_account()
    {
        $data = array();
        $data['view'] = 'front/pages/verify_account';
        $this->load->view('front/layouts/default', $data);
    }

    public function faq()
    {
        $data = array();
        $data['view'] = 'front/pages/faq';
        $this->load->view('front/layouts/default', $data);
    }

    public function products()
    {
        $data = array();
        $get = $this->input->get();
        $fetch_by = [];
        $bread_crumb = [];
        if (!empty($get['category']) && $get['category'] != '-1') {
            $fetch_by['category_id'] = $get['category'];
            $bread_crumb['cat'] = $this->getCategoryById($fetch_by['category_id']);
        }
        if (!empty($get['sub_category']) && $get['sub_category'] != '-1') {
            $fetch_by['sub_category_id'] = $get['sub_category'];
            $bread_crumb['sub_cat'] = $this->getCategoryById($fetch_by['sub_category_id']);
        }
        if (!empty($get['brand_id']) && $get['brand_id'] != '-1') {
            $fetch_by['brand_id'] = $get['brand_id'];
            $bread_crumb['brand'] = $this->getBrandById($fetch_by['brand_id']);
        }


        $products = $this->get_all_products($fetch_by);

        //echo "<pre>"; print_r($products); exit;
        $data['categories'] = $this->categories_get();
        $data['brands'] = $this->brands_get();
        $data['view'] = 'front/pages/products';
        $data['isLogin'] = TRUE;
        $data['products'] = $products;
        $data['bread_crumb'] = $bread_crumb;
        $this->load->view('front/layouts/default', $data);
    }

    public function product_details()
    {
        // redirect back if not id
        $product_id = $this->input->get('id');

        if (!$product_id) redirect($this->config->item('base_url') . 'page/products');

        $product = $this->get_all_products(['product_id' => $product_id]);

        // if not product redirect back
        if (!$product) redirect($this->config->item('base_url') . 'page/products');

        $fetch_by['category_id'] = $product[0]['category_id'];
        $related_products = $this->get_related_products_by_category($fetch_by, $product_id);

        //echo "<pre>"; print_r($related_products); exit;

        $data = array();
        $data['product'] = $product[0];
        $data['related'] = $related_products;
        $data['view'] = 'front/pages/product_detail';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function categories()
    {
        $data = array();
        $data['categories'] = $this->categories_get();
        $data['view'] = 'front/pages/categories';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function sub_categories()
    {
        $data = array();
        $_get = $this->input->get();
        if (empty($_get['category'])) redirect($this->config->item('base_url') . 'page/');
        $category_id = $_get['category'];

        $category = $this->getCategoryById($category_id);

        $data['sub_cats'] = $this->sub_categories_get($category_id);
        $data['category'] = $category;
        $data['view'] = 'front/pages/sub_categories';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function who_we_are()
    {
        $data = array();
        $data['view'] = 'front/pages/who_we_are';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function commitment()
    {
        $data = array();
        $data['view'] = 'front/pages/commitment';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }


    public function mission()
    {
        $data = array();
        $data['view'] = 'front/pages/mission';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function partner()
    {
        $data = array();
        $data['view'] = 'front/pages/partners';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }


    public function awards()
    {
        $data = array();
        $data['view'] = 'front/pages/awards';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function team()
    {
        $data = array();
        $data['view'] = 'front/pages/team';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function career()
    {
        $data = array();
        $data['view'] = 'front/pages/career';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }


    public function download_center()
    {
        $data = array();
        $data['view'] = 'front/pages/download_center';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function contact_us()
    {
        $data = array();
        $data['view'] = 'front/pages/contact_us';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }


    public function career_apply()
    {
        $post_data = $this->input->post();
        $this->validate();

        $siteKey = $post_data["g-recaptcha-response"];
        unset($post_data["g-recaptcha-response"]);
        // captcha verification function
        $res = captchaVerify($siteKey);
        if ($res->success != true) {
            $errors['error'] = 'Please use captcha';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
// upload file
            if (isset($_FILES['file']["name"][0]) && $_FILES['file']["name"][0] != '') {
                $post_data['file'] = $this->uploadImage("file", "uploads/careers/");
            }


            $this->sendCareerEmailToAdmin($post_data);
            $post_data['to_be_notified'] = 'yes';


            $insert_id = $this->Model_career->save($post_data);
            if ($insert_id > 0) {
                // save message in comments
                $success['error'] = 'false';
                $success['success'] = 'Applied successfully.';
                $success['redirect'] = true;
                $success['url'] = 'page';
                echo json_encode($success);
                exit;
            } else {
                $errors['error'] = 'There is something went wrong';
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            }
        }

    }

    public function become_a_seller_apply()
    {
        $post_data = $this->input->post();
        $this->validate(true);

        $siteKey = $post_data["g-recaptcha-response"];
        unset($post_data["g-recaptcha-response"]);
        // captcha verification function
        $res = captchaVerify($siteKey);
        if ($res->success != true) {
            $errors['error'] = 'Please use captcha';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            // upload file
            if (isset($_FILES['file']["name"][0]) && $_FILES['file']["name"][0] != '') {
                $post_data['file'] = $this->uploadImage("file", "uploads/careers/");
            }

            $post_data['request_for'] = 1;

            $insert_id = $this->Model_career->save($post_data);
            if ($insert_id > 0) {
                // save message in comments
                $success['error'] = 'false';
                $success['success'] = 'Applied successfully.';
                $success['redirect'] = true;
                $success['url'] = 'page/become_a_seller';
                echo json_encode($success);
                exit;
            } else {
                $errors['error'] = 'There is something went wrong';
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            }
        }

    }


    public function become_a_seller()
    {
        $data = array();
        $data['view'] = 'front/pages/become_a_seller';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }


    public function newsletter()
    {
        $post_data = $this->input->post();


        $existing = $this->Model_newsletter->get($post_data['email'], true, 'email');

        if (empty($existing)) {

            $insert_id = $this->Model_newsletter->save($post_data);
            if ($insert_id > 0) {
                // save message in comments
                $success['error'] = 'false';
                $success['success'] = 'Applied successfully.';
                echo json_encode($success);
                exit;
            } else {
                $errors['error'] = 'There is something went wrong';
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            }

        } else {

            $exist['error'] = 'false';
            $exist['success'] = 'Already Registered.';
            echo json_encode($exist);
            exit;


        }

    }


    public function profile_edit()
    {
        $data = array();
        $user = $this->session->userdata('user');
        $data['user'] = $user;
        $data['view'] = 'front/pages/profile_edit';
        $data['isLogin'] = TRUE;
        $data['active'] = 'profile_edit';
        $this->load->view('front/layouts/default', $data);
    }

    private function sendCareerEmailToAdmin($data, $files = false)
    {
        $career_support_users = $this->Model_general->getMultipleRows('users', array('role_id' => 6), false, 'asc', $orderBy = 'user_id'); // fetching all career support users
        foreach ($career_support_users as $support_user) {
            if ($support_user->receive_career_email_at !== '') {
                $body = '';

                $body .= '<h3>Dear ' . $support_user->full_name . ',</h3>';
                $body .= '<h3>' . date('d-m-Y H:i:s') . '</h3><br>';
                $body .= '<p>You have received a career request</p>';

                $body .= '<table>
                        <tbody>
                        <tr><td>Name </td><td>' . $data['full_name'] . ' </td></tr>
                        <tr><td>Email </td><td>' . $data['email'] . ' </td></tr>
                        <tr><td>Phone </td><td>' . $data['phone'] . ' </td></tr>
                        </tbody>
                        </table>';

                $message = emailTemplate($body);


                $email_data = array();
                $email_data['to'] = $support_user->receive_career_email_at;
                $email_data['subject'] = 'Career Email';
                $email_data['from'] = 'no_reply@msj.com';
                $email_data['body'] = $message;
                if (isset($data['file'])) {
                    $files = array($data['file']);
                }

                sendEmail($email_data, $files);
            }
        }
        /*$body = '';

        $body .= '<h3>Dear Admin,</h3>';
        $body .= '<h3>' . date('d-m-Y H:i:s') . '</h3><br>';
        $body .= '<p>You have received a career request</p>';

        $body .= '<table><tbody><tr><td>Name </td><td>' . $data['full_name'] . ' </td></tr><tr><td>Email </td><td>' . $data['email'] . ' </td></tr><tr><td>Phone </td><td>' . $data['phone'] . ' </td></tr></tbody></table>';

        $message = emailTemplate($body);


        $email_data = array();
        $email_data['to'] = 'jameel@msj.com.sa,Sales@msj.com.sa';
        $email_data['subject'] = 'Career Email';
        $email_data['from'] = 'no_reply@msj.com';
        $email_data['body'] = $message;
        if (isset($data['file'])) {
            $files = array($data['file']);
        }

        sendEmail($email_data, $files);*/
    }

    private function validate($become_seller = false)
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($become_seller) {
            $this->form_validation->set_rules('company_name', lang('company_name'), 'required');
            $this->form_validation->set_rules('director_name', lang('directory_name'), 'required');
            $this->form_validation->set_rules('email', lang('email'), 'required');

        } else {
            $this->form_validation->set_rules('full_name', 'Full Name', 'required');
            $this->form_validation->set_rules('email', 'Email Address', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required');
            $this->form_validation->set_rules('nationality', 'Nationality', 'required');
        }


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }


    private function uploadImage($file_key, $path, $id = false, $multiple = false)
    {
        $data = array();
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
            if (!$multiple) {
                return $path . $file_name;
            }

        }
        return true;


    }


    private function get_all_products($post_data = array())
    {
        $results = $this->Model_product->getAllProducts($post_data);
        $products = array();
        if (!empty($results)) {
            foreach ($results as $result) {
                $fetch_by = array();
                $fetch_by['product_id'] = $result['product_id'];
                $images = $this->Model_product_image->getMultipleRows($fetch_by, true);
                if ($images) {
                    $result['product_images'] = $images;
                } else {
                    $result['product_images'] = array();
                }

                $specifications = $this->Model_product_specification->getMultipleRows($fetch_by, true);

                if ($specifications) {
                    $result['product_specifications'] = $specifications;
                } else {
                    $result['product_specifications'] = array();
                }
                $products[] = $result;
            }
        }
        return $products;
    }

    private function get_related_products_by_category($post_data = array(), $product_id)
    {
        $results = $this->Model_product->getAllProducts($post_data, true);
        $products = array();

        if (!empty($results)) {
            foreach ($results as $result) {
                if ($result['product_id'] !== $product_id) // don't who product that we are currently viewing.
                {
                    $fetch_by = array();
                    $fetch_by['product_id'] = $result['product_id'];
                    $images = $this->Model_product_image->getMultipleRows($fetch_by, true);
                    if ($images) {
                        $result['product_images'] = $images;
                    } else {
                        $result['product_images'] = array();
                    }
                    $products[] = $result;
                }

            }
        }
        return $products;
    }


    private function getCategoryById($category_id)
    {
        if ($category_id) {
            $category = $this->Model_category->get($category_id, true, 'category_id');
            if ($category) {
                return $category;
            } else {
                return false;
            }
        }
        return false;
    }

    private function getBrandById($brand_id)
    {
        if ($brand_id) {
            $brand = $this->Model_brand->get($brand_id, true, 'brand_id');
            if ($brand) {
                return $brand;
            } else {
                return false;
            }
        }
        return false;
    }

    public function categories_get()
    {
        $post_data = $this->input->post();
        $categories = array();
        $fetch_by = array();
        $fetch_by['parent_id'] = 0;
        $fetch_by['is_active'] = 1;
        if (isset($post_data['parent_id'])) {
            $fetch_by['parent_id'] = $post_data['parent_id'];
        }
        $categories = $this->Model_category->getMultipleRows($fetch_by, true);

        if (!empty($post_data['ajax_call'])) {
            if ($categories) {
                $response['status'] = 'TRUE';
                $response['data'] = $categories;
            } else {
                $response['status'] = 'FALSE';
            }
            echo json_encode($response);
            exit;
        }
        // no ajax call
        if ($categories) {
            return $categories;
        }

        return false;

    }

    public function sub_categories_get($parent_id)
    {
        if ($parent_id) {
            $fetch_by = array();
            $fetch_by['parent_id'] = $parent_id;
            $fetch_by['is_active'] = 1;
            $categories = $this->Model_category->getMultipleRows($fetch_by, true);
            if ($categories) {
                return $categories;
            }
            return false;
        }
    }

    public function brands_get()
    {
        $fetch_by = array();

        $fetch_by['is_active'] = 1;
        $return_array = array();
        $brands = $this->Model_brand->getMultipleRows($fetch_by, true);
        if ($brands) {
            return $brands;
        }
        return false;
    }


    public function homeimages_get()
    {
        $fetch_by = array();

        $fetch_by['is_active'] = 1;
        $return_array = array();
        $homeimages = $this->Model_homeimage->getMultipleRows($fetch_by, true);
        if ($homeimages) {
            return $homeimages;
        }
        return false;
    }

    public function ads_get()
    {
        $fetch_by = array();
        $fetch_by['is_active'] = 1;
        $ads = $this->Model_ad->getMultipleRows($fetch_by, true);
        if ($ads) {
            return $ads;
        }
        return false;
    }

    public function creat_new_chat()
    {
        $user = $this->input->post();
        $save_array = array();
        $save_array['username'] = $user['name'];
        $save_array['email'] = $user['email'];
        $save_array['subject'] = $user['subject'];
        //$save_array['message'] = $user['message'];
        $save_array['to_be_notified'] = 'yes';

        $insert_id = $this->Model_chat_request->save($save_array);

        $total_count = $this->Model_chat_request->getCount($insert_id);

        pusher(array('refresh' => 1), 'admin_chat', 'admin_chat', false);

        sendChatRequestNotificationToAdminSupportUsers($insert_id);

        $response['success'] = true;
        $response['chat_id'] = $insert_id;
        $response['queued'] = $total_count;
        // $response['total_count'] = 'You are in the position of ' . $total_count . ' in que';
        $response['total_count'] = 'Please wait while our support representative attends your chat';


        echo json_encode($response);
        exit();
    }

    public function change_status_closed()
    {
        $user_id = $this->input->post('chat_request_id');
        $update = $this->Model_chat_request->update(array("is_closed" => '1'), array("id" => $user_id));
        $Chat_request = $this->Model_chat_request->getMultipleRows(array('is_admin' => '0', 'is_closed' => '0'), true);
        if ($Chat_request[0]['id'] != '') {
            $response['receiver'] = $Chat_request[0]['id'];
        } else {
            $response['receiver'] = 0;
        }
        $response['success'] = 1;
        echo json_encode($response);
        exit();
    }

    public function add_to_cart()
    {


        if ($this->session->userdata('user')) {

            $user_id = $this->session->userdata['user']['user_id'];

        } else {
            if (!get_cookie('temp_order_key')) {
                $user_id = date('YmdHis') . RandomString();
                $cookie = array(
                    'name' => 'temp_order_key',
                    'value' => $user_id,
                    'expire' => time() + 86500,

                );

                set_cookie($cookie);
            } else {
                $user_id = get_cookie('temp_order_key');
            }
        }


        //if ($this->session->userdata('user')) {
        $fetch_by = array();
        $fetch_by['user_id'] = $user_id;//$this->session->userdata['user']['user_id'];
        $fetch_by['product_id'] = $this->input->post('product_id');
        $posted_product_quantity = $this->input->post('product_quantity');

        $already_added = $this->Model_temp_order->getWithMultipleFields($fetch_by);
        if ($already_added) {
            $update = array();
            $update_by = array();

            $update['product_quantity'] = $already_added->product_quantity + $posted_product_quantity;
            $update_by['temp_order_id'] = $already_added->temp_order_id;

            $this->Model_temp_order->update($update, $update_by);


            $response['status'] = 'TRUE';
            $response['message'] = lang('add_successfully');
            $response['reload'] = false;


            echo json_encode($response);
            exit();

        } else {
            $save_data = array();
            $save_data['user_id'] = $user_id;//$this->session->userdata['user']['user_id'];
            $save_data['product_quantity'] = $posted_product_quantity;
            $save_data['product_id'] = $this->input->post('product_id');
            $this->Model_temp_order->save($save_data);

            $response['status'] = 'TRUE';
            $response['message'] = lang('add_successfully');
            $response['reload'] = false;


            echo json_encode($response);
            exit();
        }


        /*} else {
            $url = 'page/product_details?id=' . $this->input->post('product_id');
            $this->session->set_userdata('url', $url);
            $response['status'] = false;
            $response['message'] = lang('login_first');
            $response['redirect'] = true;
            $response['url'] = 'page/login';


            echo json_encode($response);
            exit();

        }*/
    }

    public function terms_and_conditions()
    {
        $data = array();
        $data['view'] = 'front/pages/terms_and_conditions';
        $this->load->view('front/layouts/default', $data);
    }

    public function projects()
    {
        $data = array();
        $data['view'] = 'front/pages/projects';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function project_detail($project_id)
    {
        $data = array();
        $data['view'] = 'front/pages/project_detail';
        $data['page_id'] = $project_id;
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function events()
    {
        $data = array();
        $data['view'] = 'front/pages/events';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function event_detail($event_id)
    {
        $data = array();
        $data['view'] = 'front/pages/event_detail';
        $data['page_id'] = $event_id;
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }

    public function sendNotificationEmailForChat()
    {
        $body = 'Dear customer, we are sorry for inconvenience. Please send your request to hepldesk@msj.com.sa';
        $chat_id = $this->input->post('chat_id');
        $chat = $this->Model_chat_request->get($chat_id, true);
        if ($chat) {
            if ($chat['is_in_progress'] == 0) { // if 120 seconds timer passed and no one has still attended chat then send email to customer
                $email_address = $this->input->post('customer_email');
                $message = emailTemplate($body);
                $email_data = array();
                $email_data['to'] = $email_address;
                $email_data['subject'] = 'MSJ Helpdesk';
                $email_data['from'] = 'no-reply@msj.com.sa';
                $email_data['body'] = $message;
                sendEmail($email_data);
            }
        }
        $response_mail['status'] = true;
        echo json_encode($response_mail);
        exit();
    }

}
